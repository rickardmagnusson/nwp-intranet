using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;

namespace PS.UserControls
{
    #region FileSystemManager Classes
    public class FileSystemItem
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _fullName;
        public string FullName
        {
            get { return _fullName; }
            set { _fullName = value; }
        }

        private string _extension;
        public string Extension
        {
            get { return _extension; }
            set
            {
                _extension = value;
                _extension = _extension.Replace(".", "");
            }
        }

        private DateTime _createdDate;
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private DateTime _lastAccessDate;
        public DateTime LastAccessDate
        {
            get { return _lastAccessDate; }
            set { _lastAccessDate = value; }
        }

        private DateTime _lastWriteDate;
        public DateTime LastWriteDate
        {
            get { return _lastWriteDate; }
            set { _lastWriteDate = value; }
        }

        private bool _isFolder;
        public bool IsFolder
        {
            get { return _isFolder; }
            set { _isFolder = value; }
        }

        private long _size;
        public long Size
        {
            get { return _size; }
            set { _size = value; }
        }

        private long _fileCount;
        public long FileCount
        {
            get { return _fileCount; }
            set { _fileCount = value; }
        }

        private long _subFolderCount;
        public long SubFolderCount
        {
            get { return _subFolderCount; }
            set { _subFolderCount = value; }
        }
    }

    public class FileSystemManager
    {
        private static string _rootFolder;

        static FileSystemManager()
        {
            _rootFolder = HttpContext.Current.Request.PhysicalApplicationPath;
            _rootFolder = _rootFolder.Substring(0, _rootFolder.LastIndexOf(@"\"));
        }

        public static string GetRootPath()
        {
            return _rootFolder;

        }

        public static void SetRootPath(string path)
        {
            _rootFolder = path;
        }

        public static List<FileSystemItem> GetItems()
        {
            return GetItems(_rootFolder);
        }

        public static List<FileSystemItem> GetItems(string path)
        {
            string[] folders = Directory.GetDirectories(path);
            string[] files = Directory.GetFiles(path);

            List<FileSystemItem> list = new List<FileSystemItem>();
            foreach (string folder in folders)
            {
                FileSystemItem item = new FileSystemItem();
                DirectoryInfo di = new DirectoryInfo(folder);
                item.Name = di.Name;
                item.FullName = di.FullName;
                item.CreatedDate = di.CreationTime;
                item.IsFolder = true;
                item.Extension = "folder";
                list.Add(item);
            }

            foreach (string file in files)
            {
                FileSystemItem item = new FileSystemItem();
                FileInfo fi = new FileInfo(file);
                item.Name = fi.Name;
                item.FullName = fi.FullName;
                item.CreatedDate = fi.CreationTime;
                item.IsFolder = true;
                item.Size = fi.Length;
                item.Extension = fi.Extension;
                list.Add(item);
            }
            if (path.ToLower() != _rootFolder.ToLower())
            {
                FileSystemItem topItem = new FileSystemItem();
                DirectoryInfo topDi = new DirectoryInfo(path).Parent;
                topItem.Name = "[Parent]";
                topItem.FullName = topDi.FullName;
                list.Insert(0, topItem);

                FileSystemItem rootItem = new FileSystemItem();
                DirectoryInfo rootDi = new DirectoryInfo(_rootFolder);
                rootItem.Name = "[Root]";
                rootItem.FullName = rootDi.FullName;
                list.Insert(0, rootItem);
            }
            return list;
        }

        public static void CreateFolder(string name, string parentName)
        {
            DirectoryInfo di = new DirectoryInfo(parentName);
            di.CreateSubdirectory(name);
        }

        public static void DeleteFolder(string path)
        {
            Directory.Delete(path);
        }

        public static void MoveFolder(string oldPath, string newPath)
        {
            Directory.Move(oldPath, newPath);
        }

        public static void CreateFile(string fileName, string path)
        {
            FileStream fs = File.Create(path + "\\" + fileName);
            fs.Close();
        }

        public static void CreateFile(string fileName, string path, byte[] contents)
        {
            FileStream fs = File.Create(path + "\\" + fileName);
            fs.Write(contents, 0, contents.Length);
            fs.Close();
        }

        public static void DeleteFile(string path)
        {
            File.Delete(path);
        }

        public static void MoveFile(string oldPath, string newPath)
        {
            File.Move(oldPath, newPath);
        }

        public static FileSystemItem GetItemInfo(string path)
        {
            FileSystemItem item = new FileSystemItem();
            if (Directory.Exists(path))
            {
                DirectoryInfo di = new DirectoryInfo(path);
                item.Name = di.Name;
                item.FullName = di.FullName;
                item.CreatedDate = di.CreationTime;
                item.IsFolder = true;
                item.LastAccessDate = di.LastAccessTime;
                item.FileCount = di.GetFiles().Length;
                item.SubFolderCount = di.GetDirectories().Length;
                item.Extension = "folder";
            }
            else
            {
                FileInfo fi = new FileInfo(path);
                item.Name = fi.Name;
                item.FullName = fi.FullName;
                item.CreatedDate = fi.CreationTime;
                item.LastAccessDate = fi.LastAccessTime;
                item.LastWriteDate = fi.LastWriteTime;
                item.IsFolder = false;
                item.Size = fi.Length;
                item.Extension = fi.Extension;
            }
            return item;
        }

        public static void CopyFolder(string source, string destination)
        {
            string[] files;
            if (destination[destination.Length - 1] != Path.DirectorySeparatorChar)
                destination += Path.DirectorySeparatorChar;
            if (!Directory.Exists(destination))
                Directory.CreateDirectory(destination);
            files = Directory.GetFileSystemEntries(source);
            foreach (string file in files)
            {
                if (Directory.Exists(file))
                    CopyFolder(file, destination + Path.GetFileName(file));
                else
                    File.Copy(file, destination + Path.GetFileName(file), true);
            }
        }
    }
    #endregion

    public partial class FileManager : System.Web.UI.UserControl
    {
        private void SetImage(ImageButton btn, string imageName)
        {
            string themeFolder = FileSystemManager.GetRootPath().ToString() +
                                "\\App_Themes\\" + this.Page.Theme + "\\FileManager";
            string codeThemeFolder = "~/App_Themes/" + this.Page.Theme + "/FileManager";
            if (File.Exists(themeFolder + "\\" + imageName))
                btn.ImageUrl = codeThemeFolder + "/" + imageName;
            else
                btn.ImageUrl = DefaultImagesFolder + imageName;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BindGrid();
                SetImage(btnCut, "cut.gif");
                SetImage(btnCopy, "copy.gif");
                SetImage(btnPaste, "paste.gif");
                SetImage(btnDelete, "delete.gif");
            }
        }

        private void BindGrid()
        {
            List<FileSystemItem> list = FileSystemManager.GetItems();
            gvwFiles.DataSource = list;
            gvwFiles.DataBind();
            lblCurrentPath.Text = FileSystemManager.GetRootPath();
            hcCheckBox.Width = WidthCheckBox;
            gvwFiles.Columns[0].ItemStyle.Width = WidthCheckBox;
            hcIcon.Width = WidthIcon;
            gvwFiles.Columns[1].ItemStyle.Width = WidthIcon;
            hcName.Width = WidthName;
            gvwFiles.Columns[2].ItemStyle.Width = WidthName;
            hcSize.Width = WidthSize;
            gvwFiles.Columns[3].ItemStyle.Width = WidthSize;
            hcCreatedDate.Width = WidthCreatedDate;
            gvwFiles.Columns[4].ItemStyle.Width = WidthCreatedDate;
            hcFunctions.Width = WidthFunctions;
            gvwFiles.Columns[5].ItemStyle.Width = WidthFunctions;
            divContainer.Style["width"] = Convert.ToString((WidthCheckBox.Value + WidthIcon.Value +
                WidthName.Value + WidthSize.Value + WidthCreatedDate.Value +
                WidthFunctions.Value) + 40) + "px";
        }
        private void BindGrid(string path)
        {
            List<FileSystemItem> list = FileSystemManager.GetItems(path);
            gvwFiles.DataSource = list;
            gvwFiles.DataBind();
            lblCurrentPath.Text = path;
        }

        protected void gvlnkName_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            if (Directory.Exists(lb.CommandArgument.ToString()))
                BindGrid(lb.CommandArgument.ToString());
            else
            {
                string path = lb.CommandArgument.ToString();
                path = path.Replace(FileSystemManager.GetRootPath(), "~");
                path = path.Replace("\\", "/");
                Response.Redirect(path);
            }
            gvwFiles.EditIndex = -1;
            BindGrid(lblCurrentPath.Text);
        }

        protected void gvwFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            foreach (GridViewRow row in gvwFiles.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {

                    row.Attributes.Add("onmouseover", "this.className='" + OnMouseOverClass + "';");
                    row.Attributes.Add("onmouseout", "this.className='" + OnMouseOutClass + "';");

                    string themeFolder = FileSystemManager.GetRootPath().ToString() +
                                "\\App_Themes\\" + this.Page.Theme + "\\FileManager";
                    string codeThemeFolder = "~/App_Themes/" + this.Page.Theme + "/FileManager";

                    LinkButton lb = (LinkButton)row.FindControl("gvlnkName");

                    #region Assign Command Argument as Gridview row values to all Image Buttons
                    ImageButton img;
                    Image im = (Image)row.FindControl("gvimgIcon");

                    if (row.RowState == DataControlRowState.Normal ||
                        row.RowState == DataControlRowState.Alternate)
                    {
                        img = (ImageButton)row.FindControl("gvbtnRename");
                        SetImage(img, "rename.gif");
                        img.CommandArgument = row.RowIndex.ToString();

                        img = (ImageButton)row.FindControl("gvbtnCut");
                        SetImage(img, "cut.gif");
                        img.CommandArgument = row.RowIndex.ToString();

                        img = (ImageButton)row.FindControl("gvbtnCopy");
                        SetImage(img, "copy.gif");
                        img.CommandArgument = row.RowIndex.ToString();

                        img = (ImageButton)row.FindControl("gvbtnDelete");
                        SetImage(img, "delete.gif");
                        img.CommandArgument = row.RowIndex.ToString();

                        img = (ImageButton)row.FindControl("gvbtnDownload");
                        SetImage(img, "download.gif");
                        img.CommandArgument = row.RowIndex.ToString();

                    #endregion



                        if (Directory.Exists(lb.CommandArgument))
                        {
                            Literal li = (Literal)row.FindControl("gvlblSize");
                            li.Visible = false;


                            if ((lb.Text == "[Root]") || (lb.Text == "[Parent]"))
                            {
                                string tempLb = lb.Text.Replace("[", "").Replace("]", "");
                                if (EnableThemingIcons && File.Exists(themeFolder + "\\folder_" + tempLb + ".gif"))
                                    im.ImageUrl = codeThemeFolder + "/folder_" + tempLb + ".gif";
                                else
                                    im.ImageUrl = DefaultImagesFolder + "folder_" + tempLb + ".gif";

                                img = (ImageButton)row.FindControl("gvbtnRename");
                                img.Visible = false;
                                img = (ImageButton)row.FindControl("gvbtnCut");
                                img.Visible = false;
                                img = (ImageButton)row.FindControl("gvbtnCopy");
                                img.Visible = false;
                                img = (ImageButton)row.FindControl("gvbtnDelete");
                                img.Visible = false;
                                li = (Literal)row.FindControl("gvlblCreatedDate");
                                li.Visible = false;
                            }
                            else
                            {
                                if (EnableThemingIcons && File.Exists(themeFolder + "\\folder.gif"))
                                    im.ImageUrl = codeThemeFolder + "/folder" + ".gif";
                                else
                                    im.ImageUrl = DefaultImagesFolder + "folder" + ".gif";

                            }
                            img = (ImageButton)row.FindControl("gvbtnDownload");
                            img.Visible = false;
                        }
                        else if (File.Exists(lb.CommandArgument))
                        {

                        }
                    }
                    else
                    {
                        img = (ImageButton)row.FindControl("gvbtnRenameOk");
                        SetImage(img, "rename_ok.gif");
                        img.CommandArgument = row.RowIndex.ToString();
                        img = (ImageButton)row.FindControl("gvbtnRenameCancel");
                        SetImage(img, "rename_cancel.gif");
                    }
                }
            }
        }

        protected void gvwFiles_DataBound(object sender, EventArgs e)
        {
            List<string> Array = new List<string>();
            Array.Add("'" + chkSelectAll.ClientID + "'");
            foreach (GridViewRow row in gvwFiles.Rows)
            {
                CheckBox cb = (CheckBox)row.FindControl("gvchkFile");
                Array.Add("'" + cb.ClientID + "'");
            }
            CheckBoxIDsArray.Text = "<script type=\"text/javascript\">" +
                                "" +
                                String.Concat("var CheckBoxIDs =  new Array(", String.Join(",", Array.ToArray()), ");") +
                                "" +
                                "</script>";

            chkSelectAll.Attributes.Add("onclick", "ChangeAllCheckBoxStates(this.checked);");
        }

        #region Gridview Rename, Cut, Copy, Delete and Download Functions

        protected void gvbtnRename_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            gvwFiles.EditIndex = Convert.ToInt32(ib.CommandArgument);
            BindGrid(lblCurrentPath.Text);
        }

        protected void gvbtnCut_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            int index = Convert.ToInt32(ib.CommandArgument);
            LinkButton lb = (LinkButton)gvwFiles.Rows[index].FindControl("gvlnkName");
            List<string> items = new List<string>();
            items.Add(lb.CommandArgument);
            ViewState["clipboard"] = items;
            ViewState["action"] = "cut";
        }
        protected void gvbtnCopy_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            int index = Convert.ToInt32(ib.CommandArgument);
            LinkButton lb = (LinkButton)gvwFiles.Rows[index].FindControl("gvlnkName");
            List<string> items = new List<string>();
            items.Add(lb.CommandArgument);
            ViewState["clipboard"] = items;
            ViewState["action"] = "copy";
        }
        protected void gvbtnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            int index = Convert.ToInt32(ib.CommandArgument);
            LinkButton lb = (LinkButton)gvwFiles.Rows[index].FindControl("gvlnkName");
            string _filename = lb.CommandArgument;
            if (Directory.Exists(_filename))
                FileSystemManager.DeleteFolder(_filename);
            else
                FileSystemManager.DeleteFile(_filename);
            BindGrid(lblCurrentPath.Text);
        }
        private void DownloadFile(string filename, bool forceDownload)
        {
            string path = filename;
            string name = Path.GetFileName(path);
            string ext = Path.GetExtension(path);
            if (forceDownload)
            {
                Response.AppendHeader("content-disposition",
                    "attachment; filename=" + name);
            }
            Response.WriteFile(path);
            Response.End();

        }
        protected void gvbtnDownload_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            int index = Convert.ToInt32(ib.CommandArgument);
            LinkButton lb = (LinkButton)gvwFiles.Rows[index].FindControl("gvlnkName");
            Session["DownloadFileName"] = lb.CommandArgument;
            Response.Redirect("Download.aspx");
        }

        #endregion

        #region Properties
        [Bindable(true), Category("Appearance"), DefaultValue(false),
            Description("Show the actual physical folder path")]
        public bool ShowActualPath
        {
            get { return lblCurrentPath.Visible; }
            set { lblCurrentPath.Visible = value; }
        }

        private bool _enableThemingIcons = true;
        [Bindable(true), Category("Appearance"), DefaultValue(true),
            Description("Enable Themeing")]
        public bool EnableThemingIcons
        {
            get { return _enableThemingIcons; }
            set { _enableThemingIcons = value; }
        }

        private string _defaultImagesFolder = "~/Images/FileManager/";
        public string DefaultImagesFolder
        {
            get { return _defaultImagesFolder; }
            set
            {
                string _folder = value;
                if (!_folder.EndsWith("/"))
                    _folder += "/";
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            lblCurrentPath.Visible = ShowActualPath;
            base.Render(writer);
        }

        private string _onMouseOutClass = "";
        public string OnMouseOutClass
        {
            get { return _onMouseOutClass; }
            set { _onMouseOutClass = value; }
        }

        private string _onMouseOverClass = "";
        public string OnMouseOverClass
        {
            get { return _onMouseOverClass; }
            set { _onMouseOverClass = value; }
        }

        private Unit _widthCheckBox = Unit.Pixel(15);
        public Unit WidthCheckBox
        {
            get { return _widthCheckBox; }
            set
            {
                _widthCheckBox = value;
                hcCheckBox.Width = _widthCheckBox;
                gvwFiles.Columns[0].ItemStyle.Width = _widthCheckBox;
            }
        }

        private Unit _widthIcon = Unit.Pixel(20);
        public Unit WidthIcon
        {
            get { return _widthIcon; }
            set
            {
                _widthIcon = value;
                hcIcon.Width = _widthIcon;
                gvwFiles.Columns[1].ItemStyle.Width = _widthIcon;
            }
        }

        private Unit _widthName = Unit.Pixel(230);
        public Unit WidthName
        {
            get { return _widthName; }
            set
            {
                _widthName = value;
                hcIcon.Width = _widthName;
                gvwFiles.Columns[2].ItemStyle.Width = _widthName;
            }
        }

        private Unit _widthSize = Unit.Pixel(50);
        public Unit WidthSize
        {
            get { return _widthSize; }
            set
            {
                _widthSize = value;
                hcIcon.Width = _widthSize;
                gvwFiles.Columns[3].ItemStyle.Width = _widthSize;
            }
        }

        private Unit _widthCreatedDate = Unit.Pixel(140);
        public Unit WidthCreatedDate
        {
            get { return _widthCreatedDate; }
            set
            {
                _widthCreatedDate = value;
                hcIcon.Width = _widthCreatedDate;
                gvwFiles.Columns[4].ItemStyle.Width = _widthCreatedDate;
            }
        }

        private Unit _widthFunctions = Unit.Pixel(100);
        public Unit WidthFunctions
        {
            get { return _widthFunctions; }
            set
            {
                _widthFunctions = value;
                hcIcon.Width = _widthFunctions;
                gvwFiles.Columns[4].ItemStyle.Width = _widthFunctions;
            }
        }
        #endregion



        protected void btnCut_Click(object sender, ImageClickEventArgs e)
        {
            List<string> items = new List<string>();
            foreach (GridViewRow row in gvwFiles.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cb = (CheckBox)row.FindControl("gvchkFile");
                    if (cb.Checked)
                    {
                        LinkButton lb = (LinkButton)row.FindControl("gvlnkName");
                        items.Add(lb.CommandArgument);
                    }
                }
            }
            ViewState["clipboard"] = items;
            ViewState["action"] = "cut";
        }
        protected void btnCopy_Click(object sender, ImageClickEventArgs e)
        {
            List<string> items = new List<string>();
            foreach (GridViewRow row in gvwFiles.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cb = (CheckBox)row.FindControl("gvchkFile");
                    if (cb.Checked)
                    {
                        LinkButton lb = (LinkButton)row.FindControl("gvlnkName");
                        items.Add(lb.CommandArgument);
                    }
                }
            }
            ViewState["clipboard"] = items;
            ViewState["action"] = "copy";
        }
        protected void btnPaste_Click(object sender, ImageClickEventArgs e)
        {
            if (ViewState["clipboard"] != null)
            {
                if (ViewState["action"].ToString() == "cut")
                {
                    List<string> items = (List<string>)ViewState["clipboard"];
                    foreach (string s in items)
                    {
                        if (Directory.Exists(s))
                        {
                            Directory.Move(s, lblCurrentPath.Text + s.Substring(s.LastIndexOf("\\")));
                        }
                        else
                        {
                            File.Move(s, lblCurrentPath.Text + "\\" + Path.GetFileName(s));
                        }
                    }
                }
                else
                {
                    List<string> items = (List<string>)ViewState["clipboard"];
                    foreach (string s in items)
                    {
                        if (Directory.Exists(s))
                        {
                            DirectoryInfo di = new DirectoryInfo(s);
                            FileSystemManager.CopyFolder(s, lblCurrentPath.Text + "\\" + di.Name);
                        }
                        else
                        {
                            File.Copy(s, lblCurrentPath.Text + "\\" + Path.GetFileName(s));
                        }
                    }
                }
            }
            ViewState["clipboard"] = null;
            ViewState["action"] = null;
            BindGrid(lblCurrentPath.Text);
        }
        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            foreach (GridViewRow row in gvwFiles.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cb = (CheckBox)row.FindControl("gvchkFile");
                    if (cb.Checked)
                    {
                        LinkButton lb = (LinkButton)row.FindControl("gvlnkName");
                        if (Directory.Exists(lb.CommandArgument))
                        {
                            FileSystemManager.DeleteFolder(lb.CommandArgument);
                        }
                        else
                        {
                            FileSystemManager.DeleteFile(lb.CommandArgument);
                        }
                    }
                }
            }
            BindGrid(lblCurrentPath.Text);
        }
        protected void gvbtnRenameCancel_Click(object sender, ImageClickEventArgs e)
        {
            gvwFiles.EditIndex = -1;
            BindGrid(lblCurrentPath.Text);
        }
        protected void gvbtnRenameOk_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            int index = Convert.ToInt32(ib.CommandArgument);
            string src = "";
            string dest = "";
            TextBox tb = (TextBox)gvwFiles.Rows[index].FindControl("gvtxtName");
            src = tb.ToolTip;
            dest = src.Substring(0, src.LastIndexOf('\\'));
            dest = dest + "\\" + tb.Text;
            if (src != dest)
            {
                if (Directory.Exists(src))
                    FileSystemManager.MoveFolder(src, dest);
                else
                    FileSystemManager.MoveFile(src, dest);
            }
            gvwFiles.EditIndex = -1;
            BindGrid(lblCurrentPath.Text);
        }
    }
}