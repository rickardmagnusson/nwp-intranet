<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileManager.ascx.cs" Inherits="PS.UserControls.FileManager" %>

<script type="text/javascript" language="javascript">
        function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], checkState);
            }
        }
        
        function ChangeHeaderAsNeeded()
        {
            // Whenever a checkbox in the GridView is toggled, we need to
            // check the Header checkbox if ALL of the GridView checkboxes are
            // checked, and uncheck it otherwise
            if (CheckBoxIDs != null)
            {
                // check to see if all other checkboxes are checked
                for (var i = 1; i < CheckBoxIDs.length; i++)
                {
                    var cb = document.getElementById(CheckBoxIDs[i]);
                    if (!cb.checked)
                    {
                        // Whoops, there is an unchecked checkbox, make sure
                        // that the header checkbox is unchecked
                        ChangeCheckBoxState(CheckBoxIDs[0], false);
                        return;
                    }
                }
                
                // If we reach here, ALL GridView checkboxes are checked
                ChangeCheckBoxState(CheckBoxIDs[0], true);
            }
        }
</script>
<div>
    <asp:ImageButton runat="server" ID="btnCut" AlternateText="Cut" OnClick="btnCut_Click" />
    <asp:ImageButton runat="server" ID="btnCopy" AlternateText="Copy" OnClick="btnCopy_Click" />
    <asp:ImageButton runat="server" ID="btnPaste" AlternateText="Paste" OnClick="btnPaste_Click" />
    <asp:ImageButton runat="server" ID="btnDelete" AlternateText="Delete"
     OnClientClick="if(confirm('Are you sure you want to delete the selected items?')==false) return false;" OnClick="btnDelete_Click" />
</div>
<asp:Table CssClass="FileManagerCssClass" runat="server" ID="tblHeader">
    <asp:TableRow>
        <asp:TableCell ID="hcCheckBox" HorizontalAlign="center">
           <asp:CheckBox runat="server" ID="chkSelectAll" />
        </asp:TableCell>
        <asp:TableCell ID="hcIcon">&nbsp;</asp:TableCell>
        <asp:TableCell ID="hcName">Name</asp:TableCell>
        <asp:TableCell ID="hcSize">Size</asp:TableCell>
        <asp:TableCell ID="hcCreatedDate">Created Date</asp:TableCell>
        <asp:TableCell ID="hcFunctions">&nbsp;</asp:TableCell>
    </asp:TableRow>
</asp:Table>
<div style="overflow:auto; height:300px;" runat="server" id="divContainer">
<asp:GridView runat="server" ID="gvwFiles" AutoGenerateColumns="false" ShowHeader="false"
      OnRowDataBound="gvwFiles_RowDataBound" OnDataBound="gvwFiles_DataBound">       
        <Columns>            
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="gvchkFile" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <EditItemTemplate>
                    <asp:CheckBox runat="server" ID="gvchkFile" Enabled="false" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image runat="server" ID="gvimgIcon" ImageUrl="~/Images/FileManager/file.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <EditItemTemplate>
                </EditItemTemplate>
            </asp:TemplateField>            
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="gvlnkName" OnClick="gvlnkName_Click" 
                    Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("FullName") %>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                <EditItemTemplate>
                    <asp:TextBox runat="Server" ID="gvtxtName"
                     Text='<%# Eval("Name") %>' ToolTip='<%# Eval("FullName") %>' />
                </EditItemTemplate>
            </asp:TemplateField>            
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Literal runat="server" ID="gvlblSize" Text='<%# Eval("Size") %>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
            </asp:TemplateField>
            
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Literal runat="server" ID="gvlblCreatedDate" Text='<%# Eval("CreatedDate") %>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
            </asp:TemplateField>
            
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton runat="server" ID="gvbtnRename" ToolTip="Rename" AlternateText="Rename" OnClick="gvbtnRename_Click" CommandName="Edit" />
                    <asp:ImageButton runat="server" ID="gvbtnCut" ToolTip="Cut" AlternateText="Cut" OnClick="gvbtnCut_Click" />
                    <asp:ImageButton runat="server" ID="gvbtnCopy" ToolTip="Copy" AlternateText="Copy" OnClick="gvbtnCopy_Click" />&nbsp;
                    <asp:ImageButton runat="server" ID="gvbtnDelete" ToolTip="Delete" AlternateText="Delete"
                     OnClientClick="if(confirm('Are you sure you want to delete?')==false)return false;" OnClick="gvbtnDelete_Click" />
                    <asp:ImageButton runat="server" ID="gvbtnDownload" ToolTip="Download" AlternateText="Download" OnClick="gvbtnDownload_Click" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <EditItemTemplate>
                    <asp:ImageButton runat="server" ID="gvbtnRenameOk" AlternateText="Rename" OnClick="gvbtnRenameOk_Click" />
                    <asp:ImageButton runat="server" ID="gvbtnRenameCancel" AlternateText="Cancel Rename" OnClick="gvbtnRenameCancel_Click" />
                </EditItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:Literal runat="server" ID="lblCurrentPath"  Visible="False" />
 <asp:Literal runat="server" ID="CheckBoxIDsArray" />
 </div>