﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>File Manager v0.0.1.1 Preview (Test Version)</title>
</head>
<body>
    <form id="form1" runat="server">
    <h1>File Manager v0.0.1.1 Preview (Test Version)</h1>
        <a href="Admin/FileManager.aspx" style="font-size:1.3em">Click here to view the file manager (v0.0.1.1).</a>
    <br />
    
    <h3>Version 0.0.1.1 Improvements</h3>
        <ul>
            <li>Uses session instead of cookies.</li>
        </ul>
    <h3>Using File Manager v0.0.1.1 Preview (Test Version) in your website</h3>
    <ol>
        <li>
            Please copy the file ~/UserControls/FileManager.ascx and ~/UserControls/FileManager.ascx.cs
            to any of your directory in your website.
        </li>
        <li>
            Then copy all the image files from the folder ~/Images/FileManager/
            <br />
            <i>File Types icons not supported in this version so you can omit this folder (~/Images/FileManager/FileTypes/) and its contents.</i>
            <br />
            <b>
                <i>
                    Further information: You can replace the default icons with your own icons, but remember to keep the file name same.<br />
                    Recommended icon size is 16X16.<br />
                    If you don't want to use the default folder hierarchy as in this example, you can copy those files in your own custom folder and especify the DefaultImagesFolder
                    Property to the folder. (The filename should remain the same.)
                    
                </i>
            </b>
        </li>     
        <li>
            Create a new .aspx file and register the user Control as below:<br />
            &lt;%@ Register Src="../UserControls/FileManager.ascx" TagName="FileManager" TagPrefix="uc1" %&gt;<br />
            You are now ready to use the File Manager User Control.
        </li>         
    </ol>
    <h3>File Manager User Control Properties</h3>
    <ol>
        <li>
            If you want to show the acutal path of the current folder as it is in the server then set the ShowActualPath Property to true. (By default it is set to false)
        </li>
        <li>
            If you want to set the on mouse over effect on row then set the OnMouseOverClass Property and OnMouseOutClass Property <br />
            To bring the OnMouseOutClass to normal view, set background-color:Transparent;
        </li>
        <li>
            You can further change the width of each column manually by setting the following properties <br />
            WidthCheckBox, WidthIcon, WidthName, WidthSize, WidthCreatedDate, WidthFunctions
        </li>
        <li>
            If EnableThemingIcons Property is set to false then Icons from the Theme folder will not be used. The Default folder will be used.<br />
            In order to use different icons in each Theme Copy the ~/Images/FileManager/*.* to ~/App_Themes/ThemeName/FileManager/<br />
            First it will check if the icon is present in the ~/App_Themes/ThemeName/FileManager/*.gif if present and EnableThemingIcons is not set to false then it will use those icons.
            If some icons are missing from the folder then it will use from the defualt folder that is ~/Images/FileManager/*.gif
        </li>
    </ol>
    <h3>Futures</h3>
    <ol>
        <li>Apply style to file manager header.</li>
        <li>Complied user control (.dll) for easier integration</li>
        <li>Ajax enabled.</li>
    </ol>
    <br />
    <br />
    <b>For more information and suggestions please feel free to contact Prabir Shrestha. <br />
    Email address:<a href="mailto:prabirshrestha@gmail.com">prabirshrestha@gmail.com</a></b>
    </form>
</body>
</html>
