<%@ Page Language="C#" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load()
    {
        try
        {
            // retrieve the path of the file to download, and create
            // a FileInfo object to read its properties
            // string path = Server.MapPath(Request.QueryString["File"]);
            string path = Session["DownloadFileName"].ToString();
            Session["DownloadFileName"] = null;

            System.IO.FileInfo file = new System.IO.FileInfo(path);

            // clear the current output content from the buffer
            Response.Clear();

            // add the header that specifies the default filename for the Download/SaveAs dialog
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            // add the header that specifies the file size, so that the browser
            // can show the download progress
            Response.AddHeader("Content-Length", file.Length.ToString());
            // specify that the response is a stream that cannot be read by the
            // client and must be downloaded
            Response.ContentType = "application/octet-stream";
            // send the file stream to the client
            Response.WriteFile(file.FullName);
            // stop the execution of this page
            Response.End();
        }
        catch (Exception)
        {
            Response.Write("<p style=\"color:red;font-size=20px;font-weight:bold\">Invalid download session</p>");
        }           
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Download</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
