﻿v0.96
-----
Fixed some missing files references
Added a derivedStyle.css reference in the Layout file so that a derived theme can easily override css classes. 

v0.95
------  
Fixed tooltip width so that it scales with its content.

v0.94
------  
Changed the dependency on JQuery to the orchard instead of the google cdn one
