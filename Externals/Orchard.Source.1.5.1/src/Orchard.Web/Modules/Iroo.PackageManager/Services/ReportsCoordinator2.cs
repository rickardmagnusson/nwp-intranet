﻿using System;
using System.Collections.Generic;
using Orchard;
using Orchard.Logging;
using Orchard.Reports;
using Orchard.Reports.Services;

namespace Iroo.PackageManager.Services {
    /// <summary>
    /// This is a copy of "IReportCoordinator" that gives access to
    /// the report ID created with the "Register" method.
    /// </summary>
    public interface IReportsCoordinator2 : IDependency {
        void Add(string reportKey, ReportEntryType type, string message);
        int Register(string reportKey, string activityName, string title);
    }

    public class ReportsCoordinator2 : IReportsCoordinator2, IDisposable {
        private readonly IReportsManager _reportsManager;
        private readonly IDictionary<string, int> _reports;

        public ReportsCoordinator2(IReportsManager reportsManager) {
            _reportsManager = reportsManager;
            Logger = NullLogger.Instance;
            _reports = new Dictionary<string, int>();
        }

        public ILogger Logger { get; set; }
        public void Dispose() {
            _reportsManager.Flush();
        }

        public void Add(string reportKey, ReportEntryType type, string message) {
            if (!_reports.ContainsKey(reportKey)) {
                // ignore message if no corresponding report
                return;
            }

            _reportsManager.Add(_reports[reportKey], type, message);
        }

        public int Register(string reportKey, string activityName, string title) {
            int reportId = _reportsManager.CreateReport(title, activityName);
            _reports.Add(reportKey, reportId);
            return reportId;
        }
    }
}