﻿Name: HTMLKickStart
Author: Erik Lenaerts based on original code from Joshua Gatcke
Website: http://www.99lime.com
Description: This theme is based on the excelent HTML KickStart; "HTML KickStart is an ultra–lean set of HTML5, CSS, and jQuery (javascript) files, layouts, and elements designed to give you a headstart and save you 10's of hours on your next web project."
Version: 0.96
Zones: Header, Navigation, Featured, BeforeMain, AsideFirst, Messages, BeforeContent, Content, AfterContent, AsideSecond, AfterMain, TripelFirst, TripelSecond, TripelThird, FooterQuadFirst, FooterQuadSecond, FooterQuadThird, FooterQuadFourth, Footer