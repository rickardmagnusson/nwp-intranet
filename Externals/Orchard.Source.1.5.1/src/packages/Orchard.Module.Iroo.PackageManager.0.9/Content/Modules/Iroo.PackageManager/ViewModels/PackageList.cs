using System.Collections.Generic;
using Iroo.PackageManager.Services;

namespace Iroo.PackageManager.ViewModels {
    public class PackageList {
        public IEnumerable<UpdatePackageEntry> Entries { get; set; }
    }
}