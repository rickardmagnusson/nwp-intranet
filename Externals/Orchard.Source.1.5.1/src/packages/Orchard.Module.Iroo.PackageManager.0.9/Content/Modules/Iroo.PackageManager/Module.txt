Name: Package Manager
AntiForgery: enabled
Author: Renaud Paquay
Website: http://IrooPackageManager.codeplex.com
Version: 0.9
OrchardVersion: 1.0.20
Description: Provide a new "Update" menu item under "Gallery" to enable auto-updating out-of-date local packages from the Gallery server. Update 0.1.1: tweak UI. Update 0.1.2: More reliable notifications, tweak UI. Update 0.9: Refresh package list in background task.
Features:
    Iroo.PackageManager:
        Category: Packaging
        Dependencies: Gallery
