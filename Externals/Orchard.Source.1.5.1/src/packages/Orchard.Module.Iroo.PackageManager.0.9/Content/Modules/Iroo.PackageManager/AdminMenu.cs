﻿using System.Linq;
using Iroo.PackageManager.Services;
using Orchard.Localization;
using Orchard.UI.Navigation;

namespace Iroo.PackageManager {
    public class AdminMenu : INavigationProvider {
        private readonly IBackgroundPackageUpdateStatus _backgroundPackageUpdateStatus;

        public AdminMenu(IBackgroundPackageUpdateStatus backgroundPackageUpdateStatus) {
            _backgroundPackageUpdateStatus = backgroundPackageUpdateStatus;
        }

        public Localizer T { get; set; }
        public string MenuName { get { return "admin"; } }

        public void GetNavigation(NavigationBuilder builder) {
            int updateCount = GetUpdateCount();
            var caption = (updateCount == 0 ? T("Updates") : T("Updates ({0})", updateCount));
            builder.Add(T("Gallery"), "30",
                        menu => menu.Add(caption, "10.0", item => item.Action("List", "Admin", new { area = "Iroo.PackageManager" })));
        }

        private int GetUpdateCount() {
            try {
                // Admin menu should never block, so we simply return the result from the background task
                return _backgroundPackageUpdateStatus.Value == null ? 
                    0 : 
                    _backgroundPackageUpdateStatus.Value.Entries.Where(e => e.NewVersionToInstall != null).Count();
            }
            catch {
                return 0;
            }
        }
    }
}