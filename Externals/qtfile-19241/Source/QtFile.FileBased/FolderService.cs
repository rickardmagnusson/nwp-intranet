namespace QtFile.FileBased
{
    using QtFile;
    using QtFile.Services;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class FolderService : FileBasedService, IFolderService
    {
        public FolderService(string rootPath, string username, bool encodeUserName, char directorySeparator) : base(rootPath, username, encodeUserName, directorySeparator)
        {
        }

        public int Count()
        {
            return base.RootFolder.CountFolders();
        }

        public OpResult Create(string path)
        {
            string str = base.RootFolder.GetPath(new string[] { path });
            if (Directory.Exists(str))
            {
                return OpResult.AlreadyExist;
            }
            Directory.CreateDirectory(str);
            return OpResult.Succeed;
        }

        public OpResult Delete(string path)
        {
            string str = base.RootFolder.GetPath(new string[] { path });
            if (!Directory.Exists(str))
            {
                return OpResult.NotFound;
            }
            Directory.Delete(str, true);
            return OpResult.Succeed;
        }

        public IEnumerable<UserFolder> GetAllFolders()
        {
            int num = base.RootFolder.RootPath.Length + 1;
            return (from p in Directory.GetDirectories(base.RootFolder.RootPath, "*", SearchOption.AllDirectories) select new UserFolder { Path = base.ResolvePathResult(p) });
        }

        public OpResult Move(string srcPath, string destPath)
        {
            srcPath = base.RootFolder.GetPath(new string[] { srcPath });
            destPath = base.RootFolder.GetPath(new string[] { destPath });
            if (!Directory.Exists(srcPath))
            {
                return OpResult.NotFound;
            }
            if (!Directory.Exists(Path.GetDirectoryName(destPath)))
            {
                return OpResult.FolderNotFound;
            }
            if (Directory.Exists(destPath))
            {
                return OpResult.AlreadyExist;
            }
            Directory.Move(srcPath, destPath);
            return OpResult.Succeed;
        }
    }
}

