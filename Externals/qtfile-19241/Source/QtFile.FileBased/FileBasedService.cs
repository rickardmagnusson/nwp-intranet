namespace QtFile.FileBased
{
    using QtFile;
    using System;
    using System.IO;
    using System.Text;

    public abstract class FileBasedService
    {
        private readonly char directorySeparator;
        private readonly bool encodeUsername;
        private QtFile.FileBased.RootFolder rootFolder;
        private readonly string rootPath;
        private readonly string username;

        public FileBasedService(string rootPath, string username, bool encodeUsername, char directorySeparator)
        {
            Helper.ValidateNotNullOrEmpty(rootPath, "rootPath");
            Helper.ValidateNotNullOrEmpty(username, "username");
            this.rootPath = rootPath;
            this.username = username;
            if (!Directory.Exists(rootPath))
            {
                Directory.CreateDirectory(rootPath);
            }
            this.encodeUsername = encodeUsername;
            this.directorySeparator = directorySeparator;
        }

        public long GetSize()
        {
            return this.RootFolder.GetSize();
        }

        protected string ResolvePathResult(string path)
        {
            if (Path.IsPathRooted(path))
            {
                path = path.Substring(this.RootFolder.RootPath.Length + 1);
            }
            StringBuilder builder = new StringBuilder(path.Length);
            foreach (char ch in path)
            {
                if ((ch == Path.DirectorySeparatorChar) || (ch == Path.AltDirectorySeparatorChar))
                {
                    builder.Append(this.directorySeparator);
                }
                else
                {
                    builder.Append(ch);
                }
            }
            return builder.ToString();
        }

        protected QtFile.FileBased.RootFolder RootFolder
        {
            get
            {
                if (this.rootFolder == null)
                {
                    this.rootFolder = new QtFile.FileBased.RootFolder(this.rootPath, this.username, this.encodeUsername, this.directorySeparator);
                }
                return this.rootFolder;
            }
        }

        protected string RootPath
        {
            get
            {
                return this.rootPath;
            }
        }
    }
}

