namespace QtFile.FileBased
{
    using QtFile;
    using QtFile.Services;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Resources;
    using System.Web;

    public class FileService : FileBasedService, IFileService
    {
        private static readonly ResourceManager mimeResource = new ResourceManager("QtFile.Resources.MimeTypes", Assembly.GetExecutingAssembly());

        public FileService(string rootPath, string username, bool encodeUserName, char directorySeparator) : base(rootPath, username, encodeUserName, directorySeparator)
        {
        }

        public int Count()
        {
            return base.RootFolder.CountFiles();
        }

        public OpResult Delete(string path)
        {
            string str = base.RootFolder.GetPath(new string[] { path });
            if (!File.Exists(str))
            {
                return OpResult.NotFound;
            }
            File.Delete(str);
            return OpResult.Succeed;
        }

        public IEnumerable<UserFileInfo> GetFileList(string path)
        {
            string str = string.IsNullOrEmpty(path) ? base.RootFolder.RootPath : base.RootFolder.GetPath(new string[] { path });
            if (!Directory.Exists(str))
            {
                return null;
            }
            int num = str.Length + 1;
            string[] files = Directory.GetFiles(str);
            List<UserFileInfo> list = new List<UserFileInfo>(files.Length);
            foreach (string str2 in files)
            {
                FileInfo info = new FileInfo(str2);
                UserFileInfo item = new UserFileInfo {
                    Name = info.Name,
                    Size = info.Length,
                    ContentType = GuessMime(info.Name),
                    Modified = info.LastWriteTimeUtc
                };
                list.Add(item);
            }
            return list;
        }

        private static string GuessMime(string filePath)
        {
            return (mimeResource.GetString(Path.GetExtension(filePath).ToLower()) ?? "application/x-unknown-content-type");
        }

        public OpResult Move(string srcPath, string destPath, bool overWrite)
        {
            srcPath = base.RootFolder.GetPath(new string[] { srcPath });
            destPath = base.RootFolder.GetPath(new string[] { destPath });
            if (!File.Exists(srcPath))
            {
                return OpResult.NotFound;
            }
            if (!Directory.Exists(Path.GetDirectoryName(destPath)))
            {
                return OpResult.FolderNotFound;
            }
            if (File.Exists(destPath))
            {
                if (!overWrite)
                {
                    return OpResult.AlreadyExist;
                }
                File.Delete(destPath);
            }
            File.Move(srcPath, destPath);
            return OpResult.Succeed;
        }

        public UserFile Retrieve(string path)
        {
            string fileName = base.RootFolder.GetPath(new string[] { path });
            FileInfo info = new FileInfo(fileName);
            if (info.Exists)
            {
                return new UserFile { Path = fileName, Name = info.Name, Size = info.Length, ContentType = GuessMime(info.Name), Modified = info.LastWriteTime };
            }
            return null;
        }

        public OpResult Save(string folderPath, HttpPostedFileBase file, bool overWrite)
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }
            string fileName = Path.GetFileName(file.FileName);
            string path = base.RootFolder.GetPath(new string[] { folderPath, fileName });
            if (!Directory.Exists(Path.GetDirectoryName(path)))
            {
                return OpResult.FolderNotFound;
            }
            if (File.Exists(path))
            {
                if (!overWrite)
                {
                    return OpResult.AlreadyExist;
                }
                File.Delete(path);
            }
            file.SaveAs(path);
            return OpResult.Succeed;
        }
    }
}

