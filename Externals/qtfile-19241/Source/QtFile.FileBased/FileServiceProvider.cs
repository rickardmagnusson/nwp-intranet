namespace QtFile.FileBased
{
    using QtFile;
    using QtFile.Services;
    using System;

    public class FileServiceProvider : IFileServiceProvider
    {
        private readonly char directorySeparator;
        private readonly bool encodeUsername;
        private readonly string rootPath;

        public FileServiceProvider(string rootPath) : this(rootPath, false, '|')
        {
        }

        public FileServiceProvider(string rootPath, bool encodeUsername) : this(rootPath, encodeUsername, '|')
        {
        }

        public FileServiceProvider(string rootPath, bool encodeUsername, char directorySeparator)
        {
            Helper.ValidateNotNullOrEmpty(rootPath, "rootPath");
            this.rootPath = rootPath;
            this.directorySeparator = directorySeparator;
            this.encodeUsername = encodeUsername;
        }

        public IFileService GetFileService(string username)
        {
            return new FileService(this.rootPath, username, this.encodeUsername, this.directorySeparator);
        }

        public IFolderService GetFolderService(string username)
        {
            return new FolderService(this.rootPath, username, this.encodeUsername, this.directorySeparator);
        }
    }
}

