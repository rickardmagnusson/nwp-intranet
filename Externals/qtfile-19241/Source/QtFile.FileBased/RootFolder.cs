namespace QtFile.FileBased
{
    using QtFile;
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Text;

    /// <summary>
    /// Represents the root folder of a user.
    /// </summary>
    public class RootFolder
    {
        private readonly char dirSeparator;
        private int fileCount = -1;
        private int folderCount = -1;
        private long size = -1L;

        /// <summary>
        /// Creates a new instance for specified user.
        /// </summary>
        /// <param name="rootPath">
        /// The root path of file service.
        /// </param>
        /// <param name="username">
        /// The name of user.
        /// </param>
        /// <param name="encodeUsername">
        /// Whether encode the username, should be true is username may contain special characters.
        /// </param>
        /// <param name="dirSeparator">
        /// The path separator.
        /// </param>
        public RootFolder(string rootPath, string username, bool encodeUsername, char dirSeparator)
        {
            Helper.ValidateNotNullOrEmpty(rootPath, "rootPath");
            Helper.ValidateNotNullOrEmpty(username, "username");
            if (encodeUsername)
            {
                username = Encode(username);
            }
            this.RootPath = Path.Combine(rootPath, username);
            if (!Directory.Exists(this.RootPath))
            {
                Directory.CreateDirectory(this.RootPath);
            }
            this.dirSeparator = dirSeparator;
        }

        public int CountFiles()
        {
            if (this.fileCount < 0)
            {
                this.UpdateInfo();
            }
            return this.fileCount;
        }

        public int CountFolders()
        {
            if (this.folderCount < 0)
            {
                this.UpdateInfo();
            }
            return this.folderCount;
        }

        private static string Encode(string username)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(username);
            long num = (long) (1.3333333333333333 * bytes.Length);
            if ((num % 4L) != 0L)
            {
                num += 4L - (num % 4L);
            }
            char[] outArray = new char[num];
            Convert.ToBase64CharArray(bytes, 0, bytes.Length, outArray, 0);
            for (int i = 0; i < outArray.Length; i++)
            {
                if (outArray[i] == '+')
                {
                    outArray[i] = '-';
                }
                else if (outArray[i] == '/')
                {
                    outArray[i] = '_';
                }
            }
            return new string(outArray);
        }

        public string GetPath(params string[] segments)
        {
            string rootPath = this.RootPath;
            foreach (string str2 in segments)
            {
                rootPath = Path.Combine(rootPath, str2.Replace(this.dirSeparator, Path.DirectorySeparatorChar));
            }
            return rootPath;
        }

        public long GetSize()
        {
            if (this.size < 0L)
            {
                this.UpdateInfo();
            }
            return this.size;
        }

        private void UpdateInfo()
        {
            this.size = 0L;
            this.fileCount = 0;
            this.folderCount = 0;
            this.UpdateInfo(this.RootPath);
        }

        private void UpdateInfo(string path)
        {
            string[] files = Directory.GetFiles(path);
            this.fileCount += files.Length;
            foreach (string str in files)
            {
                this.size += new FileInfo(str).Length;
            }
            string[] directories = Directory.GetDirectories(path);
            this.folderCount += directories.Length;
            foreach (string str2 in directories)
            {
                this.UpdateInfo(str2);
            }
        }

        /// <summary>
        /// Gets the root path of current user.
        /// </summary>
        /// <value>
        /// The root path of current user.
        /// </value>
        public string RootPath { get; private set; }
    }
}

