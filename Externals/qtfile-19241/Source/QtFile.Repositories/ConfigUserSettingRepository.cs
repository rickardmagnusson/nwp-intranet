namespace QtFile.Repositories
{
    using QtFile;
    using QtFile.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    public class ConfigUserSettingRepository : IUserSettingRepository
    {
        private HashSet<string> GetContentType(ExtensionsConfig config)
        {
            HashSet<string> set = new HashSet<string>();
            foreach (ExtensionConfig config2 in config.OfType<ExtensionConfig>())
            {
                set.Add(config2.Extension);
            }
            return set;
        }

        public IUserSetting GetSetting(string username)
        {
            Helper.ValidateNotNullOrEmpty(username, "username");
            QtFileConfig instance = QtFileConfig.Instance;
            if (instance == null)
            {
                throw new ConfigurationErrorsException("Configuration section for QtFile is not found.");
            }
            long defaultQuota = instance.DefaultQuota;
            long defaultMaxSizePerFile = instance.DefaultMaxSizePerFile;
            int defaultMaxFiles = instance.DefaultMaxFiles;
            int defaultMaxFolders = instance.DefaultMaxFolders;
            HashSet<string> contentType = this.GetContentType(instance.DefautAllowedExtensions);
            if (instance.UserSettings != null)
            {
                foreach (UserSettingConfig config2 in instance.UserSettings.OfType<UserSettingConfig>())
                {
                    if (config2.Username == username)
                    {
                        if (config2.Quota > 0L)
                        {
                            defaultQuota = config2.Quota;
                        }
                        if (config2.MaxSizePerFile > 0L)
                        {
                            defaultMaxSizePerFile = config2.MaxSizePerFile;
                        }
                        if (config2.MaxFiles > 0)
                        {
                            defaultMaxFiles = config2.MaxFiles;
                        }
                        if (config2.MaxFolders > 0)
                        {
                            defaultMaxFolders = config2.MaxFolders;
                        }
                        foreach (string str in this.GetContentType(config2.AllowedExtensions))
                        {
                            if (!contentType.Contains(str))
                            {
                                contentType.Add(str);
                            }
                        }
                    }
                }
            }
            return new UserSetting { Quota = defaultQuota, MaxSizePerFile = defaultMaxSizePerFile, MaxFiles = defaultMaxFiles, MaxFolders = defaultMaxFolders, AllowedExtensions = contentType };
        }
    }
}

