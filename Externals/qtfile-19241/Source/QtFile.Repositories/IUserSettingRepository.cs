namespace QtFile.Repositories
{
    using QtFile;
    using System;

    public interface IUserSettingRepository
    {
        IUserSetting GetSetting(string username);
    }
}

