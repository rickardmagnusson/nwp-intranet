using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

[DebuggerDisplay(@"\{ path = {path} }", Type="<Anonymous Type>"), CompilerGenerated]
internal sealed class <>f__AnonymousType1<<path>j__TPar>
{
    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private readonly <path>j__TPar <path>i__Field;

    [DebuggerHidden]
    public <>f__AnonymousType1(<path>j__TPar path)
    {
        this.<path>i__Field = path;
    }

    [DebuggerHidden]
    public override bool Equals(object value)
    {
        var type = value as <>f__AnonymousType1<<path>j__TPar>;
        return ((type != null) && EqualityComparer<<path>j__TPar>.Default.Equals(this.<path>i__Field, type.<path>i__Field));
    }

    [DebuggerHidden]
    public override int GetHashCode()
    {
        int num = 0x4f03c7db;
        return ((-1521134295 * num) + EqualityComparer<<path>j__TPar>.Default.GetHashCode(this.<path>i__Field));
    }

    [DebuggerHidden]
    public override string ToString()
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("{ path = ");
        builder.Append(this.<path>i__Field);
        builder.Append(" }");
        return builder.ToString();
    }

    public <path>j__TPar path
    {
        get
        {
            return this.<path>i__Field;
        }
    }
}

