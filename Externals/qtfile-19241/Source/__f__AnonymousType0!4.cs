using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

[CompilerGenerated, DebuggerDisplay(@"\{ name = {name}, size = {size}, contentType = {contentType}, modified = {modified} }", Type="<Anonymous Type>")]
internal sealed class <>f__AnonymousType0<<name>j__TPar, <size>j__TPar, <contentType>j__TPar, <modified>j__TPar>
{
    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private readonly <contentType>j__TPar <contentType>i__Field;
    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private readonly <modified>j__TPar <modified>i__Field;
    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private readonly <name>j__TPar <name>i__Field;
    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private readonly <size>j__TPar <size>i__Field;

    [DebuggerHidden]
    public <>f__AnonymousType0(<name>j__TPar name, <size>j__TPar size, <contentType>j__TPar contentType, <modified>j__TPar modified)
    {
        this.<name>i__Field = name;
        this.<size>i__Field = size;
        this.<contentType>i__Field = contentType;
        this.<modified>i__Field = modified;
    }

    [DebuggerHidden]
    public override bool Equals(object value)
    {
        var type = value as <>f__AnonymousType0<<name>j__TPar, <size>j__TPar, <contentType>j__TPar, <modified>j__TPar>;
        return ((((type != null) && EqualityComparer<<name>j__TPar>.Default.Equals(this.<name>i__Field, type.<name>i__Field)) && (EqualityComparer<<size>j__TPar>.Default.Equals(this.<size>i__Field, type.<size>i__Field) && EqualityComparer<<contentType>j__TPar>.Default.Equals(this.<contentType>i__Field, type.<contentType>i__Field))) && EqualityComparer<<modified>j__TPar>.Default.Equals(this.<modified>i__Field, type.<modified>i__Field));
    }

    [DebuggerHidden]
    public override int GetHashCode()
    {
        int num = -1680578599;
        num = (-1521134295 * num) + EqualityComparer<<name>j__TPar>.Default.GetHashCode(this.<name>i__Field);
        num = (-1521134295 * num) + EqualityComparer<<size>j__TPar>.Default.GetHashCode(this.<size>i__Field);
        num = (-1521134295 * num) + EqualityComparer<<contentType>j__TPar>.Default.GetHashCode(this.<contentType>i__Field);
        return ((-1521134295 * num) + EqualityComparer<<modified>j__TPar>.Default.GetHashCode(this.<modified>i__Field));
    }

    [DebuggerHidden]
    public override string ToString()
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("{ name = ");
        builder.Append(this.<name>i__Field);
        builder.Append(", size = ");
        builder.Append(this.<size>i__Field);
        builder.Append(", contentType = ");
        builder.Append(this.<contentType>i__Field);
        builder.Append(", modified = ");
        builder.Append(this.<modified>i__Field);
        builder.Append(" }");
        return builder.ToString();
    }

    public <contentType>j__TPar contentType
    {
        get
        {
            return this.<contentType>i__Field;
        }
    }

    public <modified>j__TPar modified
    {
        get
        {
            return this.<modified>i__Field;
        }
    }

    public <name>j__TPar name
    {
        get
        {
            return this.<name>i__Field;
        }
    }

    public <size>j__TPar size
    {
        get
        {
            return this.<size>i__Field;
        }
    }
}

