using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

[DebuggerDisplay(@"\{ succeed = {succeed}, status = {status} }", Type="<Anonymous Type>"), CompilerGenerated]
internal sealed class <>f__AnonymousType2<<succeed>j__TPar, <status>j__TPar>
{
    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private readonly <status>j__TPar <status>i__Field;
    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private readonly <succeed>j__TPar <succeed>i__Field;

    [DebuggerHidden]
    public <>f__AnonymousType2(<succeed>j__TPar succeed, <status>j__TPar status)
    {
        this.<succeed>i__Field = succeed;
        this.<status>i__Field = status;
    }

    [DebuggerHidden]
    public override bool Equals(object value)
    {
        var type = value as <>f__AnonymousType2<<succeed>j__TPar, <status>j__TPar>;
        return (((type != null) && EqualityComparer<<succeed>j__TPar>.Default.Equals(this.<succeed>i__Field, type.<succeed>i__Field)) && EqualityComparer<<status>j__TPar>.Default.Equals(this.<status>i__Field, type.<status>i__Field));
    }

    [DebuggerHidden]
    public override int GetHashCode()
    {
        int num = -1087490016;
        num = (-1521134295 * num) + EqualityComparer<<succeed>j__TPar>.Default.GetHashCode(this.<succeed>i__Field);
        return ((-1521134295 * num) + EqualityComparer<<status>j__TPar>.Default.GetHashCode(this.<status>i__Field));
    }

    [DebuggerHidden]
    public override string ToString()
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("{ succeed = ");
        builder.Append(this.<succeed>i__Field);
        builder.Append(", status = ");
        builder.Append(this.<status>i__Field);
        builder.Append(" }");
        return builder.ToString();
    }

    public <status>j__TPar status
    {
        get
        {
            return this.<status>i__Field;
        }
    }

    public <succeed>j__TPar succeed
    {
        get
        {
            return this.<succeed>i__Field;
        }
    }
}

