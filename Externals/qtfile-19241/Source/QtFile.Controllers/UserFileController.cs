namespace QtFile.Controllers
{
    using QtFile;
    using QtFile.FileBased;
    using QtFile.Repositories;
    using QtFile.Services;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Web;
    using System.Web.Mvc;

    public class UserFileController : Controller
    {
        /// <summary>
        /// Represents the foder path used to contruct file based services while they are not provided.
        /// </summary>
        private const string DefaultFilePath = "~/UserFiles";
        private IFileServiceProvider serviceProvider;
        private IUserSettingRepository settingRepository;

        public UserFileController()
        {
        }

        public UserFileController(IUserSettingRepository settingRepository, IFileServiceProvider serviceProvider)
        {
            this.settingRepository = settingRepository;
            this.serviceProvider = serviceProvider;
        }

        public ActionResult Download(string username, string path)
        {
            try
            {
                if (this.OnFileDownloading(username, path))
                {
                    UserFile file = this.ServiceProvider.GetFileService(username).Retrieve(path);
                    if (file != null)
                    {
                        if (file.Stream != null)
                        {
                            return this.File(file.Stream, file.ContentType, base.get_Server().UrlEncode(file.Name));
                        }
                        if (file.Path != null)
                        {
                            return this.File(file.Path, file.ContentType, base.get_Server().UrlEncode(file.Name));
                        }
                        if (file.Data != null)
                        {
                            return this.File(file.Data, file.ContentType, base.get_Server().UrlEncode(file.Name));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                this.OnError(exception);
            }
            return this.NotFound();
        }

        [Authorize]
        public virtual ActionResult FileDelete(string path)
        {
            OpResult denied = OpResult.Denied;
            try
            {
                if (this.OnFileDeleting(path, ref denied))
                {
                    denied = this.ServiceProvider.GetFileService(this.Username).Delete(path);
                }
            }
            catch (Exception exception)
            {
                denied = this.OnError(exception);
            }
            return this.OperationResult(denied);
        }

        [Authorize]
        public virtual ActionResult FileList(string path)
        {
            OpResult denied = OpResult.Denied;
            try
            {
                if (this.OnFileListing(path, ref denied))
                {
                    IEnumerable<UserFileInfo> fileList = this.ServiceProvider.GetFileService(this.Username).GetFileList(path);
                    return base.Json(from f in fileList select f.ToJson());
                }
                return this.OperationResult(denied);
            }
            catch (Exception exception)
            {
                return this.OperationResult(this.OnError(exception));
            }
        }

        [Authorize]
        public virtual ActionResult FileManager(string folder)
        {
            IFileService fileService = this.ServiceProvider.GetFileService(this.Username);
            IFolderService folderService = this.ServiceProvider.GetFolderService(this.Username);
            FileMgrViewData data = new FileMgrViewData {
                Folders = folderService.GetAllFolders(),
                CurrentFolder = folder,
                Files = fileService.GetFileList(folder ?? string.Empty)
            };
            return base.View(data);
        }

        [Authorize]
        public virtual ActionResult FileMove(string srcPath, string destPath)
        {
            OpResult denied = OpResult.Denied;
            try
            {
                if (this.OnFileMoving(srcPath, destPath, ref denied))
                {
                    denied = this.ServiceProvider.GetFileService(this.Username).Move(srcPath, destPath, false);
                }
            }
            catch (Exception exception)
            {
                denied = this.OnError(exception);
            }
            return this.OperationResult(denied);
        }

        [Authorize]
        public virtual ActionResult FileUpload(string path, HttpPostedFileBase fileUploaded)
        {
            OpResult denied = OpResult.Denied;
            try
            {
                if (this.OnFileUploading(path, fileUploaded, ref denied))
                {
                    IUserSetting setting = this.SettingRepository.GetSetting(this.Username);
                    IFileService fileService = this.ServiceProvider.GetFileService(this.Username);
                    if (!setting.IsExtAllowed(Path.GetExtension(fileUploaded.FileName)))
                    {
                        denied = OpResult.FileTypeNotAllowed;
                    }
                    else if (fileUploaded.ContentLength > setting.MaxSizePerFile)
                    {
                        denied = OpResult.FileSizeLimitExceed;
                    }
                    else if (fileService.Count() >= setting.MaxFiles)
                    {
                        denied = OpResult.FileLimitExceed;
                    }
                    else if ((fileService.GetSize() + fileUploaded.ContentLength) >= setting.Quota)
                    {
                        denied = OpResult.QuotaExceed;
                    }
                    else
                    {
                        denied = fileService.Save(path, fileUploaded, false);
                        if (denied == OpResult.Succeed)
                        {
                            return base.View(UserFileInfo.FromPostedFile(fileUploaded).ToJson());
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                denied = this.OnError(exception);
            }
            return base.View(new { succeed = denied == OpResult.Succeed, status = denied.ToString() });
        }

        [Authorize]
        public ActionResult FolderCreate(string path)
        {
            OpResult denied = OpResult.Denied;
            try
            {
                if (this.OnFolderCreating(path, ref denied))
                {
                    IUserSetting setting = this.SettingRepository.GetSetting(this.Username);
                    IFolderService folderService = this.ServiceProvider.GetFolderService(this.Username);
                    if (folderService.Count() >= setting.MaxFolders)
                    {
                        denied = OpResult.FolderLimitExceed;
                    }
                    else
                    {
                        denied = folderService.Create(path);
                    }
                }
            }
            catch (Exception exception)
            {
                denied = this.OnError(exception);
            }
            return this.OperationResult(denied);
        }

        [Authorize]
        public ActionResult FolderDelete(string path)
        {
            OpResult denied = OpResult.Denied;
            try
            {
                if (this.OnFolderDeleting(path, ref denied))
                {
                    denied = this.ServiceProvider.GetFolderService(this.Username).Delete(path);
                }
            }
            catch (Exception exception)
            {
                denied = this.OnError(exception);
            }
            return this.OperationResult(denied);
        }

        [Authorize]
        public virtual ActionResult FolderList()
        {
            OpResult denied = OpResult.Denied;
            try
            {
                if (this.OnFolderListing(ref denied))
                {
                    IEnumerable<UserFolder> allFolders = this.ServiceProvider.GetFolderService(this.Username).GetAllFolders();
                    return base.Json(from f in allFolders select f.ToJson());
                }
                return this.OperationResult(OpResult.Denied);
            }
            catch (Exception exception)
            {
                return this.OperationResult(this.OnError(exception));
            }
        }

        [Authorize]
        public ActionResult FolderMove(string srcPath, string destPath)
        {
            OpResult denied = OpResult.Denied;
            try
            {
                if (this.OnFolderMoving(srcPath, destPath, ref denied))
                {
                    denied = this.ServiceProvider.GetFolderService(this.Username).Move(srcPath, destPath);
                }
            }
            catch (Exception exception)
            {
                denied = this.OnError(exception);
            }
            return this.OperationResult(denied);
        }

        public virtual string MapPath(string path)
        {
            return base.get_Server().MapPath(path);
        }

        protected virtual ActionResult NotFound()
        {
            return new NotFoundResult();
        }

        protected virtual OpResult OnError(Exception exception)
        {
            return OpResult.Error;
        }

        protected virtual bool OnFileDeleting(string path, ref OpResult result)
        {
            return true;
        }

        protected virtual bool OnFileDownloading(string username, string path)
        {
            return true;
        }

        protected virtual bool OnFileListing(string path, ref OpResult result)
        {
            return true;
        }

        protected virtual bool OnFileMoving(string srcPath, string destPath, ref OpResult result)
        {
            return true;
        }

        protected virtual bool OnFileUploading(string path, HttpPostedFileBase fileUploaded, ref OpResult result)
        {
            return true;
        }

        protected virtual bool OnFolderCreating(string path, ref OpResult result)
        {
            return true;
        }

        protected virtual bool OnFolderDeleting(string path, ref OpResult result)
        {
            return true;
        }

        protected virtual bool OnFolderListing(ref OpResult result)
        {
            return true;
        }

        protected virtual bool OnFolderMoving(string srcPath, string destPath, ref OpResult result)
        {
            return true;
        }

        protected virtual ActionResult OperationResult(OpResult result)
        {
            return base.Json(new { succeed = result == OpResult.Succeed, status = result.ToString() });
        }

        public virtual IFileServiceProvider ServiceProvider
        {
            get
            {
                if (this.serviceProvider == null)
                {
                    this.serviceProvider = new FileServiceProvider(this.MapPath("~/UserFiles"));
                }
                return this.serviceProvider;
            }
        }

        public virtual IUserSettingRepository SettingRepository
        {
            get
            {
                if (this.settingRepository == null)
                {
                    this.settingRepository = new ConfigUserSettingRepository();
                }
                return this.settingRepository;
            }
        }

        public virtual string Username
        {
            get
            {
                return base.get_User().Identity.Name;
            }
        }
    }
}

