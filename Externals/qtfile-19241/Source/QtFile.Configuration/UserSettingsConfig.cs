namespace QtFile.Configuration
{
    using System;
    using System.Configuration;
    using System.Reflection;

    /// <summary>
    /// A collection of UserSettingConfig instances.
    /// </summary>
    [ConfigurationCollection(typeof(UserSettingConfig), CollectionType=ConfigurationElementCollectionType.AddRemoveClearMap)]
    public class UserSettingsConfig : ConfigurationElementCollection
    {
        /// <summary>
        /// The XML name of the individual <see cref="T:QtFile.Configuration.UserSettingConfig" /> instances in this collection.
        /// </summary>
        internal const string UserSettingConfigPropertyName = "userSetting";

        /// <summary>
        /// Adds the specified <see cref="T:QtFile.Configuration.UserSettingConfig" />.
        /// </summary>
        /// <param name="userSetting">The <see cref="T:QtFile.Configuration.UserSettingConfig" /> to add.</param>
        public void Add(UserSettingConfig userSetting)
        {
            base.BaseAdd(userSetting);
        }

        /// <summary>
        /// When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement" />.
        /// </summary>
        /// <returns>
        /// A new <see cref="T:System.Configuration.ConfigurationElement" />.
        /// </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new UserSettingConfig();
        }

        /// <summary>
        /// Gets the element key for a specified configuration element when overridden in a derived class.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement" /> to return the key for.</param>
        /// <returns>
        /// An <see cref="T:System.Object" /> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement" />.
        /// </returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UserSettingConfig) element).Username;
        }

        /// <summary>
        /// Indicates whether the specified <see cref="T:System.Configuration.ConfigurationElement" /> exists in the <see cref="T:System.Configuration.ConfigurationElementCollection" />.
        /// </summary>
        /// <param name="elementName">The name of the element to verify.</param>
        /// <returns>
        /// <see langword="true" /> if the element exists in the collection; otherwise, <see langword="false" />. The default is <see langword="false" />.
        /// </returns>
        protected override bool IsElementName(string elementName)
        {
            return (elementName == "userSetting");
        }

        /// <summary>
        /// Removes the specified <see cref="T:QtFile.Configuration.UserSettingConfig" />.
        /// </summary>
        /// <param name="userSetting">The <see cref="T:QtFile.Configuration.UserSettingConfig" /> to remove.</param>
        public void Remove(UserSettingConfig userSetting)
        {
            base.BaseRemove(userSetting);
        }

        /// <summary>
        /// Gets the type of the <see cref="T:System.Configuration.ConfigurationElementCollection" />.
        /// </summary>
        /// <returns>The <see cref="T:System.Configuration.ConfigurationElementCollectionType" /> of this collection.</returns>
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        /// <summary>
        /// Gets the <see cref="T:QtFile.Configuration.UserSettingConfig" /> at the specified index.
        /// </summary>
        /// <param name="index">The index of the <see cref="T:QtFile.Configuration.UserSettingConfig" /> to retrieve</param>
        public UserSettingConfig this[int index]
        {
            get
            {
                return (UserSettingConfig) base.BaseGet(index);
            }
        }
    }
}

