namespace QtFile.Configuration
{
    using System;
    using System.Configuration;

    /// <summary>
    /// The ExtensionConfig Configuration Element.
    /// </summary>
    public class ExtensionConfig : ConfigurationElement
    {
        /// <summary>
        /// The XML name of the <see cref="P:QtFile.Configuration.ExtensionConfig.Extension" /> property.
        /// </summary>
        internal const string ExtensionPropertyName = "extension";

        /// <summary>
        /// Gets or sets the Extension.
        /// </summary>
        [ConfigurationProperty("extension", IsRequired=true, IsKey=true, IsDefaultCollection=false)]
        public string Extension
        {
            get
            {
                return (string) base["extension"];
            }
            set
            {
                base["extension"] = value;
            }
        }
    }
}

