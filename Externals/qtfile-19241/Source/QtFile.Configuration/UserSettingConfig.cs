namespace QtFile.Configuration
{
    using System;
    using System.Configuration;

    /// <summary>
    /// The UserSettingConfig Configuration Element.
    /// </summary>
    public class UserSettingConfig : ConfigurationElement
    {
        /// <summary>
        /// The XML name of the <see cref="P:QtFile.Configuration.UserSettingConfig.AllowedExtensions" /> property.
        /// </summary>
        internal const string AllowedExtensionsPropertyName = "allowedExtensions";
        /// <summary>
        /// The XML name of the <see cref="P:QtFile.Configuration.UserSettingConfig.MaxFiles" /> property.
        /// </summary>
        internal const string MaxFilesPropertyName = "maxFiles";
        /// <summary>
        /// The XML name of the <see cref="P:QtFile.Configuration.UserSettingConfig.MaxFolders" /> property.
        /// </summary>
        internal const string MaxFoldersPropertyName = "maxFolders";
        /// <summary>
        /// The XML name of the <see cref="P:QtFile.Configuration.UserSettingConfig.MaxSizePerFile" /> property.
        /// </summary>
        internal const string MaxSizePerFilePropertyName = "maxSizePerFile";
        /// <summary>
        /// The XML name of the <see cref="P:QtFile.Configuration.UserSettingConfig.Quota" /> property.
        /// </summary>
        internal const string QuotaPropertyName = "quota";
        /// <summary>
        /// The XML name of the <see cref="P:QtFile.Configuration.UserSettingConfig.Username" /> property.
        /// </summary>
        internal const string UsernamePropertyName = "username";

        /// <summary>
        /// Gets or sets the AllowedExtensions.
        /// </summary>
        [ConfigurationProperty("allowedExtensions", IsRequired=false, IsKey=false, IsDefaultCollection=false)]
        public ExtensionsConfig AllowedExtensions
        {
            get
            {
                return (ExtensionsConfig) base["allowedExtensions"];
            }
            set
            {
                base["allowedExtensions"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the MaxFiles.
        /// </summary>
        [ConfigurationProperty("maxFiles", IsRequired=false, IsKey=false, IsDefaultCollection=false)]
        public int MaxFiles
        {
            get
            {
                return (int) base["maxFiles"];
            }
            set
            {
                base["maxFiles"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the MaxFolders.
        /// </summary>
        [ConfigurationProperty("maxFolders", IsRequired=false, IsKey=false, IsDefaultCollection=false)]
        public int MaxFolders
        {
            get
            {
                return (int) base["maxFolders"];
            }
            set
            {
                base["maxFolders"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the MaxSizePerFile.
        /// </summary>
        [ConfigurationProperty("maxSizePerFile", IsRequired=false, IsKey=false, IsDefaultCollection=false)]
        public long MaxSizePerFile
        {
            get
            {
                return (long) base["maxSizePerFile"];
            }
            set
            {
                base["maxSizePerFile"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the Quota.
        /// </summary>
        [ConfigurationProperty("quota", IsRequired=false, IsKey=false, IsDefaultCollection=false)]
        public long Quota
        {
            get
            {
                return (long) base["quota"];
            }
            set
            {
                base["quota"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the Username.
        /// </summary>
        [ConfigurationProperty("username", IsRequired=true, IsKey=true, IsDefaultCollection=false)]
        public string Username
        {
            get
            {
                return (string) base["username"];
            }
            set
            {
                base["username"] = value;
            }
        }
    }
}

