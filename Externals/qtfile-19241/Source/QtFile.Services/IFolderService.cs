namespace QtFile.Services
{
    using QtFile;
    using System;
    using System.Collections.Generic;

    public interface IFolderService
    {
        int Count();
        /// <summary>
        /// Creates a new folder.
        /// </summary>
        /// <param name="path">
        /// The related path of the new folder.
        /// </param>
        /// <returns>
        /// One of following results:
        /// <list type="bullet">
        /// <item><see cref="F:QtFile.OpResult.Succeed" /> - folder created;</item>
        /// <item><see cref="F:QtFile.OpResult.AlreadyExist" /> - The folder with specified path already exists.</item>
        /// </list>
        /// </returns>
        OpResult Create(string path);
        /// <summary>
        /// Deletes a folder.
        /// </summary>
        /// <param name="path">
        /// The related path of the folder.
        /// </param>
        /// <returns>
        /// One of following results:
        /// <list type="bullet">
        /// <item><see cref="F:QtFile.OpResult.Succeed" /> - folder deleted;</item>
        /// <item><see cref="F:QtFile.OpResult.NotFound" /> - the folder not found.</item>
        /// </list>
        /// </returns>
        OpResult Delete(string path);
        IEnumerable<UserFolder> GetAllFolders();
        /// <summary>
        /// Moves a folder to new location.
        /// </summary>
        /// <param name="srcPath">
        /// The related path of the source folder.
        /// </param>
        /// <param name="destPath">
        /// The related path of the destination folder.
        /// </param>
        /// <returns>
        /// One of following results:
        /// <list type="bullet">
        /// <item><see cref="F:QtFile.OpResult.Succeed" /> - folder moved;</item>
        /// <item><see cref="F:QtFile.OpResult.NotFound" /> - the source folder not found;</item>
        /// <item><see cref="F:QtFile.OpResult.FolderNotFound" /> - parent of the destination folder not found;</item>
        /// <item><see cref="F:QtFile.OpResult.AlreadyExist" /> - a namesake folder alreay exists.</item>
        /// </list>
        /// </returns>
        OpResult Move(string srcPath, string destPath);
    }
}

