namespace QtFile
{
    using System;
    using System.Web.Mvc;

    public class NotFoundResult : ActionResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            context.get_HttpContext().Response.StatusCode = 0x194;
        }
    }
}

