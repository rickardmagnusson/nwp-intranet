namespace QtFile
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Web;

    /// <summary>
    /// Represents the information of a user file.
    /// </summary>
    public class UserFileInfo
    {
        public static UserFileInfo FromPostedFile(HttpPostedFileBase file)
        {
            Helper.ValidateNotNull(file, "file");
            return new UserFileInfo { Name = Path.GetFileName(file.FileName), Size = file.ContentLength, ContentType = file.ContentType, Modified = DateTime.Now };
        }

        public virtual object ToJson()
        {
            TimeSpan span = (TimeSpan) (this.Modified - new DateTime(0x7b2, 1, 1));
            return new { name = this.Name, size = this.Size, contentType = this.ContentType, modified = span.TotalMilliseconds };
        }

        public string ContentType { get; set; }

        public DateTime Modified { get; set; }

        /// <summary>
        /// Gets or sets the name of file.
        /// </summary>
        /// <value>
        /// The name of file.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the size of file.
        /// </summary>
        /// <value>
        /// The size of file.
        /// </value>
        public long Size { get; set; }
    }
}

