namespace QtFile
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Value object contains the initial data of a file manager.
    /// </summary>
    public class FileMgrViewData
    {
        public string CurrentFolder { get; set; }

        public IEnumerable<UserFileInfo> Files { get; set; }

        public IEnumerable<UserFolder> Folders { get; set; }
    }
}

