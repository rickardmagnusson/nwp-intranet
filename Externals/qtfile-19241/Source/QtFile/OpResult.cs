namespace QtFile
{
    using System;

    public enum OpResult
    {
        Succeed,
        Error,
        NotFound,
        AlreadyExist,
        FolderNotFound,
        FolderLimitExceed,
        FileLimitExceed,
        FileSizeLimitExceed,
        FileTypeNotAllowed,
        QuotaExceed,
        Denied
    }
}

