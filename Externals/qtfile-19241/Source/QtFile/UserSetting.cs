namespace QtFile
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class UserSetting : IUserSetting
    {
        public bool IsExtAllowed(string extension)
        {
            return this.AllowedExtensions.Contains<string>(extension);
        }

        public IEnumerable<string> AllowedExtensions { get; set; }

        public int MaxFiles { get; set; }

        public int MaxFolders { get; set; }

        public long MaxSizePerFile { get; set; }

        public long Quota { get; set; }
    }
}

