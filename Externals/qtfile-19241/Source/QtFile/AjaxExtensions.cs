namespace QtFile
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Web.Mvc;

    public static class AjaxExtensions
    {
        public static string Json(this AjaxHelper ajax, object jsonObj)
        {
            return Helper.ToJsonString(jsonObj);
        }
    }
}

