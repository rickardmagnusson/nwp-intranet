namespace QtFile
{
    using System;

    /// <summary>
    /// Contract of user setting.
    /// </summary>
    public interface IUserSetting
    {
        /// <summary>
        /// Whether the extension is allowed for the user.
        /// </summary>
        /// <param name="extension">
        /// The extension of the file.
        /// </param>
        /// <returns></returns>
        bool IsExtAllowed(string extension);

        /// <summary>
        /// Gets the max number of files allowed for the user.
        /// </summary>
        /// <value>
        /// The max number of files allowed for the user.
        /// </value>
        int MaxFiles { get; }

        /// <summary>
        /// Gets the max number of folders allowed for the user.
        /// </summary>
        /// <value>
        /// The max number of folders allowed for the user.
        /// </value>
        int MaxFolders { get; }

        /// <summary>
        /// Gets the max allowed size for a single file in bytes of the user.
        /// </summary>
        /// <value>
        /// The max allowed size for a single file in bytes of the user.
        /// </value>
        long MaxSizePerFile { get; }

        /// <summary>
        /// Gets the disk space quota of the user.
        /// </summary>
        /// <value>
        /// The disk space quota of the user.
        /// </value>
        long Quota { get; }
    }
}

