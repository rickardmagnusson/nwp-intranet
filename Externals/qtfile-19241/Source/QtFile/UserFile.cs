namespace QtFile
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Represents a user file.
    /// </summary>
    /// <remarks>
    /// One and only one of <see cref="P:QtFile.UserFile.Stream" />, <see cref="P:QtFile.UserFile.Path" /> 
    /// or <see cref="P:QtFile.UserFile.Data" /> should be populated.
    /// </remarks>
    public class UserFile : UserFileInfo
    {
        /// <summary>
        /// Gets or sets the binary data of the file.
        /// </summary>
        /// <value>
        /// The binary data of the file.
        /// </value>
        public byte[] Data { get; set; }

        /// <summary>
        /// Gets or sets the path of the file.
        /// </summary>
        /// <value>
        /// The path of the file.
        /// </value>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the stream of the file.
        /// </summary>
        /// <value>
        /// The stream of the file.
        /// </value>
        public System.IO.Stream Stream { get; set; }
    }
}

