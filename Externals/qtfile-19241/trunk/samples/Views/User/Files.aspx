<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="Microsoft.Web.Mvc" %>
<asp:Content ID="FilesTitleContent" ContentPlaceHolderID="TitleContent" runat="server">
	Files
</asp:Content>
<asp:Content ID="FilesStylesContent" ContentPlaceHolderID="StylesContent" runat="server">

	<!-- Add your custom jquery-ui css here -->
	<link href="/Content/jquery-ui/<%= Url.Encode((ViewData["theme"] ?? "start").ToString()) %>/jquery-ui-1.7.custom.css" rel="stylesheet" type="text/css" />
	<!-- Default theme, based on the above juqery-ui styles -->
	<link href="/Content/qtfile/default/default.css" rel="stylesheet" type="text/css" />
	
</asp:Content>


<asp:Content ID="FilesScriptsContent" ContentPlaceHolderID="ScriptsContent" runat="server">

	<!-- Reference to jquery -->
	<script type="text/javascript" src="/Scripts/jquery-1.3.2.js"></script>
	<!-- Reference to QtFile script -->
	<script type="text/javascript" src="/Scripts/jquery.qtfile.js"></script>

</asp:Content>

<asp:Content ID="FilesMainContent" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Manage My Files</h2>
    <p>Applied jQuery UI Theme: <span class="highlight"><%= Html.Encode(ViewData["theme"]) %></span></p>
    <p>
   Checkout how the default theme working with different jQuery UI Theme: 
   <%= Html.ActionLink("start", "Files", "User", new { theme = "start" }, null)%>,
   <%= Html.ActionLink("trontastic", "Files", "User", new { theme = "trontastic" }, null)%>,
   <%= Html.ActionLink("black-tie", "Files", "User", new { theme = "black-tie" }, null)%>,
   <%= Html.ActionLink("cupertino", "Files", "User", new { theme = "cupertino" }, null)%>
   or <a href="http://jqueryui.com/themeroller/">your own</a>.
    </p>

	<!-- Render QtFile file manager -->
    <% Html.RenderAction<QtFile.Controllers.UserFileController>(c => c.FileManager(null)); %>
    
</asp:Content>

