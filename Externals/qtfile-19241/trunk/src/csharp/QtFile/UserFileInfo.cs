﻿/*
 * QtPet Online File Manager v1.0
 * Copyright (c) 2009, Zhifeng Lin (fszlin[at]gmail.com)
 * 
 * Licensed under the MS-PL license.
 * http://qtfile.codeplex.com/license
 */

using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;

namespace QtFile
{
	/// <summary>
	/// Represents the information of a user file.
	/// </summary>
	public class UserFileInfo
	{
		/// <summary>
		/// Gets or sets the name of file.
		/// </summary>
		/// <value>
		/// The name of file.
		/// </value>
		public string Name 
		{
			get; 
			set;
		}

		/// <summary>
		/// Gets or sets the size of file.
		/// </summary>
		/// <value>
		/// The size of file.
		/// </value>
		public long Size
		{ 
			get; 
			set;
		}

		public string ContentType
		{
			get;
			set;
		}

		public DateTime Modified
		{
			get;
			set;
		}

		public virtual object ToJson()
		{
			return new { name = Name, size = Size, contentType = ContentType, modified = (Modified - new DateTime(1970, 1, 1)).TotalMilliseconds };
		}

		public static UserFileInfo FromPostedFile(HttpPostedFileBase file)
		{
			Helper.ValidateNotNull(file, "file");

			return new UserFileInfo()
			{
				Name = Path.GetFileName(file.FileName),
				Size = file.ContentLength,
				ContentType = file.ContentType,
				Modified = DateTime.Now
			};
		}
	}
}
