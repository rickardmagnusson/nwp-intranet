﻿/*
 * QtPet Online File Manager v1.0
 * Copyright (c) 2009, Zhifeng Lin (fszlin[at]gmail.com)
 * 
 * Licensed under the MS-PL license.
 * http://qtfile.codeplex.com/license
 */

using System;
using System.Linq;
using System.Collections.Generic;

namespace QtFile
{
	public class UserSetting : IUserSetting
	{
		public bool IsExtAllowed(string extension)
		{
			return AllowedExtensions.Contains(extension);
		}

		public IEnumerable<string> AllowedExtensions
		{
			get;
			set;
		}

		public long Quota
		{
			get;
			set;
		}

		public long MaxSizePerFile
		{
			get;
			set;
		}

		public int MaxFolders
		{
			get;
			set;
		}

		public int MaxFiles
		{
			get;
			set;
		}
	}
}
