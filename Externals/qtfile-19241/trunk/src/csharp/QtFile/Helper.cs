﻿/*
 * QtPet Online File Manager v1.0
 * Copyright (c) 2009, Zhifeng Lin (fszlin[at]gmail.com)
 * 
 * Licensed under the MS-PL license.
 * http://qtfile.codeplex.com/license
 */

using System;
using System.Web.Script.Serialization;

namespace QtFile
{
	/// <summary>
	/// Helper class.
	/// </summary>
	public static class Helper
	{
		/// <summary>
		/// Throws exception if the given string is null or empty.
		/// </summary>
		/// <param name="value">
		/// The value to check.
		/// </param>
		/// <param name="name">
		/// The argument name.
		/// </param>
		/// <exception cref="ArgumentNullException">
		/// If the given value is null.
		/// </exception>
		/// <exception cref="ArgumentException">
		/// If the given value is empty.
		/// </exception>
		public static void ValidateNotNullOrEmpty(string value, string name)
		{
			if (value == null)
				throw new ArgumentNullException(name);
			if (value.Length == 0)
				throw new ArgumentException(name + " cannot be empty.", name);
		}

		/// <summary>
		/// Throws exception if the given value is null.
		/// </summary>
		/// <param name="value">
		/// The value to check.
		/// </param>
		/// <param name="name">
		/// The argument name.
		/// </param>
		/// <exception cref="ArgumentNullException">
		/// If the given value is null.
		/// </exception>
		public static void ValidateNotNull(object value, string name)
		{
			if (value == null)
				throw new ArgumentNullException(name);
		}

		public static string ToJsonString(object jsonObj)
		{
			return new JavaScriptSerializer().Serialize(jsonObj);
		}
	}
}
