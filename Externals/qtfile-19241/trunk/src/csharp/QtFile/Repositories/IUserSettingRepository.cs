﻿/*
 * QtPet Online File Manager v1.0
 * Copyright (c) 2009, Zhifeng Lin (fszlin[at]gmail.com)
 * 
 * Licensed under the MS-PL license.
 * http://qtfile.codeplex.com/license
 */

using System;

namespace QtFile.Repositories
{
	public interface IUserSettingRepository
	{
		IUserSetting GetSetting(string username);
	}
}
