﻿using System;
using System.Web.Mvc;

namespace QtFile
{
	public static class AjaxExtensions
	{
		public static string Json(this AjaxHelper ajax, object jsonObj)
		{
			return Helper.ToJsonString(jsonObj);
		}
	}

    public static class AjaxExt
    {
        public static string Json(this AjaxHelper ajax, object jsonObj)
        {
            return Helper.ToJsonString(jsonObj);
        }
    }
}
