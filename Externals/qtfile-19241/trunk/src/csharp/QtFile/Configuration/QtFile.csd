﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel dslVersion="1.0.0.0" Id="f83fbd8c-eb42-45a5-ab26-464769216b75" namespace="QtFile.Configuration" xmlSchemaNamespace="http://qtfile.codeplex.com/" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <configurationElements>
    <configurationSection name="QtFileConfig" codeGenOptions="Singleton" xmlSectionName="qtfile">
      <attributeProperties>
        <attributeProperty name="DefaultQuota" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="defaultQuota" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/Int64" />
          </type>
        </attributeProperty>
        <attributeProperty name="DefaultMaxSizePerFile" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="defaultMaxSizePerFile" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/Int64" />
          </type>
        </attributeProperty>
        <attributeProperty name="DefaultMaxFolders" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="defaultMaxFolders" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="DefaultMaxFiles" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="defaultMaxFiles" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/Int32" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <elementProperties>
        <elementProperty name="DefautAllowedExtensions" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="defautAllowedExtensions" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/ExtensionsConfig" />
          </type>
        </elementProperty>
        <elementProperty name="UserSettings" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="userSettings" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/UserSettingsConfig" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElement name="UserSettingConfig">
      <attributeProperties>
        <attributeProperty name="Username" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="username" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Quota" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="quota" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/Int64" />
          </type>
        </attributeProperty>
        <attributeProperty name="MaxSizePerFile" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="maxSizePerFile" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/Int64" />
          </type>
        </attributeProperty>
        <attributeProperty name="MaxFolders" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="maxFolders" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="MaxFiles" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="maxFiles" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/Int32" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <elementProperties>
        <elementProperty name="AllowedExtensions" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="allowedExtensions" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/ExtensionsConfig" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationElement>
    <configurationElementCollection name="UserSettingsConfig" collectionType="AddRemoveClearMap" xmlItemName="userSetting" codeGenOptions="Indexer, AddMethod, RemoveMethod">
      <itemType>
        <configurationElementMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/UserSettingConfig" />
      </itemType>
    </configurationElementCollection>
    <configurationElementCollection name="ExtensionsConfig" collectionType="AddRemoveClearMap" xmlItemName="extension" codeGenOptions="Indexer, AddMethod, RemoveMethod">
      <itemType>
        <configurationElementMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/ExtensionConfig" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="ExtensionConfig">
      <attributeProperties>
        <attributeProperty name="Extension" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="extension" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/f83fbd8c-eb42-45a5-ab26-464769216b75/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
  </configurationElements>
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
  </typeDefinitions>
</configurationSectionModel>