
jQuery.extend(fn, {
	// Builds buttons and binds event handlers to them
	initButtons: function() {
		var qtfile = this, options = qtfile.options;
		
		qtfile.find(options.buttonCreateFolder).click(function() {
			// Create new folder under current folder
			qtfile.createFolder(qtfile.currentFolder);
		});
		qtfile.find(options.buttonRenameFolder).click(function() {
			// Rename current folder
			qtfile.renameFolder(qtfile.currentFolder);
		});
		qtfile.find(options.buttonDeleteFolder).click(function() {
			// Delete current folder
			qtfile.deleteFolder(qtfile.currentFolder);
		});
		qtfile.find(options.buttonMoveFolder).click(function() {
			// Not root folder
			if (qtfile.currentFolder.data(DataKeyFolderPath).length) {
				// Start to move current folder
				qtfile.moveFolderBegin(qtfile.currentFolder);
			}
		});
		qtfile.find(options.buttonRefreshFolders).click(function() {
			qtfile.reloadFolders()
		});
		qtfile.find(options.buttonRefreshFiles).click(function() {
			// Enfoce reload
			qtfile.changeFolder(qtfile.currentFolder, true);
		});
				
		qtfile.initUploadInput();

		// Update button status according to current folder
		qtfile.updateButtons();
	}, // End of initButtons
	// Updates the buttons for folder operations
	updateButtons: function() {
		var qtfile = this, options = qtfile.options,
			disabledClass = options.classes.buttonDisabled;
		// Root folder
		if (qtfile.currentFolder.data(DataKeyFolderPath).length) {
			qtfile.find(options.buttonRenameFolder).removeClass(disabledClass);
			qtfile.find(options.buttonDeleteFolder).removeClass(disabledClass);
			qtfile.find(options.buttonMoveFolder).removeClass(disabledClass);
		} else {
			qtfile.find(options.buttonRenameFolder).addClass(disabledClass);
			qtfile.find(options.buttonDeleteFolder).addClass(disabledClass);
			qtfile.find(options.buttonMoveFolder).addClass(disabledClass);
		}
	}, // End of updateButtons
	// Initializes the button for upload file
	// A container wraps the upload button,
	// and hidden file input used to capture user input.
	initUploadInput: function() {
		var qtfile = this, options = qtfile.options,
			$uploadButton = qtfile.find(options.buttonUploadFile),
			createWraper = $uploadButton.is('input'),

			$uploadWraper = qtfile.uploadWraper =
				createWraper ? jQuery(HtmlTagSpan) : $uploadButton
				.mousemove(function(e) {

					// Make sure pointer is inside button area
					var buttonOffset = calcOffset($uploadButton);
					if (e.pageX >= buttonOffset.left &&
						e.pageX < buttonOffset.left + $uploadButton.outerWidth() &&
						e.pageY >= buttonOffset.top &&
						e.pageY < buttonOffset.top + $uploadButton.outerHeight()) {

						$uploadInput.show();

						// Move the input with the mouse, so the user can't misclick it	
						var offset = calcOffset(jQuery(this));
						$uploadInput.css({
							top: e.pageY - offset.top - 5 + 'px',
							left: e.pageX - offset.left - 170 + 'px'
						});
					} else {
						$uploadInput.hide();
					}
				});

		if (createWraper) {
			$uploadWraper
				// Replace the original button
				.insertAfter($uploadButton)
				.append($uploadButton);

			// Delay to let firefox to calculate the size
			setTimeout(function() {

				$uploadWraper
					.attr('class', $uploadButton.attr('class'))
					.css({
						position: 'relative',
						overflow: 'hidden',
						padding: 0,

						// Copy the margin and display to wraper
						display: $uploadButton.css('display'),
						'margin-top': $uploadButton.css('margin-top'),
						'margin-right': $uploadButton.css('margin-right'),
						'margin-bottom': $uploadButton.css('margin-bottom'),
						'margin-left': $uploadButton.css('margin-left')
					});

				$uploadButton
					// Margin has been moved to wrapper
					.css({ 'margin': 0 });
			}, 1);
		} else {
			$uploadButton
				.css({ position: 'relative' });
		}

		// The actual file input
		$uploadInput = jQuery('<input type="file" />')
			.attr('name', options.file.upload.fileName)
			.attr('class', $uploadButton.attr('class'))
			// Set a fixed size, so that we can make sure 
			// the user will hit the upload button by
			// tracing mouse position
			.css({
				position: 'absolute',
				margin: 0,
				padding: 0,
				opacity: 0,
				top: 0,
				left: 0,
				width: '220px',
				height: '10px'
			})
			.appendTo($uploadWraper)
			.hide()
			// Bind upload handler
			.change(function() {
				qtfile.uploadFile(jQuery(this));
			});
	} // End of buildUploadInput
});
