
var
	qtfile = function(selector, options) {
		this.init(selector, options);
	},
	fn = qtfile.prototype = { };

jQuery.extend(qtfile, {
	setOptions: function(options) {
		jQuery.extend(qtfile.prototype, options);
	}
});

// Builds file browser with provided elements.
// options - Options for file browser
jQuery.fn.qtfile = function(options) {
	return this.each(function() {
		new qtfile(this, options);
	});
};

jQuery.qtfile = function(selector, options) {
	jQuery(selector).qtfile(options);
};
