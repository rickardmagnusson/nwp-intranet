
var 
	window = this,
	undefined,
	jQuery = window.jQuery,
	alert = window.alert,
	prompt = window.prompt,
	confirm = window.confirm,
	Ul = 'ul',
	Li = 'li',
	Span = 'span',
	AttrTitle = 'title',
	HtmlTagUl = '<ul></ul>',
	HtmlTagLi = '<li></li>',
	HtmlTagSpan = '<span></span>',
	DataKeyFileName = 'name',
	DataKeyFolderPath = 'path',
	// Generates a random integer between 0 to (max - 1)
	rand = function(max) {
		return Math.floor(Math.random() * max);
	}, // End of rand()
	// Generates a random string with specified length
	genRandomId = function(len) {
		var id = '_';
		for (var i = 1; i < len; ++i) {
			var num = rand(26 + 26 + 10);
			if (num < 26) 
				id += 'a' + num;
			else if (num < 26 + 26) 
				id += 'A' + num - 26;
			else 
				id += '0' + num - 26 - 26;
		}
		return id;
	}, // End of genRandomId
	// Embeds parameters into the given url
	// NOTE: The used parameters will be removed
	formatUrl = function(url, params) {
		for (var k in params) {
			var argName = '{' + k + '}';
			if (url.indexOf(argName) >= 0) {
				url = url.replace(argName, escape(params[k]));
				delete params[k];	
			}
		}
		return url;
	}, // End of formatUrl
	// Converts a file size in bytes to a string
	fileSizeToString = function(size) {
		var names = ['KB', 'MB', 'GB', 'TB'], ind;
		size /= 1024;
		for (ind = 0; size > 1024 && ind < names.length; ++ind)
			size /= 1024;
		return (Math.floor(size * 100) / 100.0) + ' ' + names[ind];
	}, // End of fileSizeToString()
	// Decodes html entities from a string
	decodeHtml = function(html) {
		return html
			.replace(/&amp;/g, '&')
			.replace(/&lt;/g, '<')
			.replace(/&gt;/g, '>');
	}, // End of decodeHtml
	parseJSON = function(data) {
		return window['eval']("(" + data + ")");
	},
	getUrlRoot = function() {
		var urlSegments =
			/^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/
			.exec(window.location);
		return urlSegments[1] + '://' + urlSegments[2];
	},
	// jQuery.offset cannot proper calculate offset in Chrome 2?
	calcOffset = function($elem) {
		var donElem = $elem.get(0), curleft = 0, curtop = 0;
		if (donElem && donElem.offsetParent) {
			
			do {
				curleft += donElem.offsetLeft;
				curtop += donElem.offsetTop;
			} while (donElem = donElem.offsetParent);
		}
		return { left: curleft, top: curtop };
	},
	getExt = function(filename) {
		var dot = filename.lastIndexOf('.');
		return dot < 0 ? '' : filename.substr(dot + 1);
	};
	getNameOnly = function(filename) {
		var dot = filename.lastIndexOf('.');
		return dot < 1 ? filename : filename.substr(0, dot);
	};
