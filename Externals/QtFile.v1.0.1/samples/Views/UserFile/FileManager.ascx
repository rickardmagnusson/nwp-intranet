<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<QtFile.FileMgrViewData>" %>
<%@ Import Namespace="QtFile" %>
<div id="qtfile" class="ui-helper-reset ui-widget qtfile" title="<%= Html.AttributeEncode(Page.User.Identity.Name) %>">
	<h3 class="ui-helper-reset ui-widget-header ui-corner-top qtfile-header">
		<%= Html.Encode(Page.User.Identity.Name) %>'s files.
	</h3>
	<div class="ui-widget-content">
		<div class="menu-bar">
			<span id="buttonCreateFolder" class="button folder-create" title="Create new folder under current folder">
				<span class="icon"></span>New</span> <span id="buttonRenameFolder" class="button folder-rename"
					title="Rename current folder"><span class="icon"></span>Rename</span> <span id="buttonMoveFolder"
						class="button folder-move" title="Move current folder"><span class="icon"></span>
						Move</span> <span id="buttonDeleteFolder" class="button folder-delete" title="Delete current folder">
							<span class="icon"></span>Delete</span> <span id="buttonUploadFile" class="button file-upload"
								title="Upload file to current folder"><span class="icon"></span>Upload</span>
			<span id="buttonRefreshFileList" class="button file-refresh" title="Refresh files of current folder">
				<span class="icon"></span></span>
		</div>
		<div class="ui-helper-clearfix qtfile-content">
			<div class="folder-wraper">
				<div class="ui-state-default folder-header">
					<span>Folders</span><span id="buttonRefreshFolderList" class="button folder-refresh"
						title="Refresh files of current folder"><span class="icon"></span></span></div>
				<div id="folderListPanel" class="folder-panel">
					<% if (Model.Folders.Count() > 0)
		{ %>
					<ul>
						<% foreach (var folder in Model.Folders)
		 {
			 if (folder.Path == Model.CurrentFolder)
			 { %>
						<li class="current-folder">
							<%= Html.Encode(Ajax.Json(folder.ToJson()))%></li>
						<% }
			 else
			 { %>
						<li>
							<%= Html.Encode(Ajax.Json(folder.ToJson()))%></li>
						<% }
		 } %></ul>
					<% } %>
				</div>
			</div>
			<div class="file-wraper">
				<div class="ui-state-default ui-helper-clearfix file-header">
					<span class="ui-state-default file-name-header">Name</span><span class="ui-state-default file-size-header">Size</span>
					<span class="ui-state-default file-actions-header">Actions</span>
				</div>
				<div id="fileListPanel" class="ui-helper-clearfix ui-widget-content file-panel">
					<% if (Model.Files.Count() > 0)
		{ %>
					<ul class="file-list">
						<% foreach (var file in Model.Files)
		 {%>
						<li>
							<%= Html.Encode(Ajax.Json(file.ToJson()))%></li>
						<%  } %></ul>
					<% } %>
				</div>
			</div>
			<div class="ui-dialog ui-widget-content ui-corner-top file-preview">
				<div class="ui-dialog-titlebar ui-state-default ui-corner-all ui-helper-clearfix">
					<span class="ui-dialog-title">File details</span> <span class="ui-dialog-titlebar-close ui-corner-all file-preview-close">
						<span class="ui-icon ui-icon-closethick">close</span> </span>
				</div>
				<div class="ui-dialog-content ui-widget-content">
					<p><span class="file-preview-field-name">Name</span> <span class="file-preview-name"></span></p>
					<p><span class="file-preview-field-name">Size</span> <span class="file-preview-size"></span></p>
					<p><span class="file-preview-field-name">Content type</span> <span class="file-preview-content-type"></span></p>
					<p><span class="file-preview-field-name">Modified</span> <span class="file-preview-modified"></span></p>
					<p class="file-preview-image"><span class="file-preview-image-loading"></span></p>
				</div>
			</div>
		</div>
	</div>
	<div class="ui-helper-clearfix ui-state-default ui-corner-bottom qtfile-footer">
		<span id="statusMessage" class="status-message"></span><span class="credit">Powered
			by <a href="http://qtfile.codeplex.com">QtFile</a></span> <span class="credit">Icons
				by <a href="http://www.famfamfam.com">famfamfam</a></span>
	</div>
</div>

<script type="text/javascript">
	jQuery(function() {
		// Build file manager when DOM is ready
		jQuery('#qtfile').qtfile();
	});
</script>

