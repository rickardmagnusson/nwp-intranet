﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
	Home Page
</asp:Content>
<asp:Content ID="indexStylesContent" ContentPlaceHolderID="StylesContent" runat="server">
	<style type="text/css">
		#snapshots
		{
			float: right;
			width: 320px; 
			*width: 340px;
			margin-left: 20px;
		}
		img
		{
			border-style: none;
		}
		#snapshots img
		{
			width: 320px;
		}
		.steps
		{
			list-style-type: decimal;
		}
	</style>
</asp:Content>
<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
	<div id="snapshots">
		<a href="<%= Url.Action("Files", "User", new { theme = "start" }) %>">
			<img alt="QtFile Snapshot" src="/Content/images/snapshot1.png" /></a>
		<a href="<%= Url.Action("Files", "User", new { theme = "trontastic" }) %>">
				<img alt="QtFile Snapshot" src="/Content/images/snapshot2.png" /></a>
	</div>
	<h2>
		QtFile is a web based file browser, which intends to provide a friendly interface for your users to manage their own files and folders on your web site.
	</h2>
	<h3>
		It provides a powerful, reliable and easy web file management with lots of cool
		features:
	</h3>
	<ul>
		<li>Membership based, every user has their own files, folders and privileges; </li>
		<li>Easy theming, the default theme is based on jQuery UI Theme Framework, which can
			be simply customized by <a href="http://jqueryui.com/themeroller/">rolling
				your theme</a>; </li>
		<li>Ajax everything; </li>
		<li>Restrict upload files by size, extensions and your criterias;</li>
		<li>File details and image preview;</li>
		<li>And much more...</li>
	</ul>
	<h3><a href="<%= Url.Action("Files", "User") %>">Try it now.</a></h3>
	<h3>
		Requirements:</h3>
	<ul>
		<li><a href="http://go.microsoft.com/fwlink/?LinkId=144444">
			ASP.NET MVC 1.0</a></li>
		<li><a href="http://jquery.com/">jQuery 1.3+</a></li>
	</ul>
	<h3>Browser compliant:</h3>
	<ul>
	<li>JS works on IE 6.0+, FF 2+, Safari 3.0+, Opera 9.0+ and Chrome;</li>
	<li>The default theme has been tested on IE 7.0+, FF 3.0, Safari 4 and Chrome 2.</li>
	</ul>
	<h3>
		Installation:</h3>
	<ul class="steps">
		<li>Download the latest release of QtFile from <a href="http://qtfile.codeplex.com/Release/ProjectReleases.aspx">
			CodePlex</a>;</li>
		<li>Copy <em>dist/bin/QtFile.dll</em> and <em><a href="http://aspnet.codeplex.com/Release/ProjectReleases.aspx?ReleaseId=24471">
			ASP.NET MVC Futures</a></em> (for rendering file manager) to <em>bin/</em>
			directory of your MVC application;</li>
		<li>Copy all files under <em>dist/views/</em> to <em>Views</em>
			directory of your MVC application;</li>
		<li>Configure QtFile in <em>web.config</em>:
			<ul>
				<li>Define a configuration section for QtFile under <em>&lt;configSections/&gt;</em> element:<br />
					<img alt="Define a configuration section for QtFile" src="/Content/images/ins_config_section.png" />
				</li>
				<li>Add your settings:<br />
					<img alt="Default settings" src="/Content/images/ins_config_default.png" /></li>
				<li>For user specified settings, please refer to the <em>web.config</em> of <a href="http://qtfile.codeplex.com/Release/ProjectReleases.aspx">samples</a>.</li>
			</ul>
		</li>
		<li>Get your customized <a href="http://jqueryui.com/themeroller/">UI Theme</a> 
		and copy <em>dist/css/qtfile/</em> and <em>dist/js/jquery.qtfile.js</em> to your MVC application;</li>
		<li>In the page containing the file manager, add reference to the style sheets,<br />
			<img src="/Content/images/ins_css.png" alt="Refernce to CSS" /><br />
			and JS as well;<br />
			<img src="/Content/images/ins_js.png" alt="Refernce to JS" />
		</li>
		<li>Render the file manager,<br />
			<img src="/Content/images/ins_render.png" alt="Render file manager" /><br />
			and there you <a href="/User/Files">go</a>.<br />
		<a href="<%= Url.Action("Files", "User", new { theme = "black-tie" }) %>">
			<img src="/Content/images/ins_final.png" alt="File manager sample" /></a>
		
		</li>
	</ul>
	<p>
		P.S. Every thing needed to setup QtFile can be found in <em>reference/</em> directory.</p>
	<div style="clear: both">
	</div>
</asp:Content>
