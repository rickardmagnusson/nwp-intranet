using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace QtFile.Samples.Controllers
{
    public class UserController : Controller
	{
		public ActionResult Files(string theme)
		{
			ViewData["theme"] = theme;
			return View();
		}
    }
}
