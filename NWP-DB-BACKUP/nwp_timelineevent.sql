CREATE DATABASE  IF NOT EXISTS `nwp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nwp`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: nwp
-- ------------------------------------------------------
-- Server version	5.5.25a

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `timelineevent`
--

DROP TABLE IF EXISTS `timelineevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timelineevent` (
  `Id` int(11) NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Html` text,
  `Created` datetime DEFAULT NULL,
  `EventDate` datetime DEFAULT NULL,
  `EndEventDate` datetime DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `EventId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `EventId` (`EventId`),
  CONSTRAINT `FKE966EC06B44BC409` FOREIGN KEY (`EventId`) REFERENCES `timeline` (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timelineevent`
--

LOCK TABLES `timelineevent` WRITE;
/*!40000 ALTER TABLE `timelineevent` DISABLE KEYS */;
INSERT INTO `timelineevent` VALUES (1,'Nytt event ','','2012-08-01 07:17:47','2012-08-01 07:17:47','2012-08-01 07:17:47',1,0,NULL),(2,'Kataloger augusti','<p> Under augusti släpps nya kataloger föer CNW och Seger Trading</p>','2012-08-01 07:21:48','2012-08-01 00:00:00','2012-08-31 00:00:00',0,0,1),(3,'Uppstart Höst!','<p>-Seger stickat , Dressat,  Display,  Julteaser</p>','2012-08-01 07:27:19','2012-08-21 00:00:00','2012-08-21 00:00:00',1,0,1),(4,'Kickoff NWPP','<p>NWPP Kickoff inför höster och vintern.</p>\r\n<p> </p>','2012-08-02 10:12:18','2012-08-14 00:00:00','2012-08-14 00:00:00',1,0,1),(5,'Star for life','<p>Minimässa, Luleå.</p>','2012-08-02 11:45:30','2012-10-04 00:00:00','2012-08-04 00:00:00',1,0,1),(6,'April mässa','<p>Missa inte att anmäla dig till aprilmässan.</p>','2012-08-02 18:41:53','2012-04-04 00:00:00','2012-08-09 00:00:00',0,1,1),(7,'Mässa','<p>Minimässa med SfL Luleå</p>\r\n<p> </p>','2012-08-02 19:04:23','2012-09-26 00:00:00','2012-09-26 00:00:00',1,0,1),(8,'Katalog NWPP v.36','<p>Vecka 36 kommer den nya NWPP katalogen ut.</p>','2012-08-13 09:50:39','2012-08-20 00:00:00','2012-12-31 00:00:00',1,0,1);
/*!40000 ALTER TABLE `timelineevent` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-26 11:49:46
