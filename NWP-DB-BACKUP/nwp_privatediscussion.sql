CREATE DATABASE  IF NOT EXISTS `nwp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nwp`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: nwp
-- ------------------------------------------------------
-- Server version	5.5.25a

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `privatediscussion`
--

DROP TABLE IF EXISTS `privatediscussion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `privatediscussion` (
  `Id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `FromUserId` int(11) NOT NULL,
  `Html` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `privatediscussion`
--

LOCK TABLES `privatediscussion` WRITE;
/*!40000 ALTER TABLE `privatediscussion` DISABLE KEYS */;
INSERT INTO `privatediscussion` VALUES (1,1,2,'Från R till A',0,'0001-01-01 00:00:00'),(2,2,1,'Från admin till Rickard',0,'0001-01-01 00:00:00'),(3,2,1,'Från admin till rickard',0,'0001-01-01 00:00:00'),(4,2,4,'Hej Rickard Camilla här!',0,'0001-01-01 00:00:00'),(5,3,2,'Rickard till Zander',0,'0001-01-01 00:00:00'),(6,1,2,'Från Rickard till Admin.',0,'2012-08-14 06:39:40'),(7,2,1,'Testar att skriva ett lite längre meddelande till Rickard Magnusson för att se hur det ser ut när texten blir längre än bilden. Undrar om det räcker att skriva så här långt? ',0,'2012-08-14 07:04:33');
/*!40000 ALTER TABLE `privatediscussion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-26 11:49:29
