CREATE DATABASE  IF NOT EXISTS `nwp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nwp`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: nwp
-- ------------------------------------------------------
-- Server version	5.5.25a

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `PageId` int(11) NOT NULL,
  `ParentId` int(11) NOT NULL,
  `Layout` varchar(255) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  `Visible` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Position` varchar(255) NOT NULL,
  `Route` varchar(255) NOT NULL,
  `SubText` varchar(255) DEFAULT NULL,
  `MetaTitle` varchar(255) NOT NULL,
  `MetaDescription` varchar(255) NOT NULL,
  `MetaKeyWords` varchar(255) NOT NULL,
  `StartDate` varchar(255) DEFAULT NULL,
  `EndDate` varchar(255) DEFAULT NULL,
  `Created` varchar(255) DEFAULT NULL,
  `UseDate` tinyint(1) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,1,'_Layout.Home',1,1,'www',1,'0','','Startsidan','New Wave Profile Intranet','Startsidan','Startsidan','','','2012-08-01',0,0,'Startsidan'),(2,1,'_Layout.ThreeColumns',1,1,'www',1,'2','meddelanden','Meddelanden','Meddelanden','Meddelanden','Meddelanden','','','2012-08-01',0,0,'Meddelanden'),(3,1,'_Layout.TwoColumns',1,0,'www',1,'1','webshop','Webshoppen','WebShop','Välkommen till webshopen','webshop','','','2012-08-02',0,0,'Webshop'),(4,1,'_Layout.TwoColumns',1,0,'www',1,'4','kampanjer','Kampanjer','Kampanjer','kampanjer','kampanjer','','','2012-08-02',0,0,'Kampanjer'),(5,1,'_Layout.ThreeColumns',1,0,'www',1,'1','nyheter','Visa nyheter','Nyheter','Nyheter NW PROFILE','Nyheter','','','2012-08-02',0,0,'Nyheter'),(6,1,'_Layout.TwoColumns',1,0,'www',1,'1','media','Grafiska element','Grafiska element','Grafiska element','Grafiskt','','','2012-08-03',0,0,'Media'),(7,1,'_Layout.TwoColumns',1,0,'www',1,'1','avtal','Avtal','Avtal','Avtal','Avtal','','','2012-08-03',0,0,'Avtal'),(8,5,'_Layout.ThreeColumns',1,0,'www',1,'3','nyhetsbrev','Se nyhetsbrev','Nyhetsbrev','Beskrivning','Nyhetsbrev','','','2012-08-11',0,0,'Nyhetsbrev'),(9,1,'_Layout.MyPage',1,1,'www',1,'3','minsida','Min Sida','Min Sida','Min Sida','Min Sida','','','2012-08-11',0,0,'Min Sida'),(10,1,'_Layout.PersonalPage',1,0,'www',1,'1','personlig-sida','Besök Personlig sida','Personlig sida','Personlig sida','Personlig sida','','','2012-08-13',0,0,'Personlig sida'),(11,1,'_Layout.TwoColumns',1,0,'www',1,'6','sokresultat','Sökresultat','Sökresultat','Sökresultat','Sökresultat','','','2012-08-14',0,0,'Sökresultat');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-26 11:50:05
