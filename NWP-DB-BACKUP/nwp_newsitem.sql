CREATE DATABASE  IF NOT EXISTS `nwp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nwp`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: nwp
-- ------------------------------------------------------
-- Server version	5.5.25a

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `newsitem`
--

DROP TABLE IF EXISTS `newsitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsitem` (
  `Id` int(11) NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Message` text,
  `Created` datetime DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `NewsId` int(11) DEFAULT NULL,
  `Html` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `Teaser` varchar(200) DEFAULT NULL,
  `Owner` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `NewsId` (`NewsId`),
  CONSTRAINT `FKB69CFF05D6D6795C` FOREIGN KEY (`NewsId`) REFERENCES `news` (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsitem`
--

LOCK TABLES `newsitem` WRITE;
/*!40000 ALTER TABLE `newsitem` DISABLE KEYS */;
INSERT INTO `newsitem` VALUES (1,'Detta är en nyhet',NULL,'2012-08-02 14:17:03','2012-07-01 00:00:00','2012-08-31 00:00:00',0,1,'<p>Detta är den första nyheten i detta system.</p>',0,' The preceding example code shows how you can use the MMM format string for month strings in the.',2),(2,'Andra nyheten',NULL,'2012-08-02 16:01:35','2012-08-02 00:00:00','2012-08-31 00:00:00',0,1,'<p>Detta är den andra nyheten. Spännande va!!</p>',0,'The preceding example code shows how you can use the MMM format string for month strings in the...',2),(3,'Tredje nyheten',NULL,'2012-08-02 18:30:56','2012-08-01 00:00:00','2012-08-31 00:00:00',0,1,'<p>Nu börjar det bli spännande!! Eller hur!!!!!</p>',0,'The preceding example code shows how you can use the MMM format string for month strings in the...',2),(4,'Mer nyheter',NULL,'2012-08-05 13:38:08','2012-08-01 00:00:00','2012-08-31 00:00:00',0,1,'<p> The preceding example code shows how you can use the MMM format string for month strings in the C# language.</p>',0,' The preceding example code shows how you can use the MMM format string for month strings in the C# language. ',2),(5,'Nytt Intranet i höst',NULL,'2012-08-10 10:17:37','2012-08-10 00:00:00','2012-08-31 00:00:00',0,1,'<p>Under november kommer vi att testköra vårt nya intranet för New Wave Profile. Det kommer att vara betydligt smidigare än det gamla.</p>',1,'Under november kommer vi att testköra vårt nya intranet för New Wave Profile. Det kommer att vara betydligt ...',3),(6,'Ökad konsolidering i profilbranschen',NULL,'2012-08-10 10:28:43','2012-08-10 00:00:00','2012-08-31 00:00:00',0,1,'<p>Genom ett par stora förvärv inom branschen den sista månaden framträder en bild av stora förändringar.</p>',1,'Genom ett par stora förvärv inom branschen den sista månaden framträder en bild av...',3);
/*!40000 ALTER TABLE `newsitem` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-26 11:49:31
