CREATE DATABASE  IF NOT EXISTS `nwp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nwp`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: nwp
-- ------------------------------------------------------
-- Server version	5.5.25a

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wallcomment`
--

DROP TABLE IF EXISTS `wallcomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wallcomment` (
  `Id` int(11) NOT NULL,
  `Date` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `CommentId` int(11) DEFAULT NULL,
  `Likes` int(11) DEFAULT NULL,
  `CommentByUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `CommentId` (`CommentId`),
  CONSTRAINT `FK2560D3788C5596F8` FOREIGN KEY (`CommentId`) REFERENCES `wallpost` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallcomment`
--

LOCK TABLES `wallcomment` WRITE;
/*!40000 ALTER TABLE `wallcomment` DISABLE KEYS */;
INSERT INTO `wallcomment` VALUES (21,'08/09/2012 17:17:28','Test med kommentar!',0,1,1,2),(22,'08/09/2012 17:20:03','Fler kommentarer då!!',0,1,1,2),(23,'08/09/2012 17:20:33','Admin testar att kommentera!',0,1,1,1),(24,'08/09/2012 17:37:07','Test',0,1,1,1),(25,'08/09/2012 17:37:10','Test',0,1,1,1),(26,'08/09/2012 17:37:47','Test kommentar admin',0,2,1,1),(27,'08/10/2012 10:08:14','Ja det verkar ju fungera!',0,3,1,2),(28,'08/10/2012 10:25:37','Tjoho..',0,4,1,4),(29,'08/10/2012 10:41:42','Jaha.. Det var ju trevligt.',0,4,1,2),(30,'08/13/2012 09:11:51','Kommentar till testar igen!',0,6,1,2),(31,'08/17/2012 09:01:27','Jag är upptagen och kommer inte.',0,7,1,2);
/*!40000 ALTER TABLE `wallcomment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-26 11:49:37
