CREATE DATABASE  IF NOT EXISTS `nwp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nwp`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: nwp
-- ------------------------------------------------------
-- Server version	5.5.25a

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wallpost`
--

DROP TABLE IF EXISTS `wallpost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wallpost` (
  `Id` int(11) NOT NULL,
  `Likes` int(11) DEFAULT NULL,
  `Date` varchar(255) DEFAULT NULL,
  `Post` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `PostId` int(11) DEFAULT NULL,
  `PostedByUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `PostId` (`PostId`),
  CONSTRAINT `FKEC0DE510ECF88A84` FOREIGN KEY (`PostId`) REFERENCES `wall` (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallpost`
--

LOCK TABLES `wallpost` WRITE;
/*!40000 ALTER TABLE `wallpost` DISABLE KEYS */;
INSERT INTO `wallpost` VALUES (1,1,'08/09/2012 17:13:35','Test',0,1,2),(2,1,'08/09/2012 17:37:34','Admin testar!',0,1,1),(3,1,'08/10/2012 10:06:59','Fredrik skriver lite här då!!',0,1,3),(4,1,'08/10/2012 10:25:28','Hejsan hoppsan!',0,1,4),(5,1,'08/11/2012 11:33:43','Meddelande från meddelandesidan',0,1,2),(6,1,'08/13/2012 09:11:36','Testar igen!',0,1,2),(7,1,'08/13/2012 15:41:50','Vem kommer på kickoffen?',0,1,3);
/*!40000 ALTER TABLE `wallpost` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-26 11:49:58
