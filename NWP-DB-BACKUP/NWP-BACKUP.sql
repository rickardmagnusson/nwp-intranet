CREATE DATABASE  IF NOT EXISTS `nwp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nwp`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: nwp
-- ------------------------------------------------------
-- Server version	5.5.25a

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `shop`
--

DROP TABLE IF EXISTS `shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  `EmailFrom` varchar(255) DEFAULT NULL,
  `EmailTo` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Html` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop`
--

LOCK TABLES `shop` WRITE;
/*!40000 ALTER TABLE `shop` DISABLE KEYS */;
INSERT INTO `shop` VALUES (1,3,'CONTENT',1,'www','Produktlista NWP','',NULL,NULL,1,'1',0,'0001-01-01 00:00:00','');
/*!40000 ALTER TABLE `shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `searchpublic`
--

DROP TABLE IF EXISTS `searchpublic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchpublic` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `SearchResultPage` varchar(255) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `ShortCut` tinyint(1) DEFAULT NULL,
  `Owner` int(11) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `searchpublic`
--

LOCK TABLES `searchpublic` WRITE;
/*!40000 ALTER TABLE `searchpublic` DISABLE KEYS */;
INSERT INTO `searchpublic` VALUES (1,1,'SIDEBAR',1,1,'www','Sök','/sokresultat','4',0,1,0),(2,11,'CONTENT',1,1,'www','Sökresultat','/sokresultat','1',0,0,0);
/*!40000 ALTER TABLE `searchpublic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `privatediscussion`
--

DROP TABLE IF EXISTS `privatediscussion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `privatediscussion` (
  `Id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `FromUserId` int(11) NOT NULL,
  `Html` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `privatediscussion`
--

LOCK TABLES `privatediscussion` WRITE;
/*!40000 ALTER TABLE `privatediscussion` DISABLE KEYS */;
INSERT INTO `privatediscussion` VALUES (1,1,2,'Från R till A',0,'0001-01-01 00:00:00'),(2,2,1,'Från admin till Rickard',0,'0001-01-01 00:00:00'),(3,2,1,'Från admin till rickard',0,'0001-01-01 00:00:00'),(4,2,4,'Hej Rickard Camilla här!',0,'0001-01-01 00:00:00'),(5,3,2,'Rickard till Zander',0,'0001-01-01 00:00:00'),(6,1,2,'Från Rickard till Admin.',0,'2012-08-14 06:39:40'),(7,2,1,'Testar att skriva ett lite längre meddelande till Rickard Magnusson för att se hur det ser ut när texten blir längre än bilden. Undrar om det räcker att skriva så här långt? ',0,'2012-08-14 07:04:33');
/*!40000 ALTER TABLE `privatediscussion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsitem`
--

DROP TABLE IF EXISTS `newsitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsitem` (
  `Id` int(11) NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Message` text,
  `Created` datetime DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `NewsId` int(11) DEFAULT NULL,
  `Html` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `Teaser` varchar(200) DEFAULT NULL,
  `Owner` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `NewsId` (`NewsId`),
  CONSTRAINT `FKB69CFF05D6D6795C` FOREIGN KEY (`NewsId`) REFERENCES `news` (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsitem`
--

LOCK TABLES `newsitem` WRITE;
/*!40000 ALTER TABLE `newsitem` DISABLE KEYS */;
INSERT INTO `newsitem` VALUES (1,'Detta är en nyhet',NULL,'2012-08-02 14:17:03','2012-07-01 00:00:00','2012-08-31 00:00:00',0,1,'<p>Detta är den första nyheten i detta system.</p>',0,' The preceding example code shows how you can use the MMM format string for month strings in the.',2),(2,'Andra nyheten',NULL,'2012-08-02 16:01:35','2012-08-02 00:00:00','2012-08-31 00:00:00',0,1,'<p>Detta är den andra nyheten. Spännande va!!</p>',0,'The preceding example code shows how you can use the MMM format string for month strings in the...',2),(3,'Tredje nyheten',NULL,'2012-08-02 18:30:56','2012-08-01 00:00:00','2012-08-31 00:00:00',0,1,'<p>Nu börjar det bli spännande!! Eller hur!!!!!</p>',0,'The preceding example code shows how you can use the MMM format string for month strings in the...',2),(4,'Mer nyheter',NULL,'2012-08-05 13:38:08','2012-08-01 00:00:00','2012-08-31 00:00:00',0,1,'<p> The preceding example code shows how you can use the MMM format string for month strings in the C# language.</p>',0,' The preceding example code shows how you can use the MMM format string for month strings in the C# language. ',2),(5,'Nytt Intranet i höst',NULL,'2012-08-10 10:17:37','2012-08-10 00:00:00','2012-08-31 00:00:00',0,1,'<p>Under november kommer vi att testköra vårt nya intranet för New Wave Profile. Det kommer att vara betydligt smidigare än det gamla.</p>',1,'Under november kommer vi att testköra vårt nya intranet för New Wave Profile. Det kommer att vara betydligt ...',3),(6,'Ökad konsolidering i profilbranschen',NULL,'2012-08-10 10:28:43','2012-08-10 00:00:00','2012-08-31 00:00:00',0,1,'<p>Genom ett par stora förvärv inom branschen den sista månaden framträder en bild av stora förändringar.</p>',1,'Genom ett par stora förvärv inom branschen den sista månaden framträder en bild av...',3);
/*!40000 ALTER TABLE `newsitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic`
--

DROP TABLE IF EXISTS `dynamic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic`
--

LOCK TABLES `dynamic` WRITE;
/*!40000 ALTER TABLE `dynamic` DISABLE KEYS */;
/*!40000 ALTER TABLE `dynamic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newslist`
--

DROP TABLE IF EXISTS `newslist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newslist` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Created` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `NewsListId` int(11) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newslist`
--

LOCK TABLES `newslist` WRITE;
/*!40000 ALTER TABLE `newslist` DISABLE KEYS */;
INSERT INTO `newslist` VALUES (2,5,'CONTENT',1,'www','','',1,'',0,0);
/*!40000 ALTER TABLE `newslist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `printservice`
--

DROP TABLE IF EXISTS `printservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `printservice` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Html` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `printservice`
--

LOCK TABLES `printservice` WRITE;
/*!40000 ALTER TABLE `printservice` DISABLE KEYS */;
/*!40000 ALTER TABLE `printservice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userinrole`
--

DROP TABLE IF EXISTS `userinrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinrole` (
  `Id` int(11) NOT NULL,
  `RoleId` int(11) DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userinrole`
--

LOCK TABLES `userinrole` WRITE;
/*!40000 ALTER TABLE `userinrole` DISABLE KEYS */;
INSERT INTO `userinrole` VALUES (1,1,1,0),(2,2,1,0),(3,2,2,0),(4,2,3,0),(5,1,3,0),(6,1,4,0),(7,1,2,0);
/*!40000 ALTER TABLE `userinrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wallcomment`
--

DROP TABLE IF EXISTS `wallcomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wallcomment` (
  `Id` int(11) NOT NULL,
  `Date` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `CommentId` int(11) DEFAULT NULL,
  `Likes` int(11) DEFAULT NULL,
  `CommentByUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `CommentId` (`CommentId`),
  CONSTRAINT `FK2560D3788C5596F8` FOREIGN KEY (`CommentId`) REFERENCES `wallpost` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallcomment`
--

LOCK TABLES `wallcomment` WRITE;
/*!40000 ALTER TABLE `wallcomment` DISABLE KEYS */;
INSERT INTO `wallcomment` VALUES (21,'08/09/2012 17:17:28','Test med kommentar!',0,1,1,2),(22,'08/09/2012 17:20:03','Fler kommentarer då!!',0,1,1,2),(23,'08/09/2012 17:20:33','Admin testar att kommentera!',0,1,1,1),(24,'08/09/2012 17:37:07','Test',0,1,1,1),(25,'08/09/2012 17:37:10','Test',0,1,1,1),(26,'08/09/2012 17:37:47','Test kommentar admin',0,2,1,1),(27,'08/10/2012 10:08:14','Ja det verkar ju fungera!',0,3,1,2),(28,'08/10/2012 10:25:37','Tjoho..',0,4,1,4),(29,'08/10/2012 10:41:42','Jaha.. Det var ju trevligt.',0,4,1,2),(30,'08/13/2012 09:11:51','Kommentar till testar igen!',0,6,1,2),(31,'08/17/2012 09:01:27','Jag är upptagen och kommer inte.',0,7,1,2);
/*!40000 ALTER TABLE `wallcomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  `EmailFrom` varchar(255) DEFAULT NULL,
  `EmailTo` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign`
--

LOCK TABLES `campaign` WRITE;
/*!40000 ALTER TABLE `campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filemanager`
--

DROP TABLE IF EXISTS `filemanager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filemanager` (
  `Id` int(11) NOT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filemanager`
--

LOCK TABLES `filemanager` WRITE;
/*!40000 ALTER TABLE `filemanager` DISABLE KEYS */;
/*!40000 ALTER TABLE `filemanager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `Id` varchar(255) NOT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `WidgetId` int(11) DEFAULT NULL,
  `PageId` int(11) DEFAULT NULL,
  `Zone` varchar(255) DEFAULT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Html` varchar(255) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `Owner` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wall`
--

DROP TABLE IF EXISTS `wall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wall` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Html` varchar(255) DEFAULT NULL,
  `Created` varchar(255) DEFAULT NULL,
  `StartDate` varchar(255) DEFAULT NULL,
  `EndDate` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wall`
--

LOCK TABLES `wall` WRITE;
/*!40000 ALTER TABLE `wall` DISABLE KEYS */;
INSERT INTO `wall` VALUES (1,1,'CONTENT',1,'www',NULL,'','','','',1,'1',0);
/*!40000 ALTER TABLE `wall` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userinfo`
--

DROP TABLE IF EXISTS `userinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userinfo`
--

LOCK TABLES `userinfo` WRITE;
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `Id` int(11) NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Html` text,
  `Created` datetime DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Company` varchar(255) DEFAULT NULL,
  `Contact` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `Zip` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `SupplierId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `SupplierId` (`SupplierId`),
  CONSTRAINT `FKA8958AEEC2FB31F7` FOREIGN KEY (`SupplierId`) REFERENCES `suppliers` (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopitem`
--

DROP TABLE IF EXISTS `shopitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopitem` (
  `Id` int(11) NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Message` text,
  `Html` varchar(255) DEFAULT NULL,
  `Image` varchar(255) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `ShopId` int(11) DEFAULT NULL,
  `Price` varchar(255) DEFAULT NULL,
  `MinimumAmount` int(11) DEFAULT NULL,
  `ArtNmbr` varchar(255) DEFAULT NULL,
  `OtherInfo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ShopId` (`ShopId`),
  CONSTRAINT `FK47A6198A51FA8F04` FOREIGN KEY (`ShopId`) REFERENCES `shop` (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopitem`
--

LOCK TABLES `shopitem` WRITE;
/*!40000 ALTER TABLE `shopitem` DISABLE KEYS */;
INSERT INTO `shopitem` VALUES (1,'Testprodukt 1',NULL,'<p>Detta är texten till produkten. Denna är lite längre än de andra.</p>','','2012-08-06 15:51:26','2012-08-01 00:00:00','2012-11-30 00:00:00',1,0,1,'56',10,'1',''),(2,'Testprodukt 2',NULL,'<p>Detta är texten till produkten</p>','','2012-08-06 16:24:11','2012-08-06 00:00:00','2012-12-31 00:00:00',1,0,1,'45',25,'2',''),(3,'Testprodukt 3',NULL,'<p>Detta är texten till denna produkt.</p>','','2012-08-06 16:35:35','2012-08-01 00:00:00','2012-08-31 00:00:00',1,0,1,'89',12,'3','');
/*!40000 ALTER TABLE `shopitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletterservice`
--

DROP TABLE IF EXISTS `newsletterservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletterservice` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `EmailFrom` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletterservice`
--

LOCK TABLES `newsletterservice` WRITE;
/*!40000 ALTER TABLE `newsletterservice` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletterservice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timelineevent`
--

DROP TABLE IF EXISTS `timelineevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timelineevent` (
  `Id` int(11) NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Html` text,
  `Created` datetime DEFAULT NULL,
  `EventDate` datetime DEFAULT NULL,
  `EndEventDate` datetime DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `EventId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `EventId` (`EventId`),
  CONSTRAINT `FKE966EC06B44BC409` FOREIGN KEY (`EventId`) REFERENCES `timeline` (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timelineevent`
--

LOCK TABLES `timelineevent` WRITE;
/*!40000 ALTER TABLE `timelineevent` DISABLE KEYS */;
INSERT INTO `timelineevent` VALUES (1,'Nytt event ','','2012-08-01 07:17:47','2012-08-01 07:17:47','2012-08-01 07:17:47',1,0,NULL),(2,'Kataloger augusti','<p> Under augusti släpps nya kataloger föer CNW och Seger Trading</p>','2012-08-01 07:21:48','2012-08-01 00:00:00','2012-08-31 00:00:00',0,0,1),(3,'Uppstart Höst!','<p>-Seger stickat , Dressat,  Display,  Julteaser</p>','2012-08-01 07:27:19','2012-08-21 00:00:00','2012-08-21 00:00:00',1,0,1),(4,'Kickoff NWPP','<p>NWPP Kickoff inför höster och vintern.</p>\r\n<p> </p>','2012-08-02 10:12:18','2012-08-14 00:00:00','2012-08-14 00:00:00',1,0,1),(5,'Star for life','<p>Minimässa, Luleå.</p>','2012-08-02 11:45:30','2012-10-04 00:00:00','2012-08-04 00:00:00',1,0,1),(6,'April mässa','<p>Missa inte att anmäla dig till aprilmässan.</p>','2012-08-02 18:41:53','2012-04-04 00:00:00','2012-08-09 00:00:00',0,1,1),(7,'Mässa','<p>Minimässa med SfL Luleå</p>\r\n<p> </p>','2012-08-02 19:04:23','2012-09-26 00:00:00','2012-09-26 00:00:00',1,0,1),(8,'Katalog NWPP v.36','<p>Vecka 36 kommer den nya NWPP katalogen ut.</p>','2012-08-13 09:50:39','2012-08-20 00:00:00','2012-12-31 00:00:00',1,0,1);
/*!40000 ALTER TABLE `timelineevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeline`
--

DROP TABLE IF EXISTS `timeline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeline` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) DEFAULT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Html` varchar(255) DEFAULT NULL,
  `Created` varchar(255) DEFAULT NULL,
  `StartDate` varchar(255) DEFAULT NULL,
  `EndDate` varchar(255) DEFAULT NULL,
  `TimelineStartDateScreen` varchar(255) DEFAULT NULL,
  `TimelineBeginsDate` varchar(255) DEFAULT NULL,
  `MainHeadline` varchar(255) DEFAULT NULL,
  `MainHeadlineText` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeline`
--

LOCK TABLES `timeline` WRITE;
/*!40000 ALTER TABLE `timeline` DISABLE KEYS */;
INSERT INTO `timeline` VALUES (1,1,'HEADER',1,'www','Marknadsplan Höst 2012','Profilpraktikan på nätet','2012-08-01','2012-08-01','2012-12-31','2012-08-01','2012-12-31','Marknadsföring/Event Höst 2012','Planera din höst!',1,NULL,0),(2,1,'CONTENT',0,'www',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'1',1);
/*!40000 ALTER TABLE `timeline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsteaser`
--

DROP TABLE IF EXISTS `newsteaser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsteaser` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `ShowWidgetFromNewsList` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsteaser`
--

LOCK TABLES `newsteaser` WRITE;
/*!40000 ALTER TABLE `newsteaser` DISABLE KEYS */;
INSERT INTO `newsteaser` VALUES (1,5,1,'SIDEBARRIGHT',1,1,'www','Nyhetsgenväg nyhetssidan','1',0),(2,1,1,'SIDEBAR',1,0,'www','Nyheter','3',0);
/*!40000 ALTER TABLE `newsteaser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search`
--

DROP TABLE IF EXISTS `search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `ShortCut` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search`
--

LOCK TABLES `search` WRITE;
/*!40000 ALTER TABLE `search` DISABLE KEYS */;
INSERT INTO `search` VALUES (1,1,'SIDEBAR',1,0,'www','Sökformulär','4',0,1);
/*!40000 ALTER TABLE `search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider`
--

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `Id` int(11) NOT NULL,
  `UserName` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Company` varchar(255) DEFAULT NULL,
  `DisplayName` varchar(255) DEFAULT NULL,
  `UserIcon` varchar(255) DEFAULT NULL,
  `CompanyName` varchar(255) DEFAULT NULL,
  `PersonalDetails` mediumtext,
  `Homepage` varchar(255) DEFAULT NULL,
  `Position` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin',1,0,'admin@newwaveprofile.com','www','Administratör','1.jpg','  New Wave Profile','Admininströr är huvudkontot över alla andra konton.','www.newwaveprofile.com','Webmaster','0708-99 80 20'),(2,'rickard',1,0,'rickard.magnusson@newwave.se','www','Rickard Magnusson','2.jpg','     New Wave Mode AB','Rickard Magnusson arbetar på New Wave Mode AB, och har ansvarsområden inom allt som rör New Wave Profiles hemsidor. Arbetar också med kampanjer och de digitala nyhetsbreven. Har också många andra uppdrag som att hjälpa våra kunder med marknadsmaterial.','www.newwaveprofile.com','Marknad / Webmaster','0708-99 80 20'),(3,'fredrik',1,0,'fredrik.zander@newwave.se','www','Fredrik Zander','3.jpg','New Wave Profile',NULL,'www.newwaveprofile.com','Marknadschef','0708-99 80 88'),(4,'camilla',1,0,'camilla.fjordland@newwave.se','www','Camilla Fjordland','4.jpg','New Wave Profile',NULL,'www.newwaveprofile.com','Marknadskoordinator','0708-99 80 81');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `Id` int(11) NOT NULL,
  `GroupName` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Html` varchar(255) DEFAULT NULL,
  `Created` varchar(255) DEFAULT NULL,
  `StartDate` varchar(255) DEFAULT NULL,
  `EndDate` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,1,'SIDEBAR',1,'www','Nyheter','<p>Detta är inte texten till nyheter utan bara en behållare för nyhets delar.</p>','','','',1,'2',0),(2,1,'SIDEBAR',1,'www','ta bort','','','','',0,'',1);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contentbox`
--

DROP TABLE IF EXISTS `contentbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contentbox` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Html` varchar(255) DEFAULT NULL,
  `ColumnCount` int(11) DEFAULT NULL,
  `HtmlOne` varchar(255) DEFAULT NULL,
  `HtmlTwo` varchar(255) DEFAULT NULL,
  `HtmlThree` varchar(255) DEFAULT NULL,
  `HtmlFour` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contentbox`
--

LOCK TABLES `contentbox` WRITE;
/*!40000 ALTER TABLE `contentbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `contentbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tenant`
--

DROP TABLE IF EXISTS `tenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tenant` (
  `Id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Domain` varchar(255) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  `LocaleID` varchar(255) NOT NULL,
  `Theme` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tenant`
--

LOCK TABLES `tenant` WRITE;
/*!40000 ALTER TABLE `tenant` DISABLE KEYS */;
INSERT INTO `tenant` VALUES (1,'New Wave Profile','www',1,'sv-se','www');
/*!40000 ALTER TABLE `tenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mediamanager`
--

DROP TABLE IF EXISTS `mediamanager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mediamanager` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mediamanager`
--

LOCK TABLES `mediamanager` WRITE;
/*!40000 ALTER TABLE `mediamanager` DISABLE KEYS */;
/*!40000 ALTER TABLE `mediamanager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wallpost`
--

DROP TABLE IF EXISTS `wallpost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wallpost` (
  `Id` int(11) NOT NULL,
  `Likes` int(11) DEFAULT NULL,
  `Date` varchar(255) DEFAULT NULL,
  `Post` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `PostId` int(11) DEFAULT NULL,
  `PostedByUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `PostId` (`PostId`),
  CONSTRAINT `FKEC0DE510ECF88A84` FOREIGN KEY (`PostId`) REFERENCES `wall` (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallpost`
--

LOCK TABLES `wallpost` WRITE;
/*!40000 ALTER TABLE `wallpost` DISABLE KEYS */;
INSERT INTO `wallpost` VALUES (1,1,'08/09/2012 17:13:35','Test',0,1,2),(2,1,'08/09/2012 17:37:34','Admin testar!',0,1,1),(3,1,'08/10/2012 10:06:59','Fredrik skriver lite här då!!',0,1,3),(4,1,'08/10/2012 10:25:28','Hejsan hoppsan!',0,1,4),(5,1,'08/11/2012 11:33:43','Meddelande från meddelandesidan',0,1,2),(6,1,'08/13/2012 09:11:36','Testar igen!',0,1,2),(7,1,'08/13/2012 15:41:50','Vem kommer på kickoffen?',0,1,3);
/*!40000 ALTER TABLE `wallpost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `Id` int(11) NOT NULL,
  `RoleId` int(11) DEFAULT NULL,
  `RoleName` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,1,'admin',0),(2,2,'users',0);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersettings`
--

DROP TABLE IF EXISTS `usersettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersettings` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  `EmailFrom` varchar(255) DEFAULT NULL,
  `EmailTo` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersettings`
--

LOCK TABLES `usersettings` WRITE;
/*!40000 ALTER TABLE `usersettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) DEFAULT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Html` varchar(255) DEFAULT NULL,
  `Created` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,1,'CONTENT',1,'www','Leverantörer 2013','','',1,'1',0);
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linkitem`
--

DROP TABLE IF EXISTS `linkitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linkitem` (
  `Id` int(11) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Url` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `LinkId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `LinkId` (`LinkId`),
  CONSTRAINT `FKB892206A1A9660FA` FOREIGN KEY (`LinkId`) REFERENCES `links` (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linkitem`
--

LOCK TABLES `linkitem` WRITE;
/*!40000 ALTER TABLE `linkitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `linkitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  `EmailFrom` varchar(255) DEFAULT NULL,
  `EmailTo` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `WidgetId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `Zone` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `SortOrder` varchar(255) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `PageId` int(11) NOT NULL,
  `ParentId` int(11) NOT NULL,
  `Layout` varchar(255) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  `Visible` tinyint(1) DEFAULT NULL,
  `Domain` varchar(255) NOT NULL,
  `IsGlobal` tinyint(1) DEFAULT NULL,
  `Position` varchar(255) NOT NULL,
  `Route` varchar(255) NOT NULL,
  `SubText` varchar(255) DEFAULT NULL,
  `MetaTitle` varchar(255) NOT NULL,
  `MetaDescription` varchar(255) NOT NULL,
  `MetaKeyWords` varchar(255) NOT NULL,
  `StartDate` varchar(255) DEFAULT NULL,
  `EndDate` varchar(255) DEFAULT NULL,
  `Created` varchar(255) DEFAULT NULL,
  `UseDate` tinyint(1) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,1,'_Layout.Home',1,1,'www',1,'0','','Startsidan','New Wave Profile Intranet','Startsidan','Startsidan','','','2012-08-01',0,0,'Startsidan'),(2,1,'_Layout.ThreeColumns',1,1,'www',1,'2','meddelanden','Meddelanden','Meddelanden','Meddelanden','Meddelanden','','','2012-08-01',0,0,'Meddelanden'),(3,1,'_Layout.TwoColumns',1,0,'www',1,'1','webshop','Webshoppen','WebShop','Välkommen till webshopen','webshop','','','2012-08-02',0,0,'Webshop'),(4,1,'_Layout.TwoColumns',1,0,'www',1,'4','kampanjer','Kampanjer','Kampanjer','kampanjer','kampanjer','','','2012-08-02',0,0,'Kampanjer'),(5,1,'_Layout.ThreeColumns',1,0,'www',1,'1','nyheter','Visa nyheter','Nyheter','Nyheter NW PROFILE','Nyheter','','','2012-08-02',0,0,'Nyheter'),(6,1,'_Layout.TwoColumns',1,0,'www',1,'1','media','Grafiska element','Grafiska element','Grafiska element','Grafiskt','','','2012-08-03',0,0,'Media'),(7,1,'_Layout.TwoColumns',1,0,'www',1,'1','avtal','Avtal','Avtal','Avtal','Avtal','','','2012-08-03',0,0,'Avtal'),(8,5,'_Layout.ThreeColumns',1,0,'www',1,'3','nyhetsbrev','Se nyhetsbrev','Nyhetsbrev','Beskrivning','Nyhetsbrev','','','2012-08-11',0,0,'Nyhetsbrev'),(9,1,'_Layout.MyPage',1,1,'www',1,'3','minsida','Min Sida','Min Sida','Min Sida','Min Sida','','','2012-08-11',0,0,'Min Sida'),(10,1,'_Layout.PersonalPage',1,0,'www',1,'1','personlig-sida','Besök Personlig sida','Personlig sida','Personlig sida','Personlig sida','','','2012-08-13',0,0,'Personlig sida'),(11,1,'_Layout.TwoColumns',1,0,'www',1,'6','sokresultat','Sökresultat','Sökresultat','Sökresultat','Sökresultat','','','2012-08-14',0,0,'Sökresultat');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-26 11:51:08
