CREATE DATABASE  IF NOT EXISTS `nwp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nwp`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: nwp
-- ------------------------------------------------------
-- Server version	5.5.25a

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `shopitem`
--

DROP TABLE IF EXISTS `shopitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopitem` (
  `Id` int(11) NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Message` text,
  `Html` varchar(255) DEFAULT NULL,
  `Image` varchar(255) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `TrashBin` tinyint(1) DEFAULT NULL,
  `ShopId` int(11) DEFAULT NULL,
  `Price` varchar(255) DEFAULT NULL,
  `MinimumAmount` int(11) DEFAULT NULL,
  `ArtNmbr` varchar(255) DEFAULT NULL,
  `OtherInfo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ShopId` (`ShopId`),
  CONSTRAINT `FK47A6198A51FA8F04` FOREIGN KEY (`ShopId`) REFERENCES `shop` (`WidgetId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopitem`
--

LOCK TABLES `shopitem` WRITE;
/*!40000 ALTER TABLE `shopitem` DISABLE KEYS */;
INSERT INTO `shopitem` VALUES (1,'Testprodukt 1',NULL,'<p>Detta är texten till produkten. Denna är lite längre än de andra.</p>','','2012-08-06 15:51:26','2012-08-01 00:00:00','2012-11-30 00:00:00',1,0,1,'56',10,'1',''),(2,'Testprodukt 2',NULL,'<p>Detta är texten till produkten</p>','','2012-08-06 16:24:11','2012-08-06 00:00:00','2012-12-31 00:00:00',1,0,1,'45',25,'2',''),(3,'Testprodukt 3',NULL,'<p>Detta är texten till denna produkt.</p>','','2012-08-06 16:35:35','2012-08-01 00:00:00','2012-08-31 00:00:00',1,0,1,'89',12,'3','');
/*!40000 ALTER TABLE `shopitem` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-26 11:49:44
