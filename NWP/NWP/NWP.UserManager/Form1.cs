﻿using MySql.Data.MySqlClient;
using NWP.UserManager.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NWP.UserManager
{
    public partial class Form1 : Form
    {
        public static Form CurrentForm;

        public Form1()
        {
            InitializeComponent();
            CurrentForm = this;
            DataActions.CreateList();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (MySqlConnection connection = new MySqlConnection(DataActions.connectionString))
            {
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select UserName, Password from Users where UserName='" + listBox1.SelectedItem + "'";
                    connection.Open();
                    var r = command.ExecuteReader();
                    if (r.Read())
                    {
                        textBox1.Text = r.GetString(0);
                        textBox2.Text = r.GetString(1);

                        DataActions.GroupList(listBox2, null);
                        DataActions.GroupList(listBox3, listBox1.SelectedItem.ToString());
                    }
                }
            } 
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (MySqlConnection connection = new MySqlConnection(DataActions.connectionString))
            {
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE Users Set Password = '" +textBox2.Text+ "' where UserName='" + listBox1.SelectedItem + "'";
                    connection.Open();
                    int updated = command.ExecuteNonQuery();
                    if(updated==1)
                        MessageBox.Show("Personen uppdaterades utan problem.");
                   
                }

                DataActions.CreateList();
            } 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new AddUserForm().ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Är du säker??", "Radera användare", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                using (MySqlConnection connection = new MySqlConnection(DataActions.connectionString))
                {
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "DELETE FROM Users where UserName='" + listBox1.SelectedItem + "'";
                        connection.Open();
                        var r = command.ExecuteReader();
                        if (r.Read())
                        {
                            textBox1.Clear();
                            textBox2.Clear();
                        }
                    }
                }
                DataActions.CreateList();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void avslutaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

    }

    public static class Ext 
    { 
        public static void Clear(this TextBox textbox)
        {
            textbox.Text = string.Empty;
        }
    }
}



