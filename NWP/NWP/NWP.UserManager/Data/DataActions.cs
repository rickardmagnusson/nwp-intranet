﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NWP.UserManager.Data
{
    public static class DataActions
    {

        public static string connectionString = "Data source=localhost;Database=sso;user id=root;password=ndc885x;";

        public static void CreateList()
        {
            var listbox = (ListBox)DataActions.FindControl(Form1.CurrentForm, "listBox1");
            listbox.Items.Clear();
                      
            DataTable tables = new DataTable("Tables");
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select UserName, Id from Users";
                    connection.Open();
                    tables.Load(command.ExecuteReader(CommandBehavior.CloseConnection));
                }
            }

            foreach (DataRow row in tables.Rows)
            {
                listbox.Items.Add(row[0].ToString());
            }
        }


        public static void GroupList(ListBox listbox, string selected)
        {
            listbox.Items.Clear();

            DataTable tables = new DataTable("Tables");
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand command = connection.CreateCommand())
                {
                    if(!string.IsNullOrEmpty(selected))
                        command.CommandText = "SELECT DISTINCT * FROM users user LEFT JOIN UserInGroup ingroup ON ingroup.UserId = user.Id LEFT JOIN UserGroup groups ON groups.Id = ingroup.GroupId WHERE user.UserName = '"+selected+"';";
                    else
                        command.CommandText = "select GroupName, Id from UserGroup";
                    connection.Open();
                    tables.Load(command.ExecuteReader(CommandBehavior.CloseConnection));
                }
            }

            foreach (DataRow row in tables.Rows)
            {
                listbox.Items.Add(row[0].ToString());
            }
        }

        public static void MemberList()
        {
        }



        public static Control FindControl(Form form, string name)
        {
            foreach (Control control in form.Controls)
            {
                Control result = FindControl(form, control, name);
                if (result != null)
                    return result;
            }

            return null;
        }

        private static Control FindControl(Form form, Control control, string name)
        {
            if (control.Name == name)
            {
                return control;
            }

            foreach (Control subControl in control.Controls)
            {
                Control result = FindControl(form, subControl, name);
                if (result != null)
                    return result;
            }

            return null;
        } 

    }
}



