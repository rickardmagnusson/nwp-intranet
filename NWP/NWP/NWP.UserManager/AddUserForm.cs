﻿using MySql.Data.MySqlClient;
using NWP.UserManager.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NWP.UserManager
{
    public partial class AddUserForm : Form
    {
        string connectionString = "Data source=localhost;Database=sso;user id=root;password=passord;";

        public static Form CurrentForm;

        public AddUserForm()
        {
            CurrentForm = this;
            InitializeComponent();
            DataActions.GroupList(this.listBox1, null);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text)){
                MessageBox.Show("Ange ett namn!");
                return;
            }
            if (string.IsNullOrEmpty(textBox2.Text))
            {
                MessageBox.Show("Ange ett lösenord!");
                return;
            }

            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "INSERT INTO Users Values(null, '"+textBox1.Text+"','"+textBox2.Text+"')";
                    connection.Open();
                    int updated = command.ExecuteNonQuery();

                    DataActions.CreateList();
                    this.Close();
                }
            } 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
