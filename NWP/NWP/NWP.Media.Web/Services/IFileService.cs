﻿
using System.Web;
using System.Collections.Generic;

namespace Nwp.Media.Services
{
    public interface IFileService
	{
		/// <summary>
		/// Saves a file to new location.
		/// </summary>
		/// <param name="folderPath">
		/// The related path of the destination folder.
		/// </param>
		/// <param name="file">
		/// The file to save.
		/// </param>
		/// <param name="overWrite">
		/// Whether overwrite if namesake file exists.
		/// </param>
		/// <returns>
		/// One of following results:
		/// <list type="bullet">
		/// <item><see cref="OpResult.Succeed"/> - file saved;</item>
		/// <item><see cref="OpResult.FolderNotFound"/> - the destination folder not found;</item>
		/// <item><see cref="OpResult.AlreadyExist"/> - a namesake file alreay exists.</item>
		/// </list>
		/// </returns>
		OpResult Save(string folderPath, HttpPostedFileBase file, bool overWrite);

		UserFile Retrieve(string path);

		/// <summary>
		/// Moves a file to new location.
		/// </summary>
		/// <param name="srcPath">
		/// The related path of the source file.
		/// </param>
		/// <param name="destPath">
		/// The related path of the destination folder.
		/// </param>
		/// <param name="overWrite">
		/// Whether overwrite if namesake file exists.
		/// </param>
		/// <returns>
		/// One of following results:
		/// <list type="bullet">
		/// <item><see cref="OpResult.Succeed"/> - file moved;</item>
		/// <item><see cref="OpResult.NotFound"/> - the source file not found;</item>
		/// <item><see cref="OpResult.FolderNotFound"/> - the destination folder not found;</item>
		/// <item><see cref="OpResult.AlreadyExist"/> - a namesake file alreay exists.</item>
		/// </list>
		/// </returns>
		OpResult Move(string srcPath, string destPath, bool overWrite);

		OpResult Delete(string path);

		IEnumerable<UserFileInfo> GetFileList(string path);
		int Count();
		long GetSize();
    }
}
