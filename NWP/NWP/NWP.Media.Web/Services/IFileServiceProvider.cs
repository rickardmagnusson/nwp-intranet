﻿

namespace Nwp.Media.Services
{
	/// <summary>
	/// Contract of service provider.
	/// </summary>
	public interface IFileServiceProvider
	{
		/// <summary>
		/// Gets file service for specified user.
		/// </summary>
		/// <param name="username">
		/// The name of the user.
		/// </param>
		/// <returns>
		/// File service for specified user.
		/// </returns>
		IFileService GetFileService(string username);

		/// <summary>
		/// Gets folder service for specified user.
		/// </summary>
		/// <param name="username">
		/// The name of the user.
		/// </param>
		/// <returns>
		/// Folder service for specified user.
		/// </returns>
		IFolderService GetFolderService(string username);
	}
}
