﻿
using System.Web.Mvc;

namespace Nwp.Media
{
	public class NotFoundResult : ActionResult
	{
		public override void ExecuteResult(ControllerContext context)
		{
			context.HttpContext.Response.StatusCode = 404;
		}
	}
}
