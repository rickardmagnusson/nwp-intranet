﻿

using System.Linq;
using System.Collections.Generic;

namespace Nwp.Media
{
	public class UserSetting : IUserSetting
	{
		public bool IsExtAllowed(string extension)
		{
			return AllowedExtensions.Contains(extension);
		}

		public IEnumerable<string> AllowedExtensions
		{
			get;
			set;
		}

		public long Quota
		{
			get;
			set;
		}

		public long MaxSizePerFile
		{
			get;
			set;
		}

		public int MaxFolders
		{
			get;
			set;
		}

		public int MaxFiles
		{
			get;
			set;
		}
	}
}
