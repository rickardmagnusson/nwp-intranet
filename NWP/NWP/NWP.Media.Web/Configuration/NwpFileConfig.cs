using System.Configuration;

namespace Nwp.Media.Configuration
{
    /// <summary>
    /// The QtFileConfig Configuration Section.
    /// </summary>
    public class NwpFileConfig : ConfigurationSection
    {
        /// <summary>
        /// The XML name of the <see cref="P:Nwp.Media.Configuration.QtFileConfig.DefaultMaxFiles" /> property.
        /// </summary>
        internal const string DefaultMaxFilesPropertyName = "defaultMaxFiles";
        /// <summary>
        /// The XML name of the <see cref="P:Nwp.Media.Configuration.QtFileConfig.DefaultMaxFolders" /> property.
        /// </summary>
        internal const string DefaultMaxFoldersPropertyName = "defaultMaxFolders";
        /// <summary>
        /// The XML name of the <see cref="P:Nwp.Media.Configuration.QtFileConfig.DefaultMaxSizePerFile" /> property.
        /// </summary>
        internal const string DefaultMaxSizePerFilePropertyName = "defaultMaxSizePerFile";
        /// <summary>
        /// The XML name of the <see cref="P:Nwp.Media.Configuration.QtFileConfig.DefaultQuota" /> property.
        /// </summary>
        internal const string DefaultQuotaPropertyName = "defaultQuota";
        /// <summary>
        /// The XML name of the <see cref="P:Nwp.Media.Configuration.QtFileConfig.DefautAllowedExtensions" /> property.
        /// </summary>
        internal const string DefautAllowedExtensionsPropertyName = "defautAllowedExtensions";
        /// <summary>
        /// The XML name of the QtFileConfig Configuration Section.
        /// </summary>
        internal const string QtFileConfigSectionName = "nwpfile";
        /// <summary>
        /// The XML name of the <see cref="P:Nwp.Media.Configuration.QtFileConfig.UserSettings" /> property.
        /// </summary>
        internal const string UserSettingsPropertyName = "userSettings";

        /// <summary>
        /// Gets or sets the DefaultMaxFiles.
        /// </summary>
        [ConfigurationProperty("defaultMaxFiles", IsRequired=true, IsKey=false, IsDefaultCollection=false)]
        public int DefaultMaxFiles
        {
            get
            {
                return (int) base["defaultMaxFiles"];
            }
            set
            {
                base["defaultMaxFiles"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the DefaultMaxFolders.
        /// </summary>
        [ConfigurationProperty("defaultMaxFolders", IsRequired=true, IsKey=false, IsDefaultCollection=false)]
        public int DefaultMaxFolders
        {
            get
            {
                return (int) base["defaultMaxFolders"];
            }
            set
            {
                base["defaultMaxFolders"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the DefaultMaxSizePerFile.
        /// </summary>
        [ConfigurationProperty("defaultMaxSizePerFile", IsRequired=true, IsKey=false, IsDefaultCollection=false)]
        public long DefaultMaxSizePerFile
        {
            get
            {
                return (long) base["defaultMaxSizePerFile"];
            }
            set
            {
                base["defaultMaxSizePerFile"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the DefaultQuota.
        /// </summary>
        [ConfigurationProperty("defaultQuota", IsRequired=true, IsKey=false, IsDefaultCollection=false)]
        public long DefaultQuota
        {
            get
            {
                return (long) base["defaultQuota"];
            }
            set
            {
                base["defaultQuota"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the DefautAllowedExtensions.
        /// </summary>
        [ConfigurationProperty("defautAllowedExtensions", IsRequired=true, IsKey=false, IsDefaultCollection=false)]
        public ExtensionsConfig DefautAllowedExtensions
        {
            get
            {
                return (ExtensionsConfig) base["defautAllowedExtensions"];
            }
            set
            {
                base["defautAllowedExtensions"] = value;
            }
        }

        /// <summary>
        /// Gets the QtFileConfig instance.
        /// </summary>
        public static NwpFileConfig Instance
        {
            get
            {
                return (ConfigurationManager.GetSection("qtfile") as NwpFileConfig);
            }
        }

        /// <summary>
        /// Gets or sets the UserSettings.
        /// </summary>
        [ConfigurationProperty("userSettings", IsRequired=false, IsKey=false, IsDefaultCollection=false)]
        public UserSettingsConfig UserSettings
        {
            get
            {
                return (UserSettingsConfig) base["userSettings"];
            }
            set
            {
                base["userSettings"] = value;
            }
        }
    }
}

