﻿
using System.Collections.Generic;

namespace Nwp.Media
{
	/// <summary>
	/// Value object contains the initial data of a file manager.
	/// </summary>
	public class FileMgrViewData
	{
		public IEnumerable<UserFolder> Folders { get; set; }
		public string CurrentFolder { get; set; }
		public IEnumerable<UserFileInfo> Files { get; set; }
	}
}
