﻿

namespace Nwp.Media.Repositories
{
	public interface IUserSettingRepository
	{
		IUserSetting GetSetting(string username);
	}
}
