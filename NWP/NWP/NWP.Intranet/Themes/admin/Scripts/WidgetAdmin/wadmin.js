﻿(function () {
    if (!/*@cc_on!@*/0) return;
    var e = ("abbr article aside audio canvas command datalist details figure figcaption footer " +
		"header hgroup mark meter nav output progress section summary time video").split(' '),
	i = e.length;
    while (i--) {
        document.createElement(e[i]);
    }
})(document.documentElement, 'className');




$(document).ready(function () {
    $("#workarea .zone").sortable({
        placeholder: "placeholder",
        connectWith: ".zone",
        handle: ".title",
        dropOnEmpty: true,
        forceHelperSize: true,
        forcePlaceholderSize: true,
        revert: 300,
        delay: 100,
        opacity: 0.8,
        start: function (e, ui) {
        },
        stop: function (e, ui) {
            var fromZone = $(this).attr("id"); //ZoneID
            var toWidget = $(this).data().sortable.currentItem;
            var zone = toWidget.parent().attr("id");
            var list = new Array();
            list.push(toWidget.parent().attr("id").replace("template", ""));
            var i = 0;

            $("#" + zone + " .widget").each(function (ui, index) {
                list.push($(this).attr("id"));
            });

            var s = "";
            for (i = 0; i < list.length; i++) {
                s += list[i] + ",";
            }

            $.post("/admin/pages/widgetsort", { widgets: s },
            function (data, textStatus) {
                // alert("Response from server: " + data.data);
            });

            $(this).effect("transfer", { from: fromZone, to: toWidget }, 300);
        }
    }).disableSelection();


    //DELETE WIDGET
    $(".deleteWidget").click(function () {
        var wid = $(this).parent().parent(".widget").attr("id");
        
        $.post("/admin/WidgetManager/Delete/" + wid, {},
        function (data, textStatus) {
            //alert($("#" +data.WidgetID).attr("class"));
            $("#" + data.WidgetID).remove();
        });
        return false;
    });

    //ADD WIDGET
    $("#addWidget").click(function () {

        var pageid = $("#widgetcontrol #Page").val();
        var zone = $("#widgetcontrol #Zone").val();
        var widget = $("#widgetcontrol #widget").val();
        var fromZone = $(this).attr("id");
        var toZone = $("#template" + zone).attr("id");

        $.post("/Widget/WidgetManager/AddWidget", { page: pageid, type: widget, zone: zone },
            function (data, textStatus) {

                var template = '<div class="widget" id="' + data.WidgetID + '"><div class="title">' + widget + '<a class="editWidgetLink" href="/admin/' + widget + '/edit/' + data.WidgetID + '">Redigera</a> <a class="deleteWidget">Ta bort</a></div>';
                template += '<div class="content">';
                template += '<p>&nbsp;</p>';
                template += '</div>';
                template += '</div>';
                //alert(template);

                $("#template" + zone).prepend(template);
            });

        return false;
    });

});
