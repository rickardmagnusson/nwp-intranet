﻿jQuery.fn.addOption = function () {
    if (arguments.length == 0) return this;
    // select option when added? default is true         
    var selectOption = true;
    // multiple items         
    var multiple = false;
    if (typeof arguments[0] == "object") {
        multiple = true;
        var items = arguments[0];
    } if (arguments.length >= 2) {
        if (typeof arguments[1] == "boolean") selectOption = arguments[1];
        else if (typeof arguments[2] == "boolean") selectOption = arguments[2];
        if (!multiple) {
            var value = arguments[0];
            var text = arguments[1];
        }
    }
    this.each(
        function () {
            if (this.nodeName.toLowerCase() != "select") return;
            if (multiple) {
                for (var v in items) {
                    jQuery(this).addOption(v, items[v], selectOption);
                }
            } else {
                var option = document.createElement("option");
                option.value = value;
                option.text = text;
                this.options.add(option);
            }
            if (selectOption) {
                this.options[this.options.length - 1].selected = true;
            }
        })
    return this;
}