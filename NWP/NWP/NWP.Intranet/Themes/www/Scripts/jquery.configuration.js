﻿

$(document).ready(function () {

    $("#menubar").click(function () {
        if ($("#state").is(":hidden")) {
            $("#slider").animate({
                marginTop: "-100px"
            }, { queue: false, duration: 600, easing: 'easeInBounce' }, 300);
            $("#state").show();
        } else {
            $("#slider").animate({
                marginTop: "0px"
            }, { queue: false, duration: 600, easing: 'easeOutBounce' }, 300);
            $("#state").hide();
        }
    });

    //$("#container").addClass("shadow");


    $(".listitem").hover(
      function () {
          var template = "<a href='" + $(this).attr('rel') + "' class='button pop'>Redigera</a>";
          $(this).append(template);
      },
      function () {
          $(this).find(".pop").remove();
      }
    );




    $("#btnCreateComment").click(function () {
        
        if ($("#txtComment").val() == '') {
            alert("Du måste skriva ett meddelande som du kan skicka!");
            return;
        }
        var uid = $("#userid").val();
        var message = $("#txtComment").val();
        var commentid = $("#Id").val();
        $.ajax({
            type: "GET",
            url: "/admin/Comments/AddComment" + '?userid=' + uid + '&commentid=' + commentid + '&message=' + message,
            dataType: "text",
            success: function (xml) {
                alert("Tack, vi skickade ditt meddelande utan problem.");
            }
        });

    });


    // DATATABLE
    $('table.all').dataTable({
        "bInfo": false,
        "iDisplayLength": 20,
        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
        "sPaginationType": "full_numbers",
        "bPaginate": true,
        "sDom": '<f>t<pl>',
        "oLanguage": {
            "sEmptyTable": "Tabellen innehåller ingen data",
            "sInfo": "Visar _START_ till _END_ av totalt _TOTAL_ rader",
            "sInfoEmpty": "Visar 0 till 0 av totalt 0 rader",
            "sInfoFiltered": "(filtrerade från totalt _MAX_ rader)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Visa _MENU_ rader",
            "sLoadingRecords": "Laddar...",
            "sProcessing": "Bearbetar...",
            //"sSearch": "Sök:",
            "sZeroRecords": "Hittade inga matchande resultat",
            "oPaginate": {
                "sFirst": "Första",
                "sLast": "Sista",
                "sNext": "Nästa",
                "sPrevious": "Föregående"
            },
            "oAria": {
                "sSortAscending": ": aktivera för att sortera kolumnen i stigande ordning",
                "sSortDescending": ": aktivera för att sortera kolumnen i fallande ordning"
            }
        }
    });

    $('table.pagesort').dataTable({
        "bInfo": false,
        "iDisplayLength": 20,
        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
        "sPaginationType": "full_numbers",
        "bPaginate": true,
        "bFilter": false,
        "sDom": 't<pl>',
        "oLanguage": {
            "sEmptyTable": "Tabellen innehåller ingen data",
            "sInfo": "Visar _START_ till _END_ av totalt _TOTAL_ rader",
            "sInfoEmpty": "Visar 0 till 0 av totalt 0 rader",
            "sInfoFiltered": "(filtrerade från totalt _MAX_ rader)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Visa _MENU_ rader",
            "sLoadingRecords": "Laddar...",
            "sProcessing": "Bearbetar...",
            //"sSearch": "Sök:",
            "sZeroRecords": "Hittade inga matchande resultat",
            "oPaginate": {
                "sFirst": "Första",
                "sLast": "Sista",
                "sNext": "Nästa",
                "sPrevious": "Föregående"
            },
            "oAria": {
                "sSortAscending": ": aktivera för att sortera kolumnen i stigande ordning",
                "sSortDescending": ": aktivera för att sortera kolumnen i fallande ordning"
            }
        }

    });

    $('table.sortsearch').dataTable({
        "bInfo": false,
        "bPaginate": false,
        "sDom": 't<plf>'
    });

    $('table.sorting').dataTable({
        "bInfo": false,
        "bPaginate": false,
        "bFilter": false,
        "sDom": 't<plf>'
    });

    $(".dataTables_wrapper .dataTables_length select").addClass("entries");



});