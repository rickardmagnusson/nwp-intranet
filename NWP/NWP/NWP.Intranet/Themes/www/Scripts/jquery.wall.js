﻿
/*
* Project: NWP Intranet
* Author: Rickard Magnusson, New Wave Mode AB 2012
* Description: Contains all functionallity to hande the wall such as create and post comments and also view personal details.
*/

/*
    TODO: En bugg i createComment gör att meddelandet hamnar först!!! Måste kollas snarast.!! 
    Undrar om detta fortfarande uppstår.. Har inte upplevt detta hittils.
*/

/*public common variables*/

//$.getScript('/themes/www/views/wall.cshtml', function () {
   
//});

var users = new Array();
var currentUser=0;
var currentID = 0;
var personalPage = '/personlig-sida';
var userImageFolder = '/themes/www/styles/images/users/';
var wallUrl = "/wall/wall/GetComments/1";
var wallNextUrl = "/wall/wall/GetNextComments/1/";
var postUrl = "/wall/wall/post";
var commentUrl = "/wall/wall/comment/";
var wallUsers = "/wall/wall/GetAllUsers/1"; 
var getCurrentUser = "/wall/wall/CurrentUserId/1/";
var last = 0;
var currentUserId = 0;
var autoload = true;

//User placeholder:: Do not delete!!
function User() {
    this.Id = '';
    this.Name = '';
	this.Email = '';
	this.Company = '';
	this.Position = '';
	this.Phone = '';
	this.UserIcon = '';

    this.complete = function() {
        return this.Name + '\n' + this.Email + ' \n' + this.Company + ' \n' + this.Position + ' \n' + this.Phone;
    };

    this.Name = function() {
        return this.Name;
    };
}


/*JQuery extension*/

jQuery.extend({

    initNwp: function () {
      
        //Checks all textfield for default text
        $(".defaultText").focus(function (srcc) { if ($(this).val() == $(this)[0].title) { $(this).removeClass("defaultTextActive"); $(this).val(""); } });
        $(".defaultText").blur(function () { if ($(this).val() == "") { $(this).addClass("defaultTextActive"); $(this).val($(this)[0].title); } });
        $(".defaultText").blur();

        //Register templates
        $.template("fbwallmarkup", $.getWallTemplate());
        $.template("fbmarkupfirst", $.getCommentFirstTemplate());
        $.template("fbmarkup", $.getCommentTemplate());

        //Get current userid
        $.ajax({
            url: getCurrentUser,
            type: 'GET',
            dataType: 'text',
            async: false,
            success: function (data) {
                currentUserId = data;
                currentUser = users[data];
            }
        });


        //Load all users
        users = $.getUsers();

        //Only temporary
        //Start message recieve
        $.getMessages();
        //$.messageAnnounce();

        var isupdate = false;
        var loadid = $.getParam('id') == null ? 0 : $.getParam('id');
        if (loadid > 0) {
            autoload = false;
            $.loadWall('/wall/wall/GetSinglePost/1/' + loadid, loadid, true);
        } else {
            //Setup and create wall
            $.loadWall(wallUrl, loadid, false);
        }


    },

    getParam: function(name)
        {
          name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.search);
        if(results == null)
            return null;
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
     },

    /*
    Extract all users from xml and put in cache.
    */
    getUsers: function () {
        var result = null;
        var users = new Array();
        $.ajax({
            url: wallUsers,
            type: 'GET',
            dataType: 'xml',
            async: false,
            success: function (data) {
                result = data;
            }
        });

        $(result).find('users').each(function () {
            $(this).find('user').each(function () {
                var user = new User();
                user.Id = $(this).attr('id');
                user.Name = $(this).find('name').text();
                user.Company = $(this).find('company').text();
                user.Email = $(this).find('email').text();
                user.Phone = $(this).find('phone').text();
                user.Position = $(this).find('position').text();
                user.UserIcon = $(this).find('usericon').text();
                users.push(user);
            });
        });
        return users;
    },

    /*
    Share button. Saves a new (thread) comment.
    */
    share: function () {
        var post = $("#wall").val();
        var date = Date();
        var c = parseInt(currentID) + 1;

        if (post != '' && post != 'Skriv något') {
            $.tmpl("fbwallmarkup", {
                "id": c,
                "uid": currentUserId,
                "user": $.getUser(currentUserId).Name,
                "date": date,
                "post": post,
                "userpage": personalPage,
                "imagefolder": userImageFolder,
                "usericon": $.getUser(currentUserId).UserIcon
            }).prependTo('#thewall');


            //TODO:: Flush data to database while insert here!!!

            $.ajax({
                type: "GET",
                url: postUrl + '?id=1,' + post, //Ett står för vilken wall, (Bör vara dynamisk)
                dataType: "text",
                success: function (xml) {

                }, error: function (xml) {
                    alert(xml.responseText);
                }
            });
            $("#wall").val('');
        }
    },

    /* 
    Create a new comment and insert into current list of comments
    */
    createComment: function (txt, id) {
        if (txt != '') {
            var textboxID = '#comments' + id;
           
            if ($(textboxID).parent().find("span").hasClass("fb-wall-likes")) { //Adds an up arrow on first post.
                $.tmpl("fbmarkup", { "comment": txt,
                    "user": $.getUser(currentUserId).Name,
                    "date": Date(),
                    "uid": currentUserId,
                    "id": id,
                    "userpage": personalPage,
                    "imagefolder": userImageFolder,
                    "usericon": $.getUser(currentUserId).UserIcon
                }).appendTo(textboxID);
            } else {
                $.tmpl("fbmarkupfirst", { "comment": txt,
                    "user": $.getUser(currentUserId).Name,
                    "date": Date(),
                    "uid": currentUserId,
                    "id": id,
                    "userpage": personalPage,
                    "imagefolder": userImageFolder,
                    "usericon": $.getUser(currentUserId).UserIcon
                }).appendTo(textboxID);
            }



            //TODO:: Need to flush data to database while insert here!!!
            $("#txt" + id).val('');

            $.ajax({
                type: "GET",
                url: commentUrl + '?id=' + id + ',' + txt, //Ett står för vilket wallId vi arbetar med
                dataType: "text",
                success: function (xml) {
                   
                },
                error: function (err) {
                    alert(err)
                }
            });
        }
    },

    getUser: function (uid) {
        
        for (i = 0; i < users.length; i++) {
            if (users[i].Id == uid) {
                return users[i];
                break;
            }
        }
       
    },

    /*
    Loads the wall with all messages.
    */
    loadWall: function (url, first, isupdate) {
        last = url;
        $.ajax({
            type: "GET",
            url: url,
            dataType: "xml",
            success: function (xml) {

                $(xml).find('message').each(function () {
                    var id = $(this).attr('id');
                    
                    //Riktiga user id
                    var uid = $(this).find('user').text();
                    var user = $.getUser(uid);
                    var date = $(this).find('date').text();
                    var post = $(this).find('post').text();
                    var usericon = user.UserIcon;
                    currentID++;


                    $.tmpl("fbwallmarkup", {
                        "id": id,
                        "user": user.Name,
                        "date": date,
                        "post": post,
                        "uid": uid,
                        "userpage": personalPage,
                        "imagefolder": userImageFolder,
                        "usericon": usericon
                    }).appendTo("#thewall");


                    $(this).find('comment').each(function () {
                        var comment = $(this).text();
                        var u = parseInt($(this).attr('by'));
                        var name = '#comments' + id + '';
                        var likes = $(this).attr('likes');
                        var dt = $(this).attr('date');
                        if (first == 0) {
                            $.tmpl("fbmarkupfirst", { "comment": comment,
                                "user": $.getUser(u).Name,
                                "date": dt,
                                "likes": likes,
                                "uid": u,
                                "userpage": personalPage,
                                "imagefolder": userImageFolder,
                                "usericon": $.getUser(u).UserIcon
                            }).appendTo(name);
                        } else {
                            $.tmpl("fbmarkup", { "comment": comment,
                                "user": $.getUser(u).Name,
                                "date": dt,
                                "likes": likes,
                                "uid": u,
                                "userpage": personalPage,
                                "imagefolder": userImageFolder,
                                "usericon": $.getUser(u).UserIcon
                            }).appendTo(name);
                        }
                        first++;
                    });
                    first = 0;
                });

                setTimeout("$.pushLoader()", 2000); //Avakta tills listan av kommentarer är komplett.
            },
            error: function (data) { }
        });
    },

    /*
    TODO: Push and loads messages from server on scroll to end of page.
    */
    pushLoader: function () {
        var alreadyloading = false;

        $(window).scroll(function () {
            if ($('body').height() <= ($(window).height() + $(window).scrollTop())) {
                if (alreadyloading == false) {
                    var id = $("#thewall textarea:last").attr("id").replace('txt', '');
                    var url = wallNextUrl + id;

                    if (last != url && autoload) {
                        alreadyloading = true;
                        $.loadWall(url, id, true);
                        alreadyloading = false;
                    }
                }
            }
        });
    },

    lastID: function (id) {
        return id;
    },

    /*
    All templates below. Requires Templates from JQuery (jquery.tmpl.js)
    */

    /*Wall data template*/
    getWallTemplate: function () {
        return '<div class="fb-wall-box" id="fb${id}"><a href="${userpage}?user=${uid}"><img class="fb-wall-avatar" src="${imagefolder}${usericon}" /></a>'
		 + '<div class="fb-wall-data">'
		 + '    <span class="fb-wall-message"><a href="${userpage}?user=${uid}" class="fb-wall-message-from">${user}</a><br/> ${post}</span>'
		 + '    <span class="fb-wall-date">${date}</span>'
		 + '    <div id="comments${id}"></div>'
		 + '    <a href="#${id}" onclick="$(\'#commentbox${id}\').toggle(); $(\'#post${id}\').addClass(\'activeButton\');">Kommentera</a>'
		 + '    <div class="commentbox hide" id="commentbox${id}">'
		 + '    	<textarea id="txt${id}" class="defaultText commentTxt" title="Kommentera"></textarea>'
		 + '		<a class="smallbutton" id="post${id}" onclick="$.createComment( $(\'#txt${id}\').val() ,${id})">Skicka</a><div class="fb-wall-clean"></div>'
		 + '    </div>'
		 + '</div></div><div class="fb-wall-clean"></div>';
    },



    /*Comment data template*/
    getCommentTemplate: function () {
        return ''
			+ '<span class="fb-wall-comment">'
			+ ' <a href="${userpage}?user=${uid}" class="fb-wall-comment-avatar"><img src="${imagefolder}${usericon}" /></a>'
			+ ' <span class="fb-wall-comment-message">'
			+ '		<a class="fb-wall-comment-from-name" href="${userpage}?user=${uid}">${user}</a> '
			+ '		${comment}'
			+ '		<span class="fb-wall-comment-from-date">${date}</span>'
			+ '	</span>'
			+ '</span>';
    },

    /*Comment data template*/
    getCommentFirstTemplate: function () {
        return '<span class="fb-wall-likes"></span>'
			+ '<span class="fb-wall-comment">'
			+ ' <a href="${userpage}?user=${uid}" class="fb-wall-comment-avatar"><img src="${imagefolder}${usericon}" /></a>'
			+ ' <span class="fb-wall-comment-message">'
			+ '		<a class="fb-wall-comment-from-name" href="${userpage}?user=${uid}">${user}</a> '
			+ '		${comment}'
			+ '		<span class="fb-wall-comment-from-date">${date}</span>'
			+ '	</span>'
			+ '</span>';
    },

    getMessages: function () {
        //$("#announce").text("0");

    },

    messageAnnounce: function (time) {
        //Receive message from database each 5 seconds" 

        setInterval(function () {
            var randomNum = Math.ceil(Math.random() * 10); //Randowm 10 messages (for test)
            $("#announce").text(randomNum);
            $("#announce").click(function () {
                $.message();
            });
        }, 5000);
    },

    message: function () {
        $("#announceBox").toggle();
    }

});	


jQuery.extend({
	//Expands and toggles any list 
	expandToggle: function(item){
			
	}
});


$(document).ready(function () {
  
    try {
  
        $.initNwp();
    } catch (e) { alert(e) }
});





































