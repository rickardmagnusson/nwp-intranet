﻿//<script language="javascript">
  $(document).ready(function () {
        $.ajax({
            url: "http://ssoservice.m33.se/SSOService.svc/user/RequestToken?callback=?",
            data: {},
            dataType: 'jsonp',
            type: "GET",
            crossDomain: !$.browser.msie,
            beforeSend: function (request) {
            },
            success: function (ssodata, textStatus, request) {

                var logonPage = '@Url.Action("LogOn", "Account")';
                if (ssodata.Status == 'SUCCESS') {
                    var redirect = '@Request["redirectUrl"]';
                    if (redirect == '')
                        redirect = '@Url.Action("Index", "Cm", "")';

                    // verify the token is genuine
                    $.post('@Url.Action("Authenticate", "Account")',
                    { token: ssodata.Token, createPersistentCookie: true },
                        function (data) {
                            if (data.result == 'SUCCESS')
                                document.location = redirect;
                            else
                                document.location = logonPage;
                        }, 'json').error(function (error, text, r) {
                            alert(error.responseText);
                        });
                } else {
                    // user needs to logon to SSO service
                    document.location.href = logonPage;
                }
            },
            error: function (request, status, error) {
                alert(error);
            }
        });
    });
//</script>
			