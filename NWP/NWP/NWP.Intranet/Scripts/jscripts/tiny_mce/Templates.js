﻿var tinyMCETemplateList = [
// Name, URL, Description
    ["Grön box", "/Scripts/jscripts/tiny_mce/Templates/Greenbox.htm", "Green box."],
    ["Orange box", "/Scripts/jscripts/tiny_mce/Templates/Orangebox.htm", "Orange box."],
    ["Gul box", "/Scripts/jscripts/tiny_mce/Templates/Yellowbox.htm", "Yellow box."],
    ["Blå box", "/Scripts/jscripts/tiny_mce/Templates/Bluebox.htm", "Blue box."],
    ["Svart box", "/Scripts/jscripts/tiny_mce/Templates/Blackbox.htm", "Black box."]
];
