﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Security;
using System.IO;
using System.ServiceModel.Web;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace Nwp.SSOServices.Utils
{
    public class ImageServer
    {
        public ImageServer() { }

        private string imageTemplate = "~/Logos/{0}";

        public Stream GetImage(string domain)
        {
            string template = string.Format(imageTemplate, domain);

            FileStream fs = File.OpenRead(HttpContext.Current.Server.MapPath(template));
            WebOperationContext.Current.OutgoingResponse.ContentType = "image/jpeg";

            return fs;
        }

        public Stream GetImage(string domain, int width, int height)
        {
            FileStream fs = File.OpenRead(HttpContext.Current.Server.MapPath(string.Format(imageTemplate, domain)));

            Image img = Image.FromStream(fs);
            Image a = resizeImage(img, new Size(width, height));
            Bitmap bitmap = new Bitmap(a, new Size(a.Width, a.Height));

            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            ms.Position = 0;
            WebOperationContext.Current.OutgoingResponse.ContentType = "image/jpeg";
            return ms;
        }

        public Size WithDimensions(int currW, int currH, int destW, int destH)
        {
            double multiplier = 0;
            Layout layout;

            if (currH > currW)
                layout = Layout.PORTRAIT;
            else
                layout = Layout.LANDSCAPE;

            switch (layout)
            {
                case Layout.PORTRAIT:
                    //calculate multiplier on heights
                    if (destH > destW)
                    {
                        multiplier = (double)destW / (double)currW;
                    }
                    else
                    {
                        multiplier = (double)destH / (double)currH;
                    }
                    break;
                case Layout.LANDSCAPE:
                    //calculate multiplier on widths
                    if (destH > destW)
                    {
                        multiplier = (double)destW / (double)currW;
                    }
                    else
                    {
                        multiplier = (double)destH / (double)currH;
                    }
                    break;
            }
            return new Size((int)(currW * multiplier), (int)(currH * multiplier));
        }


        public Image resizeImage(Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }

        internal enum Layout
        {
            LANDSCAPE,
            PORTRAIT
        }
    }
}