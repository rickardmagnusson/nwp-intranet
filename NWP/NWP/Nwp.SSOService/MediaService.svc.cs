﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Security;
using System.IO;
using System.ServiceModel.Web;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Nwp.SSOServices.Utils;

namespace Nwp.SSOServices
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MediaService : IMediaService, IMediaContractService
	{
        public Stream Logo(string domain)
        {
            var media = MediaToken.Find(m => m.Domain == domain);
            string imageTemplate = string.Format("~/Logos/{0}", media.Logo);
           
            FileStream fs = File.OpenRead(HttpContext.Current.Server.MapPath(imageTemplate));
            WebOperationContext.Current.OutgoingResponse.ContentType = "image/jpeg";
           
            return fs;
        }

        public Stream LogoScale(string domain, int w, int h)
        {
            return GetImage(domain, w, h);
        }

        public Stream GetImage(string domain, int width, int height)
        {
            var media = MediaToken.Find(m => m.Domain == domain);
            string imageTemplate = string.Format("~/Logos/{0}", media.Logo);

            FileStream fs = File.OpenRead(HttpContext.Current.Server.MapPath(imageTemplate));
            Image img = Image.FromStream(fs);

            Image a = resizeImage(img, new Size(width, height));

            Bitmap bitmap = new Bitmap(a, new Size(a.Width, a.Height));
          
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            ms.Position = 0;
            WebOperationContext.Current.OutgoingResponse.ContentType = "image/jpeg";
            return ms;
        }

        internal enum Layout
        {
            LANDSCAPE,
            PORTRAIT
        }

        public Size GenerateImageDimensions(int currW, int currH, int destW, int destH)
        {
            double multiplier = 0;
            Layout layout;

            if (currH > currW) 
                layout = Layout.PORTRAIT;
            else 
                layout = Layout.LANDSCAPE;

            switch (layout)
            {
                case Layout.PORTRAIT:
                    //calculate multiplier on heights
                    if (destH > destW) {
                        multiplier = (double)destW / (double)currW;
                    }else{
                        multiplier = (double)destH / (double)currH;
                    }
                    break;
                case Layout.LANDSCAPE:
                    //calculate multiplier on widths
                    if (destH > destW) {
                        multiplier = (double)destW / (double)currW;
                    }else{
                        multiplier = (double)destH / (double)currH;
                    }
                    break;
            }
            return new Size((int)(currW * multiplier), (int)(currH * multiplier));
        }


        private static Image resizeImage(Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }

        


        public List<MediaToken> MediaToken
        {
            get
            {
                var list = new List<MediaToken>
                {
                    //new MediaToken{ Domain="addprofile", Logo="new_wave_logotyp_add_profile_cmyk.jpg"},
                    new MediaToken{ Domain="arenareklam", Logo="new_wave_logotyp_arena_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="barekosport", Logo="new_wave_logotyp_bareko_sport_cmyk.jpg"},
                    new MediaToken{ Domain="bravoprofil", Logo="new_wave_logotyp_bravo_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="broderiet", Logo="new_wave_logotyp_broderiet_cmyk.jpg"},
                    new MediaToken{ Domain="chardenreklam", Logo="new_wave_logotyp_charden_reklam_cmyk.jpg"},
                    //new MediaToken{ Domain="clijofashion", Logo="new_wave_logotyp_clijo_fashion_cmyk.jpg"},
                    new MediaToken{ Domain="companyline", Logo="new_wave_logotyp_companyline_cmyk.jpg"},
                    new MediaToken{ Domain="daylight", Logo="new_wave_logotyp_daylight_cmyk.jpg"},
                    new MediaToken{ Domain="defactoyrkesklader", Logo="new_wave_logotyp_de_facto_yrkesklader_cmyk.jpg"},
                    new MediaToken{ Domain="felestad", Logo="new_wave_logotyp_felestad_cmyk.jpg"},
                    new MediaToken{ Domain="flinkreklam", Logo="new_wave_logotyp_flink_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="foretagsprofilen", Logo="new_wave_logotyp_foretagsprofilen_cmyk.jpg"},
                    new MediaToken{ Domain="gewepromotion", Logo="new_wave_logotyp_gewe_cmyk.jpg"},
                    new MediaToken{ Domain="industriprofil", Logo="new_wave_logotyp_industriprofil_cmyk.jpg"},
                    new MediaToken{ Domain="intersport", Logo="new_wave_logotyp_intersport_cmyk.jpg"},
                    new MediaToken{ Domain="jobb-fritid", Logo="new_wave_logotyp_jobb_&_fritid_cmyk.jpg"},
                    //new MediaToken{ Domain="json", Logo="new_wave_logotyp_json_cmyk.jpg"},
                    //new MediaToken{ Domain="justintime", Logo="new_wave_logotyp_just_in_time_cmyk.jpg"},
                    new MediaToken{ Domain="kents", Logo="new_wave_logotyp_kents_cmyk.jpg"},
                    new MediaToken{ Domain="kereklamtryck", Logo="new_wave_logotyp_ke_reklamtryck_cmyk.jpg"},
                    new MediaToken{ Domain="lhprofildesign", Logo="new_wave_logotyp_lh_profildesign_cmyk.jpg"},
                    new MediaToken{ Domain="lptreklam", Logo="new_wave_logotyp_lpt_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="meraprofil-reklam", Logo="new_wave_logotyp_mera_profil_&_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="meri", Logo="new_wave_logotyp_meri_cmyk.jpg"},
                    new MediaToken{ Domain="myshkin", Logo="new_wave_logotyp_myshkin_cmyk.jpg"},
                    new MediaToken{ Domain="newpromotion", Logo="new_wave_logotyp_new_promotion_cmyk.jpg"},
                    new MediaToken{ Domain="nobleart", Logo="new_wave_logotyp_noble_art_cmyk.jpg"},
                    new MediaToken{ Domain="nojab", Logo="new_wave_logotyp_nojab_cmyk.jpg"},
                    new MediaToken{ Domain="nordtrend", Logo="new_wave_logotyp_nordtrend_cmyk.jpg"},
                    new MediaToken{ Domain="olympus", Logo="new_wave_logotyp_olympus_cmyk.jpg"},
                    new MediaToken{ Domain="orebroscreen", Logo="new_wave_logotyp_orebro_screen_cmyk.jpg"},
                    new MediaToken{ Domain="pahlsonsreklam", Logo="new_wave_logotyp_pahlsons_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="produktpartners", Logo="new_wave_logotyp_produkt_partners_cmyk.jpg"},
                    new MediaToken{ Domain="profilesupport", Logo="new_wave_logotyp_profile_support_cmyk.jpg"},
                    new MediaToken{ Domain="protex", Logo="new_wave_logotyp_protex_cmyk.jpg"},
                    new MediaToken{ Domain="practive", Logo="new_wave_logotyp_pr_active_cmyk.jpg"},
                    new MediaToken{ Domain="reklamproffsen", Logo="new_wave_logotyp_reklamproffsen_cmyk.jpg"},
                    new MediaToken{ Domain="roupez", Logo="new_wave_logotyp_roupez_cmyk.jpg"},
                    new MediaToken{ Domain="screenbolaget", Logo="new_wave_logotyp_screenbolaget_cmyk.jpg"},
                    new MediaToken{ Domain="sormlindsreklam", Logo="new_wave_logotyp_sormlinds_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="sunscreen", Logo="new_wave_logotyp_sun_screen_cmyk.jpg"},
                    new MediaToken{ Domain="teamgamewi", Logo="new_wave_logotyp_team_gamewi_cmyk.jpg"},
                    new MediaToken{ Domain="tm", Logo="new_wave_logotyp_tm_cmyk.jpg"},
                    new MediaToken{ Domain="toolsskeppa", Logo="new_wave_logotyp_tools_skeppa_cmyk.jpg"},
                    new MediaToken{ Domain="topline", Logo="new_wave_logotyp_topline_cmyk.jpg"},
                    new MediaToken{ Domain="triffiq", Logo="new_wave_logotyp_triffiq_cmyk.jpg"},
                    new MediaToken{ Domain="trimdon", Logo="new_wave_logotyp_trimdon_cmyk.jpg"},
                    new MediaToken{ Domain="trycket", Logo="new_wave_logotyp_trycket_cmyk.jpg"},
                    new MediaToken{ Domain="wallex", Logo="new_wave_logotyp_wallex_cmyk.jpg"},
                    new MediaToken{ Domain="waypoint", Logo="new_wave_logotyp_waypoint_cmyk.jpg"},
                    new MediaToken{ Domain="yrkesoprofilklader", Logo="new_wave_logotyp_yrkes_o_profil_klader_cmyk.jpg"},
                    new MediaToken{ Domain="www", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="sagaform", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="dochj", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="toppoint", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="projob", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="newwavesports", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="jobman", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="orreforskostaboda", Logo="new_wave_logotyp_www_cmyk.jpg"}

                };
                return list;
            }
        }
    }

    
}
