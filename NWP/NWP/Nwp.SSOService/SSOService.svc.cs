﻿using System;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Security;
using Nwp.SSOServices.Data;

namespace Nwp.SSOServices
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class SSOService : ISSOService, ISSOPartnerService
	{
		#region ISSOService Members

		/// <summary>
		/// Returns the client's current identity token so it can
		/// assert its identity to a partner application
		/// </summary>
		/// <returns>encrypted identity token</returns>
		public SSOToken RequestToken()
		{
			// default response
			SSOToken token = new SSOToken
			{
				Token = string.Empty,
				Status = "DENIED"
			};

			// verify we've already authenticated the client
			if (HttpContext.Current.Request.IsAuthenticated)
			{
				// get the current identity
				FormsIdentity identity = (FormsIdentity)HttpContext.Current.User.Identity;

				// we'll send the client its own FormsAuthenticationTicket, but 
				// we'll encrypt it so only we can read it when the partner app
				// needs to validate who the client is through our service
				token.Token = FormsAuthentication.Encrypt(identity.Ticket);
				token.Status = "SUCCESS";
			}

			return token;
		}

		/// <summary>
		/// Log client out of SSO service
		/// </summary>
		/// <returns>true if successful</returns>
		public bool Logout()
		{
			HttpContext.Current.Session.Clear();
			FormsAuthentication.SignOut();
			HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName);
			cookie.Expires = DateTime.Now.AddDays(-10000.0);
			HttpContext.Current.Response.Cookies.Add(cookie);

		    FormsAuthentication.SignOut();

			return true;
		}

		/// <summary>
		/// Log client into SSO service
		/// </summary>
		/// <returns></returns>
		public SSOToken Login(string username, string password)
		{
			// default response
			SSOToken token = new SSOToken
			{
				Token = string.Empty,
				Status = "DENIED"
			};

			// authenticate user
            if (GetUser(username, password))
			{
				// mock data to simulate passing around additional data
				Guid temp = Guid.NewGuid();

				// manage cookie lifetime
				DateTime issueDate = DateTime.Now;
				DateTime expireDate = issueDate.AddMonths(1);

				// create the ticket and protect it
				FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, username, issueDate, expireDate, true, temp.ToString());
				string protectedTicket = FormsAuthentication.Encrypt(ticket);

				// save the protected ticket with a cookie
				HttpCookie authorizationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, protectedTicket);
				authorizationCookie.Expires = expireDate;

				// protect the cookie from session hijacking
				authorizationCookie.HttpOnly = true;

				// write the cookie to the response stream
				HttpContext.Current.Response.Cookies.Add(authorizationCookie);

				// update the response to indicate success
				token.Status = "SUCCESS";
				token.Token = protectedTicket;
			}

			return token;
		}

        private bool GetUser(string username, string password)
        {
            return new DataModel().GetUser(username, password);
        }

		/// <summary>
		/// Confirms the client is signed into SSO service
		/// </summary>
		/// <param name="token">the client's asserted identity token</param>
		/// <returns>user's identity information</returns>
		public SSOUser ValidateToken(string token)
		{
			try
			{
				// if we can decrypt the ticket then it wasn't tampered with
				// and token is valid
				FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(token);

				return new SSOUser { 
					Username = ticket.Name, 
					SessionToken = new Guid(ticket.UserData) 
				};
			}
			catch
			{
				// validation failed
				return new SSOUser { 
					Username = string.Empty, 
					SessionToken = Guid.Empty 
				};
			}
		}

		#endregion
	}
}
