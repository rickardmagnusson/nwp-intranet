﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using Nwp.SSOServices.JSONP;
using System.Collections.Generic;
using System.IO;

namespace Nwp.SSOServices
{
	[ServiceContract]
	public interface IMediaService
	{
        [OperationContract]
        [WebGet(UriTemplate = "/Logo?domain={domain}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        [JSONPBehavior(callback = "callback")]
        Stream Logo(string domain);

        [OperationContract]
        [WebGet(UriTemplate = "/LogoScale?domain={domain}&w={w}&h={h}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        [JSONPBehavior(callback = "callback")]
        Stream LogoScale(string domain, int w, int h);
	}

    [DataContract]
    public class MediaToken
    {
        [DataMember]
        public string Domain { get; set; }

        [DataMember]
        public string Logo { get; set; }
    }

    [ServiceContract]
    public interface IMediaContractService
    {
        [OperationContract]
        Stream Logo(string domain);

        [OperationContract]
        Stream LogoScale(string domain, int w, int h);
    }
	
}
