﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using Nwp.SSOServices.JSONP;

namespace Nwp.SSOServices
{
	// NOTE: If you change the interface name "ISSOService" here, you must also update the reference to "ISSOService" in Web.config.
	[ServiceContract]
	public interface ISSOService
	{
		[OperationContract]
		[WebGet(UriTemplate="/RequestToken", BodyStyle=WebMessageBodyStyle.WrappedRequest, ResponseFormat=WebMessageFormat.Json)]
		[JSONPBehavior(callback = "callback")]
		SSOToken RequestToken();

		[OperationContract]
		// javascript can't post w/ jsonp - has to be WebGet
		[WebGet(UriTemplate="/Login?username={username}&password={password}", BodyStyle = WebMessageBodyStyle.WrappedResponse, ResponseFormat = WebMessageFormat.Json)]
		[JSONPBehavior(callback = "callback")]
		SSOToken Login(string username, string password);

		[OperationContract]
		[WebGet(UriTemplate = "/Logout", BodyStyle = WebMessageBodyStyle.WrappedResponse, ResponseFormat = WebMessageFormat.Json)]
		[JSONPBehavior(callback = "callback")]
		bool Logout();
	}

	[ServiceContract]
	public interface ISSOPartnerService
	{
		[OperationContract]
		SSOUser ValidateToken(string token);
	}

	[DataContract]
	public class SSOToken
	{
		[DataMember]
		public string Token { get; set; }

		[DataMember]
		public string Status { get; set; }
	}

	[DataContract]
	public class SSOUser
	{
		[DataMember]
		public string Username { get; set; }

		[DataMember]
		public Guid SessionToken { get; set; }
	}
}
