﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Security;

namespace Nwp.SSOServices
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WallService : IWallService
	{
        /// <summary>
        /// Get all walldata for the current user
        /// </summary>
        /// <returns>List of MessageToken</returns>
        public IEnumerable<MessageToken> GetWallData()
        {
            return new List<MessageToken> { 
                    new MessageToken { Message = "Detta är ett test!", User = "1" },
                    new MessageToken { Message = "Detta är ett test nummer 2!", User = "2" },
                    new MessageToken { Message = "Detta är ett test nummer 3!", User = "5" }
            };
        }

        public MessageToken CreateMessage(string message, string user)
        {
            //Possible insert here, reply with the message to confirm everything is ok.
            var m = new MessageToken { Message = message, User = "1" };
            return m;
        }
    }
}
