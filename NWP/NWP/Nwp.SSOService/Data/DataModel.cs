﻿using MySql.Data.MySqlClient;

namespace Nwp.SSOServices.Data
{
    public class DataModel
    {
        private readonly MySqlConnection _conn;
        private const string ConnectionString = "Server=127.0.0.1;Database=SSO;Uid=root;Pwd=;";

        public DataModel()
        {
            _conn = new MySqlConnection(ConnectionString);
            Connection.Open();
        }

        private MySqlConnection Connection
        {
            get { return _conn; }
        }

        public bool GetUser(string username, string password)
        {
            string query = string.Format("SELECT * FROM Users WHERE UserName='{0}' AND Password='{1}'", username, password);
            var cmd = new MySqlCommand(query,Connection);
            MySqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
                return true;
            return false;
        }
    }
}