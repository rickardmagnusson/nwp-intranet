﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using Nwp.SSOServices.JSONP;
using System.Collections.Generic;

namespace Nwp.SSOServices
{
	[ServiceContract]
	public interface IWallService
	{
		[OperationContract]
		[WebGet(UriTemplate="/GetWallData", BodyStyle=WebMessageBodyStyle.WrappedRequest, ResponseFormat=WebMessageFormat.Json)]
		[JSONPBehavior(callback = "callback")]
		IEnumerable<MessageToken> GetWallData();

        [OperationContract]
        [WebGet(UriTemplate = "/CreateMessage?message={message}&user={user}", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        [JSONPBehavior(callback = "callback")]
        MessageToken CreateMessage(string message, string user);
	}

    [DataContract]
    public class MessageToken
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string User { get; set; }
    }
	
}
