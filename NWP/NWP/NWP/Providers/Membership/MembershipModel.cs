﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Models;

namespace NWP.Providers.Membership
{
    /// <summary>
    /// Membership actions and helpers.
    /// </summary>
    public class MembershipModel
    {
        public void CreateRole(string rolename)
        {
            var role = ActiveRecordMediator<NWP.Models.Roles>.FindOne(Restrictions.Eq("RoleName", rolename));
            if (role != null) return;
            var newRole = new NWP.Models.Roles {RoleName= rolename};
            ActiveRecordMediator.Create(newRole);
        }


        /// <summary>
        /// Get all roles for a user.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public List<string> GetRolesForUser(string username)
        {
            var user = ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("UserName", username.ToLower()));
            var userInrole = ActiveRecordMediator<UserInRole>.FindAll(Restrictions.Eq("UserId", user.Id));

            List<string> rolenames= new List<string>();
            foreach(var role in userInrole) {
                var r = ActiveRecordMediator<NWP.Models.Roles>.FindOne(Restrictions.Eq("Id", role.RoleId));
                rolenames.Add(r.RoleName);
            }
            return rolenames;
        }

        /// <summary>
        /// Roles
        /// </summary>
        /// <returns>Get all roles in application</returns>
        public List<string> GetAllRoles()
        {
            return ActiveRecordMediator<NWP.Models.Roles>.FindAll().Select(role=> role.RoleName).ToList();
        }


        /// <summary>
        /// Get a user object
        /// </summary>
        /// <param name="username">Username to get, isonline</param>
        /// <param name="isOnline"></param>
        /// <returns></returns>
        public MembershipUser GetUser(string username, bool isOnline)
        {
            User user = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", username.ToLower()));
            if (user != null)
            {
                var memUser = new MembershipUser("AccountMembershipProvider",
                                               username, user.Id, user.Email,
                                               string.Empty, string.Empty,
                                               true, false, DateTime.MinValue,
                                               DateTime.MinValue,
                                               DateTime.MinValue,
                                               DateTime.Now, DateTime.Now);
                return memUser;
            }
            return null;
        }

        /// <summary>
        /// Get User object
        /// </summary>
        /// <param name="providerUserKey">Provider key to get user from</param>
        /// <param name="userIsOnline">If user is online</param>
        /// <returns></returns>
        public MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            User user = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("Id", Convert.ToInt32(providerUserKey)));
            if (user != null)
            {
                var memUser = new MembershipUser("AccountMembershipProvider",
                                                user.UserName, user.Id, user.Email,
                                                string.Empty, string.Empty,
                                                true, false, DateTime.MinValue,
                                                DateTime.MinValue,
                                                DateTime.MinValue,
                                                DateTime.Now, DateTime.Now);
                return memUser;
            }
            return null;
        }

        /// <summary>
        /// Delete a user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="deleteAllRelatedData"></param>
        /// <returns></returns>
        public bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            User user = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", username.ToLower()));
            if (user != null){
                user.Delete();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get user by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string GetUserNameByEmail(string email)
        {
            User user = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("Email", email.ToLower()));
            if (user != null)
            {
                return user.UserName;
            }
            return null;
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public MembershipUserCollection GetAllUsers()
        {
            var users = ActiveRecordMediator<User>.FindAll().ToList();
            var collection = new MembershipUserCollection();

            foreach(var user in users) {

                var memUser = new MembershipUser("AccountMembershipProvider",
                                                   user.UserName, user.Id, user.Email,
                                                   string.Empty, string.Empty,
                                                   true, false, DateTime.MinValue,
                                                   DateTime.MinValue,
                                                   DateTime.MinValue,
                                                   DateTime.Now, DateTime.Now);
                    collection.Add(memUser);
 
                }
            return collection;
        }

   
    }
}