﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using NWP.Models;

namespace NWP.Providers.Membership
{
    /// <summary>
    /// RoleProvider.
    /// Handle all roles in application.
    /// </summary>
    public class AccountRoleProvider : RoleProvider
    {
        private MembershipModel _model;
        public AccountRoleProvider()
        {
                _model = new MembershipModel();
        }

        private MembershipModel Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value;
            }
        }


        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        private string name = "Nwp.Intranet";
        public override string ApplicationName
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public override void CreateRole(string roleName)
        {
            Model.CreateRole(roleName);
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            return Model.GetAllRoles().ToArray();
        }

        public override string[] GetRolesForUser(string username)
        {
            return Model.GetRolesForUser(username).ToArray();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            var roles = GetRolesForUser(username);
            return roles.Any(role => role == roleName);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}