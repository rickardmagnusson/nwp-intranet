﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace NWP.Providers.Membership
{
    /// <summary>
    /// Custom MembershipProvider
    /// Does not handle passwords, only UserNames and email addresses.
    /// </summary>
    public class AccountMembershipProvider : MembershipProvider
    {
        private MembershipModel _model;
        public AccountMembershipProvider()
        {
                _model = new MembershipModel();
        }

        private MembershipModel Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value;
            }
        }

        private string name = "Nwp.Intranet";
        public override string ApplicationName
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        /*
            All changes for a user must also be made in SSOService
         */

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            //Need to be changed in SSO
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            //Need to be changed in SSO
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
           return Model.DeleteUser(username, deleteAllRelatedData);
        }

        public override bool EnablePasswordReset
        {
            get { return false; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return false; }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            totalRecords = 1000;
            return Model.GetAllUsers();
        }

        public override int GetNumberOfUsersOnline()
        {
            return 10;
        }

        public override string GetPassword(string username, string answer)
        {
            //Need to be set in SSO, not here
            return Model.GetUser(username,false).UserName;
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            return Model.GetUser(username, userIsOnline);
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            return Model.GetUser(providerUserKey, userIsOnline);
        }

        public override string GetUserNameByEmail(string email)
        {
            return Model.GetUserNameByEmail(email);
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return 10; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return 0; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return 5; }
        }

        public override int PasswordAttemptWindow
        {
            get { return 10; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return MembershipPasswordFormat.Encrypted; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return ""; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return true; }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            return Model.GetUser(username, true)!=null;
        }
    }
}