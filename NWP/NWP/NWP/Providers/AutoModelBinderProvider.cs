﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace NWP.Providers
{
    /// <summary>
    /// Only a test class. Not users.
    /// </summary>
    [Obsolete("Do not use this class it will be removed")]
    public class AutoModelBinderProvider : IModelBinderProvider
    {
        /// <summary>
        /// Cached collection of existing binders
        /// </summary>
        static Dictionary<Type, IModelBinder> Binders { get; set; }

        static object syncLock = new object();

        /// <summary>
        /// Static constructor to initialize Binders
        /// </summary>
        static AutoModelBinderProvider()
        {
            Binders = new Dictionary<Type, IModelBinder>();
        }

        /// <summary>
        /// Manually register ModelBinders for some strange reason
        /// </summary>
        /// <param name="type">The type of object we're binding to</param>
        /// <param name="instance">The instance of our Model Binder</param>
        public static void RegisterType(Type type, IModelBinder instance)
        {
            lock (syncLock)
            {
                if (Binders.ContainsKey(type))
                    Binders[type] = instance;
                else
                    Binders.Add(type, instance);
            }
        }

        /// <summary>
        /// Interface for IModelBinderProvider.GetBinder.
        /// Checks Binders for the passed type else loop through the assembly and look for a binder
        /// </summary>
        /// <param name="modelType">The type of object we're going to bind to</param>
        /// <returns>An instance of IModelBinder from our list of found Binders.</returns>
        public IModelBinder GetBinder(Type modelType)
        {
            //Check to see if a type was already bound.
            lock (syncLock)
            {
                if (Binders.ContainsKey(modelType))
                    return Binders[modelType];

                //Check the assembly and look for a <name>ModelBinder
                Assembly assembly = Assembly.GetExecutingAssembly();
                Type[] types = assembly.GetTypes();
                foreach (Type type in types)
                {
                    //Convention over configuration
                    if (type.Name == modelType.Name + "ModelBinder")
                    {
                        var instance = (IModelBinder)assembly.CreateInstance(type.FullName);
                        Binders.Add(modelType, instance);
                        return instance;
                    }
                }
            }
            return null;
        }
    }
}