﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NWP.Models;

namespace NWP.Providers
{
    /// <summary>
    /// Test class of dynamic model binding. Not used.
    /// </summary>
    [Obsolete("Do not use this class it will be removed")]
    public class PageModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var pageId = bindingContext.ValueProvider.GetValue("PageId");

            Page page = null;

            if (pageId != null)
            {
                int id;
                if (int.TryParse(pageId.AttemptedValue, out id))
                {
                    page = new Page(id);
                }
            }

            return page;
        }
    }
}