﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.ActiveRecord.Framework.Config;

namespace NWP.Configuration
{
    /// <summary>
    /// Contains helper methods for get corrent database and ConnectionString
    /// </summary>
    public static class FileUtils
    {
        /// <summary>
        /// Get App_Data folder path
        /// </summary>
        /// <returns>Absolute path to App_Data folder</returns>
        public static string GetAppDataFolder()
        {
            var dir = HttpContext.Current.Server.MapPath("~/");
            dir = System.IO.Path.Combine(dir, "App_Data");
            return dir;
        }


        /// <summary>
        /// Get App_Data folder path
        /// </summary>
        /// <returns>Absolute path to App_Data folder</returns>
        public static string GetBaseDir(string path)
        {
            var dir = HttpContext.Current.Server.MapPath("~/");
            dir = System.IO.Path.Combine(dir, path);
            return dir;
        }

        /// <summary>
        /// Creates a ConnectionString
        /// </summary>
        /// <param name="database">The name of the database</param>
        /// <returns>Path to database</returns>
        public static string ConnectionString(string database)
        {
            return string.Format("{0}\\{1}.sdf", GetAppDataFolder(), database);
        }


        /// <summary>
        /// Data Source path
        /// </summary>
        /// <param name="dbtype"> </param>
        /// <param name="database">Database name</param>
        /// <returns>A complete Connectionstring for SqlCE (Data Source=[path to db])</returns>
        public static string ConnectionStringCombined(DatabaseType dbtype, string database)
        {
            switch (dbtype)
            {
                case DatabaseType.MsSqlCe:
                    return string.Format("Data Source={0}\\{1}.sdf", GetAppDataFolder(), database);
                case DatabaseType.MySql5:
                    return Settings.CreateInstance().GetValue("ConnectionStringMySql5");
            }
            return null;
        }


        public static DatabaseType CurrentDatabase()
        {
            var dbtype = Settings.CreateInstance().GetValue("DatabaseType");
            switch (dbtype)
            {
                case "MsSqlCe":
                    return DatabaseType.MsSqlCe;
                case "MySql5":
                    return DatabaseType.MySql5;
            }
            return DatabaseType.MsSqlCe;
        }

        private static string defName = string.Empty;
        /// <summary>
        /// Returns the default installed database name.
        /// </summary>
        public static string DefaultDatabaseName
        {
            get
            {
                if (defName != string.Empty)
                    return defName;
                return Settings.CreateInstance().GetValue("DataBaseName");
            }
            set { defName = value; }
        }
    }
}