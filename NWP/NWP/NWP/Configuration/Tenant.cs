﻿using Castle.ActiveRecord;
using NWP.ContentManager;

namespace NWP.Configuration
{
    /// <summary>
    /// Tenant holds all information about a domain.
    /// </summary>
    [ActiveRecord]
    public class Tenant : DataPersister<Tenant>
    {
        public Tenant() : base(){}

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = true)]
        public string Name { get; set; }

        [Property(NotNull = true)]
        public string Domain { get; set; }

        [Property(NotNull = true)]
        public bool Active { get; set; }

        [Property(NotNull = true)]
        public string LocaleID { get; set; }

        [Property(NotNull = true)]
        public string Theme { get; set; }
    }
}