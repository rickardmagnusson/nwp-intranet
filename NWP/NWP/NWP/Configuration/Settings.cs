﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NWP.ContentManager;

namespace NWP.Configuration
{
    /// <summary>
    /// Settings for this application. Invoked at startup.
    /// </summary>
    public class Settings : DataManager<Settings.Data>
    {
        public Settings() : base(){
            
        }

        private static Settings _instance;
        public static Settings CreateInstance()
        {
            if (_instance == null)
            {
                _instance = new Settings();
                _instance.LoadData();
            }

            return _instance;
        }

        public List<ValuePair> ValuePairs
        {
            get { return data.ValuePairs; }
            set { data.ValuePairs = value; }
        }

        public string GetValue(string key)
        {
            base.LoadData();
            return data.ValuePairs.Find(k => k.Key.ToLower()==key.ToLower()).Value;
        }

        public void SetValue(string key, string value)
        {
            var val = data.ValuePairs.Find(d => d.Key == key);
            if (val == null)
                data.ValuePairs.Add(new ValuePair { Key = key, Value = value });
            else
                val.Value = value;
            SaveData();
        }

        protected override string Config()
        {
            return "Settings";
        }

        public class Data
        {
            public List<ValuePair> ValuePairs = new List<ValuePair>();
        }

        public class ValuePair
        {
            public string Key;
            public string Value;
        }

        //Only save noncritical data in this file.
        public void LoadDefaults()
        {
            var settings = new Settings();
            settings.SetValue("DatabaseType", "MySql5");
            settings.SetValue("DatabaseName", "nwp");
            settings.SetValue("ConnectionStringSqlCe", "Data Source=~/App_Data/Nwp.sdf");
            settings.SetValue("DataBaseExist", "true");
            settings.SetValue("LocaleID", "sv-SE");
            settings.SetValue("DefaultDomain", "www");
            settings.SetValue("ConnectionStringMySql5", "Server=127.0.0.1;Database=nwp;Uid=root;Pwd=;");
            settings.SetValue("SSODomain", "http://ssoservice.newwaveprofile.com");
        }
    }
}