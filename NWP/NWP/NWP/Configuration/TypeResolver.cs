﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NWP.ContentManager;

namespace NWP.Configuration
{
    /// <summary>
    /// Find and loads specific types in this assembly.
    /// </summary>
    public class TypeResolver
    {
        /// <summary>
        /// Get all installed dependencies
        /// </summary>
        /// <param name="installed">Reference List</param>
        public void GetInstalledTypes(out List<Type> installed)
        {
            var types = GetType().Assembly.GetTypes();
            var list = types.Where(t => t.IsClass && (t.GetInterface("IDependency") != null) && (!t.IsAbstract)).ToList();
            list.AddRange(types.Where(t => t.IsClass && (t.GetInterface("IWidget") != null) && (!t.IsAbstract)).ToList());
            installed = list;
        }


        /// <summary>
        /// Creates a List of Types from the specified type
        /// </summary>
        /// <param name="installed">List reference</param>
        /// <param name="type">string type of class to get</param>
        public void GetInstalledTypes(out List<Type> installed, string type)
        {
            var types = GetType().Assembly.GetTypes();
            var list = types.Where(t => t.IsClass && (t.GetInterface(type) != null) && (!t.IsAbstract)).ToList();
            installed = list;
        }


        /// <summary>
        /// Creates a List of T from the speified class
        /// </summary>
        /// <typeparam name="T">Object T</typeparam>
        /// <param name="installed">List reference</param>
        /// <param name="type">Type of class to get</param>
        public void GetInstalledTypesByClass<T>(out List<T> installed, Type type) where T : class
        {
            var types = GetType().Assembly.GetTypes();
            var list = types.Where(t => t.IsClass && (t.IsAssignableFrom(type)) && (!t.IsAbstract)).ToList();
            installed = list.Cast<T>().ToList();
        }



        /// <summary>
        /// List of Widget types
        /// </summary>
        /// <returns>List of Widget types</returns>
        public List<Type> GetAllWidgets()
        {
            var list = new List<Type>();
            var types = GetType().Assembly.GetTypes();
            list.AddRange(types.Where(t => t.IsClass && (t.GetInterface("IWidget") != null) && (!t.IsAbstract)));
            return list;
        }


        /// <summary>
        /// Get's all Types that inherit IDependency or IWidget
        /// </summary>
        /// <returns>Array of Types</returns>
        public Type[] GetTypes()
        {
            var list = new List<Type>();
            var types = GetType().Assembly.GetTypes();
            list.AddRange(types.Where(t => t.IsClass && (t.GetInterface("IDependency") != null) && (!t.IsAbstract)));
            list.AddRange(types.Where(t => t.IsClass && (t.GetInterface("IWidget") != null) && (!t.IsAbstract)));
            return list.ToArray();
        }


        /// <summary>
        /// Get's all Types that inherit IDependency or IWidget and not in TrashBin.
        /// </summary>
        /// <returns>Array of Types</returns>
        public Type[] GetValidTypes()
        {
            var list = new List<Type>();
            var types = GetType().Assembly.GetTypes();
            list.AddRange(types.Where(t => t.IsClass && (t.GetInterface("IDependency") != null) && (!t.IsAbstract) && t.GetProperty("TrashBin")!=null));
            list.AddRange(types.Where(t => t.IsClass && (t.GetInterface("IWidget") != null) && (!t.IsAbstract)));
            return list.ToArray();
        }
    }

}