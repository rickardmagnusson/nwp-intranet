﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

/// <summary>
/// Contains static settings.
/// </summary>
public static class SettingsService
{
    /// <summary>
    /// Return the current SSODomain for this application.
    /// </summary>
    public static string SSOService
    {
        get{return WebConfigurationManager.AppSettings["SSODomain"];}
    }

    /// <summary>
    /// Return the current admin theme for this application.
    /// </summary>
    public static string AdminTheme
    {
        get { return WebConfigurationManager.AppSettings["AdminTheme"]; }
    }

    /// <summary>
    /// Return the current admin theme for this application.
    /// </summary>
    public static string AdminLayout
    {
        get { return WebConfigurationManager.AppSettings["AdminLayout"]; }
    }

    /// <summary>
    /// Returns the value from current key.
    /// </summary>
    public static string GetValue(string key)
    {
        return WebConfigurationManager.AppSettings[key];
    }
}
