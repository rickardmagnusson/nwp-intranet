﻿
using System;
using System.Data.SqlServerCe;
using System.IO;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using NWP.Models;
using NWP.Schema;
using NWP.Widgets;
using Castle.ActiveRecord.Framework.Internal;
using NHibernate.Tool.hbm2ddl;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace NWP.Configuration
{
    /// <summary>
    /// Host configuration and setup.
    /// </summary>
    public static class Host
    {
        private static string Database { get; set; }

        /// <summary>
        /// Init application and startup.
        /// </summary>
        public static void Init()
        {
            var config = new InPlaceConfigurationSource();
            var dbType = FileUtils.CurrentDatabase();
            var database = Settings.CreateInstance().GetValue("DatabaseType");
            Database = FileUtils.DefaultDatabaseName;

            switch (database)
            {
                case "SqlCE":
                    config.Add(typeof(ActiveRecordBase), new Descriptor(DatabaseType.MsSqlCe, FileUtils.ConnectionStringCombined(dbType,Database)));
                    dbType = DatabaseType.MsSqlCe;
                    break;
                case "MySql5":
                    config.Add(typeof(ActiveRecordBase), new Descriptor(DatabaseType.MySql5, FileUtils.ConnectionStringCombined(dbType,Database)));
                    dbType = DatabaseType.MySql5;
                    break;
            }

            config.Searchable = true;
            config.IsRunningInWebApp = true;
            config.VerifyModelsAgainstDBSchema = true;


            //Try init ActiveRecord and also check for changes in schema.
            try{
                    ActiveRecordStarter.Initialize(config, new TypeResolver().GetTypes());
            }
            catch //By using a default exception we can catch the error and continue our updates. Do not use any other exceptiontypes here, it will fail load the application.
            {
                ActiveRecordStarter.UpdateSchema();
                //CreateOrUpdate(dbType);
            }

        }


        /// <summary>
        /// Setup default data
        /// </summary>
        public static void SetupInitialData()
        {
            var tenant = new Tenant{ Id=1, Name="New Wave Profile", Active = true, Domain = "www", LocaleID = "sv-se", Theme = "www"};
            tenant.Create();

            var page = new Page
            {
                Active = true,
                Domain = "www",
                Layout = "_Layout.Home",
                ParentId = 0,
                Route = "",
                Position = "0",
                MetaDescription = "",
                MetaTitle = "",
                MetaKeyWords = "",
                SubText = "Startsidan",
                Created = DateTime.Now.ToString(),
                Title = "New Page",
                TrashBin = false,
                Visible = true,
                UseDate = false,
                IsGlobal = true
            };
            page.Create();

            var content = new Content
            {
                Title = "Welcome to the Intranet",
                Html = "<p>This is the default setup text and can be deleted or changed below administration. A admin account has been created for you. And we created admin, users role by default.</p>",
                PageId = 1,
                Zone = "CONTENT",
                IsGlobal = true,
                Active = false,
                Owner = 1,
                Created = DateTime.Now,
                Domain = "www",
                SortOrder = "1",
                TrashBin = false    
            };
            content.Create();

            //Setup default roles
            var roles = new Roles { Id= 1, RoleName = "admin", TrashBin=false };
            roles.Create();

            var userrole = new Roles { Id= 1, RoleName = "users", TrashBin=false };
            userrole.Create();

            //Create admin account, remember that admin needs to be added in SSO before login is enabled.
            var admin = new User { Active = true, Company = "www", CompanyName = "Admins", DisplayName = "Administrator", Email = "youremail", TrashBin = false, Roles = new System.Collections.Generic.List<string> { "admin", "users"} };
            admin.Create();
        }

        /// <summary>
        /// Determine if a domain shuld be setup or just it. Also checks for database updates.
        /// </summary>
        /// <param name="dbType"></param>
        private static void CreateOrUpdate(DatabaseType dbType)
        {
            switch(dbType)
            {
                case DatabaseType.MsSqlCe:
                    if (!DataBaseExist())
                        CreateDatabase(DatabaseType.MsSqlCe);
                    else
                        ActiveRecordStarter.UpdateSchema();
                    break;
                case DatabaseType.MySql5:
                    if (!Convert.ToBoolean(Settings.CreateInstance().GetValue("DataBaseExist")))
                        CreateDatabase(DatabaseType.MySql5);
                    else
                        ActiveRecordStarter.UpdateSchema();
                    break;
            }
        }

        /// <summary>
        /// Create a new database.
        /// </summary>
        private static void CreateDatabase(DatabaseType dbType)
        {
            switch (dbType)
            {
                case DatabaseType.MsSqlCe:
                    var engine = new SqlCeEngine(FileUtils.ConnectionStringCombined(dbType,Database));
                    engine.CreateDatabase();
                    CreateSchema();
                    SetupInitialData();
                    break;
                case DatabaseType.MySql5:
                     CreateSchema();
                     SetupInitialData();
                    break;
            }
        }


        /// <summary>
        /// Create database schema('s)
        /// </summary>
        private static void CreateSchema()
        {
            ActiveRecordStarter.CreateSchema();
        }


        /// <summary>
        /// Check if the database already exist.
        /// </summary>
        /// <returns></returns>
        private static bool DataBaseExist()
        {
            return System.IO.File.Exists(FileUtils.ConnectionString(Database));
        }
    }
}