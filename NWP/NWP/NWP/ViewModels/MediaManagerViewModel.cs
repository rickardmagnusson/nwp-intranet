﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using NHibernate.Criterion;
using NWP.Models;

namespace NWP.ViewModels
{
    public class MediaManagerViewModel
    {
        //{}
        public MediaManagerViewModel(string path)
        {
            if (string.IsNullOrEmpty(path))
                GetRootDirectory();
            else
                GetDirectories(path);

            BreadCrumb = ContructBreadCrumb(path);
        }

        public string BreadCrumb { get; set; }
        public List<string> Directories { get; set; }
        public List<string> Files { get; set; }

        private string CreateIconFolder(string path)
        {
            var folder = new DirectoryInfo(path);
            return string.Format("<tr><td><a href='/admin/mediamanager/Index?path={0}' class='filem'><span class='folder'>{1}</span></a><td>{2}</td></td></tr>", ContructUrl(folder.FullName), folder.Name, folder.LastWriteTime);
        }

        public string ContructUrl(string path)
        {
            if (string.IsNullOrEmpty(path))
                return "";
            return path.Substring(path.ToLower().LastIndexOf("userdata") - 1).Replace("\\", "/");
        }

        public string ContructBreadCrumb(string path)
        {
            var urls = string.Empty;
            var s = ContructUrl(path);
            var output = "<li><a href='/admin/mediamanager/index?path=/userdata'>Filhantering</a></li>\n";
            var items = s.Split('/');
            
            foreach(var item in items)
            {
                urls += "/" + item;
                if(item!="")
                 output += "<li><a href='/admin/mediamanager/index?path=" + urls +"'>"+item+"</a></li>\n";
            }
            return output;
        }



        public void GetRootDirectory()
        {

            var path = HttpContext.Current.Server.MapPath("/UserData");
            var dirs = Directory.GetDirectories(path).ToList();
            var list = new List<string>();

            foreach (var dir in dirs){
                var folder = new DirectoryInfo(dir);
                list.Add(CreateIconFolder(folder.FullName));
            }
            Directories = list;
            GetFiles(path);
        }

        public void GetDirectories(string path)
        {
           
            var url = HttpContext.Current.Server.MapPath(path);
            var dirs = Directory.GetDirectories(url).ToList();
            
            var list = new List<string>();

            foreach (var dir in dirs){
                var folder = new DirectoryInfo(dir);
                list.Add(CreateIconFolder(folder.FullName));
            }
            Directories = list;
            GetFiles(url);
        }


        public void GetFiles(string path)
        {
         
            var dirs = Directory.GetFiles(path);
            var list = new List<string>();
            if (!dirs.Any())
            {
                Files = list;
                return;
            }
            foreach (var dir in dirs)
            {
                var folder = new FileInfo(dir);
                list.Add(CreateIconFolder(folder.FullName));
            }
            Files= list;
        }


        public string UserFolder
        {
            get{
                var folder = string.Format("/UserData/Users/{0}", User.FindOne(Restrictions.Eq("UserName", HttpContext.Current.User.Identity.Name)).UserName);
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(folder)))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(folder));
                return folder;
            }
        }

        public string GlobalFolder
        {
            get
            {
                return "/UserData/Public";
            }
        }

        //Skapa en funktion som kontrollerar varje mapp och fil. Visa endast de mappar och filer som en användare har rättigheter i.
        //Om en mapp tillhör användaren: tillåt access. 
        //Om en mapp är publik, tillåt access, men bara se och använda, inte redigera eller radera.

        //Create's a folder. Securitycheck is needed
        public void CreateFolder(string path) { }
        
        //Delete a folder. Securitycheck is needed
        public void DeleteFolder(string path) { }

        //Rename a folder. Securitycheck is needed
        public void RenameFolder(string path) { }

        //Uploads a file to folder. Securitycheck is needed
        public void UploadToFolder(string path) { }

        //Takes a zipped folder and extract it intto folder. Securitycheck is needed
        public void ExtractToFolder(string path) { }
    }

    public class FileFolderAccess
    {
        public int Owner { get; set; }
        public string Path { get; set; }
        public Access Access { get; set; }

        public static bool HasAccess(Access level, int owner)
        {
            bool access = false;
            switch(level)
            {
                case Access.FULL:
                    if (CurrentUser.IsInRole("admin"))
                        access = true;
                    break;
                case Access.OWNER:
                    if (owner == CurrentUserId)
                        return true;
                    break;
                case Access.VIEW:
                    if (owner != CurrentUserId)
                        access = true;
                    break;
                case Access.NONE:
                    if (CurrentUser.IsInRole("admin"))
                        access = true;
                    break;
                default:
                    access = false;
                    break;
            }
            return access;
        }

        public static int CurrentUserId
        {
            get
            {
                return User.FindOne(Restrictions.Eq("UserName", HttpContext.Current.User.Identity.Name)).Id;
            } 
        }

        public static System.Security.Principal.IPrincipal CurrentUser
        {
            get
            {
                return HttpContext.Current.User;
            }
        }
    }


    /// <summary>
    /// Controls access or edit a file or folder
    /// </summary>
    public enum Access
    {
        FULL = 1,   // AdminOnly
        OWNER = 2,  // The user has all access
        VIEW = 3,   // User as only view access
        NONE = 4    // Only admin can view this item, its hidden from all users except admin
    }
}