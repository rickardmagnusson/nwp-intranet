﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using NWP.Providers.Membership;

namespace NWP.ViewModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            AllRoles = GetAllRoles();
            AllUsers = GetAllUsers();
        }

        public string SelectedUser { set { UserRoles = GetRolesForUser(value); }}
        public List<string> AllRoles { get; set; }
        public List<string> UserRoles { get; set; }
        public IEnumerable<SelectListItem> AllUsers { get; set; }



        public IEnumerable<SelectListItem> GetAllUsers()
        {
            var model = new MembershipModel();
            var users = model.GetAllUsers().Cast<MembershipUser>().OrderBy(u => u.UserName);

            return users.Select(user => new SelectListItem { Text = user.UserName, Value = user.ProviderUserKey.ToString() }).ToList();
        }

       private List<string> GetRolesForUser(string id)
       {
           var model = new MembershipModel();
           return model.GetRolesForUser(id);
       }

       private List<string> GetAllRoles()
       {
           var model = new MembershipModel();
           return model.GetAllRoles();
       }

    }
}