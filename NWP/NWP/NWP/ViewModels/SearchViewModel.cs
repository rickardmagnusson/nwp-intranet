﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.ActiveRecord;
using NWP.Configuration;
using NWP.Models;
using MySql.Data.MySqlClient;

namespace NWP.ViewModels
{
    public class SearchViewModel
    {
        public SearchViewModel(string s)
        {
            Search = s;
            SearchResults = new List<SearchResult>();
           
            var conn = new MySqlConnection(FileUtils.ConnectionStringCombined(FileUtils.CurrentDatabase(), ""));
            conn.Open();
            
            if(!string.IsNullOrEmpty(Search))
            {
                var contentQuery = "SELECT WidgetId as Id, Title, Html, 'Content', Created, Owner From Content WHERE Title LIKE '%{0}%' Or Html LIKE '%{0}%'";
                contentQuery += " UNION ";
                contentQuery += "SELECT Id as Id, Title, Html, 'NewsItem', Created, Owner From NewsItem WHERE Title LIKE '%{0}%' Or Html LIKE '%{0}%'";
                contentQuery += " UNION ";
                contentQuery += "SELECT Id as Id, Date as Title, Post, 'WallPost', Date, PostedByUser From WallPost WHERE Post LIKE '%{0}%'";
                contentQuery += " UNION ";
                contentQuery += "SELECT Id as Id, Date as Title, Comment, 'WallComment', Date, CommentByUser From WallComment WHERE Comment LIKE '%{0}%'";

                var query = string.Format(contentQuery, Search);

                var cmd = new MySqlCommand(query, conn);
                var rs = cmd.ExecuteReader();
                Count = 0;

                while(rs.Read())
                {
                    var type = rs["Content"].ToString();
                    var created = rs["Created"].ToString();
                    var title = rs["Title"].ToString();
                    var content = NWP.Templates.Utils.HtmlRemoval.StripTagsRegex(rs["Html"].ToString());
                    var url = string.Format("/{0}/{1}", type, rs["Id"].ToString());
                    var owner = Convert.ToInt32(rs["Owner"]);
                    
                    SearchResults.Add(new SearchResult{Title = title, Url=url, Relevance = "1", Content = content, TableType = type, Created = created, Owner = owner });
                    Count++;
                }
            }
        }

        public string CreateUrl (string url)
        {
            return url;
        }

        public string Search { get; set; }
        public Int32 Count { get; set; }
        public List<SearchResult> SearchResults { get; set; }
    }

    public class SearchResult
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string TableType { get; set; }
        public string Url { get; set; }
        public string Relevance { get; set; }
        public string Created { get; set; }
        public int Owner{ get; set; }
    }
}