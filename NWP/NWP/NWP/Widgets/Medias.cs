﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using System.ComponentModel.DataAnnotations;
using NWP.Parser;
using System.Web;
using System.IO;
using NWP.Models;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class Medias : Widget<Medias>
    {
        public Medias() : base() { }
        public Medias(int id)
            : base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = false)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public override string Title { get; set; }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Html { get; set; }

        [Property(NotNull = false)]
        public string Created { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }
      
        private string a;
        [Property(NotNull = false, Length = 255)]
        public string AttachementListId
        {
            get
            {
                if (string.IsNullOrEmpty(a))
                    return Guid.NewGuid().ToString();
                else
                    return a;
            }
            set { a = value; }
        }

        public List<Medias> GetAllMedias()
        {
            var list = ActiveRecordMediator.FindAll(typeof(Medias));
            return list.Cast<Medias>().ToList();
        }

        public string LoadFile()
        {
            string filePath = HttpContext.Current.Server.MapPath("/UserData/Groups/{0}");
            var companyname = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", HttpContext.Current.User.Identity.Name)).Company;
            var dir = Directory.GetFiles(string.Format(filePath, companyname));
            string template = "<li><a href='/Pages/Download?pathCode=/UserData/Groups/{0}/{1}'><span class='{2} icon'></span> {1} ({3})</a></li>";
            string o = string.Empty;

            foreach (var img in dir) {
                if(!img.Contains(".DS_Store")){
                    o += string.Format(template, companyname, Path.GetFileName(img), Path.GetExtension(img).Replace(".", ""), getSize(img));
                }
            }

            
            return o;
        }

        public string getSize(string path) {
            var size = new FileInfo(path);
            long bytes = size.Length;
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB" };
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return num.ToString() + suf[place];
        }

        public string LoadCommon()
        {
            string filePath = HttpContext.Current.Server.MapPath("/UserData/Common");
            string template = "<li><a href='/Pages/Download?pathCode=/UserData/Common/{0}'><img src='/UserData/Common/{1}' alt='{1}' title='{2}' /></a></li>";
            string o = string.Empty;
            var dir = Directory.GetFiles(filePath);

            foreach (var img in dir)
            {
                if (!img.Contains(".DS_Store") && img.Contains(".jpg"))
                    o += string.Format(template, Path.GetFileName(img).Replace(".jpg", ".eps"), Path.GetFileName(img), getSize(img.Replace(".jpg", ".eps")));
            }

            return o;
        }

        public override string Icon { get { return "archive"; } }
    }

}