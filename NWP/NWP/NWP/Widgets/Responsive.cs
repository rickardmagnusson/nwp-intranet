﻿
using System;
using System.ComponentModel.DataAnnotations;
using Castle.ActiveRecord;
using NHibernate.SqlTypes;
using Castle.ActiveRecord.Attributes;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class Responsive : Widget<Responsive>
    {
        public Responsive() : base() { }
        public Responsive(int id): base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [UIHint("YesNo")]
        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public override string Title { get; set; }

        [Property(NotNull = false)]
        public DateTime Created { get; set; }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Html { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [Property(NotNull = false)]
        public int Owner { get; set; }

        public override string Icon { get { return ""; } }
    }
}