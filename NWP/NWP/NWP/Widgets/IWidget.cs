﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NWP.Widgets
{
    public interface IWidget
    {
        int WidgetId { get; set; }
        int PageId { get; set; }
        string Zone { get; set; }
        string Template { get; }
        string Domain { get; set; }
        bool IsGlobal { get; set; }
        bool Active { get; set; }
        string Icon { get; }
        bool TrashBin { get; set; }
        string SortOrder { get; set; }
        string Title { get; set; }
    }
}