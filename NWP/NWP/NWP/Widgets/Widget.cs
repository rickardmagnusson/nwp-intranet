﻿using System;
using Castle.ActiveRecord;

namespace NWP.Widgets
{
    public abstract class Widget<T> : ActiveRecordBase<T>, IWidget
    {
        protected T data;
        protected Widget() : base() { }

        protected Widget(int id)
        {
            data = (T)Activator.CreateInstance(typeof(T));
        }
        public abstract int WidgetId { get; set; }
        public abstract int PageId { get; set; }
        public abstract string Zone { get; set; }
        public abstract bool IsGlobal { get; set; }
        public abstract bool Active { get; set; }
        public abstract string Domain { get; set; }
        public abstract string SortOrder { get; set; }
        public abstract bool TrashBin { get; set; }
        public string Template { get { return string.Format("~/Views/Widgets/_{0}.cshtml", GetType().Name); } }
        public virtual string Title { get; set; }

        /// <summary>
        /// Dashboard Icon, Leave empty to hide.
        /// </summary>
        public abstract string Icon { get; }
    }
}