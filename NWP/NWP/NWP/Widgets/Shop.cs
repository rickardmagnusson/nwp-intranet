﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Castle.ActiveRecord;
using NWP.ContentManager;
using NHibernate.Criterion;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class Shop : Widget<Shop>
    {
        public Shop() : base() { }
        public Shop(int id)
            : base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public override string Title { get; set; }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Html { get; set; }

        [Property(NotNull = false)]
        public string EmailFrom { get; set; }

        [Property(NotNull = false)]
        public string EmailTo { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public DateTime Created { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        private IList ShopList = new ArrayList();

        [HasMany(typeof(ShopItem),
                Table = "ShopItems",
                ColumnKey = "ShopId",
                Inverse = true,
                Cascade = ManyRelationCascadeEnum.AllDeleteOrphan)]
        public IList ShopItems
        {
            get { return ShopList; }
            set { ShopList = value; }
        }

        public override string Icon { get { return "folder"; } }
    }

    [ActiveRecord]
    public class ShopItem : DataRecord<ShopItem>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = true, Length = 50)]
        public string Title { get; set; }

        [Property(NotNull = false, Length = 8000)]
        public string Message { get; set; }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Html { get; set; }

        [Property]
        public string Image { get; set; }

        [Property]
        public DateTime Created { get; set; }

        [Property]
        public DateTime StartDate { get; set; }

        [Property]
        public DateTime EndDate { get; set; }

        [Property]
        public string Price { get; set; }

        [Property]
        public int MinimumAmount { get; set; }

        [Property]
        public string ArtNmbr { get; set; }

        [Property]
        public string OtherInfo { get; set; }

        [BelongsTo("ShopId")]
        public Shop Shop { get; set; }

        [Property(NotNull = false)]
        public bool Active { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [Property(NotNull = false, Default="1")]
        public int ShopCategoryId { get; set; }

        private string a;
        [Property(NotNull = false, Length = 255)]
        public string AttachementListId
        {
            get
            {
                if (string.IsNullOrEmpty(a))
                    return Guid.NewGuid().ToString();
                else
                    return a;
            }
            set { a = value; }
        }


        public void AddCategory(string category)
        {
            var item = new ShopCategory { CategoryName = category, TrashBin = false };
            ActiveRecordMediator<ShopCategory>.Create(item);
        }

        public IEnumerable<ShopCategory> GetCategories()
        {
            return ActiveRecordMediator<ShopCategory>.FindAll();
        }

    }

    [ActiveRecord]
    public class ShopCategory : DataRecord<ShopCategory>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property]
        public string CategoryName { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }
    }

    [ActiveRecord]
    public class OrderData : DataRecord<OrderData>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id
        {
            get;
            set;
        }

        [Property]
        public string OrderList //innehåller en kommaseparerad list över produkter.
        {
            get;
            set;
        }

        [Property]
        public string OrderListAmount // Innehåller en kommaseparerad list över antalet produkter i OrderList.
        {
            get;
            set;
        }

        [Property]
        public int OrderByUserId //Användarens order
        {
            get;
            set;
        }

        [Property]
        public int SupplierId //Mottagen av leverantör
        {
            get;
            set;
        }

        [Property]
        public DateTime Created  //Skapad
        {
            get;
            set;
        }

        /// <summary>
        /// Optional, not used for now.
        /// </summary>
        [Property]
        public int Status  //Vad är statusen för denna order, Sparad = 0, Mottagen = 1, Behandlad = 2, Skickad = 3, Klar = 4
        {
            get;
            set;
        }

        [Property(NotNull = false)]
        public override bool TrashBin
        {
            get;
            set;
        }

        public IEnumerable<OrderItem> GetOrderList()
        {
            var list = new List<OrderItem>();
            foreach(var item in OrderList.Split(','))
            {
                var si = ShopItem.Find(Restrictions.Eq("Id", item));
                var oi = si.AddAmount(Convert.ToInt32(item));
                list.Add(oi);
            }

            return list;
        }
    }

    public class OrderItem
    {
        public ShopItem ShopItem
        {
            get;
            set;
        }

        public int Amount
        {
            get;
            set;
        }
    }

    public static class OrderHelper
    {
        public static OrderItem AddAmount(this ShopItem order, int mt)
        {
            return new OrderItem{ ShopItem=order, Amount= mt};
        }
    }
}