﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Castle.ActiveRecord;
using NWP.ContentManager;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class PriceTable : Widget<PriceTable>
    {
        public PriceTable() : base() { }
        public PriceTable(int id)
            : base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public override string Title { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        public override string Icon { get { return "web"; } }

        private IList _PriceTableItems = new ArrayList();

        [HasMany(typeof(PriceTableItem),
               Table = "PriceTableItems",
               ColumnKey = "PriceTableId",
               Inverse = true,
               Cascade = ManyRelationCascadeEnum.AllDeleteOrphan)]
        public IList PriceTableItems
        {
            get { return _PriceTableItems; }
            set { _PriceTableItems = value; }
        }

        public List<PriceTableItem> GetItems()
        {
            var items = _PriceTableItems.Cast<PriceTableItem>().Where(a => a.TrashBin == false).ToList();
            return items;
        }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Html{ get; set; }
    }

    [ActiveRecord]
    public class PriceTableItem : DataRecord<PriceTableItem>
    {
        [BelongsTo("PriceTableId")]
        public PriceTable PriceTable { get; set; }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public string Name { get; set; }

        [Property(NotNull = false)]
        public string Url { get; set; }

        [Property(NotNull = false)]
        public bool Active { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [Property(NotNull = false)]
        public string ImageIcon { get; set; }

        private string a;
        [Property(NotNull = false, Length = 255)]
        public string AttachementListId
        {
            get
            {
                if (string.IsNullOrEmpty(a))
                    return Guid.NewGuid().ToString();
                else
                    return a;
            }
            set { a = value; }
        }
    }
}