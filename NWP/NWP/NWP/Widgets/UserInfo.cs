﻿using System;
using Castle.ActiveRecord;
using NWP.Models;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class UserInfo : Widget<UserInfo>
    {
        public UserInfo() : base() { }
        public UserInfo(int id)
            : base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public string Title { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        public override string Icon { get { return ""; } }

        public User User { get; set; }
    }
}