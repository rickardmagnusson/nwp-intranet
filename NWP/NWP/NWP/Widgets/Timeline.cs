﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Castle.ActiveRecord;
using NWP.ContentManager;
using NHibernate.Criterion;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class Timeline : Widget<Timeline>
    {
        public Timeline() : base() { }
        public Timeline(int id)
            : base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = false)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public override string Title { get; set; }

        [Property(NotNull = false)]
        public string Html { get; set; }

        [Property(NotNull = false)]
        public string Created { get; set; }

        [Property(NotNull = false)]
        public string StartDate { get; set; }

        [Property(NotNull = false)]
        public string EndDate { get; set; }

        [UIHint("DatePicker")]
        [Property(NotNull = false)]
        public string TimelineStartDateScreen { get; set; }

        [Property(NotNull = false)]
        public string TimelineBeginsDate { get; set; }
        
        [Property(NotNull = false)]
        public string MainHeadline { get; set; }

        [Property(NotNull = false)]
        public string MainHeadlineText { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        private IList EventList = new ArrayList();

        [HasMany(typeof(TimelineEvent),
             Table = "TimelineEvent",
             ColumnKey = "EventId",
             Inverse = true,
             Cascade = ManyRelationCascadeEnum.AllDeleteOrphan)]
        public IList TimelineEvents
        {
            get { return EventList; }
            set { EventList = value; }
        }

        public IEnumerable<TimelineEvent> GetItems()
        {
            return EventList.Cast<TimelineEvent>().Where(n => n.TrashBin ==false);
        }

        public override string Icon { get { return "calendar"; } }


        /// <summary>
        /// Calulate the closest date based from todays date.
        /// </summary>
        /// <param name="pageid">The pageid where the timline are placed.</param>
        /// <returns>The Slide number that is closest by todays date.</returns>
        public Int32 GetClosestDate(Int32 pageid)
        {
            var tm = ActiveRecordMediator<Timeline>.FindFirst(Restrictions.Eq("PageId", pageid));
            List<CDate> datelist = new List<CDate>();
            DateTime today = DateTime.Now;

            int slide = 1;
            foreach (var item in tm.GetItems().OrderBy(d => d.EventDate)) {
                datelist.Add(new CDate { 
                    Date = DateTime.ParseExact(item.EventDate.ToShortDateString(), "yyyy-mm-dd", null), 
                    Slide = slide, 
                    Diff = DateDiff(today, item.EventDate) });
                slide++;
            }

            var closest = datelist.OrderBy(d => d.Diff).First();

            return closest.Slide;
        }

        /// <summary>
        /// DateDiff helper
        /// </summary>
        /// <param name="Date1">Compare date one</param>
        /// <param name="Date2">Compare date two</param>
        /// <returns>Difference as an integer between these two dates.</returns>
        private int DateDiff(DateTime Date1, DateTime Date2)
        {
            TimeSpan time = Date1 - Date2;
            return Math.Abs(time.Days);
        }


    }

    public class CDate
    {
        public Int32 Slide { get; set; } //Identifyer for the date
        public DateTime Date { get; set; } //Actuall date
        public Int32 Diff { get; set; }
    }


    [ActiveRecord]
    public class TimelineEvent : DataRecord<TimelineEvent>
    {

        [BelongsTo("EventId")]
        public Timeline Timeline { get; set; }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = true, Length = 50)]
        public string Title { get; set; }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, Length = 8000)]
        public string Html { get; set; }

        [Property]
        public DateTime Created { get; set; }

        [Property]
        public DateTime EventDate { get; set; }

        [Property]
        public DateTime EndEventDate { get; set; }

        [Property]
        public bool Active { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }
    }
}