﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Castle.ActiveRecord;
using NWP.ContentManager;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class Links : Widget<Links>
    {
        public Links() : base() { }
        public Links(int id): base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public override string Title { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [Property(NotNull = false)]
        public bool Alternate { get; set; }

        public override string Icon { get { return "web"; } }

        private IList _linkitems = new ArrayList();

        [HasMany(typeof(LinkItem),
               Table = "LinkItems",
               ColumnKey = "LinkId",
               Inverse = true,
               Cascade = ManyRelationCascadeEnum.AllDeleteOrphan)]
        public IList LinkItems
        {
            get { return _linkitems; }
            set { _linkitems = value; }
        }

        public List<LinkItem> GetItems()
        {
            var items = _linkitems.Cast<LinkItem>().Where(a=> a.TrashBin == false).ToList();
            return items;
        }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Html{ get; set; }
    }

    [ActiveRecord]
    public class LinkItem : DataRecord<LinkItem>
    {
        [BelongsTo("LinkId")]
        public Links Links { get; set; }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public string Name { get; set; }

        [Property(NotNull = false)]
        public string Url { get; set; }

        [Property(NotNull = false)]
        public bool Active { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [Property(NotNull = false)]
        public string ImageIcon { get; set; }

        [Property(NotNull = false)]
        public string UserName { get; set; }

        [Property(NotNull = false)]
        public string Password { get; set; }
    }
}