﻿
using System;
using System.ComponentModel.DataAnnotations;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NHibernate.SqlTypes;
using Castle.ActiveRecord.Attributes;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class NewsTeaser : Widget<NewsTeaser>
    {
        public NewsTeaser() : base() { }
        public NewsTeaser(int id)
            : base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public int ShowWidgetFromNewsList { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [UIHint("YesNo")]
        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public string Title { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        public override string Icon { get { return ""; } }

        public News GetNewsFrom(int id)
        {
            var list = ActiveRecordMediator<News>.FindOne(Restrictions.Eq("WidgetId", id));
            return list;
        }
    }
}