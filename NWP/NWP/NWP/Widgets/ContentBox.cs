﻿
using System;
using Castle.ActiveRecord;

namespace NWP.Widgets
{
    //Skall bli dynamisk
    [ActiveRecord]
    public class ContentBox : Widget<ContentBox>
    {
        public ContentBox() : base() { }
        public ContentBox(int id): base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public string Title { get; set; }

        [Property(NotNull = false)]
        public string Html { get; set; }

        [Property(NotNull = false)]
        public int ColumnCount { get; set; }

        [Property(NotNull = false)]
        public string HtmlOne { get; set; }

        [Property(NotNull = false)]
        public string HtmlTwo { get; set; }

        [Property(NotNull = false)]
        public string HtmlThree { get; set; }

        [Property(NotNull = false)]
        public string HtmlFour { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        public override string Icon { get { return ""; } } //file-edit
    }
}