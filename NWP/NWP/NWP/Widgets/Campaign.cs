﻿using System;
using Castle.ActiveRecord;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using NHibernate.Criterion;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class Campaign : Widget<Campaign>
    {
        public Campaign() : base() {}
        public Campaign(int id) : base() {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public override string Title { get; set; }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Html { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property]
        public DateTime StartDate { get; set; }

        [Property]
        public DateTime EndDate { get; set; }

        [Property]
        public DateTime Created { get; set; }

        [Property]
        public int CreatedBy { get; set; }

        private string a;
        [Property(NotNull = false, Length = 255)]
        public string AttachementListId
        { 
            get{
                if (string.IsNullOrEmpty(a))
                    return Guid.NewGuid().ToString();
                else
                    return a;
            }
            set { a = value; } 
        }
   
        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        public override string Icon { get { return "web"; } }


        public static IEnumerable<Campaign> GetCampaigns()
        {
            return ActiveRecordMediator<Campaign>.FindAll(Restrictions.Eq("TrashBin", false));
        }
    }
}