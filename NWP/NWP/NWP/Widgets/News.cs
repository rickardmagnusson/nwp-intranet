﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Castle.ActiveRecord;
using NWP.ContentManager;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class News : Widget<News>
    {
        public News() : base() { }
        public News(int id): base()
        {
            WidgetId = id;
        }
        

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override sealed int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public override string Title { get; set; }

        [Property(NotNull = false)]
        public string Html { get; set; }

        [Property(NotNull = false)]
        public string Created { get; set; }

        [Property(NotNull = false)]
        public string StartDate { get; set; }

        [Property(NotNull = false)]
        public string EndDate { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        private IList _newsList = new ArrayList();

        [HasMany(typeof(NewsItem),
                 Table = "NewsItems",
                 ColumnKey = "NewsId",
                 Inverse = true,
                 Cascade = ManyRelationCascadeEnum.AllDeleteOrphan)]
        public IList NewsItems
        {
            get { return _newsList; }
            set { _newsList = value; }
        }

        public IEnumerable<NewsItem> GetItemsByTake(int howMany)
        {
            return _newsList.Cast<NewsItem>().OrderByDescending(n => n.StartDate).Where(n=> n.Show()).Take(howMany);
        }


        public IEnumerable<NewsItem> GetAllItemsSortedByDateDescending()
        {
            return _newsList.Cast<NewsItem>().OrderByDescending(cust => cust.Created).Where(n => n.Show());
        }

        public override string Icon { get { return "calendar"; } }
    }

   
    [ActiveRecord]
    public class NewsItem : DataRecord<NewsItem>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = true, Length = 50)]
        public string Title { get; set; }

        [Property(NotNull = false, Length = 200)]
        public string Teaser { get; set; }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Html { get; set; }

        [Property]
        public DateTime Created { get; set; }

        [Property]
        public DateTime StartDate { get; set; }

        [Property]
        public DateTime EndDate { get; set; }

        [Property(NotNull = false)]
        public bool Active { get; set; }

        [BelongsTo("NewsId")]
        public News News { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [Property(NotNull = false)]
        public int Owner { get; set; }

        private string a;
        [Property(NotNull = false, Length = 255)]
        public string AttachementListId
        {
            get
            {
                if (string.IsNullOrEmpty(a))
                    return Guid.NewGuid().ToString();
                else
                    return a;
            }
            set { a = value; }
        }
    }

    public static class DateHelper
    {
        public static bool Show(this NewsItem n)
        {
            var today = DateTime.Now;
            return ((today >= n.StartDate) && (today <= n.EndDate) && n.Active && n.TrashBin==false);
        }
    }
}