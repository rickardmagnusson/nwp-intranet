﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Castle.ActiveRecord;
using NWP.ContentManager;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class Comments : Widget<Comments>
    {
        public Comments() : base() {}
        public Comments(int id) : base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public override string Title { get; set; }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Html { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property]
        public DateTime Created { get; set; }

        [Property]
        public int CreatedBy { get; set; }

   
        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        public override string Icon { get { return "users"; } }

        private IList _commentList = new ArrayList();
        [HasMany(typeof(Comment),
                 Table = "CommentItems",
                 ColumnKey = "CommentId",
                 Inverse = true,
                 Cascade = ManyRelationCascadeEnum.AllDeleteOrphan)]
        public IList CommentItems
        {
            get { return _commentList; }
            set { _commentList = value; }
        }

        public IEnumerable<Comment> GetItems()
        {
            return _commentList.Cast<Comment>().OrderByDescending(c=> c.Id);
        }

    }

    [ActiveRecord]
    public class Comment : DataRecord<Comment>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [BelongsTo("CommentId")]
        public Comments Comments { get; set; }

        [Property]
        public int UserId { get; set; }

        [Property]
        public DateTime Created { get; set; }

        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Message { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }
    }
}