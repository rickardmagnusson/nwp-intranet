﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Castle.ActiveRecord;
using NHibernate.Criterion;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class NewsList : Widget<NewsList>
    {
        public NewsList() : base(){
        }

        public NewsList(int id): base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override sealed int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public string Title { get; set; }

        [Property(NotNull = false)]
        public string Created { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [Property(NotNull = false)]
        public int NewsListId { get; set; }

        public override string Icon
        {
            get { return ""; }
        }

        public IEnumerable<NewsItem> GetItems(int fromNewsList)
        {
            return ActiveRecordMediator<News>.FindOne(
                Restrictions.Eq("WidgetId", fromNewsList))
                .GetAllItemsSortedByDateDescending();
        }

        public NewsItem GetLatestItem(int fromNewsList)
        {
            var list =ActiveRecordMediator<News>.FindOne(Restrictions.Eq("WidgetId", fromNewsList)).GetAllItemsSortedByDateDescending();
            return list.Cast<NewsItem>().OrderByDescending(n => n.Created).Take(1).First();
        }

        public NewsItem GetCurrentItem(int fromNewsList, int current)
        {
            var list = ActiveRecordMediator<News>.FindOne(
                Restrictions.Eq("WidgetId", fromNewsList))
                .GetAllItemsSortedByDateDescending();

            return (from b in list select b).Where(d => d.Id == current).Cast<NewsItem>().First();
            
        } 
    }
}