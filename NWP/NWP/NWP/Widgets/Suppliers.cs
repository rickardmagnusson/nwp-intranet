﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Castle.ActiveRecord;
using NHibernate.Criterion;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class Suppliers : Widget<Suppliers>
    {
        public Suppliers() : base() { }
        public Suppliers(int id)
            : base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = false)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public override string Title { get; set; }

        [Property(NotNull = false)]
        public string Html { get; set; }

        [Property(NotNull = false)]
        public string Created { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [Property(NotNull = false)]
        public string Email { get; set; }

        [Property(NotNull = false)]
        public string Company { get; set; }

        [Property(NotNull = false)]
        public string Contact { get; set; }

        [Property(NotNull = false)]
        public string Address { get; set; }

        [Property(NotNull = false)]
        public string Zip { get; set; }

        [Property(NotNull = false)]
        public string City { get; set; }

        [Property(NotNull = false)]
        public string Phone { get; set; }

        public List<Suppliers> GetAllSuppliers()
        {
            var list = ActiveRecordMediator.FindAll(typeof(Suppliers));
            return list.Cast<Suppliers>().OrderByDescending(c => c.Company).ToList();
        }


        /*
        private IList SupplierList = new ArrayList();

        
        [HasMany(typeof(Supplier),
             Table = "Supplier",
             ColumnKey = "SupplierId",
             Inverse = true,
             Cascade = ManyRelationCascadeEnum.AllDeleteOrphan)]
        public IList AllSuppliers
        {
            get { return SupplierList; }
            set { SupplierList = value; }
        }*/

        public override string Icon { get { return "archive"; } }
    }

    /*
    [ActiveRecord]
    public class Supplier : DataRecord<Supplier>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = true, Length = 50)]
        public string Title { get; set; }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, Length = 8000)]
        public string Html { get; set; }

        [Property]
        public DateTime Created { get; set; }

        [Property]
        public string Email { get; set; }

        [Property]
        public string Company { get; set; }

        [Property]
        public string Contact { get; set; }

        [Property]
        public string Address { get; set; }

        [Property]
        public string Zip { get; set; }

        [Property]
        public string City { get; set; }

        [Property]
        public string Phone { get; set; }

        [Property]
        public bool Active { get; set; }

        [BelongsTo("SupplierId")]
        public Suppliers Suppliers { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }
    }*/
}