﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Castle.ActiveRecord;
using NHibernate.SqlTypes;
using Castle.ActiveRecord.Attributes;
using NWP.ViewModels;

namespace NWP.Widgets
{
    [ActiveRecord]
    public class SearchPublic : Widget<SearchPublic>
    {
        public SearchPublic() : base() { }
        public SearchPublic(int id)
            : base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [UIHint("YesNo")]
        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public string Title { get; set; }

        [Property(NotNull = false)]
        public string SearchResultPage { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [Property(NotNull = false)]
        public bool ShortCut { get; set; }

        [Property(NotNull = false)]
        public int Owner { get; set; }

        public override string Icon { get { return ""; } }

        public List<SearchResult> SearchFor(string content)
        {
            /*TODO : Implement a searchengine here!*/
            var model = new SearchViewModel(content);

            return model.SearchResults.Select(item => new SearchResult {Title = item.Title, Relevance = "1", Url = item.Url, TableType = item.TableType, Content = item.Content, Created = item.Created, Owner= item.Owner}).ToList();
        }
    }

}