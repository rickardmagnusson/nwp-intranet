﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using Castle.ActiveRecord;
using NWP.ContentManager;
using NHibernate.Criterion;
using System.Web;

namespace NWP.Widgets
{
    
    [ActiveRecord]
    public class Wall : Widget<Wall>
    {
        public Wall() : base() { }
        public Wall(int id)
            : base()
        {
            WidgetId = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public override int WidgetId { get; set; }

        [Property(NotNull = true)]
        public override int PageId { get; set; }

        [Property(NotNull = true)]
        public override string Zone { get; set; }

        [Property(NotNull = false)]
        public override bool IsGlobal { get; set; }

        [Property(NotNull = false)]
        public override string Domain { get; set; }

        [Property(NotNull = false)]
        public string Title { get; set; }

        [Property(NotNull = false)]
        public string Html { get; set; }

        [Property(NotNull = false)]
        public string Created { get; set; }

        [Property(NotNull = false)]
        public string StartDate { get; set; }

        [Property(NotNull = false)]
        public string EndDate { get; set; }

        [Property(NotNull = false)]
        public override bool Active { get; set; }

        [Property(NotNull = false)]
        public override string SortOrder { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        public override string Icon { get { return ""; } }

        private IList  _posts = new ArrayList();

        [HasMany(typeof(WallPost),
         Table = "PostItems",
         ColumnKey = "PostId",
         Inverse = true,
         Cascade = ManyRelationCascadeEnum.AllDeleteOrphan)]
        public IList PostItems
        {
            get { return _posts; }
            set { _posts = value; }
        }

        /// <summary>
        /// Get all posts by amount.
        /// </summary>
        /// <param name="howMany">How many posts to get</param>
        /// <returns>A limited list of WallPost</returns>
        public IEnumerable<WallPost> GetItemsByTake(int howMany)
        {
            return _posts.Cast<WallPost>().OrderByDescending(w => w.Date).Take(howMany);
        }

        /// <summary>
        /// Get all posts by amount.
        /// </summary>
        /// <param name="previousId"> </param>
        /// <param name="howMany">How many posts to get</param>
        /// <returns>A limited list of WallPost</returns>
        public IEnumerable<WallPost> GetItemsByTakeNext(int previousId, int howMany)
        {
            return _posts.Cast<WallPost>().OrderByDescending(w => w.Date).Where(w=>w.Id < previousId).Take(howMany);
        }


        /// <summary>
        /// Get all unread posts. A user need to click away all unread messages, in this way we know the user has read it.
        /// </summary>
        /// <returns>A list of unread Posts</returns>
        public static List<PostRead> UnreadPosts() 
        {
            var userId = NWP.Models.User.GetUserId(HttpContext.Current.User.Identity.Name);
            var currentlist = ActiveRecordMediator<PostRead>.FindAll(Restrictions.Eq("UserId", userId)).OrderByDescending(d=> d.Id).ToList();

            return currentlist;
        }
  }

    [ActiveRecord]
    public class PostRead : DataRecord<PostRead>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public int UserId { get; set; }

        [Property(NotNull = false)]
        public int PostId { get; set; }

        [Property(NotNull = false)]
        public int CommentId { get; set; }

        [Property(NotNull = false)]
        public bool IsRead { get; set; }

        [Property(NotNull = false)]
        public PostType Type { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }
    }

    public enum PostType{
        POST = 1,
        COMMENT = 2
    }
    


    [DataContract]
    [ActiveRecord]
    public class WallPost :  DataRecord<WallPost>
    {
        [BelongsTo("PostId")]
        public Wall Wall { get; set; }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public int Likes { get; set; }

        [Property(NotNull = false)]
        public int PostedByUser { get; set; }

        [Property(NotNull = false)]
        public string Date { get; set; }

        [Property(NotNull = false)]
        public string Post { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        private IList _comments = new ArrayList();

        [HasMany(typeof(WallComment),
         Table = "CommentItems",
         ColumnKey = "CommentId",
         Inverse = true,
         Cascade = ManyRelationCascadeEnum.AllDeleteOrphan)]
        public IList CommentItems
        {
            get { return _comments; }
            set { _comments = value; }
        }

    }

    [DataContract]
    [ActiveRecord]
    public class WallComment : DataRecord<WallComment>
    {
        [BelongsTo("CommentId")]
        public WallPost WallPost { get; set; }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public string Date { get; set; }

        [Property(NotNull = false)]
        public string Comment { get; set; }

        [Property(NotNull = false)]
        public int Likes { get; set; }

        [Property(NotNull = false)]
        public int CommentByUser { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

    }


/*
 Klasserna ovan skall producera denna xml struktur.
 <messages>
   <message id="5">
		<user>5</user>
		<likes>1</likes>
		<date>Thu May 10 2012 09:32:41 GMT+0200 (CEST)</date>
		<post>Detta är posten som jag skrivit in i xml filen(2).</post>
    	<comments>
			<comment by="1" likes="2">And this is the long description. See how long it is.</comment>
			<comment by="1" likes="2">And this is the long description. See how long it is.</comment>
			<comment by="1" likes="2">And this is the long description. See how long it is.</comment>
		</comments>
   </message>
 <messages>
     
     */
}

/*
public static class Ext
{
    public static IQueryable<TSource> Between<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, TKey low, TKey high) where TKey : IComparable<TKey>
    {
        Expression key = Expression.Invoke(keySelector, keySelector.Parameters.ToArray());
        Expression lowerBound = Expression.GreaterThanOrEqual(key, Expression.Constant(low));
        Expression upperBound = Expression.LessThanOrEqual(key, Expression.Constant(high));
        Expression and = Expression.AndAlso(lowerBound, upperBound);
        Expression<Func<TSource, bool>> lambda = Expression.Lambda<Func<TSource, bool>>(and, keySelector.Parameters);
        return source.Where(lambda);
    }
}*/