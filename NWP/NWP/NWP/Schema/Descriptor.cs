﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.ActiveRecord.Framework.Config;

namespace NWP.Schema
{
    /// <summary>
    /// nHibernate Configuration specific settings.
    /// </summary>
    public class Descriptor : Dictionary<string, string>
    {
        /// <summary>
        /// Creates Configuration for nHibernate
        /// </summary>
        /// <param name="type">DataBaseType type(For eg. DatabaseType.MsSqlCe)</param>
        /// <param name="database">Name of the database</param>
        public Descriptor(DatabaseType type, string database)
        {
            switch (type)
            {
                case DatabaseType.MsSqlCe:
                    Add("connection.driver_class", "NHibernate.Driver.SqlServerCeDriver");
                    Add("dialect", "NHibernate.Dialect.MsSqlCeDialect");
                    Add("connection.provider", "NHibernate.Connection.DriverConnectionProvider");
                    Add("proxyfactory.factory_class", "NHibernate.ByteCode.Castle.ProxyFactoryFactory, NHibernate.ByteCode.Castle");
                    Add("show_sql", "true");
                    Add("connection.connection_string", database);
                    break;

                case DatabaseType.MySql5:
                    Add("connection.driver_class", "NHibernate.Driver.MySqlDataDriver");
                    Add("dialect", "NHibernate.Dialect.MySQL5Dialect");
                    Add("connection.provider", "NHibernate.Connection.DriverConnectionProvider");
                    Add("proxyfactory.factory_class", "NHibernate.ByteCode.Castle.ProxyFactoryFactory, NHibernate.ByteCode.Castle");
                    Add("show_sql", "true");
                    Add("connection.connection_string", database);
                    break;
                //Initialize, if needed, more database types here:
            }
        }
    }
}