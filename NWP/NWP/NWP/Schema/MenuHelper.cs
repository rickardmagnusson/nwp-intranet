﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages;
using NWP.Configuration;
using NWP.Models;

namespace NWP.Schema
{
    /// <summary>
    /// MenuHelper makes it easier to get current menuitems data.
    /// </summary>
    public static class MenuHelper
    {
        /// <summary>
        /// Get the root node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static Page RootNode
        {
            get
            {
                return GetAllNodes.Find(d => d.Route == "" && d.Domain== Settings.CreateInstance().GetValue("DefaultDomain"));
            }
        }


        /// <summary>
        /// Get the current WebPage
        /// </summary>
        public static Page CurrentNode
        {
            get
            {
                var node = RootNode;
                var url = HttpContext.Current.Request.Path.TrimStart('/').TrimEnd('/').ToLower();

                if (GetAllNodes != null && GetAllNodes.Find(p => p.Route == url) != null)
                    node = Page.Find(MenuHelper.GetAllNodes.Find(p => p.Route.ToLower() == url).PageId);

                return node;
            }
        }


        /// <summary>
        /// Geta all nodes
        /// </summary>
        public static List<Page> GetAllNodes
        {
            get
            {
                var list = Page.FindAll().ToList().Where(d => (d.Domain == Settings.CreateInstance().GetValue("DefaultDomain")) && d.Visible).ToList();
              
                return list;
            }
        }


        /// <summary>
        /// Get parent page
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public static Page Parent(this Page parent)
        {
            return Page.Find(parent.PageId);
        }




        /// <summary>
        /// Get all childitems in the current menulevel
        /// </summary>
        /// <returns></returns>
        public static List<Page> LevelChildren()
        {
            string pos = LevelRoot().Position;
            return MenuHelper.GetAllNodes.FindAll(o => (o.Position.StartsWith(pos)));
        }


        /// <summary>
        /// Get all childitems in the current menulevel
        /// </summary>
        /// <returns></returns>
        public static List<Page> TopLevelsOnly()
        {
            return MenuHelper.GetAllNodes.FindAll(o => o.Position.Count(c => c == '.') == 0);
        }


        /// <summary>
        /// Get current levels root item
        /// </summary>
        /// <returns></returns>
        public static Page LevelRoot()
        {
            string pos = CurrentNode.Position;

            if (pos.IndexOf(".", System.StringComparison.Ordinal) > 0)
                pos = pos.Substring(0, pos.IndexOf(".", System.StringComparison.Ordinal));

            return GetAllNodes.Find(o => o.Position == pos);
        }


        public static void DeletePage(int id)
        {

            var deleteList = GetAllNodes.FindAll(d => d.ParentId == Convert.ToInt32(id));

            if (deleteList.Any())
            {
                foreach (var del in deleteList)
                {
                    if (del.PageId != RootNode.PageId)
                    {
                        var delid = del.PageId;
                        del.Delete();
                        //Recursive delete all pages below this page.
                        DeletePage(delid);
                    }
                }
            }
            var page = new Page(Convert.ToInt32(id));
            //page.Destroy();
        }
    }
}