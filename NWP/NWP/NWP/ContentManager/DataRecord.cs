﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Castle.ActiveRecord;
using NWP.Cache;

namespace NWP.ContentManager
{
    /// <summary>
    /// DataRecord, just passes all inherited classes to ActiveRecord
    /// </summary>
    /// <typeparam name="T">Type of data to persist</typeparam>
      [DataContract]
    public abstract class DataRecord<T> : ActiveRecordBase<T>, IDependency
    {
        protected T data;
        public abstract bool TrashBin { get; set; }
        

        protected DataRecord()
        {
        }

    }
}