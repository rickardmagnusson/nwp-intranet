﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.ActiveRecord;
using NWP.Cache;

namespace NWP.ContentManager
{
    /// <summary>
    /// Base class for all modules, widgets etc.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class DataPersister<T> : ActiveRecordBase<T>, IDependency
    {
        protected T data;

        protected DataPersister()
        {
           
        }
    }
}