﻿using System;
using System.Collections.Generic;
using Castle.ActiveRecord;
using NHibernate.Criterion;

namespace NWP.ContentManager
{
    /// <summary>
    /// Persists data back to a controller.
    /// Dynamic implementation.
    /// </summary>
    public class DataMediator
    {
        public T LoadById<T>(int key) where T : class
        {
            return ActiveRecordMediator<T>.FindByPrimaryKey(key, true);
        }

        public T LoadById<T>(string key) where T : class
        {
            return ActiveRecordMediator<T>.FindByPrimaryKey(key, true);
        }

        public T LoadByRoute<T>(string column, string key) where T : class
        {
            ICriterion criteria = Restrictions.Eq(column, Convert.ToInt32(key));
            return ActiveRecordMediator<T>.FindOne(criteria);
        }

        public T LoadByValue<T>(string column, string key) where T : class
        {
            ICriterion criteria = Restrictions.Eq(column, key);
            return ActiveRecordMediator<T>.FindOne(criteria);
        }

        public IList<T> FindResults<T>(ICriterion[] criteria) where T : class
        {
            return ActiveRecordMediator<T>.FindAll(criteria);
        }

        public IList<T> FindResults<T>() where T : class
        {
            return ActiveRecordMediator<T>.FindAll();
        }

        //Loads between to dates(For eg. in News)
        public IList<Order> LoadByDateRange(DateTime start, DateTime end)
        {
            var criteria = new ICriterion[] { Restrictions.Between("Date", start, end) };
            return ActiveRecordMediator<Order>.FindAll(criteria);
        }   
    }
}