﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NWP.Widgets;

namespace NWP.ContentManager
{
    public static class DataModel
    {
        /// <summary>
        /// Assaign data to a Widget. Call <see cref="Save<Model>()"/> after UpdateModel to save data.
        /// </summary>
        /// <param name="widget"> </param>
        /// <param name="form">FormCollection to save onto T</param>
        public static void UpdateModel(dynamic data, FormCollection form)
        {
            Type type = data.GetType();
            string e = string.Empty;
           
            string o = string.Empty;

            if (data == null)
                throw new Exception("Widget is null?");

            foreach (string name in form)
            {
                //if (name.Equals("WidgetId")) continue;
                //if (name.Equals("WidgetType")) continue;
                //if (name.Equals("PageId")) continue;
                try
                {
                    var info = type.GetProperty(name);
                    var value = Decode(form[name]);
                    o += name + "=" + value + " :" + info.PropertyType + "\n";

                    if (info.PropertyType == typeof(System.Int32) || info.PropertyType == typeof(int))
                        type.GetProperty(name).SetValue(data, Convert.ToInt32(value), null);
                    if (info.PropertyType == typeof(System.String) || info.PropertyType == typeof(string))
                        type.GetProperty(name).SetValue(data, value, null);
                    if (info.PropertyType == typeof(bool) || info.PropertyType == typeof(System.Boolean))
                        type.GetProperty(name).SetValue(data, Check(value), null);
                    if (info.PropertyType == typeof(System.Guid))
                        type.GetProperty(name).SetValue(data, new Guid(value), null);
                    if (info.PropertyType == typeof(System.DateTime))
                        type.GetProperty(name).SetValue(data, DateTime.Parse(value), null); 
                }
                catch(Exception ex)
                {
                    //throw new Exception(ex.ToString() + "\n" + o);
                }
            }
        }

        //MVC form sends two name values of the same type?
        private static bool Check(string value)
        {
            return Convert.ToBoolean(value.Contains(',') ? value.Substring(0, value.IndexOf(',')) : value);
        }


        /// <summary>
        /// Assaign data to a Widget. Call <see cref="Save<Model>()"/> after UpdateModel to save data.
        /// </summary>
        /// <param name="widget"> </param>
        /// <param name="form">FormCollection to save onto T</param>
        public static void UpdateModel(IWidget widget, FormCollection form)
        {
            Type type = widget.GetType();
            string e = string.Empty;
            //string[] data = form.AllKeys;
            string o = string.Empty;

            if(widget==null)
                throw new Exception("Widget is null?");

            foreach (string name in form)
            {
               
                if (name.Equals("WidgetId")) continue;
                try{
                    var value = Decode(form[name]);
                    o += name + "=" + value + "\n";

                    var info = type.GetProperty(name);
                    if (info.PropertyType == typeof(System.Int32) || info.PropertyType == typeof(int))
                        type.GetProperty(name).SetValue(widget, Int32.Parse(value), null);
                    if (info.PropertyType == typeof(System.String))
                        type.GetProperty(name).SetValue(widget, value, null);
                    if (info.PropertyType == typeof(bool))
                        type.GetProperty(name).SetValue(widget, Check(value), null);
                    if (info.PropertyType == typeof(System.Guid))
                        type.GetProperty(name).SetValue(widget, new Guid(value), null);
                    if (info.PropertyType == typeof(System.DateTime))
                        type.GetProperty(name).SetValue(widget, DateTime.Parse(value), null);
                    if (!name.Equals("WidgetType") && (!string.IsNullOrEmpty(value)))
                        type.GetProperty(name).SetValue(widget, value, null);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
            }
        }

        private static string Decode(string s)
        {
            return HttpContext.Current.Server.HtmlDecode(s);
        }
    }
}