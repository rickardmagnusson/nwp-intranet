﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace NWP.ContentManager
{
    /// <summary>
    /// Base DataManager, Read and write's from config files. Used in initalize of website.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class DataManager<T>
    {
        protected T data;
        private string path;
        protected abstract string Config();

        protected DataManager()
        {
            data = (T) Activator.CreateInstance(typeof (T));
        }

        /// <summary>
        /// Static implementation. Loads data from .config files
        /// </summary>
        protected void LoadData()
        {
            path = HttpContext.Current.Server.MapPath(Config().StartsWith("~/App_Data/") ? Config() : string.Format("~/App_Data/{0}.config", Config()));

            lock (path)
            {
                if (!System.IO.File.Exists(path))
                {
                    SaveData();
                }

                using (var reader = System.IO.File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    data = (T)serializer.Deserialize(reader);
                }
            }
        }

        /// <summary>
        /// Save data to name.config
        /// </summary>
        protected void SaveData()
        {
            path = HttpContext.Current.Server.MapPath(Config().StartsWith("~/App_Data") ? Config() : string.Format("~/App_Data/{0}.config", Config()));

            lock (path)
            {
                if (!Directory.Exists(Path.GetDirectoryName(path)))
                    Directory.CreateDirectory(Path.GetDirectoryName(path));

                using (var writer = System.IO.File.Create(path))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    serializer.Serialize(writer, data);
                }
            }
        }

        protected void DeleteData()
        {
           
        }

    }
}