﻿using System;
using System.Web.Helpers;
using System.Web.Mvc;
using NHibernate.Criterion;
using NWP.Models;
using NWP.Mvc.Filters;
using Nwp.Mvc.Filters;
using System.IO;
using Castle.ActiveRecord;
using MySql.Data.MySqlClient;

namespace NWP.Controllers
{
   
    public class CmController : BaseController
    {
        public CmController():base(){
        }

        public ActionResult Index(string id)
        {
            var page = GetPageByRoute(id);
            return View(page.Layout, Repository);
        }

        //widget/{controller}/{action}/{page}/{type}/{zone}
        public ActionResult AddWidget(string page, string type, string zone)
        {
            var pages = GetPageByRoute(page);
            CreateWidget(type, zone);
            return View(pages.Layout, Repository);
        }


        /// <summary>
        /// Processes data from form to database. 
        /// Route: Widget/Edit/1
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="invokeAction"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowHttpPost]
        public ActionResult Process(string widgetId, string invokeAction)
        {
            var form = new FormCollection(Request.Unvalidated().Form);
            return base.SaveModelData(form, widgetId, invokeAction);
        }


        public ActionResult Message(string id, string value, Int32 all)
        {
            var currentuser = NWP.Models.User.FindOne(Restrictions.Eq("UserName", User.Identity.Name));
            if (all == 0)
            {
                var pd = new PrivateDiscussion();
                pd.FromUserId = currentuser.Id;
                pd.UserId = Convert.ToInt32(id);
                pd.Html = value;
                pd.Created = DateTime.Now;
                pd.Create();

                HttpContext.Response.Write(value);
            }
            else
            {
                var users = NWP.Models.User.FindAll();
                foreach (var user in users)
                {
                    var pd = new PrivateDiscussion();
                    pd.FromUserId = currentuser.Id;
                    pd.UserId = user.Id;
                    pd.Html = value;
                    pd.Created = DateTime.Now;
                    pd.Create();

                    HttpContext.Response.Write(value);
                }
            }
            return new EmptyResult();
        }


        
        public ActionResult SaveUserData(string id, string value)
        {
            var user = NWP.Models.User.FindOne(Restrictions.Eq("UserName", User.Identity.Name));
            if (user == null)
                return null;
            switch(id)
            {
                case "DisplayName":
                    user.DisplayName = value;
                    break;
                case "Email":
                    user.Email = value;
                    break;
                case "PersonalDetails":
                    user.PersonalDetails = value;
                    break;
                case "Phone":
                    user.Phone = value;
                    break;
                case "Position":
                    user.Position = value;
                    break;
                case "CompanyName":
                    user.CompanyName = value;
                    break;
                case "UserType":
                    //user.UserType = value;
                    break;
            }

            user.Update();
            user.Save();
            HttpContext.Response.Write(value);
            return new EmptyResult();
        }

        //Webmaster funtion //Min sida
        public ActionResult GetUser(Int32 id) 
        {
            var user = ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("Id", id));
            return Json(user.Active, JsonRequestBehavior.AllowGet);
        }

        //Webmaster funtion //Min sida
        public ActionResult SaveUpdateUser(Int32 id, bool active, string password)
        {
            var user = ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("Id", id));
            user.Active = active;
            user.Update();

            //We need to contact SSO service and update to the new password
            //Alter SSO, Update data.
            if (!String.IsNullOrEmpty(password))
            {
                MySqlConnection ssoconn = new MySqlConnection("Server=127.0.0.1;Database=SSO;Uid=root;Pwd=;");
                ssoconn.Open();
                MySqlCommand ssocomm = ssoconn.CreateCommand();
                string s = string.Format("UPDATE Users Set Password='{0}' WHERE UserName='{1}'", password, user.UserName);
                ssocomm.CommandText = s;
                ssocomm.ExecuteNonQuery();
                ssoconn.Close();
            }

            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveMyPassword(string password)
        {
            var user = ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("UserName", HttpContext.User.Identity.Name));

            //We need to contact SSO service and update to the new password
            //Alter SSO, Update data.
            if (!String.IsNullOrEmpty(password))
            {
                try
                {
                    MySqlConnection ssoconn = new MySqlConnection("Server=127.0.0.1;Database=SSO;Uid=root;Pwd=;");
                    ssoconn.Open();
                    MySqlCommand ssocomm = ssoconn.CreateCommand();
                    string s = string.Format("UPDATE Users Set Password='{0}' WHERE UserName='{1}'", password, user.UserName);
                    ssocomm.CommandText = s;
                    ssocomm.ExecuteNonQuery();
                    ssoconn.Close();
                }
                catch (Exception ex) {
                    throw ex;
                }
            }

            return Json("ok", JsonRequestBehavior.AllowGet);
        }


    }
}
