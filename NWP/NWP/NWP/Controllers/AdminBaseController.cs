﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Configuration;
using NWP.ContentManager;
using NWP.Models;
using NWP.Widgets;
using Nwp.Mvc.Filters;

namespace NWP.Controllers
{
    /// <summary>
    /// Base class for all pages/admincontrollers.
    /// It contain a lot of dynamic implementations to ease the way of creating new views.
    /// Handles such as create, edit save and delete pages. 
    /// Route admin pages.
    /// The controller also loads model for views.
    /// A small implementation for creating Widgets.
    /// </summary> //Attribute removed while debug
    //[AllowAdmin]
    public abstract class AdminBaseController : Controller
    {
        public readonly DataRepository Repository;

        protected AdminBaseController()
        {
            Repository = new DataRepository();
        }

        protected AdminBaseController(dynamic t)
        {
            Repository = new DataRepository();
        }

        protected Page GetPageById(string id)
        {
            Repository.WebPage = LoadByRoute<Page>("PageId", Convert.ToInt32(id));

            return Repository.WebPage;
        }

        [HttpPost]
        public string Save()
        {
            var form = new FormCollection(Request.Unvalidated().Form);
            string pageid = form.Get("PageId");
  
            var page = ActiveRecordMediator<Page>.FindOne(Restrictions.Eq("PageId", Convert.ToInt32(pageid)));

            DataModel.UpdateModel(page, form);
            ActiveRecordMediator.Update(page);

            return pageid;
        }


        public dynamic ModelList
        {
           get{ return CreateModelList<dynamic>();}
        } 

        public dynamic Model(string id)
        {
             return CreateModel<dynamic>(id); 
        }

        public T CreateModel<T>(string id) where T : class
        {
            return Load<T>(new Guid(id));
        }

        public IList<T> CreateModelList<T>() where T : class
        {
            return LoadById<T>().ToList();
        }

        public T Load<T>(Guid key) where T : class
        {
            ICriterion criteria = Restrictions.Eq("WidgetId", key);
            return ActiveRecordMediator<T>.FindOne(criteria);
        }

        public IList<T> LoadById<T>() where T : class
        {
            return ActiveRecordMediator<T>.FindAll();
        }

        public T LoadById<T>(int key) where T : class
        {
            return ActiveRecordMediator<T>.FindByPrimaryKey(key, true);
        }

        public T LoadById<T>(string key) where T : class
        {
            return ActiveRecordMediator<T>.FindByPrimaryKey(key, true);
        }

        public T LoadByRoute<T>(string column, string key) where T : class
        {
            ICriterion criteria = Restrictions.Eq(column, key);
            return ActiveRecordMediator<T>.FindOne(criteria);
        }

        public T LoadByRoute<T>(string column, int key) where T : class
        {
            ICriterion criteria = Restrictions.Eq(column, key);
            return ActiveRecordMediator<T>.FindOne(criteria);
        }

        public void CreateWidget(string widgetType, string zone)
        {
            var page = Repository.WebPage;
            var widget = (IWidget)Activator.CreateInstance(Type.GetType(string.Format("NWP.Widgets.{0}", widgetType)));

           
            widget.IsGlobal = false;
            widget.PageId = page.PageId;
            widget.Zone = zone;
            widget.Domain = page.Domain;

            ActiveRecordMediator.Create(widget);
        }
    }
}
