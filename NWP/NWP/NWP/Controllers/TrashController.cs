﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Queries;
using MySql.Data.MySqlClient;
using NHibernate.Criterion;
using NWP.Cache;
using NWP.Configuration;
using NWP.ContentManager;
using NWP.Models;
using NWP.Utils;
using NWP.Widgets;

namespace NWP.Controllers
{
    public class TrashController : AdminBaseController
    {
        public ActionResult Index()
        {
            var deletedList = new List<DeletedItem>();
            var t = new TypeResolver();
            var types = t.GetValidTypes();

            foreach (var type in types)
            {
                var list = ActiveRecordMediator.FindAll(type, Restrictions.Eq("TrashBin", true));

                foreach (var item in list)
                {
                    var id = 0;

                    if (type == typeof(Page))
                        id = ((Page)item).PageId;
                    else if (type == typeof(TimelineEvent))
                        id = ((TimelineEvent)item).Id;
                    else if (type.GetInterface("IWidget") !=null)
                        id = ((IWidget)(item)).WidgetId;

                    if(id!=0)
                        deletedList.Add(new DeletedItem{ Type=item.GetType().Name, ID = id});
                }
            }
            return View("Trash/Index", deletedList);
        }

        public ActionResult Delete(string type, string id)
        {
            
            Trash.FinalDeleteObject(type, id);

            return RedirectToAction("Index");
        }

        public ActionResult Restore(string type, string id)
        {
           Trash.RestoreObject(type, id);
            
           return RedirectToAction("Index");
        }

    }

    public class DeletedItem
    {
        public string Type { get; set; }
        public int ID { get; set; }
    }
}
