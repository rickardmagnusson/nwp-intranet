﻿using System;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Widgets;

namespace NWP.Controllers
{
    public class LinksController : WidgetBaseController<Links>
    {
        public ActionResult CreateItem(string id)
        {
            var tmodel = CreateModel<Links>(id);
            var item = new LinkItem()
            {
                Name = "",
                TrashBin = false,
                Url = "",
                Links = tmodel
            };
            item.Create();
            return RedirectToAction("EditItem", new { id = item.Id.ToString() });
        }
     
        public ActionResult EditItem(string id)
        {
            return View("Links/EditItem", Repository.Model<LinkItem>("Id", id));
        }

        public ActionResult DeleteItem(string id)
        {
            var item = ActiveRecordMediator<LinkItem>.FindOne(Restrictions.Eq("Id", Convert.ToInt32(id)));
            item.TrashBin = true;
            ActiveRecordMediator.Update(item);
            return Index();
        }

        [HttpPost]
        public ActionResult SaveObject(string id)
        {
            return EditItem(SaveItem<LinkItem>(id));
        }
    }
}
