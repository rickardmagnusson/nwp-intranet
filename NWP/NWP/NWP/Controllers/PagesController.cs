﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Configuration;
using NWP.Models;
using NWP.Widgets;
using System.Text.RegularExpressions;
using NWP.Templates.Utils;
using System.IO;
using Ionic.Zip;
using NWP.Mvc.Filters;
using System.Web.Security;
using Nwp.Mvc.Filters;
using System.Web.Helpers;

namespace NWP.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class PagesController : AdminBaseController
    {
        /// <summary>
        /// Home route
        /// </summary>
        /// <returns>Home page</returns>
        public ActionResult Index()
        {
            return View("pages/index",Repository.Pages);
        }


        /// <summary>
        /// Edit a page
        /// </summary>
        /// <param name="id">The id of the page to edit.</param>
        /// <returns></returns>
        public ActionResult Edit(string id)
        {
            return View("pages/edit", GetPageById(id));
        }

        /// <summary>
        /// Redirect to Search route
        /// </summary>
        /// <param name="id">The if to get</param>
        /// <returns>A search route</returns>
        [HttpPost]
        public ActionResult Search(string id)
        {
            return View("Search/Index", GetPageById(id));
        }


        /// <summary>
        /// Deletes a page
        /// </summary>
        /// <param name="id">The id to delete</param>
        /// <returns>Index</returns>
        public ActionResult Delete(string id)
        {
            ActiveRecordMediator<Page>.FindOne(Restrictions.Eq("PageId", Convert.ToInt32(id))).Delete();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Save page data.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveData(string id)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Title", new Exception("Check all fields, some data seams invalid!"));
                return View();
            }

            Session["pagetab"] = Request.Form["tab"];
            return RedirectToAction("Edit","Pages", new{ id=Save() });
        }


        /// <summary>
        /// Check if a route is available.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AvailableRoute(string id)
        {
            var message = "OK";
            var o = id.Split(',');
            var url = o[0].ToLower();
            var pageid = Convert.ToInt32(o[1]);

            var pages = Page.FindAll(Restrictions.Eq("Route", url)).ToList();
            if (pages.Any())
            {
                message = "ERROR";
                if(pages.Count()==1 && pages[0].PageId==pageid)
                    message = "OK";
            }
  
             return Json(
             new
             {
                 data = message,
                 URL = url,
                 count = pages.Count()

             }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Controll if the route exist.
        /// </summary>
        /// <param name="id">The id(route name)</param>
        /// <returns>Status of created page.</returns>
        [HttpPost]
        public JsonResult CreateRoute(string id)
        {
            var error = "";
            var valid = "ERROR";

            var o = id.Split(',');
            var title = o[0].ToLower();
            var pageid = Convert.ToInt32(o[1]);

            string url = title.CreateValidUrl();
            try{
                var page = Page.FindFirst(Restrictions.Eq("Route", url));
                if (page == null || page.PageId==pageid)
                    valid = "OK";

            }catch(Exception ex){
                error = ex.ToString(); 
            }
            
            return Json(
                new
                {
                    data = url,
                    message = valid,
                    err = error

                }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Creates a new page
        /// </summary>
        /// <returns>Redirect to Edit (Page)</returns>
        public ActionResult AddPage()
        {
            var page = new Page();
            page.MetaTitle = "New page";
            page.MetaDescription = "New page";
            page.MetaKeyWords = "Ny";
            page.MetaDescription = "Beskrivning";
            page.SubText = "Text";
            page.Active = false;
            page.Position = "1";
            page.ParentId = 0;
            page.Created = DateTime.Now.ToShortDateString();
            page.IsGlobal = false;
            page.ParentId = 1;
            page.Route = Guid.NewGuid().ToString();
            page.Domain = "www";
            page.Layout = "_Layout.Home";

            ActiveRecordMediator.Create(page);
            return Edit(page.PageId.ToString());
        }


        /// <summary>
        /// Sort of widgets when drag and drop.
        /// </summary>
        /// <param name="widgets">A comma separated list of widgets</param>
        /// <returns>Ok if success</returns>
        [HttpPost]
        public JsonResult WidgetSort(string widgets)
        {
            var array = widgets.Substring(0, widgets.Length - 1).Split(new[] { ',' });

            for (var index = 1; index < array.Length; index++)
            {
                var s = array[index];
                var id = s;
                var wid = (from t in new TypeResolver().GetAllWidgets()
                           select ActiveRecordMediator.FindAll(t)
                               into widget
                               where widget.Length > 0
                               from item in ((IWidget[])widget)
                               select item
                                   into a
                                   where a.WidgetId == Convert.ToInt32(id)
                                   select a).First();

                wid.SortOrder = Convert.ToString(index);
                wid.Zone = array[0];
                ActiveRecordMediator.Update(wid);
            }

            return Json(
            new
            {
                data = "OK"

            }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Fileuploads
        /// </summary>
        /// <param name="fileData">Posted file</param>
        /// <returns>Ok if success</returns>
        [HttpPost]
        public JsonResult Upload(HttpPostedFileBase fileData)
        {
                var message = "OK";
                try
                {
                    var listid = Request.Form["ListId"]; //Wish attachemntlist this file is in.
                    var folder = Request.Form["folder"]; //Current folder.

                    var path = Server.MapPath("~/UserData/Public/" + folder + "/" + listid);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    var fileName = Path.Combine(path, System.IO.Path.GetFileName(fileData.FileName)); //Full path to file.
                    fileData.SaveAs(fileName);

                    var type = System.IO.Path.GetExtension(fileData.FileName.ToUpper()).Replace(".", "");
                    var contentType = ContentType.JPEG;
                    switch (type)
                    {
                        case "JPEG":
                            contentType = ContentType.JPEG;
                            break;
                        case "PNG":
                            contentType = ContentType.PNG;
                            break;
                        case "GIF":
                            contentType = ContentType.GIF;
                            break;
                        case "PDF":
                            contentType = ContentType.PDF;
                            break;
                        case "ZIP":
                            contentType = ContentType.BINARY;
                            break;
                        case "RAR":
                            contentType = ContentType.BINARY;
                            break;
                        case "doc":
                            contentType = ContentType.WORDDOC;
                            break;
                        case "XLS":
                            contentType = ContentType.EXCEL;
                            break;
                        case "EPS":
                            contentType = ContentType.EPS;
                            break;
                        case "AI":
                            contentType = ContentType.AI;
                            break;
                    }

                    Attachment a = new Attachment();
                    a.SortOrder = 1;
                    a.ListId = listid;
                    a.FileName = fileData.FileName;
                    a.FilePath = fileName;
                    a.ContentType = contentType;
                    a.TrashBin = false;
                    a.Create();
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
                

                return Json(
                    new
                    {
                        data = message

                    }, JsonRequestBehavior.AllowGet);
        }


        //[HttpPost]
        public ActionResult UserImageUpload()
        {
            ProfileViewModel model = new ProfileViewModel();

            var image = WebImage.GetImageFromRequest();
            

            if (image != null)
            {
                if (image.Width > 500)
                {
                    image.Resize(500, ((500 * image.Height) / image.Width));
                }
                var filename = Path.GetFileName(image.FileName);
                image.Save(Path.Combine(Server.MapPath("~/UserData/Tmp"), filename));
                filename = Path.Combine(Server.MapPath("~/UserData/Tmp"), filename);
                model.ImageUrl = "/UserData/Tmp/" + Path.GetFileName(filename);
                var editModel = new EditorInputModel()
                {
                    Profile = model,
                    Width = image.Width,
                    Height = image.Height,
                    Top = image.Height * 0.1,
                    Left = image.Width * 0.9,
                    Right = image.Width * 0.9,
                    Bottom = image.Height * 0.9
                };
                Repository.EditorInputModel = editModel;

                return PartialView("/themes/www/Views/Editor.cshtml", editModel);
            }
            Repository.ProfileViewModel = model;

            return PartialView("/themes/www/Views/DisplayTemplates/ProfileImage.cshtml");
        }

        public ActionResult SaveUserImage(EditorInputModel editor)
        {
             var user = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", HttpContext.User.Identity.Name));

            var image = new WebImage("~" + editor.Profile.ImageUrl);
            var height = image.Height;
            var width = image.Width;
            image.Crop((int)editor.Top, (int)editor.Left, (int)(height - editor.Bottom), (int)(width - editor.Right));

            var originalFile = editor.Profile.ImageUrl;
            string ext = System.IO.Path.GetExtension(editor.Profile.ImageUrl);

            editor.Profile.ImageUrl = Server.MapPath(string.Format("/Themes/www/Styles/Images/Users/{0}{1}", user.Id, ext));

            image.Resize(100, 100, true, false);
            image.Save(@"" + editor.Profile.ImageUrl);

            return View("/themes/www/Views/DisplayTemplates/Close.cshtml");
        }


        /// <summary>
        /// Deletes attachement(file)
        /// </summary>
        /// <param name="id">The id to delete</param>
        /// <returns>Ok if success</returns>
        [HttpPost]
        public JsonResult GetImageInfo(HttpPostedFileBase fileData)
        {
            var id = Request.Form["Id"]; 
            var a = Attachment.FindOne(Restrictions.Eq("Id", Convert.ToInt32(id)));
            var info = a.Html;
            return Json(
             new
             {
                 data = info

             }, JsonRequestBehavior.AllowGet);
        }




        /// <summary>
        /// Save attachment text
        /// </summary>
        /// <param name="id">The id to put data into</param>
        /// <returns>Ok if success</returns>
        [HttpPost]
        public JsonResult PutImageInfo(HttpPostedFileBase fileData)
        {
            var id = Request.Form["Id"]; 
            var text = Request.Form["text"]; 
            var a = Attachment.FindOne(Restrictions.Eq("Id", Convert.ToInt32(id)));
            a.Html = text;
            a.Save();
           
            return Json(
             new
             {
                 data = "OK"
             }, JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        /// Save attachment text
        /// </summary>
        /// <param name="id">The id to put data into</param>
        /// <returns>Ok if success</returns>
        [HttpPost]
        public JsonResult DeleteFile(HttpPostedFileBase fileData)
        {
            var id = Request.Form["Id"];
           
            var a = Attachment.FindOne(Restrictions.Eq("Id", Convert.ToInt32(id)));
            a.Delete();

            return Json(
             new
             {
                 data = "OK"
             }, JsonRequestBehavior.AllowGet);
        }
        
        /// <summary>
        /// Force download of a file.
        /// </summary>
        /// <param name="pathCode">The id in database to delete</param>
        /// <returns>The file to download</returns>
        /// 
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Download(string pathCode)
        {
            var FileName = System.IO.Path.GetFileName(pathCode);
            var path = pathCode;

            var user = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", HttpContext.User.Identity.Name));
            var logg = new NWP.Models.Logg { Added = DateTime.Now.ToString(), FileName = FileName, UserId = user.Id, TrashBin = false, Type="Download", Path="" };
            logg.Create();

            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", FileName));
            Response.WriteFile(path);

            Response.End();
            Response.StatusCode = 200;
            return null;
        }

        /// <summary>
        /// Creates an archive av all common and userfiles from the folders/ UserData/Common and /userData/Groups/"CompanyName"
        /// </summary>
        /// <returns>A zipped archive</returns>
        [HttpGet]
        public ActionResult CreateZip()
        {
            var companyname = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", HttpContext.User.Identity.Name)).Company;

            Response.Clear();
            Response.BufferOutput = false;
           
            string archiveName = string.Format("{0}-{1}.zip", companyname, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
            Response.ContentType = "application/zip";
            Response.AddHeader("content-disposition", "inline; filename=\"" + archiveName + "\"");
        
            using (ZipFile zip = new ZipFile(archiveName))
            {
                string filePath = HttpContext.Server.MapPath("/UserData/Common");
                var dir = Directory.GetFiles(filePath);

                foreach (var f in dir) {
                    zip.AddFile(f, "Common");
                }
                
                filePath = HttpContext.Server.MapPath(string.Format("/UserData/Groups/{0}", companyname));
                dir = Directory.GetFiles(filePath);
                foreach (var f in dir){
                    zip.AddFile(f, companyname);
                }
                zip.Save(Response.OutputStream);
            }

            var user = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", HttpContext.User.Identity.Name));

            var logg = new NWP.Models.Logg { Added = DateTime.Now.ToString(), FileName = archiveName, UserId = user.Id, TrashBin = false, Type = "Zip", Path = "" };
            logg.Create();

            Response.End();
            return null;
        }
    }
}
