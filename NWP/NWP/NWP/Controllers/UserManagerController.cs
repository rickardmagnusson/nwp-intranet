﻿using Castle.ActiveRecord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NWP.Controllers
{
    public class UserManagerController : AdminController
    {
        //
        // GET: /UserManager/
        public ActionResult Index()
        {
            return View("UserManager/Index");
        }

        public ActionResult GetAllUsers()
        {
            return Json("", JsonRequestBehavior.AllowGet);
        }


    }
}
