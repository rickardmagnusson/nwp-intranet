﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using NWP.ViewModels;

namespace NWP.Controllers
{
    public class SearchController : AdminBaseController
    {
        [HttpPost]
        public ActionResult Index()
        {
            var form = new FormCollection(Request.Unvalidated().Form);
            var q = form["searchstring"];
            return View("Search/Index", new SearchViewModel(q));
        }
    }
}
