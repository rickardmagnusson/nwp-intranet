﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using NWP.Models;
using NWP.ViewModels;

namespace NWP.Controllers
{
    public class MediaManagerController : Controller
    {
        public ActionResult Index()
        {
            var m = new MediaManagerViewModel(Request["path"]);
            ViewData["Menu"] = m.BreadCrumb;
            return View("MediaManager/Index", m);
        }

        /// <summary>
        /// Everything below this Action is handled by the ajax calls from popupbrowser(TinyMCE).
        /// </summary>
        /// <returns></returns>
        public ActionResult Browser()
        {
            return View("Browser");
        }


        public void FileData(string path)
        {
            var directory = new DirectoryInfo(Server.MapPath(path));
            foreach (var file in directory.GetFiles())
            {
                Response.Write(string.Format("<div class='item'><img src='{0}'/></div>", ContructPath(file.FullName)));
            }
            
        }

        public void TreeData()
        {
            var directory = new DirectoryInfo(Server.MapPath("/UserData"));
            var txt = new StringWriter();
            var writer = new HtmlTextWriter(txt);

            CreateTree(writer, directory.GetDirectories(), true);
            Response.Write(txt.ToString());  
        }

        public void CreateTree(HtmlTextWriter writer, IEnumerable<DirectoryInfo> collection, bool first)
        {
            writer.WriteFullBeginTag("ul");
     
            foreach (var data in collection)
            {
                writer.WriteBeginTag("li");
                writer.WriteAttribute("class", "expandable");
                writer.Write(HtmlTextWriter.TagRightChar);

                writer.WriteBeginTag("div");
                writer.WriteAttribute("class", "hitarea expandable-hitarea");
                writer.Write(HtmlTextWriter.TagRightChar);
                writer.WriteEndTag("div");

                writer.WriteBeginTag("a");
                writer.WriteAttribute("href", ContructUrl(data.FullName));
                writer.Write(HtmlTextWriter.TagRightChar);
                writer.Write(data.Name);
                writer.WriteEndTag("a");

                if (data.GetDirectories().Any())
                    CreateTree(writer, data.EnumerateDirectories(), false);
                writer.WriteEndTag("li");
            }
            writer.WriteEndTag("ul");
        }


        public string ContructUrl(string path)
        {
            return path.Substring(path.ToLower().LastIndexOf("userdata")-1).Replace("\\", "/");
        }

        public string ContructPath(string path)
        {
            return path.Substring(path.ToLower().LastIndexOf("userdata")-1).Replace("\\", "/");
        }


  

        [HttpPost]
        public ActionResult CreateFolder(string path, string newname)
        {
            Directory.CreateDirectory(path + "\\" + newname);
           
            return null;
        }

        public void MoveDirectory(string source, string target)
        {
            var stack = new Stack<Folders>();
            stack.Push(new Folders(source, target));

            while (stack.Count > 0)
            {
                var folders = stack.Pop();
                Directory.CreateDirectory(folders.Target);
                foreach (var file in Directory.GetFiles(folders.Source, "*.*"))
                {
                    string targetFile = Path.Combine(folders.Target, Path.GetFileName(file));
                    if (System.IO.File.Exists(targetFile)) System.IO.File.Delete(targetFile);
                    System.IO.File.Move(file, targetFile);
                }

                foreach (var folder in Directory.GetDirectories(folders.Source))
                {
                    stack.Push(new Folders(folder, Path.Combine(folders.Target, Path.GetFileName(folder))));
                }
            }
            Directory.Delete(source, true);
        }

        public class Folders
        {
            public string Source { get; private set; }
            public string Target { get; private set; }

            public Folders(string source, string target)
            {
                Source = source;
                Target = target;
            }
        }
    }
}
