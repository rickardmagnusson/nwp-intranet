﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Configuration;
using NWP.Models;
using NWP.Widgets;

namespace NWP.Controllers
{
    /// <summary>
    /// Admin operations of widget. Add edit, delete any widget.
    /// </summary>
    public class WidgetManagerController : AdminBaseController
    {
        public ActionResult Index()
        {
            return View("WidgetManager/index",Repository);
        }

        public ActionResult Edit(string widgetid)
        {
            Repository.WebPage = GetPageById(widgetid);
            return View("WidgetManager/edit", Repository);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            var widget = (from t in new TypeResolver().GetAllWidgets()
                    select ActiveRecordMediator.FindAll(t)
                        into widgets
                        where widgets.Length > 0
                        from item in ((IWidget[])widgets)
                        select item
                            into a
                            where a.WidgetId == Convert.ToInt32(id)
                            select a).First();

            ActiveRecordMediator.Delete(widget);

            return Json(
            new
            {
                data = "DELETED",
                WidgetID=id
            }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddWidget(string page, string type, string zone)
        {
            var widget = (IWidget)Activator.CreateInstance(Type.GetType(string.Format("NWP.Widgets.{0}",type)));
            widget.IsGlobal = true;
            widget.PageId = Convert.ToInt32(page);
            widget.Zone = zone;
            widget.Domain = "www";

            ActiveRecordMediator.Create(widget);

            return Json(
            new
            {
                data = "OK",
                WidgetID = widget.WidgetId, 
                WName = widget.GetType().Name
            }, JsonRequestBehavior.AllowGet);
        }
    }
}