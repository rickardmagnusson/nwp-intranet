﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.WebPages;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Configuration;
using NWP.ContentManager;
using NWP.Models;
using NWP.Widgets;

namespace NWP.Controllers
{
    /// <summary>
    /// Base class for all widgets.
    /// This class i hihgly dynamic and has several dymamic loads of Models for a widget. 
    /// It can also create subitems from ActiveRecord class. For eg. News ==> Contains a NewsItem
    /// Save's / create, edit and delete a widget.
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public abstract class WidgetBaseController<TModel>: Controller where TModel : class
    {
        public readonly DataRepository Repository;

        /// <summary>
        /// Copy a widget.
        /// </summary>
        /// <param name="id">The widgetid to copy from</param>
        /// <returns>View (Edit)</returns>
        public ActionResult Copy(string id)
        {
            var widget = CreateCopy<TModel>(id);
            var wid = ((IWidget) widget).WidgetId.ToString();
            
            return Edit(wid);
        }

        /// <summary>
        /// Implementation of above.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        private IWidget CreateCopy<T>(string id) where T:class
        {
            var obj = ActiveRecordMediator<T>.FindOne(Restrictions.Eq("WidgetId", Convert.ToInt32(id)));
            var newwidget = ActiveRecordMediator<T>.SaveCopy(obj);
            ((IWidget)newwidget).Title = ((IWidget)newwidget).Title + " (Kopia)";
            ActiveRecordMediator.Create(newwidget);
            
            return (IWidget) newwidget;
        }
        
        /// <summary>
        /// Loads Index View
        /// </summary>
        /// <returns>Return Index view in widget folder.</returns>
        public virtual ActionResult Index()
        {
            return View(string.Format("{0}/Index", typeof(TModel).Name), Repository.ModelList<TModel>());
        }

        /// <summary>
        /// Edit view
        /// </summary>
        /// <param name="id">The Widget to edit</param>
        /// <returns>The Edit view in widget folder.</returns>
        public virtual ActionResult Edit(string id)
        {
            return View(string.Format("{0}/Edit", typeof(TModel).Name), Repository.Model<TModel>(id));
        }

        /// <summary>
        /// Save widget data
        /// </summary>
        /// <param name="id">The id to save onto</param>
        /// <returns>The Edit view</returns>
        [HttpPost]
        public virtual ActionResult SaveData(string id)
        {
            Response.Redirect(string.Format("/admin/{0}/Edit/{1}", typeof(TModel).Name,Save<TModel>(id)));
            return View();
        }

        /// <summary>
        /// Delete's a widget
        /// </summary>
        /// <param name="id">The widget id to delete.</param>
        /// <returns>View Index</returns>
        public virtual ActionResult Delete(string id)
        {
            var item = ActiveRecordMediator<TModel>.FindOne(Restrictions.Eq("WidgetId", Convert.ToInt32(id)));
            ((IWidget)item).TrashBin = true;
            ActiveRecordMediator.Update(item);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Creates a Widget
        /// </summary>
        /// <returns>Edit view</returns>
        public virtual ActionResult Create()
        {
            var name = typeof (TModel).Name;
            if (name.IndexOf('.') > 0)
                name = name.Substring(name.LastIndexOf('.'));

            var widget = (IWidget)Activator.CreateInstance(Type.GetType(string.Format("NWP.Widgets.{0}", name)));

            widget.Title = "Ny " + name;
            widget.Active = false;
            widget.Domain = "www";
            widget.TrashBin = false;
            widget.SortOrder = "1";
            widget.IsGlobal = false;
            widget.PageId = 1;
            widget.Zone = "CONTENT";
            widget.Domain = "www";

            ActiveRecordMediator.Create(widget);
            Response.Redirect("/admin/" + name + "/edit/" + widget.WidgetId.ToString());
            return View();
        }

        protected WidgetBaseController()
        {
            var m =  typeof(TModel).Name;
            var localizedName = Resources.RC.ResourceManager.GetString(m);
            ViewData["MENU"] = "";

            string menudata = string.Format("<li class='current'><a href='/admin/{0}/create'>Skapa {1}</a></li>", m, localizedName);
            menudata += string.Format("<li class='current'><a href='/admin/{0}/index'>Visa lista</a></li>", m);
            ViewData["MENU"] = menudata;

            Repository = new DataRepository();
        }

        /// <summary>
        /// Bind's the current page to the model 
        /// </summary>
        /// <param name="id">Id to load</param>
        /// <returns>A Page</returns>
        protected Page GetPageById(string id)
        {
            Repository.WebPage = LoadByRoute<Page>("PageId", Convert.ToInt32(id));

            return Repository.WebPage;
        }

        /// <summary>
        /// Save widget data. Dynamic. Takes any widget as TModel.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public string Save<TModel>(string id) where TModel : class
        {
            var form = new FormCollection(Request.Unvalidated().Form);

            if (!ModelState.IsValid) {
                ModelState.AddModelError(form.Get("widgetType"), new Exception("Check all fields, some data seams invalid!"));
                return "";
            }

            string widgetid = form.Get("WidgetId");

            ICriterion criteria = Restrictions.Eq("WidgetId", Convert.ToInt32(widgetid));
            var widget = ActiveRecordMediator<TModel>.FindOne(criteria);

            DataModel.UpdateModel(widget, form);
            ActiveRecordMediator.Update(widget);

            return widgetid;
        }


        /// <summary>
        /// Save any item from ActiveRecord.
        /// </summary>
        /// <typeparam name="TModel">Widget class</typeparam>
        /// <param name="id">Id of widget</param>
        /// <returns></returns>
        [HttpPost]
        public string SaveItem<TModel>(string id) where TModel : class
        {
            var form = new FormCollection(Request.Unvalidated().Form);
            /*if (!ModelState.IsValid)
            {
                ModelState.AddModelError(form.Get("widgetType"), new Exception("Check all fields, some data seams invalid!"));
                return "";
            }*/

            string widgetid = form.Get("Id");

            ICriterion criteria = Restrictions.Eq("Id", Convert.ToInt32(widgetid));
            var widget = ActiveRecordMediator<TModel>.FindOne(criteria);

            DataModel.UpdateModel(widget, form);
            ActiveRecordMediator.Update(widget);

            return widgetid;
        }

        /// <summary>
        /// Delete a widget
        /// </summary>
        /// <typeparam name="TModel">Widget class</typeparam>
        /// <param name="id">Id to delete</param>
        public void DeleteWidget<TModel>(string id) where TModel : class
        {
            var widget = ActiveRecordMediator<TModel>.FindOne(Restrictions.Eq("WidgetId", Convert.ToInt32(id)));
            ((IWidget) widget).TrashBin = true;
            ActiveRecordMediator.Update(widget);
        }

        /// <summary>
        /// Delete a sub item
        /// </summary>
        /// <typeparam name="TModel">ActiveRecord class</typeparam>
        /// <param name="id">The id to delete</param>
        public void DeleteObject<TModel>(string id) where TModel : class
        {
            var o = ActiveRecordMediator<TModel>.FindOne(Restrictions.Eq("Id", Convert.ToInt32(id)));
            ((IWidget)o).TrashBin = true;
            ActiveRecordMediator.Update(o);
        } 


        /*******************************************************************/
        /********************Dynamic operations below***********************/
        /*******************************************************************/
        public dynamic ModelList
        {
            get { return CreateModelList<dynamic>(); }
        } 

        public dynamic Model(string id)
        {
             return CreateModel<dynamic>(id); 
        }

        //Dynamic impl. of the functions above
        public T CreateModel<T>(string id) where T : class
        {
            return Load<T>(id);
        }

        public IList<T> CreateModelList<T>() where T : class
        {
            return LoadById<T>().ToList();
        }


        public T Load<T>(string key) where T : class
        {
            ICriterion criteria = Restrictions.Eq("WidgetId", Convert.ToInt32(key));
            return ActiveRecordMediator<T>.FindOne(criteria);
        }

        public IList<T> LoadById<T>() where T : class
        {
            return ActiveRecordMediator<T>.FindAll(Restrictions.Eq("TrashBin", false));
        }

        public T LoadById<T>(int key) where T : class
        {
            return ActiveRecordMediator<T>.FindByPrimaryKey(key, true);
        }

        public T LoadById<T>(string key) where T : class
        {
            return ActiveRecordMediator<T>.FindByPrimaryKey(key, true);
        }

        public T LoadByRoute<T>(string column, string key) where T : class
        {
            ICriterion criteria = Restrictions.Eq(column, key);
            return ActiveRecordMediator<T>.FindOne(criteria);
        }

        public T LoadByRoute<T>(string column, int key) where T : class
        {
            ICriterion criteria = Restrictions.Eq(column, key);
            return ActiveRecordMediator<T>.FindOne(criteria);
        }

   
    }
}
