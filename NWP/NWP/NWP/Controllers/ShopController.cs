﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Widgets;

namespace NWP.Controllers
{
    public class ShopController : WidgetBaseController<Shop>
    {
        public ActionResult CreateItem(string id)
        {
            var tmodel = CreateModel<Shop>(id);
            var item = new ShopItem()
            {
                Active = true,
                Html = "",
                Title = "Ny produkt",
                Created = DateTime.Now,
                Shop = tmodel
            };
            item.Create();
            return RedirectToAction("EditItem", new { id = item.Id.ToString() });
        }

        public ActionResult EditItem(string id)
        {
            return View("Shop/EditItem", Repository.Model<ShopItem>("Id", id));
        }

        public ActionResult DeleteItem(string id)
        {
            var item = ActiveRecordMediator<ShopItem>.FindOne(Restrictions.Eq("Id", Convert.ToInt32(id)));
            item.TrashBin = true;
            ActiveRecordMediator.Update(item);
            return Index();
        }

        [HttpPost]
        public ActionResult SaveObject(string id)
        {
            return EditItem(SaveItem<ShopItem>(id));
        }

        [HttpGet]
        public JsonResult AddCategory(string text)
        {
            var cat = new ShopCategory{ CategoryName = text, TrashBin=false};
            ActiveRecordMediator<ShopCategory>.Create(cat);
            try
            {
                return Json(
                 new
                 {
                     data = "OK",
                     id = cat.Id,
                     categoryname = cat.CategoryName

                 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) {
                return Json(
                    new
                    {
                        data = ex.ToString()

                    }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
