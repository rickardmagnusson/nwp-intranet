﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NWP.Widgets;
using NHibernate.Criterion;
using Castle.ActiveRecord;

namespace NWP.Controllers
{
    public class CommentsController : WidgetBaseController<Comments>
    {
        public JsonResult AddComment(string uid, string commentid, string message)
        {
            string m = "OK";
            try{
               
                Comments comments =  ActiveRecordMediator<Comments>.FindOne(Restrictions.Eq("WidgetId", Convert.ToInt32(commentid)));
                Comment comment = new Comment { UserId=Convert.ToInt32(uid), Created=DateTime.Now, Message = message, Comments= comments, TrashBin=false };
                comment.Create();

            }catch(Exception ex) {
                m = ex.ToString();
            }
            return Json(
                new
                {
                    data = m

                }, JsonRequestBehavior.AllowGet);
        }
    }
}
