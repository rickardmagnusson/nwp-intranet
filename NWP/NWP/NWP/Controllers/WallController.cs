﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Models;
using NWP.Widgets;
using RelativeTime;

namespace NWP.Controllers
{
    /// <summary>
    /// Wall operations
    /// Using xml to load post's and comment's to the wall on front
    /// </summary>
    public class WallController : WidgetBaseController<Wall>
    {
        /// <summary>
        /// Get comments from selected wall.
        /// </summary>
        /// <param name="id"></param>
        public void GetComments(int id)
        {
            var wall = ActiveRecordMediator<Wall>.FindOne(Restrictions.Eq("WidgetId", id)); //Wall loaded
            var posts = wall.PostItems;                                                     //Postitems loaded
            var sortedList = posts.Cast<WallPost>().OrderByDescending(cust => cust.Id).Take(5);

            var sb = new StringBuilder();
            sb.Append("<messages>");

            foreach (var p in sortedList)
            {
                var post = (WallPost) p;
                sb.Append("<message id='" + post.Id + "'>");
                sb.Append("<user>" + post.PostedByUser + "</user>");
                sb.Append("<likes>" + post.Likes + "</likes>");
                sb.Append("<date>" + post.Date + "</date>");
                sb.Append("<post>"+post.Post+"</post>");
                sb.Append("<comments>");

                foreach (var c in post.CommentItems)
                {
                    var comment = (WallComment) c;
                    /*
                    DateTime comp;
                    DateTime.TryParse(comment.Date, out comp);

                    int yy = comp.Year;
                    int mm = comp.Month;
                    int dd = comp.Day;
                    int h = comp.Hour;
                    int m = comp.Minute;
                    int s = comp.Second;

                   

                    DateTime startTime = DateTime.Now;
                    DateTime endTime = DateTime.Now.AddDays(dd).AddHours(h).AddMinutes(m).AddSeconds( s );
                    TimeSpan t = new TimeSpan(endTime.Ticks);
                    //TimeSpan time = endTime.Subtract(endTime.Ticks);

                    var humantime = t.ToHumanString();
                    */

                    sb.Append("<comment by='"+comment.CommentByUser+"' likes='"+comment.Likes+"' date='"+comment.Date+"'>"+comment.Comment+"</comment>");
                }
                sb.Append("</comments>");
                sb.Append("</message>");
            }

            sb.Append("</messages>");
            
            HttpContext.Response.ContentType = "text/xml";
            HttpContext.Response.Write(sb.ToString());
        }


        /// <summary>
        /// Get next comment's. Contine load comments when scroll reach ends of a page.
        /// </summary>
        /// <param name="id">Wall id</param>
        /// <param name="prevId">Load comments from this id and below</param>
        public void GetNextComments(int id, int prevId)
        {
            var wall = ActiveRecordMediator<Wall>.FindOne(Restrictions.Eq("WidgetId", id)); //Wall loaded
            var posts = wall.GetItemsByTakeNext(prevId, 5);                                 //Postitems loaded
  
            var sortedList = posts.Cast<WallPost>().Take(5);

            var sb = new StringBuilder();
            sb.Append("<messages>");

            foreach (var p in sortedList)
            {
                var post = (WallPost)p;
                sb.Append("<message id='" + post.Id + "'>");
                sb.Append("<user>" + post.PostedByUser + "</user>");
                sb.Append("<likes>" + post.Likes + "</likes>");
                sb.Append("<date>" + post.Date + "</date>");
                sb.Append("<post>" + post.Post + "</post>");
                sb.Append("<comments>");

                foreach (var c in post.CommentItems)
                {
                    var comment = (WallComment)c;
                    sb.Append("<comment by='" + comment.CommentByUser + "' likes='" + comment.Likes + "' date='" + comment.Date + "'>" + comment.Comment + "</comment>");
                }
                sb.Append("</comments>");
                sb.Append("</message>");
            }

            sb.Append("</messages>");

            HttpContext.Response.ContentType = "text/xml";
            HttpContext.Response.Write(sb.ToString());
        }


        /// <summary>
        /// Get next comment's. Contine load comments when scroll reach ends of a page.
        /// </summary>
        /// <param name="id">Wall id</param>
        /// <param name="prevId">Load comments from this id and below</param>
        public void GetSinglePost(int id, int prevId)
        {
            var wall = ActiveRecordMediator<Wall>.FindOne(Restrictions.Eq("WidgetId", id)); //Wall loaded
            var post = WallPost.FindOne(Restrictions.Eq("Id", prevId));                              //Postitems loaded

            var sb = new StringBuilder();
            sb.Append("<messages>");
            sb.Append("<message id='" + post.Id + "'>");
            sb.Append("<user>" + post.PostedByUser + "</user>");
            sb.Append("<likes>" + post.Likes + "</likes>");
            sb.Append("<date>" + post.Date + "</date>");
            sb.Append("<post>" + post.Post + "</post>");
            sb.Append("<comments>");

            foreach (var c in post.CommentItems)
            {
                var comment = (WallComment)c;
                sb.Append("<comment by='" + comment.CommentByUser + "' likes='" + comment.Likes + "' date='" + comment.Date + "'>" + comment.Comment + "</comment>");
            }
            sb.Append("</comments>");
            sb.Append("</message>");
            

            sb.Append("</messages>");

            HttpContext.Response.ContentType = "text/xml";
            HttpContext.Response.Write(sb.ToString());
        }


        /// <summary>
        /// Add a Post.
        /// A new post in Wall
        /// </summary>
        /// <param name="id">Current Wall id</param>
        /// <returns>Ok if success</returns>
        public ActionResult Post(string id)
        {
            var post = id.Split(',');
            var currentWall = post[0];
            var postedText = Server.HtmlEncode(id.Substring(id.IndexOf(',')+1));
            var wall = ActiveRecordMediator<Wall>.FindOne(Restrictions.Eq("WidgetId", Convert.ToInt32(currentWall)));
            var userid = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", HttpContext.User.Identity.Name)).Id;
            var dt = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now, CultureInfo.CreateSpecificCulture("sv-SE")); //Why doesn't date display my date?  
            var wallPost = new WallPost { Date = dt, Likes = 1, Post = postedText, Wall = wall, PostedByUser = userid, TrashBin = false };
            ActiveRecordMediator<WallPost>.Create(wallPost);

            PostRead read;
            foreach (var user in NWP.Models.User.FindAll(Restrictions.Eq("Active", true)))
            {
                if (user.Id != userid) //Dont add to user who made the post.
                {
                    read = new PostRead { IsRead = false, PostId = wallPost.Id, TrashBin = false, UserId = user.Id, Type= PostType.POST };
                    read.Create();
                    read.SaveAndFlush();
                }
            }

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Creates a new Comment to selected Wall
        /// </summary>
        /// <param name="id">Wall id</param>
        /// <returns>Ok if success.</returns>
        public ActionResult Comment(string id)
        {
            var post = id.Split(',');
            var commentText = Server.HtmlEncode(id.Substring(id.IndexOf(',')+1));
            var currentPost = post[0];

            //throw new Exception(id + ";" + commentText + ";" + currentPost);

            var wallpost = ActiveRecordMediator<WallPost>.FindOne(Restrictions.Eq("Id", Convert.ToInt32(currentPost)));
            var userid = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", HttpContext.User.Identity.Name)).Id;
            var dt = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now, CultureInfo.CreateSpecificCulture("sv-SE")); //Why doesn't date display my date? 
            var commentPost = new WallComment { Date = dt, Likes = 1, Comment = commentText, WallPost = wallpost, CommentByUser = userid, TrashBin = false };
            ActiveRecordMediator.Create(commentPost);


            PostRead read;
            foreach (var user in NWP.Models.User.FindAll(Restrictions.Eq("Active", true)))
            {
                if (user.Id != userid) //Dont add to user who made the post.
                {
                    read = new PostRead { IsRead = false, PostId = commentPost.Id, TrashBin = false, UserId = user.Id, Type= PostType.COMMENT };
                    read.Create();
                    read.SaveAndFlush();
                }
            }


            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult UnSetMessage(int id)
        {
            var deleteItem = ActiveRecordMediator<PostRead>.FindOne(Restrictions.Eq("Id", id));
            deleteItem.Delete();

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get current UserName,(Misspelled, but userid is acuallty the username)
        /// </summary>
        /// <param name="id">Id of user</param>
        public void CurrentUserId(string id)
        {
            HttpContext.Response.Write(ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("UserName", HttpContext.User.Identity.Name)).Id);
        }

        /// <summary>
        /// Loads all users.
        /// </summary>
        /// <param name="id">Wall to load</param>
        public void GetAllUsers(int id)
        {
            
            var users = ActiveRecordMediator<User>.FindAll();

            var sb = new StringBuilder();
           
            sb.Append("<?xml version='1.0' encoding='UTF-8'?>");
            sb.Append("<users>");

            foreach (var user in users)
            {
                var post = (User)user;
                sb.Append("<user id='" + user.Id + "'>");
                sb.Append("<name>" + user.DisplayName + "</name>");
                sb.Append("<email>" + user.Email + "</email>");
                sb.Append("<company>" + user.Company + "</company>");
                sb.Append("<phone></phone>");
                sb.Append("<position></position>");
                sb.Append("<usericon>"+user.UserIcon+"</usericon>");
                sb.Append("</user>");
            }

            sb.Append("</users>");

            HttpContext.Response.ContentType = "text/xml";
            HttpContext.Response.Write(sb.ToString());
        }


        /*
         <users>
	        <user id="1">
		        <name>Rickard Magnusson</name>
		        <email>rickard.magnusson@newwave.se</email>
		        <company>NWP Profile</company>
		        <phone>0708 99 80 20</phone>
		        <position>Marknad/Webmaster</position>
	        </user>
         */
    }

    /// <summary>
    /// Helper class for Post and Comments.
    /// </summary>
    public class Message
    {
        public WallPost Messages { get; set; }
        public IList Comments { get; set; }
    }
}



/*
 Klasserna ovan skall producera denna xml struktur.
 <messages>
   <message id="5">
		<user>5</user>
		<likes>1</likes>
		<date>Thu May 10 2012 09:32:41 GMT+0200 (CEST)</date>
		<post>Detta är posten som jag skrivit in i xml filen(2).</post>
    	<comments>
			<comment by="1" likes="2">And this is the long description. See how long it is.</comment>
			<comment by="1" likes="2">And this is the long description. See how long it is.</comment>
			<comment by="1" likes="2">And this is the long description. See how long it is.</comment>
		</comments>
   </message>
 <messages>
     
     */