﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using System.Web.Mvc;
using NWP.Widgets;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json;



namespace NWP.Controllers
{
    public class TimelineController : WidgetBaseController<Timeline>
    {
        public ActionResult CreateItem(string id)
        {
            var tmodel = CreateModel<Timeline>(id);
            var item = new TimelineEvent
                           {

                               Active = true,
                               Html = "",
                               Title = "Nytt event ",
                               Created = DateTime.Now,
                               Timeline = tmodel,
                               EndEventDate = DateTime.Now,
                               EventDate = DateTime.Now
                           };
            item.Create();
            return RedirectToAction("EditItem", new { id = item.Id.ToString() });
        }



        public ActionResult EditItem(string id)
        {
            return View("Timeline/EditEvent", Repository.Model<TimelineEvent>("Id", id));
        }

        public ActionResult DeleteItem(string id)
        {
            var item = ActiveRecordMediator<TimelineEvent>.FindOne(Restrictions.Eq("Id", Convert.ToInt32(id)));
            item.TrashBin = true;
            ActiveRecordMediator.Update(item);
            return Index();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveEvent(string id)
        {
            return EditItem(SaveItem<TimelineEvent>(id));
        }

        [HttpGet]
        public JsonResult GetTimelineJs(Int32? pageid)
        {
            var tm = ActiveRecordMediator<Timeline>.FindFirst(Restrictions.Eq("PageId", pageid));

            var events = new List<date>();
            var datelist = tm.GetItems();
            
            foreach (var item in datelist)
            {
                if (!item.Active) continue;

                events.Add(new date
                {
                    headline = item.Title,
                    text = item.Html.Embed(),
                    startDate = new DateTime(item.EventDate.Year, item.EventDate.Month, item.EventDate.Day, item.EventDate.Hour, item.EventDate.Minute, item.EventDate.Second).ToShortDateString().Replace("-", ","),
                    endDate = new DateTime(item.EndEventDate.Year, item.EndEventDate.Month, item.EndEventDate.Day, item.EndEventDate.Hour, item.EndEventDate.Minute, item.EventDate.Second).ToShortDateString().Replace("-", ",")
                }
                );
            }

            var wrapper = new Wrapper
            {
                timeline = new timeline()
                {
                    headline = tm.Title,
                    text = tm.Html.Embed(),
                    type = "default",
                    lang = "sv",
                    font = "Arvo & PT Sans",
                    date = events
                }
            };

            string json2 = JsonConvert.SerializeObject(wrapper, Formatting.Indented);
            var dez = JsonConvert.DeserializeObject<Wrapper>(json2);

            return Json(dez, JsonRequestBehavior.AllowGet);
        }
     }
        /// <summary>
        /// Helpers for the Timeline classes to return Json objects
        /// </summary>
        public class date
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string headline { get; set; }
            public string text { get; set; }
        }

        public class timeline
        {
            public string headline { get; set; }
            public string text { get; set; }
            public string type { get; set; }
            public string lang { get; set; }
            public List<date> date { get; set; }
            public string font { get; set; }
        }


        public class Wrapper
        {
            public timeline timeline { get; set; }
        }

        public class CDate {
            public Int32 Value { get; set; } //Identifyer for the date
            public DateTime Date { get; set; } //Actuall date
        }


        public static class Ext
        {
            public static string Quote(this string text)
            {
                return string.Format("\"{0}\"", text);
            }

            public static string Embed(this string text)
            {
                if ((!text.Contains("<p>")) && (!text.Contains("</p>")))
                    return string.Format("<p>{0}</p>", text);
                return text;
            }
        }
    

}