﻿using System.Web.Mvc;
using NWP.Providers.Membership;
using NWP.ViewModels;
using NWP.Widgets;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Collections;
using Castle.ActiveRecord;
using NWP.Models;
using MySql.Data.MySqlClient;

namespace NWP.Controllers
{
    public class MemberController : AdminBaseController
    {
        private UserViewModel Model;

        public MemberController()
        {
            Model = new UserViewModel();
        }

        public ActionResult Index()
        {
            return View("member/index" , Model);
        }

        [HttpGet]
        public ActionResult GetRoles(int id)
        {
            var model = new MembershipModel();

            Model.SelectedUser = model.GetUser(id, false).UserName;
            return Json(Model.UserRoles, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAllRoles(string id)
        {
            return Json(Model.AllRoles, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SaveUser()
        {
                string userid, roles, password, active, position, companyname, phone, usericon;

                userid = HttpContext.Request.Params["id"];
                roles = HttpContext.Request.Params["roles"];
                password = HttpContext.Request.Params["password"];
                active = HttpContext.Request.Params["active"];
                position = HttpContext.Request.Params["position"];
                companyname = HttpContext.Request.Params["companyname"];
                phone = HttpContext.Request.Params["phone"];
                usericon = HttpContext.Request.Params["usericon"];

                //Gather all info
                var c = new char[';'];
                bool updatePassword = (string.IsNullOrEmpty(password)) ? false : true;
                bool isActive = (active == "true") ? true : false;
                
                List<string> roleList = new List<string>();

                //Save data
                var user = ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("Id", Convert.ToInt32(userid)));
                user.Active = isActive;
                user.Position = position;
                user.CompanyName = companyname;
                user.Phone = phone;
                user.UserIcon = usericon;
                user.Save();

                if (roles.Contains(";"))
                {
                    var arrList = roles.Split(new char[]{ ';' });
                    roleList.AddRange(arrList);
                }
                else
                {
                    roleList.Add(roles);
                }

                //Remove user from all roles to reset
                var userroles = ActiveRecordMediator<UserInRole>.FindAll(Restrictions.Eq("UserId", user.Id));
                foreach (var roleToRemove in userroles)
                    roleToRemove.Delete();

                //Add user to current roles
                foreach (string role in roleList)
                {
                    var currentRoleId = ActiveRecordMediator<Roles>.FindFirst(Restrictions.Eq("RoleName", role)).Id;
                    UserInRole newRole = new UserInRole { RoleId = currentRoleId, UserId = user.Id, TrashBin = false };
                    newRole.Create();
                }


            if (updatePassword)
            {
                MySqlConnection conn = new MySqlConnection("Server=127.0.0.1;Database=SSO;Uid=root;Pwd=;");
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(string.Format("Update Users Set Password = '{0}' Where UserId={1}", password, user.Id), conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }

     

            return Json(
                      new
                      {
                        data = "ok"
                      }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddNewUser()
        {
            return View("member/addnewuser");
        }



        [HttpGet]
        public JsonResult GetUser(string id)
        {
            NWP.Models.User user = NWP.Models.User.FindFirst(Restrictions.Eq("Id", Convert.ToInt32(id)));

            return Json(
                new
                {
                    active = user.Active,
                    email = user.Email,
                    username = user.UserName,
                    phone = user.Phone,
                    companyname = user.CompanyName,
                    position = user.Position,
                    usericon = user.UserIcon

                }, JsonRequestBehavior.AllowGet);
        }
    }

    public class Role {
        public Int32 Id { get; set; }
        public string RoleName { get; set; }
    }
}
