﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Configuration;
using NWP.ContentManager;
using NWP.Models;
using NWP.Widgets;

namespace NWP.Controllers
{
    /// <summary>
    /// Base controller for all Controllers(Admin,Widget).
    /// </summary>
    public abstract class BaseController : IBaseController
    {
        public readonly DataRepository Repository;
        protected BaseController()
        {
            Repository = new DataRepository();
        }

        public Page GetPageByRoute(string route)
        {
            if (route == null)
                route = "";

            Repository.WebPage = LoadByRoute<Page>("Route", route);

            Repository.Widgets =  (from t in new TypeResolver().GetAllWidgets()
                           select ActiveRecordMediator.FindAll(t)
                           into widget
                           where widget.Length > 0
                           from item in ((IWidget[]) widget)
                           select item
                           into a
                           where a.PageId == Repository.WebPage.PageId
                           select a);

            return Repository.WebPage;
        }

        public void CreateWidget(string widgetType, string zone)
        {
            var page = Repository.WebPage;
            var widget = (IWidget)Activator.CreateInstance(Type.GetType(string.Format("NWP.Widgets.{0}", widgetType)));

            
            widget.IsGlobal = false;
            widget.PageId = page.PageId;
            widget.Zone = zone;
            widget.Domain = page.Domain;

            ActiveRecordMediator.Create(widget);
        }

        /// <summary>
        /// Process the result from a posted form and Invokes the method called with parameters.
        /// </summary>
        /// <param name="form"> </param>
        /// <param name="widgetid">The Widget ID</param>
        /// <param name="invokeAction">The Method to Invoke</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        protected ActionResult SaveModelData(FormCollection form, string widgetid, string invokeAction)
        {
            //var form = new FormCollection(Request.Unvalidated().Form);
            //var widget = (IWidget) ActiveRecordMediator<IWidget>.FindAll().Select(w=> w.WidgetID==widgetid);
            //IWidget widget = (IWidget)Activator.CreateInstance(Type.GetType(string.Format("M3.Razor.Widgets.{0}", widgetType)));
            var widget = LoadById<IWidget>(widgetid);
            var page = new Page(Convert.ToInt32(widget.PageId));

            var param = new List<string>();

            //Find current method to invoke
            var method = widget.GetType().GetMethod(
                    invokeAction,
                    BindingFlags.InvokeMethod |
                    BindingFlags.Instance |
                    BindingFlags.Public
            );

            //Get parameters in class and invoke with current parameters
            var info = method.GetParameters().ToList();


            if (info.Any())
            {
                param.AddRange(info.Select(inf => form[inf.Name]));

                Repository.WebPage = page;
                ViewData["IsPostBack"] = true;

                //Reflect this class information
                method.Invoke(widget, param.ToArray());

                return View(page.Layout, Repository);
            }

            return new EmptyResult();
        }

        string uid = "1";
        public string UserID
        {
            get
            {
                return uid;
            }
            set
            {
                uid = value;
            }
        }
    }
}
