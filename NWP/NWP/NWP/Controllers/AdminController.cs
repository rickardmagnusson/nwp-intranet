﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NWP.Configuration;
using NWP.Widgets;

namespace NWP.Controllers
{
    public class AdminController : AdminBaseController
    {
        //
        // GET: /Admin/
        public ActionResult Index()
        {
            List<IWidget> widgetlist = new List<IWidget>();
          
            foreach (var item in new TypeResolver().GetAllWidgets())
            {
                 IWidget w = (IWidget) Activator.CreateInstance(Type.GetType(string.Format("NWP.Widgets.{0}", item.Name)));
                 widgetlist.Add(w);
            }
           
            Repository.DashboardIcons = widgetlist;
            return View(Repository);
        }
    }
}
