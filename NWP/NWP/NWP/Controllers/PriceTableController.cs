﻿using System;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Widgets;

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Castle.ActiveRecord;

namespace NWP.Controllers
{
    public class PriceTableController : WidgetBaseController<PriceTable>
    {
        public ActionResult CreateItem(string id)
        {
            var tmodel = CreateModel<PriceTable>(id);
            var item = new PriceTableItem()
            {
                Name = "",
                TrashBin = false,
                Url = "",
                PriceTable = tmodel
            };
            item.Create();
            return RedirectToAction("EditItem", new { id = item.Id.ToString() });
        }
     
        public ActionResult EditItem(string id)
        {
            return View("PriceTable/EditItem", Repository.Model<PriceTableItem>("Id", id));
        }

        public ActionResult DeleteItem(string id)
        {
            var item = ActiveRecordMediator<PriceTableItem>.FindOne(Restrictions.Eq("Id", Convert.ToInt32(id)));
            item.TrashBin = true;
            ActiveRecordMediator.Update(item);
            return Index();
        }

        /// <summary>
        /// Method to get next item in PriceTableItem
        /// </summary>
        /// <param name="id">The current id</param>
        /// <returns>RedirectToEdit</returns>
        public ActionResult NextItem(string id)
        {
            var item = ActiveRecordMediator<PriceTableItem>.FindOne(Restrictions.Eq("Id", Convert.ToInt32(id)));
            var nextItems = ActiveRecordMediator<PriceTableItem>.FindAll().OrderBy(o=> o.Id);
            var found = false;
            PriceTableItem foundItem = null;
            
            foreach (var next in nextItems) 
            {
                if (found)  {
                    foundItem = next;
                    break;
                }
                if (next.Id.ToString() == id) {
                    found = true;
                }
            }
           

            if(found)
                return RedirectToAction("EditItem","PriceTable", new {id=foundItem.Id.ToString()});

            return RedirectToAction("EditItem", "PriceTable", new { id = id });
        }

        [HttpPost]
        public ActionResult SaveObject(string id)
        {
            return EditItem(SaveItem<PriceTableItem>(id));
        }
    }
}
