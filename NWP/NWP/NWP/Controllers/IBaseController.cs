﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Configuration;

namespace NWP.Controllers
{
    public class IBaseController : Controller, IRepository
    {
        public T LoadById<T>(int key) where T : class
        {
            return ActiveRecordMediator<T>.FindByPrimaryKey(key, true);
        }

        public T LoadById<T>(string key) where T : class
        {
            return ActiveRecordMediator<T>.FindByPrimaryKey(key, true);
        }

        public T LoadByRoute<T>(string column, string key) where T : class
        {
            ICriterion criteria = Restrictions.Eq(column, key);
            return ActiveRecordMediator<T>.FindOne(criteria);
        }

        public T LoadByRoute<T>(string column, int key) where T : class
        {
            ICriterion criteria = Restrictions.Eq(column, key);
            return ActiveRecordMediator<T>.FindOne(criteria);
        }

        public IList<T> FindResults<T>(ICriterion[] criteria) where T : class
        {
            return ActiveRecordMediator<T>.FindAll(criteria);
        }


        public IList<T> LoadById<T>() where T : class
        {
            return ActiveRecordMediator<T>.FindAll();
        }

        public T Load<T>(Guid key) where T : class
        {
            ICriterion criteria = Restrictions.Eq("WidgetId", key);
            return ActiveRecordMediator<T>.FindOne(criteria);
        }


        //Loads between to dates(For eg. in News)
        public IList<Order> LoadByDateRange(DateTime start, DateTime end)
        {
            var criteria = new ICriterion[]{ Restrictions.Between("Date", start, end) };  
            return ActiveRecordMediator<Order>.FindAll(criteria);
        }   
    }

    public interface IRepository
    {
        T LoadById<T>(int key) where T : class;
        T LoadById<T>(string key) where T : class;
        IList<T> FindResults<T>(ICriterion[] criteria) where T : class;
    }
}
