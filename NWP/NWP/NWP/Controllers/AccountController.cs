﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using NWP.Mvc.Filters;
using NWP.Security;
using Nwp.SSO.Client;
using System.Web.Security;
using Castle.ActiveRecord;
using NWP.Models;
using NHibernate.Criterion;

namespace NWP.Controllers
{
   
    [AllowAnonymous]
    public class AccountController : Controller
    {
        public AccountController()
            : this(null)
        {
        }

        public AccountController(IFormsAuthentication formsAuth)
        {
            FormsAuth = formsAuth ?? new FormsAuthenticationService();
        }

        public IFormsAuthentication FormsAuth
        {
            get;
            private set;
        }

        [AllowAnonymous]
        [LayoutInjecter("_Layout.Login")]
        public ActionResult Authenticate()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllowAnonymous]
        public ActionResult Authenticate(string token, bool createPersistentCookie)
        {

            var auth = (Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value);

            var client = new SSOPartnerServiceClient("partnerEndpoint");
            var user = client.ValidateToken(token);

            if (string.IsNullOrEmpty(user.Username) || Guid.Empty.Equals(user.SessionToken))
            {
                try //If the username is wrong an exception will be thrown. Catch it.
                {
                    NWP.Models.User loguser = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", user.Username));
                    var logg = new NWP.Models.Logg { Added = DateTime.Now.ToString(), FileName = "", UserId = loguser.Id, TrashBin = false, Type = "LoginFailed", Path = "" };
                    logg.Create();
                }
                catch { }

                return Json(new { result = "DENIED", USERNAME= user.Username });
            }


            var loginuser = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", user.Username));
            var logger = new NWP.Models.Logg { Added = DateTime.Now.ToString(), FileName = "", UserId = loginuser.Id, TrashBin = false, Type = "Login", Path = "" };
            logger.Create();

            FormsAuth.SignIn(user, createPersistentCookie);

            return Json(new { result = "SUCCESS" }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [LayoutInjecter("_Layout.Login")]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [LayoutInjecter("_Layout.Login")]
        public ActionResult LogOn()
        {
            return View();
        }

        [AllowAnonymous]
        [LayoutInjecter("_Layout.Login")]
        public ActionResult LogOff()
        {
            return RedirectToAction("Exit", "Account");
        }

        [AllowAnonymous]
        [LayoutInjecter("_Layout.Login")]
        public ActionResult Exit()
        {
            FormsAuth.SignOut();
            return View();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity is WindowsIdentity)
            {
                throw new InvalidOperationException("Windows authentication is not supported.");
            }
        }
    }
}
