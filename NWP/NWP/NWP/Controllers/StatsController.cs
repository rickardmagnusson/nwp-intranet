﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.ContentManager;
using NWP.Widgets;

namespace NWP.Controllers
{
    public class StatsController : AdminBaseController
    {
        public ActionResult Index()
        {
            return View("Stats/Index");
        }
    }
}
