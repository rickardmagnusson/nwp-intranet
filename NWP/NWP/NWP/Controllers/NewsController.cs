﻿using System;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Widgets;

namespace NWP.Controllers
{
    public class NewsController : WidgetBaseController<News>
    {
        public ActionResult CreateItem(string id)
        {
            var tmodel = CreateModel<News>(id);
            var item = new NewsItem()
            {
                Active = true,
                Html = "",
                Title = "Ny nyhet",
                Created = DateTime.Now,
                News = tmodel,
                Owner = NWP.Models.User.GetUserId(User.Identity.Name)
            };
            item.Create();
            return RedirectToAction("EditItem", new { id = item.Id.ToString() });
        }
     
        public ActionResult EditItem(string id)
        {
            return View("News/EditItem", Repository.Model<NewsItem>("Id", id));
        }

        public ActionResult DeleteItem(string id)
        {
            var item = ActiveRecordMediator<NewsItem>.FindOne(Restrictions.Eq("Id", Convert.ToInt32(id)));
            item.TrashBin = true;
            ActiveRecordMediator.Update(item);
            return Index();
        }

        [HttpPost]
        public ActionResult SaveObject(string id)
        {
            return EditItem(SaveItem<NewsItem>(id));
        }
    }
}
