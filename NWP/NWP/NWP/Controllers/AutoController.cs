﻿using System.IO;
using System.Web.Mvc;

namespace NWP.Controllers
{
    public class AutoController : Controller
    {
        public ActionResult Index()
        {
            return new AutoView();
        }

        public ActionResult Create()
        {
            return new AutoView();
        }

        public ActionResult CreateItem()
        {
            return new AutoView();
        }

        public ActionResult Edit()
        {
            return new AutoView();
        }

        public ActionResult EditItem()
        {
            return new AutoView();
        }

        public ActionResult Save()
        {
            return new AutoView();
        }

        public ActionResult SaveItem()
        {
            return new AutoView();
        }
    }

    public class AutoView : ViewResult
    {
        public AutoView(){}
        public AutoView(dynamic T){}
        public AutoView(IView view, dynamic T){}

        private object Widget { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            base.ExecuteResult(context);
    
        }
    }
}
