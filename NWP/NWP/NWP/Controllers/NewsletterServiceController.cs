﻿using System;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Widgets;

namespace NWP.Controllers
{
    public class NewsletterServiceController : WidgetBaseController<NewsletterService>
    {
        public ActionResult NewsLetter(int year, int month, int day)
        {
            return View("NewsletterService/Newsletter");
        }
    }
}
