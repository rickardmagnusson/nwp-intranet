﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GhostscriptSharp;
using System.IO;

namespace NWP.Tools
{
    public static class PdfTools
    {

        public static void GeneratePreview(HttpPostedFile file)
        {
            string path = HttpContext.Current.Server.MapPath("/UserData/Public/Uploads");
            string filename = Path.Combine(path, Path.GetFileName(file.FileName));
            file.SaveAs(filename);
            CreatePdfImage(filename, path, 100, 200);

        }

        /// <summary>
        /// Throws an exception. Doesn't work
        /// </summary>
        /// <param name="inputPath"></param>
        /// <param name="outputPath"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public static void CreatePdfImage(string inputPath, string outputPath, int width, int height)
        {
            GhostscriptWrapper.GeneratePageThumb(inputPath, outputPath, 1, width, height);
        }
      
    }
}
