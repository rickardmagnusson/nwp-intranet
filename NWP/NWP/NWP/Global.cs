﻿#define TIMERS
using System.Web.Mvc;
using System.Web.Routing;
using NWP.Configuration;
using NWP.Mvc.ViewEngines.Razor;
using Nwp.Mvc.Filters;
using System;
using System.Web;
using System.Web.Security;

namespace NWP
{
   public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new LogonAuthorize()); //Allow only authorized users to view pages
            filters.Add(new AdminAuthorize()); //Allow admins only below /admin
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
            "UserEdit", // Route name
            "user/{action}/{id}/{value}", // URL with parameters
             new { controller = "Cm", action = "Index", id = UrlParameter.Optional, value = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "MediaManager", // Route name
                "mediamanager/{action}/{id}", // URL with parameters
             new { controller = "MediaManager", action = "Browser", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
               "Admin", // Route name
              "admin/{controller}/{action}/{id}", // URL with parameters
               new { controller = "Admin", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
             "Trash", // Route name
            "admin/{controller}/{action}/{type}/{id}", // URL with parameters
             new { controller = "Admin", action = "Index", type=UrlParameter.Optional, id = UrlParameter.Optional } // Parameter defaults
          );

            routes.MapRoute(
              "Wall", // Route name
              "wall/{controller}/{action}/{id}/{prevId}", // URL with parameters
              new { controller = "Wall", action = "Index", id = UrlParameter.Optional, prevId=UrlParameter.Optional } // Parameter defaults
           );

           routes.MapRoute(
               "Widget", // Route name
               "widget/{controller}/{action}/{page}/{type}/{zone}", // URL with parameters
               new { controller = "Cm", action = "AddWidget", type = UrlParameter.Optional, zone = UrlParameter.Optional, page=UrlParameter.Optional } // Parameter defaults
            );

           routes.MapRoute(
               "Pages", // Route name
               "{id}", // URL with parameters
                new { controller = "Cm", action = "Index", id = "" } // Parameter defaults
            );



            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Cm", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            Logg.Start("Application_Start");

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            ViewEngines.Engines.Add(new NWPViewEngine());

            //Settings.CreateInstance().LoadDefaults();
            Host.Init();

            Logg.Start("Application has started");
        }


       /*
        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            if (HttpContext.Current.Request["RequireUploadifySessionSync"] != null)
                UploadifySessionSync();
        }

        /// <summary>
        /// Uploadify uses a Flash object to upload files. This method retrieves and hydrates Auth and Session objects when the Uploadify Flash is calling.
        /// </summary>
        /// <remarks>
        ///     Kudos: http://geekswithblogs.net/apopovsky/archive/2009/05/06/working-around-flash-cookie-bug-in-asp.net-mvc.aspx
        ///     More kudos: http://stackoverflow.com/questions/1729179/uploadify-session-and-authentication-with-asp-net-mvc
        /// </remarks>
        protected void UploadifySessionSync()
        {
            try
            {
                string session_param_name = "SessionId";
                string session_cookie_name = "ASP.NET_SessionId";

                if (HttpContext.Current.Request[session_param_name] != null)
                    UploadifyUpdateCookie(session_cookie_name, HttpContext.Current.Request.Form[session_param_name]);
            }
            catch { }

            try
            {
                string auth_param_name = "SecurityToken";
                string auth_cookie_name = FormsAuthentication.FormsCookieName;

                if (HttpContext.Current.Request[auth_param_name] != null)
                    UploadifyUpdateCookie(auth_cookie_name, HttpContext.Current.Request.Form[auth_param_name]);
            }
            catch { }
        }

        private void UploadifyUpdateCookie(string cookie_name, string cookie_value)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(cookie_name);
            if (cookie == null)
                cookie = new HttpCookie(cookie_name);
            cookie.Value = cookie_value;
            HttpContext.Current.Request.Cookies.Set(cookie);
        }*/
    }
}
