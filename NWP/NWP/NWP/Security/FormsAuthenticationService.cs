﻿using System;
using System.Web;
using System.Web.Security;
using Nwp.SSO.Client;

namespace NWP.Security
{
    /// <summary>
    /// Service to authenticate a user.
    /// </summary>
    public class FormsAuthenticationService : IFormsAuthentication
    {
        public FormsAuthenticationService(){
        }


        /// <summary>
        /// Sign in a user if valid data is sent.
        /// </summary>
        /// <param name="user">Current user</param>
        /// <param name="createPersistentCookie">If create a login cookie.</param>
        public void SignIn(SSOUser user, bool createPersistentCookie)
        {
            DateTime issueDate = DateTime.Now;
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, user.Username, issueDate, issueDate.AddMinutes(20), true, user.SessionToken.ToString());

            string protectedTicket = FormsAuthentication.Encrypt(ticket);

            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, protectedTicket);
            cookie.HttpOnly = true;

            cookie.Expires = issueDate.AddMonths(1);

            HttpContext.Current.Response.Cookies.Add(cookie);
        }


        /// <summary>
        /// Logout a user.
        /// </summary>
        public void SignOut()
        {
            HttpContext.Current.Session.Clear();
            FormsAuthentication.SignOut();
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName);
            cookie.Expires = DateTime.Now.AddDays(-10000.0);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }
}