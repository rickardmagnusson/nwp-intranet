﻿
using Nwp.SSO.Client;

namespace NWP.Security
{
    public interface IFormsAuthentication
    {
        void SignIn(SSOUser user, bool createPersistentCookie);
        void SignOut();
    }
}