﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Models;

namespace NWP.Utils
{
    public static class Position
    {
        /// <summary>
        /// Reorder all numbers in menulist. Depending on "PositionComparer" and "Range".
        /// </summary>
        public static void Reorder()
        {
            var numbers = new Dictionary<Int32, Int32>();
            var items = Pages.OrderBy(m => m.Position, new PositionComparer()).ToList();
            var positions = items.OrderBy(m => m.Position, new PositionComparer()).ToList().Select(p => p.Position).ToList();
            var startLevel = -1;
            var current = 0;

            foreach (var level in positions.Select(pos => pos.Count(c => c == '.')))
            {
                if (level > startLevel)
                    numbers[level] = 1;
                else
                    numbers[level] += 1;
                startLevel = level;

                var result = String.Join(".", numbers.Where(n => n.Key <= level).OrderBy(n => n.Key).Select(n => n.Value.ToString()).ToArray());
                items.ElementAt(current).Position = result;
                items.ElementAt(current).Save();
                current++;
            }
        }

        public static string GetNext(IEnumerable<Page> menuItems)
        {
            var topMenuItem = menuItems.FirstOrDefault();

            if (topMenuItem != null)
            {
                var maxMenuItem = Pages.Where(PositionHasNumber).OrderByDescending(mi => mi.Position, new PositionComparer()).FirstOrDefault();
                var positionParts = maxMenuItem.Position.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries).Where(s => s.Trim() != "");
                if (positionParts.Any())
                {
                    int result;
                    if (int.TryParse(positionParts.ElementAt(0), out result))
                    {
                        return (result + 1).ToString(CultureInfo.InvariantCulture);
                    }
                }
            }

            return "1";
        }

        private static bool PositionHasNumber(Page mi)
        {
            int foo;
            var major = mi.Position.Split('.')[0];
            return !string.IsNullOrEmpty(major) && int.TryParse(major, out foo);
        }

        private static IEnumerable<Page> Pages
        {
            get { return FindResults<Page>(); }
        } 

        private static IList<T> FindResults<T>() where T : class
        {
            return ActiveRecordMediator<T>.FindAll();
        }
    }
}