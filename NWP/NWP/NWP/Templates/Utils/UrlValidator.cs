﻿using NWP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace NWP.Templates.Utils
{
    /// <summary>
    /// Validates Url's
    /// </summary>
    public static class UrlValidator
    {
        public static bool ValidateUrl(this string url)
        {
            return IsValidUrl(url);
        }

        private static bool IsValidUrl(string url)
        {
            url = url.ToLower().Replace("å", "a").Replace("ä", "a").Replace("ö", "o");

            string pattern = @"^(http|https|ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
            Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return reg.IsMatch(url);
        }

        public static string CreateValidUrl(this string title)
        {
            title = title.ToLower().Replace("å", "a").Replace("ä", "a").Replace("ö", "o");     
            // remove entities 
            title = Regex.Replace(title, @"&\w+;", ""); 
            // remove anything that is not letters, numbers, dash, or space 
            title = Regex.Replace(title, @"[^a-z0-9\-\s]", ""); 
            // replace spaces 
            title = title.Replace(' ', '-'); 
            // collapse dashes 
            title = Regex.Replace(title, @"-{2,}", "-"); 
            // trim excessive dashes at the beginning 
            title = title.TrimStart(new [] {'-'}); 
            // if it's too long, clip it 
            if (title.Length > 100) 
                title = title.Substring(0, 99); 
            // remove trailing dashes 
            title = title.TrimEnd(new [] {'-'}); 
            return title; 
        }

    }
}