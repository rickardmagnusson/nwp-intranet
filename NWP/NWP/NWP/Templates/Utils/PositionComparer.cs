﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NWP.Utils
{
    public class PositionComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            if (x == null || y == null)
            {
                return x == null && y == null ? 0 : (x == null ? -1 : 1);
            }
            if (x == "" || y == "")
            {
                return x == "" && y == "" ? 0 : (x == "" ? -1 : 1);
            }

            var xRange = new Range {Length = x.Length};
            var yRange = new Range {Length = y.Length};

            while (xRange.Start != xRange.Length || yRange.Start != yRange.Length)
            {
                var xSize = xRange.NextDot(x);
                var ySize = yRange.NextDot(y);

                if (xSize == 0 || ySize == 0)
                {
                    // one or both sides are empty
                    if (xSize != 0 || ySize != 0)
                    {
                        // favor the side that's not empty
                        return xSize - ySize;
                    }
                    // otherwise continue to the next segment if both are
                }
                else if (xRange.NumericValue != -1 && yRange.NumericValue != -1)
                {
                    // two strictly numeric values 

                    // return the difference
                    var diff = xRange.NumericValue - yRange.NumericValue;
                    if (diff != 0)
                        return diff;

                    // or continue to next segment
                }
                else
                {
                    if (xRange.NumericValue != -1)
                    {
                        // left-side only has numeric value, right-side explicitly greater
                        return -1;
                    }

                    if (yRange.NumericValue != -1)
                    {
                        // right-side only has numeric value, left-side explicitly greater
                        return 1;
                    }

                    // two strictly non-numeric
                    var diff = string.Compare(x, xRange.Start, y, yRange.Start, Math.Min(xSize, ySize),
                                              StringComparison.OrdinalIgnoreCase);
                    if (diff != 0)
                        return diff;
                    if (xSize != ySize)
                        return xSize - ySize;
                }

                xRange.Advance();
                yRange.Advance();
            }

            return 0;
        }
    }
}   