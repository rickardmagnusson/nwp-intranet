﻿using Castle.ActiveRecord.Framework.Config;
using MySql.Data.MySqlClient;
using NWP.Configuration;
using System;

namespace NWP.Utils
{
    /// <summary>
    /// Cms trashcan
    /// Adds any item to the trashcan that can be restored later by administrators.
    /// </summary>
    public static class Trash
    {
        /// <summary>
        /// Restore any deleted item
        /// </summary>
        /// <param name="type">Type of item to restore.</param>
        /// <param name="id">Id of the deleted item</param>
        public static void RestoreObject(string type, string id)
        {
            if(IsWidget(type))
                Execute(string.Format("UPDATE {0} SET TrashBin = 0 Where WidgetId={1}", type, id));
            else 
                Execute(string.Format("UPDATE {0} SET TrashBin = 0 Where Id={1}", type, id));
        }

        /// <summary>
        /// Final delete of an item
        /// </summary>
        /// <param name="type">Type of item to delete</param>
        /// <param name="id">Id of the item to delete.</param>
        public static void FinalDeleteObject(string type, string id)
        {
            if (IsWidget(type))
                Execute(string.Format("DELETE FROM {0} Where WidgetId={1}", type, id));
            else
                Execute(string.Format("DELETE FROM {0} Where Id={1}", type, id));
        }

        private static void Execute(string query)
        {
            var conn = new MySqlConnection(FileUtils.ConnectionStringCombined(FileUtils.CurrentDatabase(), ""));
            conn.Open();
            var cmd = new MySqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }

        public static bool IsWidget(string type) 
        {
            Type t = Type.GetType(string.Format("NWP.Widgets.{0}", type));

            if(t.GetProperty("WidgetId")!=null)
                return true;
            
            return false;
        }
    }
}