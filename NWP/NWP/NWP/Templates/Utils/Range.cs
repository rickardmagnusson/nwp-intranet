﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NWP.Utils
{
    public struct Range
    {
        public int Start { get; private set; }
        private int End { get; set; }
        public int Length { get; set; }

        public int NumericValue { get; private set; }

        public int NextDot(string value)
        {
            if (Start == -1)
            {
                End = -1;
                return 0;
            }

            End = value.IndexOf('.', Start);

            int numeric;
            NumericValue = int.TryParse(Segment(value), out numeric) ? numeric : -1;

            return End == -1 ? Length - Start : End - Start;
        }

        private string Segment(string value)
        {
            if (Start == 0 && End == -1)
            {
                return value;
            }
            if (End == -1)
            {
                return value.Substring(Start);
            }
            return value.Substring(Start, End - Start);
        }

        public void Advance()
        {
            if (End == -1)
                Start = Length;
            else
                Start = End + 1;
        }
    }
}