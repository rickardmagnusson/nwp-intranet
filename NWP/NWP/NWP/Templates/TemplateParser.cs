﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using NWP.Configuration;

namespace NWP.Templates
{
    /// <summary>
    /// Template parser
    /// Reads and load actions for a template file.
    /// </summary>
    public class TemplateParser
    {
        public List<TemplateItem> TemplateItems
        {
            get; set; 
        }

        public TemplateItem Template
        {
            get;
            set;
        }

        /// <summary>
        /// Parse layoutfiles. Reads Templates.txt for a theme.
        /// </summary>
        public void ParseLayouts()
        {
            var list = new List<TemplateItem>();
            var file = FileUtils.GetBaseDir("Themes/" + Settings.CreateInstance().GetValue("DefaultDomain") + "/Views/Templates.txt");
            if (!File.Exists(file))
                return;

            var re = File.OpenText(file);
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                if(!input.StartsWith("#"))
                {
                    var line = input.Split(',');
                    list.Add(new TemplateItem { Name = line[1], FileName = line[0], TemplateHtml = line[3] });
                }
            }
            TemplateItems = list;
            re.Close();
        }

        /// <summary>
        /// Reads all zones from a theme template file.
        /// </summary>
        /// <param name="template"></param>
        public void ParseZoneLayouts(string template)
        {
            var list = new TemplateItem();
            var file = FileUtils.GetBaseDir("Themes/" + Settings.CreateInstance().GetValue("DefaultDomain") + "/Views/Templates.txt");
            if (!File.Exists(file))
                return;

            var re = File.OpenText(file);
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                if (input.StartsWith("#")) continue;
                var temp = string.Concat(template, ".cshtml");
                var line = input.Split(',');
     
                if (line[0].Equals(temp))
                {
                    var pagezones = line[2].Split('|').ToList();
                    list = new TemplateItem { Name = line[1], FileName = line[0], Zones = pagezones, TemplateHtml = line[3]};
                }
            }
            Template = list;
            re.Close();
        }

        /// <summary>
        /// Helper class for loading template zones.
        /// </summary>
        public class ZoneItem
        {
            public string Name { get; set; }
        }

        /// <summary>
        /// Helper for templates.
        /// </summary>
        public class TemplateItem
        {
            public string Name { get; set; }
            public string FileName { get; set; }
            public string TemplateHtml { get; set; }
            public List<string> Zones = new List<string>();


            /// <summary>
            /// Creates a selectlist with the zone items for this template
            /// </summary>
            /// <returns></returns>
            public string ToString(string selectedValue)
            {   
                var sb = new StringBuilder("<select id=\"Zone\" name=\"Zone\" size=\"1\">\n");
                foreach(var item in Zones)
                {
                    string s = Resources.RC.ResourceManager.GetString(item.ToUpper());
 

                    string sel = (item == selectedValue) ? " selected" : "";
                    sb.Append(string.Format("<option value=\"{0}\"{2}>{1}</option>\n",item, s, sel));
                }
                sb.Append("</select>\n");
                return sb.ToString();
            }
        }
    }
}