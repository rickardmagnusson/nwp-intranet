﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Castle.ActiveRecord;
using NWP.ContentManager;
using NHibernate.Criterion;
using System;

namespace NWP.Models
{
    /// <summary>
    /// Users in intranet.
    /// </summary>
    [ActiveRecord]
    public class User : DataRecord<User>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public string UserName  { get; set; }

        [Property(NotNull = false)]
        public string DisplayName { get; set; }

        [Property(NotNull = false)]
        public string UserIcon { get; set; }

        [Property(NotNull = false)]
        public string Email { get; set; }

        [Property(NotNull = false)]
        public string Company { get; set; }

        [Property(NotNull = false)]
        public string CompanyName { get; set; }

        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string PersonalDetails { get; set; }

        [Property(NotNull = false)]
        public string Homepage { get; set; }

        [Property(NotNull = false)]
        public string Position { get; set; }

        [Property(NotNull = false)]
        public string Phone { get; set; }

        [Property(NotNull = false)]
        public bool Active { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        public List<string> Roles { get; set; }

        [Property(NotNull = false)]
        public string Birth { get; set; }

        public static int GetUserId(string username)
        {
            return ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", username)).Id;
        }

        public static User GetUserById(int id)
        {
            return ActiveRecordMediator<User>.FindOne(Restrictions.Eq("Id", id));
        }
    }

    /// <summary>
    /// Represents all  UserGroups in this intranet.
    /// </summary>
    [ActiveRecord]
    public class Groups : DataRecord<Groups>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public string GroupName { get; set; }

        [Property(NotNull = false)]
        public bool Active { get; set; }

        public List<string> Roles { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }
    }

    /// <summary>
    /// All roles.
    /// </summary>
    [ActiveRecord]
    public class Roles : DataRecord<Roles>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public string RoleName { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }
    }


    /// <summary>
    /// Helper to put user and roles in groups.
    /// A users may exist in several groups.
    /// </summary>
    [ActiveRecord]
    public class UserInRole : DataRecord<Roles>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public int RoleId { get; set; }

        [Property(NotNull = false)]
        public int UserId { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }
    }
}