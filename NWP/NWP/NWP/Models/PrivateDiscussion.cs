﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Castle.ActiveRecord;
using NWP.ContentManager;

namespace NWP.Models
{
    [ActiveRecord]
    public class PrivateDiscussion : DataRecord<PrivateDiscussion>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = true)]
        public int UserId{ get; set; }

        [Property(NotNull = true)]
        public int FromUserId { get; set; }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Html { get; set; }

        [Property(NotNull = true)]
        public DateTime Created { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin{ get; set; }

        [Property(NotNull = false)]
        public bool IsRead { get; set; }
    }
}