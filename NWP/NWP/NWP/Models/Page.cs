﻿using System.ComponentModel.DataAnnotations;
using Castle.ActiveRecord;
using NWP.ContentManager;

namespace NWP.Models
{
    /// <summary>
    /// Page class. Contains all data for a page.
    /// </summary>
    [ActiveRecord]
    public class Page : DataRecord<Page>
    {
        public Page():base(){}
        public Page(int pageId)
        {
            PageId = pageId;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int PageId { get; set; }

        [Property(NotNull = true)]
        public int ParentId { get; set; }

        [Property(NotNull = true)]
        public string Layout { get; set; }

        [Property(NotNull = true)]
        public bool Active { get; set; }

        [Property(NotNull = false)]
        public bool Visible { get; set; }

        [Property(NotNull = true)]
        public string Domain { get; set; }

        [Property(NotNull = false)]
        public bool IsGlobal { get; set; }

        [Property(NotNull = true)]
        public string Position { get; set; }

        [Property(NotNull = true)]
        public string Route { get; set; }

        [Required(ErrorMessage = "Please enter a Title for the page")]
        [Property(NotNull = false)]
        public string Title { get; set; }

        [Property(NotNull = false)]
        public string SubText { get; set; }

        [Property(NotNull = true)]
        public string MetaTitle { get; set; }

        [Property(NotNull = true)]
        public string MetaDescription{ get; set; }

        [Property(NotNull = true)]
        public string MetaKeyWords { get; set; }

        [UIHint("Date")]
        [Property(NotNull = false)]
        public string StartDate { get; set; }

        [UIHint("Date")]
        [Property(NotNull = false)]
        public string EndDate { get; set; }

        [UIHint("Date")]
        [Property(NotNull = false)]
        public string Created { get; set; }

        [Property(NotNull = false)]
        public bool UseDate { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }
    }
}
