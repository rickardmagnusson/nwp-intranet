﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Configuration;
using NWP.Mvc.Html;

namespace NWP.Models
{
    /// <summary>
    /// Base class for all pages. Contain a lot of HtmlExtension helpers.
    /// </summary>
    /// <typeparam name="TModel">A dynamic model as strongly typed class</typeparam>
    public abstract class ViewPage<TModel> : WebViewPage<TModel>
    {
        private static ViewContext context;
        private static IViewDataContainer container;
        private readonly HtmlHelperFactory factory = new HtmlHelperFactory();
        public static HtmlHelper<TModel> Html { get; private set; }

        private void CreateHelper(TModel model)
        {
            Html = factory.CreateHtmlHelper(model, new StringWriter(new StringBuilder()));
        }

        internal virtual TModel BaseModel
        {
            get { return base.Model; }
            set { CreateHelper(value); }
        }

        /// <summary>
        /// Initialize all helpers
        /// </summary>
        public override void InitHelpers()
        {
            base.InitHelpers();
            context = ViewContext;
            container = this;
            Html = new HtmlHelper<TModel>(context, container);

            var localeID = Settings.CreateInstance().GetValue("LocaleID");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(localeID);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(localeID);
        }

        #region Helpers


        public static dynamic GetDistinctCompanies()
        {
            return Html.GetDistinctCompanies();
        }

        public static dynamic GetUnreadPosts()
        {
            return Html.GetUnreadPosts();
        }


        public static dynamic LogFile()
        {
            return Html.LogFile();
        }

        public static dynamic ImageExt(string path, string text, string listid)
        {
            return Html.ImageExt(path, text, listid);
        }

        public static dynamic ImageExtPublic(string path, string text, string listid)
        {
            return Html.ImageExtPublic(path, text, listid);
        }

        public static dynamic CompanyLogo()
        {
            return Html.CompanyLogo();
        }

        public static dynamic ImageIcon(string path)
        {
            return Html.ImageIcon(path);
        }

        public static dynamic UserList()
        {
            return Html.UserList();
        }

        /// <summary>
        /// Creates a filee input with markup.
        /// </summary>
        /// <param name="id">The id for this fileupload</param>
        /// <returns>fileupload markup</returns>
        public static dynamic FileUpload(string id)
        {
            return Html.FileUpload(id);
        }

        /// <summary>
        /// Creates a filee input with markup.
        /// </summary>
        /// <param name="id">The id for this fileupload</param>
        /// <returns>fileupload markup</returns>
        public static dynamic FileUpload(string id, string text)
        {
            return Html.FileUpload(id, text);
        }


        /// <summary>
        /// Formats text(html) as html output
        /// </summary>
        /// <param name="text">The text to format</param>
        /// <returns>A html string</returns>
        public static dynamic ToHtml(string text)
        {
            return Html.ToHtml(text);
        }

        
        /// <summary>
        /// Formats a relativepath to an url to UserData folder
        /// </summary>
        /// <param name="text">The path to format</param>
        /// <returns>A html path</returns>
        public static dynamic UrlToRelative(string path)
        {
            return Html.UrlToRelative(path);
        }


        public dynamic WebmasterUserList()
        {
            return Html.WebmasterUserList();
        }

        /// <summary>
        /// Get user from id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static dynamic GetUserById(int id)
        {
            return Html.GetUserById(id);
        }

        /// <summary>
        /// A user name from string id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static dynamic GetUserName(string id)
        {
            return Html.GetUserName(id);
        }


        /// <summary>
        /// A username from id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static dynamic GetUserNameById(int id)
        {
            return Html.GetUserNameById(id);
        }

        public static dynamic GetUserIdByName(string name)
        {
            return Html.GetUserIdByName(name);
        }


        /// <summary>
        /// Page name from id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static dynamic GetPageName(int id)
        {
            return Html.GetPageName(id);
        }

        /// <summary>
        /// Page count in application.
        /// </summary>
        /// <returns></returns>
        public static dynamic PageCount()
        {
            return Html.PageCount();
        }


        /// <summary>
        /// Comment count.
        /// </summary>
        /// <returns>A number of comments in wall</returns>
        public static dynamic CommentCount()
        {
            return Html.CommentCount();
        }

        /// <summary>
        /// User count
        /// </summary>
        /// <returns>A string(Count) of users in this application</returns>
        public static dynamic UserCount()
        {
            return Html.UserCount();
        }

        /// <summary>
        /// A widget list.
        /// </summary>
        /// <returns>A list of widgets</returns>
        public static dynamic GetAllWidgetForList()
        {
            return Html.GetAllWidgetForList();
        }

        /// <summary>
        /// Translate a string.
        /// </summary>
        /// <param name="toTranslate">The value to translate</param>
        /// <returns>The translated string(If exist, else return same parameter as value sent.)</returns>
        public static dynamic RC(string toTranslate)
        {
            return Html.RC(toTranslate);
        }

        /// <summary>
        /// Translate a string.
        /// </summary>
        /// <param name="toTranslate">The html string to parse as value(Extension for Labels)</param>
        /// <returns>The translated string(If exist, else return same parameter as value sent.)</returns>
        public static dynamic RC(MvcHtmlString toTranslate)
        {
            return Html.RC(toTranslate);
        }

        public static dynamic Zone(string zone)
        {
            dynamic model = Html.ViewDataContainer.ViewData.Model;
            return HtmlExtensions.Zone(Html, model, zone);
        }

        /// <summary>
        /// Topmenu
        /// </summary>
        /// <returns>A topmenu</returns>
        public static dynamic TopMenu()
        {
            return Html.Menu(1, "topmenu");
        }

        /// <summary>
        /// Submenu
        /// </summary>
        /// <returns>A submenu from current level</returns>
        public static dynamic SubMenu()
        {
            return Html.SubMenu("submenu");
        }

        public static dynamic ParseLayouts(int id)
        {
            return Html.ParseLayouts(id);
        }

        /// <summary>
        /// Creates a Zone
        /// </summary>
        /// <param name="id">id for the zone</param>
        /// <param name="selectedValue">Selected Page id</param>
        /// <returns></returns>
        public static dynamic Zones(string id, string selectedValue)
        {
            var page = ActiveRecordMediator<Page>.FindOne(Restrictions.Eq("PageId", Convert.ToInt32(id)));
            return Html.Zones(page.Layout, selectedValue);
        }

        /// <summary>
        /// Creates a Zone.
        /// </summary>
        /// <param name="id">id for the zone</param>
        /// <param name="selectedValue">Selected Page id</param>
        /// <returns></returns>
        public static dynamic Zones(int id, string selectedValue)
        {
            var page = ActiveRecordMediator<Page>.FindOne(Restrictions.Eq("PageId", id));
            return Html.Zones(page.Layout, selectedValue);
        }

        /// <summary>
        /// A list of available templates
        /// </summary>
        /// <param name="id">Current pageId</param>
        /// <param name="selectedValue">Selected value</param>
        /// <returns>A list of PageLayouts</returns>
        public static dynamic PageLayouts(int id, string selectedValue)
        {
            return Html.PageLayouts(id, selectedValue);
        }

        /// <summary>
        /// Al list of pages in a selectlist
        /// </summary>
        /// <param name="selectedValue">Selected item in selectlist</param>
        /// <returns>The page that should be selected</returns>
        public static dynamic Pages(int selectedValue)
        {
            return Html.Pages(selectedValue);
        }

        /// <summary>
        /// Child parent pages
        /// </summary>
        /// <param name="selectedValue">Wish page is the parent</param>
        /// <returns>A list of pages with the selected page</returns>
        public static dynamic ParentPages(int selectedValue)
        {
            return Html.ParentPages(selectedValue);
        }

        /// <summary>
        /// A Css meta tag..
        /// </summary>
        /// <param name="link">Link to css file</param>
        /// <param name="isGlobal">If this item is global</param>
        /// <returns>Meta Css</returns>
        public static dynamic Css(string link, bool isGlobal)
        {
            return Html.Css(link, isGlobal);
        }

        /// <summary>
        /// A script meta tag.
        /// </summary>
        /// <param name="link">The script link</param>
        /// <returns>Script meta link</returns>
        public static dynamic Script(string link)
        {
            return Html.Script(link);
        }

        /// <summary>
        /// Return a script from outside script folder.
        /// </summary>
        /// <param name="link">Complete relative link to script</param>
        /// <param name="ext">If the link is external(dummy)</param>
        /// <returns></returns>
        public static dynamic Script(string link, bool ext)
        {
            return Html.Script(link,ext);
        }

        /// <summary>
        /// User logotype
        /// </summary>
        /// <returns>A img object user logo</returns>
        public static dynamic UserLogo()
        {
            return Html.UserLogo();
        }


        /// <summary>
        /// A css link from global css theme folder.
        /// </summary>
        /// <returns></returns>
        public static dynamic Css()
        {
            var isglobal = true;
            dynamic model = Html.ViewDataContainer.ViewData.Model;
            var datamodel = (Models.DataRepository)model;

            if (datamodel.Current.Domain != Settings.CreateInstance().GetValue("DefaultDomain"))
                isglobal = false;

            return Html.Css(datamodel.Current.Name.ToLower().Replace(" ",""), isglobal);
        }


        #endregion
    }
}