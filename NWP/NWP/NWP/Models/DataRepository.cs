﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Configuration;
using NWP.ContentManager;
using NWP.Widgets;
using System.IO;
using System.Web.Mvc;
using System.Web.Helpers;

namespace NWP.Models
{
    public class DataRepository
    {
        /// <summary>
        /// Encapsulates all data.
        /// </summary>
        public DataRepository(){          
        }

        public dynamic ViewModel { get; set; }
        public dynamic ViewList { get; set; }

        /// <summary>
        /// Dynamic functions for widgets and Models
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IList<T> ModelList<T>() where T : class
        {
            return CreateModelList<T>();
        }

        public T Model<T>(string id) where T : class
        {
            return CreateModel<T>(id);
        }

        public T Model<T>(string key, string id) where T : class
        {
            return Load<T>(key, id);
        }

        //Dynamic impl. of the functions above
        private T CreateModel<T>(string id) where T : class
        {
            return Load<T>(id);
        }

        private IList<T> CreateModelList<T>() where T : class
        {
            return LoadAll<T>().ToList();
        } 


        private static IList<T> LoadAll<T>() where T : class
        {
            return ActiveRecordMediator<T>.FindAll(Restrictions.Eq("TrashBin", false));
        }

        private static T Load<T>(string key) where T : class
        {
            ICriterion criteria = Restrictions.Eq("WidgetId", Convert.ToInt32(key));
            return ActiveRecordMediator<T>.FindOne(criteria);
        }


        private static T Load<T>(string column, string id) where T : class
        {
            ICriterion criteria = Restrictions.Eq(column, Convert.ToInt32(id));
            return ActiveRecordMediator<T>.FindOne(criteria);
        }

        public User GetCurrent()
        {
            return ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("UserName", HttpContext.Current.User.Identity.Name));
        }

        public static User GetUser()
        {
            return ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("UserName", HttpContext.Current.User.Identity.Name));
        }

        public User UserInfo(string id)
        {
            return NWP.Models.User.FindOne(Restrictions.Eq("Id", Convert.ToInt32(id)));
        } 

        /*
           Sidans user = 1
         * Mitt UserID = 2
         * Hämta alla diskussioner som är samma som mitt id
         */
        //From = den visade sidans UserId
        public List<PrivateDiscussion> Discussions(int from)
        {
            var currentUser = User.FindOne(Restrictions.Eq("UserName", HttpContext.Current.User.Identity.Name));
            var fromlist = PrivateDiscussion.FindAll().ToList().Where(d => d.UserId == from && (d.UserId == currentUser.Id || d.FromUserId == currentUser.Id)); //Todo: hämta alla meddelanden mellan två personer inkl. den egna loggen är inte klar...
            var discussion = new List<PrivateDiscussion>();
            discussion.AddRange(fromlist);

            return discussion.OrderByDescending(d => d.Id).ToList();
        }

        public string UnreadMessagesCount()
        {
            var currentUser = User.FindOne(Restrictions.Eq("UserName", HttpContext.Current.User.Identity.Name));
            int cnt = PrivateDiscussion.FindAll().ToList().Where(d => d.UserId == currentUser.Id && d.IsRead==false).Count();
            if (cnt > 0)
                return string.Format("<span class='notify red'>{0}</span>", cnt);
            else
                return string.Empty;
        }

        public bool SetRead() 
        {
            var currentUser = User.FindOne(Restrictions.Eq("UserName", HttpContext.Current.User.Identity.Name));
            var updatelist = PrivateDiscussion.FindAll().ToList().Where(d => d.UserId == currentUser.Id);
            foreach (var item in updatelist)
            {

                item.IsRead = true;
                item.UpdateAndFlush();
               
            }

            return false;
        }

        public ProfileViewModel ProfileViewModel { get; set; }
        public EditorInputModel EditorInputModel { get; set; }

      

        /// <summary>
        /// A list of Dashboard icons
        /// </summary>
        public List<IWidget> DashboardIcons { get; set; }

        /// <summary>
        /// Get all pages
        /// </summary>
        /// <returns>A list of WebPage's</returns>
        public List<Page> Pages
        {
            get { return Page.FindAll().OrderBy(s=> s.Position).ToList(); }
        }

        private Page _webPage;
        /// <summary>
        /// Get, set current page
        /// </summary>
        public Page WebPage
        {
            get
            {
                return _webPage;
            } 
            set
            {
                _webPage = value;
            }
        }

        /// <summary>
        /// Return the current Tenant's data
        /// </summary>
        public Tenant Current
        {
            get
            {
             
                return new DataMediator().LoadByValue<Tenant>("Domain", _webPage.Domain);
            }
        }

        public IEnumerable<IWidget> Widgets { get; set; }

    }
}