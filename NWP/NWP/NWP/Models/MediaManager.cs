﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.ActiveRecord;
using NWP.ContentManager;

namespace NWP.Models
{
    [ActiveRecord]
    public class FileManager : DataRecord<FileManager>
    {
        public FileManager() : base(){}

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin{ get; set; }

        public IEnumerable<Folder> Folders { get; set; }
    }

    [ActiveRecord]
    public class Folder
    {
        [Property(NotNull = false)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public string FolderName { get; set; }
        public IEnumerable<Folder> Files { get; set; }

        [Property(NotNull = false)]
        public string Path { get; set; }

        [Property(NotNull = false)]
        public AccessItem AccessItem { get; set; }
    }

    [ActiveRecord]
    public class File
    {
        [Property(NotNull = false)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public string FileName { get; set; }

        [Property(NotNull = false)]
        public string Path { get; set; }

        [Property(NotNull = false)]
        public AccessItem AccessItem{ get; set; }
    }


    [ActiveRecord]
    public class AccessItem
    {
        [Property(NotNull = false)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public string Name { get; set; }

        [Property(NotNull = false)]
        public int FileId { get; set; }

        [Property(NotNull = false)]
        public int UserID { get; set; }
    }
}