﻿using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.ContentManager;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NWP.Models
{
    /// <summary>
    /// Attach files to a any object
    /// </summary>
    [ActiveRecord]
    public class AttachmentList : DataRecord<AttachmentList>
    {
        public AttachmentList():base(){}
        public AttachmentList(int id) {
            Id = id;
        }

        [Property(NotNull = false, Length = 255)]
        public string ListId { get; set; }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public string Title { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [Property(NotNull = false)]
        public bool Active { get; set; }

        [Property(NotNull = false)]
        public int CreatedByUser { get; set; }

        public IEnumerable<Attachment> Items
        {
            get{
                var list = new List<Attachment>();
                var data = ActiveRecordMediator<Attachment>.FindAll().Where(d => d.ListId == ListId).ToList().OrderBy(o=> o.SortOrder);
                if (data.Any())
                    list = data.ToList();
                return list;
            }
        }

        public static AttachmentList GetList(string listid)
        {
            var att = ActiveRecordMediator<AttachmentList>.FindOne(Restrictions.Eq("ListId", listid));
            if (att == null){
                att = new AttachmentList();
                att.ListId = listid;
                att.TrashBin = false;
                att.Title = "Ny lista";
                att.CreatedByUser = 1;
                att.Create();
            }
            return att;
        }



        /// <summary>
        /// Creates a new Attachment
        /// </summary>
        /// <param name="type"></param>
        /// <param name="fileName"></param>
        /// <param name="filePath"></param>
        /// <param name="sortOrder"></param>
        /// <param name="listId"></param>
        /// <returns></returns>
        public static Attachment Create(ContentType type, string fileName, string filePath, int sortOrder, string listId)
        {
            Attachment a = new Attachment();
            a.ContentType = type;
            a.FileName = fileName;
            a.FilePath = filePath;
            a.SortOrder = sortOrder;
            a.ListId = listId;
            a.Html = "";
            ActiveRecordMediator<Attachment>.Create(a);
            return a;
        }

        /// <summary>
        /// Deletes an Attachment from database.
        /// Also deletes the file if there is no other reference to it.
        /// </summary>
        /// <param name="id">Attachment id to delete</param>
        public static void Delete(int id)
        {
            var a = ActiveRecordMediator<Attachment>.FindOne(Restrictions.Eq("Id", id));
            if (!HasManyReferencesWith(a.FilePath)) { //Delete file only if there's only one reference to it.
                System.IO.File.Delete(a.FilePath);
            }
            ActiveRecordMediator.Delete(a);
        }

        public static bool HasManyReferencesWith(string path)
        {
            var count = ActiveRecordMediator<Attachment>.FindAll().Where(d=> d.FilePath==path).ToList().Count();
            if (count > 1)
                return false;
            return true;
        }


    }

    [ActiveRecord]
    public class Attachment : DataRecord<Attachment>
    {
        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false, Length = 255)]
        public string ListId { get; set; }

        [Property(NotNull = false)]
        public string FileName { get; set; }

        [Property(NotNull = false)]
        public string FilePath { get; set; }

        [Property(NotNull = false)]
        public int SortOrder { get; set; }

        [Property(NotNull = false)]
        public ContentType ContentType { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [UIHint("HtmlEdit")]
        [DataType(DataType.MultilineText)]
        [Property(NotNull = false, ColumnType = "StringClob")]
        public string Html { get; set; }


    }

    public enum ContentType
    {
        UNKNOWN = 0,
        BINARY = 1, //ZIP, RAR ETC
        PDF = 2, //PDF files
        JPEG = 3,
        GIF = 4,
        PNG = 5,
        EXCEL = 6,
        WORDDOC = 7,
        EPS = 8,
        AI = 9
    }
}