﻿using System.ComponentModel.DataAnnotations;
using Castle.ActiveRecord;
using NWP.ContentManager;
using System;

namespace NWP.Models
{
    [ActiveRecord]
    public class Logg : DataRecord<Logg>
    {
        public Logg() : base() { }
        public Logg(int id)
        {
            Id = id;
        }

        [PrimaryKey(Generator = PrimaryKeyType.Increment)]
        public int Id { get; set; }

        [Property(NotNull = false)]
        public string FileName { get; set; }

        [Property(NotNull = false)]
        public int UserId { get; set; }

        [Property(NotNull = false)]
        public string Added { get; set; }

        [Property(NotNull = false)]
        public override bool TrashBin { get; set; }

        [Property(NotNull = false)]
        public string Path { get; set; }

        [Property(NotNull = false)]
        public string Type { get; set; }

    }
}