﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NWP.Enumeration
{
    public enum Types
    {
        MSSQL,
        MYSQL,
        MsSQLCE
    }

    public enum MetaType
    {
        TITLE,
        KEYWORDS,
        DESCRIPTION
    }
}