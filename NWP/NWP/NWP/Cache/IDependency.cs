﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NWP.Cache
{
    /// <summary>
    /// Interface for all DataRecord's
    /// Actually just a base initializer for ActiveRecord and all instances using it, will be persisted as a DataRecord.
    /// </summary>
    public interface IDependency
    {
    }
}