﻿#define TIMERS
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using NWP.Configuration;
using NWP.Schema;
using NWP.Utils;

namespace NWP.Mvc.Html
{
    /// <summary>
    /// Menu helper class
    /// </summary>
    public static partial class HtmlExtensions
    {
        public static IHtmlString SubMenu(this HtmlHelper html, string classname)
        {
           // Logg.Start("SubMenu");

            var url = HttpContext.Current.Request.Path.TrimEnd('/').TrimStart('/');
            //object o = HttpContext.Current.Cache["SubMenu" + Settings.CreateInstance().GetValue("DefaultDomain") + url];
           // if (o != null)
            //    return new HtmlString((string)o);

            var w = new StringBuilder();
            var sb = new StringBuilder();

            var menu = MenuHelper.LevelChildren()
                                .OrderBy(menuPartEntry => menuPartEntry
                                .Position, new PositionComparer()).Where(d=> d.Visible)
                                .ToList();

            

            if (menu.Any())
            {
                int counter = 0, count = menu.Count() - 1;
                var previousLevel = 1;

                foreach (var menuItem in menu)
                {
                    if (counter > 0)
                    {
                        var sbClass = new StringBuilder();

                        if (counter == 0)
                            sbClass.Append("first ");
                        if (counter == count)
                        {
                            sbClass.Append("last ");
                        }


                        if (url == string.Empty)
                            url = "";

                        if (string.Equals(menuItem.Route, url, StringComparison.OrdinalIgnoreCase))
                            sbClass.Append("current ");

                        var currentLevel = menuItem.Position.Split('.').Length;

                        if ((previousLevel == currentLevel) && counter > 1)
                        {
                            sb.Append("</li>");
                        }
                        if (previousLevel < currentLevel)
                        {
                            sbClass.Append("first ");

                            if (counter == 1)
                                sb.Append("<ul class=\"" + classname + "\">");
                            else
                                sb.Append("<ul>");
                        }
                        if (previousLevel > currentLevel)
                        {
                            for (var i = 0; i < (previousLevel - currentLevel); i++)
                            {
                                sb.Append("</li></ul>");

                            }
                            sb.Append("</li>");
                        }
                        var classValue = sbClass.ToString().TrimEnd();


                        sb.Append("\n<li");
                        sb.Append(!string.IsNullOrEmpty(classValue) ? string.Format(" class=\"{0}\"", classValue) : "");
                        sb.Append("><a href='/" + menuItem.Route + "'>" + menuItem.Title + "</a>");

                        previousLevel = currentLevel;
                        if (counter == count)
                        {
                            sb.Append("</li>\n");
                            for (var i = 2; i < previousLevel; i++)
                            {
                                sb.Append("</ul></li>");
                            }
                        }
                    }
                    ++counter;
                }

                

                if (counter > 1)
                    sb.Append("</ul>");

            }

            var s = Indent(sb);
            //HttpContext.Current.Cache["SubMenu" + Settings.CreateInstance().GetValue("DefaultDomain") + url] = s;

            //Logg.Stop("SubMenu");
            return new HtmlString(sb.ToString());
        }




        /// <summary>
        /// Typically used for a topmenu
        /// </summary>
        /// <param name="html"></param>
        /// <param name="levelsToShow">How deep iteration can be</param>
        /// <returns>Unordered list of menuitems</returns>
        public static IHtmlString Menu(this HtmlHelper html, int levelsToShow, string id)
        {
            Logg.Start("Menu");
            string url = HttpContext.Current.Request.Path.TrimEnd('/').TrimStart('/');
            var o = HttpContext.Current.Cache["Menu" + Settings.CreateInstance().GetValue("DefaultDomain") + url];
            //if (o != null)
            //    return new HtmlString(o.ToString());

            var pages = MenuHelper.TopLevelsOnly();
            var sb = new StringBuilder();

            int counter = 0, count = pages.Count() - 1;
            var previousLevel = 1;



            foreach (var menuItem in pages.OrderBy(m => m.Position))
            {

                int currentLevel = menuItem.Position.Split('.').Length;

                if (currentLevel <= levelsToShow)
                {
                    var sbClass = new StringBuilder(10);
                    if (counter == 0)
                        sbClass.Append("first ");
                    if (counter == count)
                    {
                        sbClass.Append("last ");
                    }

                    if (url == string.Empty)
                        url = "";

                    if (menuItem.Route == "" & url == "")
                    {
                        sbClass.Append("current ");

                    }else
                    {
                        if (string.Equals(menuItem.Route, url, StringComparison.OrdinalIgnoreCase) || (MenuHelper.LevelChildren().Find(w => w.ParentId == menuItem.PageId) != null))
                            sbClass.Append("current ");
                    }

                    if ((previousLevel == currentLevel) && counter >= 1)
                    {
                        sb.Append("</li>");
                    }

                    if (previousLevel < currentLevel)
                    {
                        sb.Append("<ul>");
                    }

                    if (previousLevel > currentLevel)
                    {
                        for (var i = 0; i < (previousLevel - currentLevel); i++)
                        {
                            sb.Append("</li>\n");
                            sb.Append("</ul>");
                        }
                        sb.Append("</li>\n");
                    }

                    var classValue = sbClass.ToString().TrimEnd();
                    var linkAttributes = new Dictionary<string, object>();
                    if (menuItem.Route.StartsWith("http"))
                        linkAttributes["target"] = "_blank";

                    if (!string.IsNullOrEmpty(classValue))
                        linkAttributes.Add("class", classValue);

                    sb.Append("<li" + (!string.IsNullOrEmpty(classValue) ? string.Format(" class=\"{0}\"", classValue) : "") + ">" + Link(html, menuItem.Title, menuItem.Route.Replace("/#", "#"),menuItem.SubText, linkAttributes));

                    previousLevel = currentLevel;
                    if (counter == count)
                    {
                        sb.Append("</li>");
                        for (int i = 1; i < previousLevel; i++)
                        {
                            sb.Append("</ul>\n");
                            sb.Append("</li>\n");
                        }
                    }
                }
                else
                {
                    for (int i = currentLevel; i < counter + 1; i++)
                        sb.Append("</li>");
                }


                ++counter;
            }


            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                if (string.IsNullOrEmpty(id))
                    id = "menu";

                sb.Insert(0, string.Format("<ul id=\"{0}\">", id));
                sb.Append("</ul>");
            }

            if (sb.Length > 0)
            {
                HttpContext.Current.Cache["Menu" + Settings.CreateInstance().GetValue("DefaultDomain") + url] = Indent(sb);
                Logg.Stop("Menu");
                return new HtmlString(Indent(sb));
            }
            else
            {
                HttpContext.Current.Cache["Menu" + Settings.CreateInstance().GetValue("DefaultDomain") + url] = sb.ToString();
                Logg.Stop("Menu");
                return new HtmlString(sb.ToString());
            }
        }

        public static IHtmlString Link(this HtmlHelper html, string title, string link, Dictionary<string, object> attributes)
        {
            bool start = false;
            if (link.Equals(""))
            {
                link = "/";
                start = true;

            }
            string css = string.Empty;
            if (attributes.Count > 0)
            {
                bool hasCss = attributes.ContainsKey("class");
                css = string.Format(" class=\"{0}\"", (string)attributes["class"]);
            }

            string add = (start == false) ? "/" : "";

            return new HtmlString(string.Format("<a href=\"{3}{1}\"{2}>{0}</a>", title, link, css, add));
        }


        public static IHtmlString Link(this HtmlHelper html, string title, string link, string subtext, Dictionary<string, object> attributes)
        {
            bool start = false;
            if (link.Equals(""))
            {
                link = "/";
                start = true;
            }
            string css = string.Empty;
            if (attributes.Count > 0)
            {
                bool hasCss = attributes.ContainsKey("class");
                css = string.Format(" class=\"{0}\"", (string)attributes["class"]);
            }
            string add = (start == false) ? "/" : "";
            return new HtmlString(string.Format("<a href=\"{4}{1}\"{2}>{0}<span>{3}</span></a>", title, link, css, subtext, add));
        }
    }
}