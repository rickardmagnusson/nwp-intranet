﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Configuration;
using NWP.Models;
using NWP.Templates;
using NWP.Widgets;

namespace NWP.Mvc.Html
{
    /// <summary>
    /// Widget helper methods.
    /// </summary>
    public static partial class HtmlExtensions
    {
        /// <summary>
        /// public method
        /// </summary>
        /// <param name="html"></param>
        /// <param name="model"></param>
        /// <param name="zone"></param>
        /// <returns></returns>
        public static IHtmlString Zone(this HtmlHelper html, dynamic model, string zone)
        {
            var datamodel = (Models.DataRepository) model;
            var widgets = datamodel.Widgets.Where(d=> d.TrashBin==false).OrderBy(d=> d.SortOrder);
            var sb = new StringBuilder();

            foreach (var w in widgets.Where(w => w.Zone==zone && w.Active)){
                sb.Append(RenderWidget(html,w.Template,w));
            }
            if(sb.ToString().Length>0)
                return new HtmlString(sb.ToString());
            
            return new HtmlString("&nbsp;");
        }

        public static IHtmlString ParseLayouts(this HtmlHelper html, int pageid)
        {
            var tmp = new TemplateParser();
            var page = ActiveRecordMediator<Page>.FindOne(Restrictions.Eq("PageId", pageid));

            tmp.ParseZoneLayouts(Convert.ToString(page.Layout));
            var template = tmp.Template;
            var templateHtml = template.TemplateHtml;

            foreach(var zone in template.Zones)
            {
                var widgets = GetWidgets(zone, pageid);

                var widgetList = string.Empty;
                var templateFormat = string.Concat( "{", zone, "}" );
                widgetList = (widgets.Select(widget => new {widget, text = widget.GetType().Name}).Where(d=> d.widget is Timeline == false).Select(
                    @t =>
                    string.Format(
                            "<div class=\"widget\" id=\"{0}\">\r\t\t<div class=\"title\">{1}<a class=\"editWidgetLink\" href=\"/admin/{3}/edit/{0}\">Redigera</a> &nbsp; <a class=\"deleteWidget\">Ta bort</a></div>\r\t\t\t<div class=\"content\">{2}</div>\n</div>\n",
                            @t.widget.WidgetId, @t.text, RenderWidget(html, @t.widget.Template, @t.widget),
                            @t.widget.GetType().Name))
                        ).Aggregate(widgetList, (current, widgetTemplate) => current + widgetTemplate);
                templateHtml = templateHtml.Replace(templateFormat, widgetList);
            }
            
            return new HtmlString(templateHtml);
        }

        public static List<IWidget> GetWidgets(string zone, int id)
        {
            return (from t in new TypeResolver().GetAllWidgets()
                    select ActiveRecordMediator.FindAll(t)
                        into widgets
                        where widgets.Length > 0
                        from item in ((IWidget[])widgets)
                        select item
                            into a
                            where a.Zone.Equals(zone) && a.PageId.Equals(id) && a.Active
                            orderby a.SortOrder
                            select a).ToList();
        }

        public static IWidget GetWidget(string id)
        {
            return (from t in new TypeResolver().GetAllWidgets()
                    select ActiveRecordMediator.FindAll(t)
                        into widgets
                        where widgets.Length > 0
                        from item in ((IWidget[])widgets)
                        select item
                            into a
                            where a.WidgetId == Convert.ToInt32(id) && a.Active
                            select a).First();
        }


        public static IHtmlString GetAllWidgetForList(this HtmlHelper html)
        {
            var tr = new TypeResolver();
            var widgets = tr.GetAllWidgets();
            var sb = new StringBuilder();

            foreach(var widget in widgets)
            {
                sb.Append(string.Format("<option value=\"{0}\">{1}</option>\n", widget.Name, RC(html,widget.Name)));
            }
            return new HtmlString(sb.ToString());
        }


        public static IHtmlString GetDistinctCompanies(this HtmlHelper html)
        {
            var usergroups = ActiveRecordMediator<User>.FindAll();
            Dictionary<string, string> companies = new Dictionary<string, string>();

            foreach (var group in usergroups)
            {
                if (!companies.ContainsKey(group.Company.ToLower().Trim())) {
                    companies.Add(group.Company.ToLower().Trim(), group.CompanyName);
                }
            }


            var sb = new StringBuilder();


            foreach (KeyValuePair<string, string> item in companies.OrderBy(d=> d.Key))
            {
                sb.Append(string.Format("<option value=\"{0}\">{1}</option>\n", item.Key, item.Value));
            }

            return new HtmlString(sb.ToString());
        }

    }
}