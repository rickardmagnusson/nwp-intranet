﻿using System.Web;
using System.Web.Mvc;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Linq;
using NHibernate.Criterion;
using NWP.Configuration;
using NWP.Enumeration;
using NWP.Models;
using NWP.Providers.Membership;
using File = System.IO.File;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using NWP.Widgets;

namespace NWP.Mvc.Html
{
    /// <summary>
    /// Html Meta tag helper
    /// </summary>
    public static partial class HtmlExtensions
    {

        public static IHtmlString GetUnreadPosts(this HtmlHelper html)
        {
            string o = string.Empty;
           
            var posts = Wall.UnreadPosts();

            if (posts.Count() == 0)
                return new HtmlString("");

                var template =     "<div class=\"UIBeeper\">";
                template +=     "       <div class=\"UIBeeper_Full\">";
                template +=     "           <div class=\"Beeps\">";
                template +=     "               <div class=\"UIBeep UIBeep_Top UIBeep_Bottom UIBeep_Selected\" style=\"opacity: 1; \">";
                template +=     "                   <a class=\"UIBeep_NonIntentional\" href=\"/meddelanden?id={0}\">";
                template +=     "                   <div class=\"UIBeep_Icon\">";
                template +=     "                       <i class=\"beeper_icon image2\"></i>";
                template +=     "                   </div>";
                template += "                       <span class=\"beeper_x\" id=\"{4}\">&nbsp;</span>";
                template +=     "                       <div class=\"UIBeep_Title\">";
                template +=     "                           <span class=\"blueName\">{1}</span> {3} <span class=\"blueName\">{2}</span>.";
                template +=     "                       </div>";
                template +=     "                   </a>";
                template +=     "               </div>";
                template +=     "            </div>";
                template +=     "      </div>";
                template +=     "</div>";

                foreach (var post in posts) {

                    User user = null;
                    var text = string.Empty;

                    if (post.Type == PostType.POST) 
                    { 
                        text = "har gjort ett inlägg.";
                        var p = WallPost.FindOne(Restrictions.Eq("Id", post.PostId));
                        user = NWP.Models.User.GetUserById(p.PostedByUser);
                        o += string.Format(template, post.PostId, user.DisplayName, CreateTitle(p.Post), text, post.Id);
                    } 
                    else
                    { 
                        text = "har kommenterat.";
                        var c = WallComment.FindOne(Restrictions.Eq("Id", post.PostId));
                        user = NWP.Models.User.GetUserById(c.CommentByUser);
                     
                        var wallpost = c.WallPost;
                        o += string.Format(template, wallpost.Id, user.DisplayName, CreateTitle(wallpost.Post), text, post.Id);
                       
                    }
                }
                return new HtmlString("<b>Du har olästa meddelanden!</b><br/>" + o);
        }

        public static string CreateTitle(string text){
            if (text.Length > 50)
                return text.Substring(0, 50) + "...";
            else
                return text;
        }


        public static IHtmlString Meta(this HtmlHelper html, string content, MetaType metaType)
        {
            switch (metaType)
            {
                case MetaType.DESCRIPTION:
                    return new MvcHtmlString(string.Format("\t<meta name=\"description\" content=\"{0}\" />", content));
                case MetaType.KEYWORDS:
                    return new MvcHtmlString(string.Format("\t<meta name=\"keywords\" content=\"{0}\" />", content));
                default:
                    return new MvcHtmlString(content);
            }
        }

        public static HtmlString ImageExt(this HtmlHelper html, string path, string text, string listid)
        {
            var type = System.IO.Path.GetExtension(path.ToLower()).Replace(".","") + ".png";

            if (string.IsNullOrEmpty(type))
            {
                string t = path;
              
            }

            bool hasHtml = string.IsNullOrEmpty(ActiveRecordMediator<Attachment>.FindOne(Restrictions.Eq("Id", Convert.ToInt32(listid))).Html);
            var color = hasHtml ? "" : "<div class=\"info green\" title=\"Denna fil innehåller text.\"><span>T</span></div>";

            var template = "<div class=\"uploadify-queue-item\"><div class=\"cancel\" id=\"{2}\"><a href=\"#\">X</a></div>{3}<span class=\"fileName\"><img src=\"/themes/admin/Styles/Icons/32px/{0}\" alt=\"\" /> {1}</span><span class=\"data\"></span></div>";

            return new HtmlString(string.Format(template, type, text, listid, color));
        }


      
        public static IHtmlString ImageIcon(this HtmlHelper html, string id)
        {
            var icons = Directory.EnumerateFiles(HttpContext.Current.Server.MapPath("~/themes/admin/styles/icons/32px/"));
            var sb = new StringBuilder("<select id=\"ImageIcon\" name=\"ImageIcon\" size=\"1\">\n");
           
            foreach (string icon in icons)
            {
                var current = System.IO.Path.GetFileName(id);
                var i = System.IO.Path.GetFileName(icon);

                var path = string.Format("/themes/admin/styles/icons/32px/{0}", System.IO.Path.GetFileName(icon));
                var sel = (i==current) ? " selected" : "";
                sb.Append(string.Format("\r\t<option value=\"{0}\"{1}>{2}</option>\n", path, sel, System.IO.Path.GetFileNameWithoutExtension(icon).ToUpper()));
            }
            sb.Append("</select>\n");
            return new HtmlString(sb.ToString());
        }

        public static IHtmlString LogFile(this HtmlHelper html)
        {
            var logg = NWP.Models.Logg.FindAll().ToList().OrderByDescending(l=> l.Added).Take(100);

            string table = "<table class=\"all\" rel='10'>";
            string templateheaderrow = "<thead><tr><th>Användare</th><th>Typ</th><th>Sökväg</th></tr></thead>\n<tbody>";
            string templaterow = "<tr><td><a href='/personlig-sida?user={3}'>{0}</a></td><td>{1}</td><td>{2}</td></tr>\n";

            table += templateheaderrow;

            foreach (var item in logg)
            {
                var user = GetUserById(html, item.UserId);
                var file = string.IsNullOrEmpty(item.FileName) ? item.Path: item.FileName;

                table += string.Format(templaterow, user.DisplayName, item.Type, file, user.Id);
            }
            table += "</tbody></table>";

            return new HtmlString(table);
  
        }


        /*
        public static HtmlString UserCount(this HtmlHelper html)
        {

            var users = ActiveRecordMediator<User>.FindAll();

            int list = 0;

            foreach (var user in users)
            {
                if (user.Active)
                    list += 1;
            }
            return new HtmlString(string.Format("{0}", list));
        }*/


        public static HtmlString UserList(this HtmlHelper html)
        {
           
            var users = ActiveRecordMediator<User>.FindAll();

            var template = "<li><a class=\"tooltip\" href=\"/personlig-sida?user={0}\"><img src=\"/Themes/www/Styles/Images/users/{0}.jpg\" alt='{1}' /><span class=\"information\"><b>{1} - {4}</b><br/> <b>Tel.</b> {2}<br/> <b>E-post:</b> {3}</span></a></li>";
            string list = string.Empty;

            foreach (var user in users) {
                if(user.Active)
                    list += string.Format(template, user.Id, user.DisplayName, user.Phone, user.Email, user.CompanyName);
            }
            return new HtmlString(string.Format("<ul class='userlist'>{0}</ul>", list));
        }

        public static User GetUser(this HtmlHelper html)
        {
            var current = HttpContext.Current.User.Identity.Name;
            return ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("UserName",current));
        }


        public static HtmlString WebmasterUserList(this HtmlHelper html)
        {
            var group = GetUser(html).Company;

            var users = ActiveRecordMediator<User>.FindAll(Restrictions.Eq("Company", group));

            var template = "<option value=\"{0}\">{1}</option>";
            string list = string.Empty;

            foreach (var user in users)
            {
                list += string.Format(template, user.Id, user.DisplayName);
            }
            return new HtmlString(string.Format("<select class='webmasteruserlist' size='10'>{0}</select>", list));
        }



        public static HtmlString ImageExtPublic(this HtmlHelper html, string path, string text, string listid)
        {
            var type = System.IO.Path.GetExtension(path.ToLower()).Replace(".", "") + ".png";
            string url = path.Substring(path.LastIndexOf("UserData")).Replace("\\", "/");


            var template = "<div class=\"image-item\"><span class=\"fileName\"><a href=\"/Pages/Download?pathCode=/{2}\" target=\"/_blank\"><img src=\"/themes/admin/Styles/Icons/32px/{0}\" alt=\"\" /> {1}</a><span></div>";

            return new HtmlString(string.Format(template, type, text, url));
        }


        public static HtmlString ImagLinkPublic(this HtmlHelper html, string path)
        {
            string url = path.Substring(path.LastIndexOf("UserData")).Replace("\\", "/");
            var template = "<a href=\"/Pages/Download?pathCode=/{0}\" target=\"/_blank\"><img src=\"/{0}\" alt=\"\" /></a>";

            return new HtmlString(string.Format(template, url));
        }



        public static HtmlString Css(this HtmlHelper html, string link, bool isGlobal)
        {
            if (isGlobal)
                return new HtmlString(string.Format("\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/Themes/{0}/Styles/{1}.css\" />", Settings.CreateInstance().GetValue("DefaultDomain"), link));
            else
                return new HtmlString(string.Format("\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/Themes/{0}/Styles/{1}.css\" />", Settings.CreateInstance().GetValue("DefaultDomain"), link));
        }

        public static HtmlString Css(this HtmlHelper html, string link)
        {
            string d = string.Format("/Themes/{0}/Styles/{1}.css", Settings.CreateInstance().GetValue("DefaultDomain"), link);
            if (File.Exists(d))
                return new HtmlString(string.Format("\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/Themes/{0}/Styles/{1}.css\" />", Settings.CreateInstance().GetValue("DefaultDomain"), link));
            return new HtmlString(string.Format("\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/Themes/{0}/Styles/{1}.css\" />", "www", "www"));
        }

        public static HtmlString Script(this HtmlHelper html, string link)
        {
            string meta = string.Format("\t<script type=\"text/javascript\" src=\"Themes/{0}/Scripts/{1}.js\"></script>", Settings.CreateInstance().GetValue("DefaultDomain"), link);
            return new HtmlString(meta);
        }

        public static HtmlString Script(this HtmlHelper html, string link, bool external)
        {
            string meta = string.Format("\t<script type=\"text/javascript\" src=\"{0}\"></script>", link);
            return new HtmlString(meta);
        }

        public static HtmlString UserLogo(this HtmlHelper html)
        {
            var o = ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("UserName", HttpContext.Current.User.Identity.Name));
            var company = o.Company;

            string templateImg = string.Format("<a href=\"/\"><img src=\"{1}/MediaService.svc/Logo?domain={0}\" alt=\"{0}\" /></a>", company, Settings.CreateInstance().GetValue("SSODomain"));

            return new HtmlString(templateImg);
        }


        public static HtmlString CompanyLogo(this HtmlHelper html)
        {
            var o = ActiveRecordMediator<User>.FindOne(Restrictions.Eq("UserName", HttpContext.Current.User.Identity.Name));
            string company = o.Company;

            var logos = Directory.EnumerateFiles(HttpContext.Current.Server.MapPath("/UserData/Logos"));
            string userlogo = string.Empty;

            foreach(var logo in logos){
                var filename = System.IO.Path.GetFileName(logo).Replace("_", "");
                if (filename.ToLower().Contains(company.ToLower())) {
                    userlogo = System.IO.Path.GetFileName(logo);
                }
            }

            string templateImg = string.Format("<a href=\"/Pages/Download?pathCode=/UserData/Logos/{2}\"><img src=\"{1}/MediaService.svc/Logo?domain={0}\" alt=\"{0}\" /></a>", company, Settings.CreateInstance().GetValue("SSODomain"), userlogo);

            return new HtmlString(templateImg);
        }


        public static HtmlString FileUpload(this HtmlHelper html, string id)
        {
            string template = string.Format("<div class=\"row\"><div class=\"editor-label\">{1}</div><div class=\"editor-field\"><input type=\"file\" id=\"{0}\" /></div></div>", id, html.RC("FileUpload"));
            return new HtmlString(template);
        }

        public static HtmlString FileUpload(this HtmlHelper html, string id, string title)
        {
            string template = string.Format("<div class=\"row\"><div class=\"editor-label\">{1}</div><div class=\"editor-field\"><input type=\"file\" id=\"{0}\" /></div></div>", id, title);
            return new HtmlString(template);
        }
    }
}