﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Models;
using NWP.Providers.Membership;
using NWP.Templates;
using NWP.Widgets;

namespace NWP.Mvc.Html
{
    /// <summary>
    /// Html helper class
    /// </summary>
    public static partial class HtmlExtensions
    {
        /// <summary>
        /// Text to Html returns html from text.
        /// </summary>
        /// <param name="html"></param>
        /// <param name="htmltext"></param>
        /// <returns></returns>
        public static IHtmlString ToHtml(this HtmlHelper html, string htmltext)
        {
            return new HtmlString(htmltext);
        }

        /// <summary>
        /// Get a user from id
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static User GetUserById(this HtmlHelper html, int id)
        {
            var user = ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("Id", id));
            return user;
        }


        /// <summary>
        /// Get username from id
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IHtmlString GetUserName(this HtmlHelper html, string id)
        {
            var name = ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("UserName", id)).DisplayName;

            return new HtmlString(name);
        }

        public static IHtmlString GetUserNameById(this HtmlHelper html, int id)
        {
            var name = ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("Id", id)).DisplayName;

            return new HtmlString(name);
        }


        public static IHtmlString GetUserIdByName(this HtmlHelper html, string name)
        {
            var id = ActiveRecordMediator<User>.FindFirst(Restrictions.Eq("UserName", name)).Id;

            return new HtmlString(id.ToString());
        }

        public static IHtmlString GetPageName(this HtmlHelper html, int id)
        {
            var pageName = ActiveRecordMediator<Page>.FindFirst(Restrictions.Eq("PageId", id)).Title;
          
            return new HtmlString(pageName);
        }

        public static IHtmlString PageCount(this HtmlHelper html)
        {
            int count = ActiveRecordMediator<Page>.Count();
          
            return new HtmlString(count.ToString());
        }

        public static IHtmlString CommentCount(this HtmlHelper html)
        {
            int count = ActiveRecordMediator<WallComment>.Count();

            return new HtmlString(count.ToString());
        }

        public static IHtmlString UserCount(this HtmlHelper html)
        {
            int count = ActiveRecordMediator<User>.Count();

            return new HtmlString(count.ToString());
        }

        public static IHtmlString UrlToRelative(this HtmlHelper html, string path)
        {
            string url = path.Substring(path.LastIndexOf("UserData")).Replace("\\", "/");

            return new HtmlString("/"+ url);
        }


        public static IHtmlString LastUpdated(this HtmlHelper html)
        {
            int count = ActiveRecordMediator<Page>.Count();

            return new HtmlString(count.ToString());
        }

        public static IHtmlString PageZonez(this HtmlHelper html, dynamic model, string selectedValue)
        {
            var datamodel = (Models.DataRepository)model;
            var widgets = datamodel.Widgets;
            var sb = new StringBuilder();

            foreach (var w in widgets)
            {
                sb.Append(RenderWidget(html, w.Template, w));
            }
            return new HtmlString(sb.ToString());
        }


        public static IHtmlString PageLayouts(this HtmlHelper html, int id, string selectedValue)
        {
            var p = new TemplateParser();
            p.ParseLayouts();
            var layouts = p.TemplateItems;
            var sb = new StringBuilder("<select id=\"Layout\" name=\"Layout\" size=\"1\">\n");
            foreach(var layout in layouts)
            {
                var sel = (layout.FileName.Replace(".cshtml", "") == selectedValue) ? " selected" : "";
                sb.Append(string.Format("\r\t<option value=\"{0}\"{2}>{1}</option>\n", layout.FileName.Replace(".cshtml", ""), RC(html,string.Concat("Template", layout.Name)), sel));
            }
            sb.Append("</select>\n");
            return new HtmlString(sb.ToString());
        }

        public static IHtmlString Zones(this HtmlHelper html, string layout, string selectedValue)
        {
            var p = new TemplateParser();
            p.ParseZoneLayouts(layout);
            return new HtmlString(p.Template.ToString(selectedValue));
        }

        public static IHtmlString Pages(this HtmlHelper html, int selectedValue)
        {
            var pages = ActiveRecordMediator<Page>.FindAll().ToList();

            var sb = new StringBuilder("<select id=\"PageId\" name=\"PageId\" size=\"1\">\n");
            foreach(var page in pages)
            {
                string sel = (page.PageId == selectedValue) ? " selected" : "";
                sb.Append(string.Format("\r\t<option value=\"{0}\"{2}>{1}</option>\n", page.PageId, page.Title, sel));
            }
            sb.Append("</select>\n");
            return new HtmlString(sb.ToString());
        }


        public static IHtmlString ParentPages(this HtmlHelper html, int selectedValue)
        {
            var pages = ActiveRecordMediator<Page>.FindAll().ToList();

            var sb = new StringBuilder("<select id=\"ParentId\" name=\"ParentId\" size=\"1\">\n");
            foreach (var page in pages)
            {
                string sel = (page.PageId == selectedValue) ? " selected" : "";
                sb.Append(string.Format("\r\t<option value=\"{0}\"{2}>{1}</option>\n", page.PageId, page.Title, sel));
            }
            sb.Append("</select>\n");
            return new HtmlString(sb.ToString());
        }


        /// <summary>
        /// Translate a string from resources
        /// </summary>
        /// <param name="html"></param>
        /// <param name="val">The string to translate</param>
        /// <returns>A htmlstring containg the translated value</returns>
        public static HtmlString RC(this HtmlHelper html, string toTranslate)
        {
            var rc = toTranslate;
            //The stupid keys are case sensetive som we have to run thrue all keys to get value.
            var rm = new ResXResourceReader(HttpContext.Current.Server.MapPath("~/App_GlobalResources/RC.resx"));
            foreach (DictionaryEntry resource in rm.Cast<DictionaryEntry>().Where(resource => resource.Key.ToString().ToLower()==toTranslate.ToLower()))
            {
                rc = resource.Value.ToString();
                break;
            }
            return new HtmlString(rc);
        }

        /// <summary>
        /// Translate a string from resources
        /// </summary>
        /// <param name="html"></param>
        /// <param name="toTranslate">The MvcHtmlString to translate</param>
        /// <returns>A htmlstring containg the translated value</returns>
        public static HtmlString RC(this HtmlHelper html, MvcHtmlString toTranslate)
        {
            if (toTranslate == null)
                return null;

            if(toTranslate.ToString().Contains("<label for")){
                var start = toTranslate.ToString().IndexOf('>')+1;
                var key = toTranslate.ToString().Substring(start).Replace("</label>", "");

                var str = toTranslate.ToString();
                var left = str.Substring(0, str.IndexOf('>')+1);
                var right = str.Substring(str.LastIndexOf('<'));

                var k = left + RC(html,key) + right;

                return new HtmlString(k);
            }
 
            var rc = Resources.RC.ResourceManager.GetString(toTranslate.ToString());
            if (string.IsNullOrEmpty(rc))
                rc = toTranslate.ToString();

            return new HtmlString(rc);
        }

        /// <summary>
        /// Produces a nice indented code from source
        /// </summary>
        /// <param name="sb">StringBuilder source</param>
        /// <returns>Well formatted string</returns>
        public static string Indent(StringBuilder sb)
        {
            try
            {
                var w = new System.Text.StringBuilder();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                XmlWriter xw = XmlTextWriter.Create(w, new XmlWriterSettings() { Indent = true, OmitXmlDeclaration = true, NewLineHandling = NewLineHandling.Entitize, ConformanceLevel = ConformanceLevel.Fragment, NewLineChars = "\r\t\t" });
                doc.WriteTo(xw);
                xw.Flush();
                return w.ToString();
            }
            catch
            {
                return sb.ToString();
            }
        }


        /// <summary>
        /// Produces a nice indented code from source
        /// </summary>
        /// <param name="sb">StringBuilder source</param>
        /// <returns>Well formatted string</returns>
        public static string Indent(string sb)
        {
            System.Text.StringBuilder w = new System.Text.StringBuilder();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb);
            XmlWriter xw = XmlTextWriter.Create(w, new XmlWriterSettings() { Indent = true, OmitXmlDeclaration = true, NewLineHandling = NewLineHandling.Entitize, ConformanceLevel = ConformanceLevel.Fragment, NewLineChars = "\r\t\t" });
            doc.WriteTo(xw);
            xw.Flush();
            return w.ToString();
        }

        /// <summary>
        /// Renders a IWidget
        /// </summary>
        /// <typeparam name="T">Model (T) Usually a Widget</typeparam>
        /// <param name="html"></param>
        /// <param name="viewPath"></param>
        /// <param name="model">IWidget model</param>
        /// <returns>Rendered IWidget</returns>
        public static string RenderWidget<T>(this HtmlHelper html, string viewPath, T model)
        {
            try
            {
                using (var writer = new StringWriter())
                {
                    var view = new WebFormView(html.ViewContext.Controller.ControllerContext, viewPath);
                    var vdd = new ViewDataDictionary<T>(model);
                    var context = new ViewContext(html.ViewContext.Controller.ControllerContext, view, vdd, new TempDataDictionary(), writer);
                    var v = new RazorView(html.ViewContext.Controller.ControllerContext, viewPath, null, false, null);
                    v.Render(context, writer);

                    return writer.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}