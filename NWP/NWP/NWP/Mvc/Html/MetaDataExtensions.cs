﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NWP.Mvc.Html
{
    /// <summary>
    /// Provides helper for SEO.
    /// </summary>
    public static partial class HtmlExtensions
    {
        ///TODO: Create a TagBuilder 
        /// <summary>
        /// Return Meta description
        /// </summary>
        /// <param name="html"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public static IHtmlString MetaDescription(this HtmlHelper html, string val)
        {
            return new HtmlString(string.Format("<meta name=\"description\" content=\"{0}\" />", val));
        }

        /// <summary>
        /// Return Meta Keywords
        /// </summary>
        /// <param name="html"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public static IHtmlString MetaKeywords(this HtmlHelper html, string val)
        {
            return new HtmlString(string.Format("<meta name=\"keywords\" content=\"{0}\" />", val));
        }
    }
}