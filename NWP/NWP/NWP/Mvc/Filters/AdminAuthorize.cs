﻿

using System.Web.Mvc;

namespace Nwp.Mvc.Filters
{
    /// <summary>
    /// All entry with this attibute will be denied access to it if not user is an admin.
    /// </summary>
    public sealed class AdminAuthorize : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool adminAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAdminAttribute), true) || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAdminAttribute), true);

            //Hack for upload(Uploadify will break)
            bool hasCookie = filterContext.HttpContext.Request.Params["token"] != null;

            if (filterContext.HttpContext.Request.Path.Contains("upload") && hasCookie){
                adminAuthorization = false;
            }
            
            if (adminAuthorization)
            {
                base.OnAuthorization(filterContext);
            }
        }
    }
}