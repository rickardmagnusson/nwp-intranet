﻿using System.Web.Mvc;

namespace Nwp.Mvc.Filters
{
    /// <summary>
    /// Alter login information for a user. Attribute requires a valid user to pass to be authenticated. 
    /// </summary>
    public sealed class LogonAuthorize : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
            || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);


            if (filterContext.HttpContext.Request.Path.Contains("upload")) {
                skipAuthorization = true;
            }

            if (!skipAuthorization)
            {
                if (filterContext.HttpContext.Request.Path.IndexOf("upload") > 0)
                {
                    base.OnAuthorization(filterContext);
                }
                else
                {
                    base.OnAuthorization(filterContext);
                }
            }
        }
    }
}


