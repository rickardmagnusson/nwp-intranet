﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

/// <summary>
/// Only allow admin's below admin folder(views/admin)
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
public sealed class AllowAdminAttribute : System.Web.Mvc.AuthorizeAttribute
{
    public AllowAdminAttribute(){

    }

    /// <summary>
    /// Authorize user
    /// </summary>
    /// <param name="filterContext">AuthorizationContext filterContext</param>
    public override void OnAuthorization(AuthorizationContext filterContext)
    {
        bool hasCookie = filterContext.HttpContext.Request.Params["token"] != null;

        if (filterContext.HttpContext.Request.Path.Contains("upload") && hasCookie)
        {
          
        }
        else
        {
            if (!filterContext.HttpContext.User.IsInRole("admin"))
                filterContext.Result = GetFailedResult();
        }
    }

    /// <summary>
    /// Display the login view if user is a non admin and tries to access admin area.
    /// </summary>
    /// <returns>A login view</returns>
    private ActionResult GetFailedResult()
    {
        return new ViewResult { ViewName = "/Views/Shared/LogOn.cshtml" };
    }
}