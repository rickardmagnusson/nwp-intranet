﻿using Castle.ActiveRecord;
using NHibernate.Criterion;
using NWP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NWP.Mvc.Helpers
{
    /// <summary>
    /// En simpel men så värdefull klass som hjälper till att skapa text från rena htmlblock.
    /// I Innehållet för html kan du enkelt skriva ##DATE## för att få ut dagens datum.
    /// </summary>
    public static class ShortCodes
    {
        public static string ShortCode(string html)
        {
            List<Codes> codes = new List<Codes>();
            codes.Add(new Codes() { key = "##DATE##", value = DateTime.Now.ToShortDateString() });
            codes.Add(new Codes() { key = "##USERID##", value = GetUserId() });
            codes.Add(new Codes() { key = "##CLOSESTBIRTH##", value = GetClosestBirth() });
            codes.Add(new Codes() { key = "##UNREADMESSAGES##", value = GetUnreadMessages() });

            foreach(var code in codes)
            {
                if(html.IndexOf(code.key)>0) {
                    html = html.Replace(code.key, code.value);
                }
            }
            return html;
        }

        private static string GetUnreadMessages()
        {
            var currentUser = User.FindOne(Restrictions.Eq("UserName", HttpContext.Current.User.Identity.Name));
            int cnt = PrivateDiscussion.FindAll().ToList().Where(d => d.UserId == currentUser.Id && d.IsRead == false).Count();
            if (cnt > 0)
            {
                string mess = "";
                if (cnt == 1)
                    mess = "Du har {0} meddelande.";
                else
                    mess = "Du har {0} meddelanden.";
                return string.Format(mess, cnt);
            }
            else
            {
                return "Du har inga nya meddelanden.";
            }
        }

        private static string GetUserId() 
        {
            return ActiveRecordMediator<User>.FindFirst(
                Restrictions.Eq("UserName", 
                HttpContext.Current.User.Identity.Name))
                .Id.ToString();
        }

        private static string GetClosestBirth()
        {
            int c = 0;
            try
            {
                var list = ActiveRecordMediator<User>.FindAll().ToList();
                c = list.Count();
                var user = GetClosestDate(list);
                return ""; // string.Format("Nästa födelsedag har: {0} - {1}", user.DisplayName, user.Id);
            }
            catch(Exception ex){
                return ex.Message + "("+c+")";
            }
        }

        private static User GetClosestDate(List<User> tm)
        {
            List<BirthDate> datelist = new List<BirthDate>();
            

            foreach (var item in tm.OrderBy(d => d.Birth))
            {
                if (string.IsNullOrEmpty(item.Birth)) continue;

                var dt = item.Birth;
                if (!dt.StartsWith("19"))
                    dt = "19" + dt;
                DateTime b;
                bool foundDate = DateTime.TryParse(dt, out b);

                datelist.Add(new BirthDate
                {
                    Date = DateTime.ParseExact(b.ToShortDateString(), "yyyy-mm-dd", null),
                    User = item,
                    Diff = DateDiff(DateTime.Now, b)
                });
                
            }

            var closest = datelist.OrderBy(d => d.Diff).First();

            return closest.User;
        }

        /// <summary>
        /// DateDiff helper
        /// </summary>
        /// <param name="Date1">Compare date one</param>
        /// <param name="Date2">Compare date two</param>
        /// <returns>Difference as an integer between these two dates.</returns>
        private static int DateDiff(DateTime Date1, DateTime Date2)
        {
            TimeSpan time = Date1 - Date2;
            return Math.Abs(time.Days);
        }

    }

    public class BirthDate
    {
        public DateTime Date { get; set; }
        public User User { get; set; }
        public Int32 Diff { get; set; }
    }

    public class Codes
    {
        public string key { get; set; }
        public string value { get; set; }
    }
}