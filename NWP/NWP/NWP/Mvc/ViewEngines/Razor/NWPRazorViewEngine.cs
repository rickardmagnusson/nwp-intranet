﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using NWP.Configuration;

namespace NWP.Mvc.ViewEngines.Razor
{
    /// <summary>
    /// Custom ViewEngine to get map path to templates, themes, partial views etc.
    /// </summary>
    public class NWPViewEngine : VirtualPathProviderViewEngine
    {
        public NWPViewEngine()
        {
            base.MasterLocationFormats = null;
            base.MasterLocationFormats = new[] { 
                "~/Views/Shared/_Layout.cshtml", 
                "~/Views/Shared/_Layout.Admin.cshtml",
                "~/Views/Shared/_Layout.Login.cshtml"
            };

            base.ViewLocationFormats = null;
            base.ViewLocationFormats = new[] { 
                 "~/Themes/www/Views/{0}.cshtml",
                 "~/Views/Admin/{0}.cshtml",
                 "~/Views/Admin/{1}/{0}.cshtml",
                 "~/Views/Shared/{0}.cshtml",
                 "~/Views/PageWidget/{0}.cshtml"
            };

            base.PartialViewLocationFormats = new[] { 
                "~/Views/Widgets/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml"
            };

            /*

            base.PartialViewLocationFormats = new string[] {
                "~/Views/Templates/{0}.cshtml"
            }.Union(base.PartialViewLocationFormats).ToArray<string>();
            */

         
        }

        private ControllerContext context;

        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            this.context = controllerContext;
            
            string url = controllerContext.RequestContext.HttpContext.Request.Url.ToString().ToLower();
            bool auth = url.Contains("/admin");
            bool login = url.ToLower().Contains("/admin/account");

            if (login)
                return base.FindView(controllerContext, viewName, MasterLocationFormats[2], useCache);

            if (auth)
                return base.FindView(controllerContext, viewName, MasterLocationFormats[1], useCache);

            bool account = url.Contains("/account");
            if (account)
                return base.FindView(controllerContext, viewName, MasterLocationFormats[2], useCache);

            viewName = CombinePath(viewName);
            return base.FindView(controllerContext, viewName, MasterLocationFormats[0], useCache);
           
        }


        public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
        {
            string requestDomain = GetSubdomain(HttpContext.Current.Request.Headers["Host"]);
            string viewName = string.Format("/Themes/{0}/Views/", requestDomain) + string.Format("{0}.cshtml", partialViewName);
            if (!System.IO.File.Exists(HttpContext.Current.Server.MapPath(viewName)))
                return base.FindPartialView(controllerContext, viewName, useCache);
            return base.FindPartialView(controllerContext, partialViewName, useCache);
        }


        private string CombinePath(string viewPath)
        {
            bool admin = context.RequestContext.HttpContext.Request.Url.ToString().ToLower().Contains("/admin");
            string requestDomain = GetSubdomain(HttpContext.Current.Request.Headers["Host"]);

            string file = string.Format("/Themes/{0}/Views/", requestDomain) + string.Format("{0}.cshtml", viewPath);
            if (admin)
                return string.Format("/Views/Admin/{0}.cshtml", viewPath);

            if (!System.IO.File.Exists(HttpContext.Current.Server.MapPath(file)))
                return System.IO.Path.Combine(string.Format("/Themes/{0}/Views/", "www"), string.Format("{0}.cshtml", viewPath));
            else
                return System.IO.Path.Combine(string.Format("/Themes/{0}/Views/", requestDomain), string.Format("{0}.cshtml", viewPath));
        }



        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string layoutPath)
        {
            return new RazorView(controllerContext, viewPath, layoutPath, true, new[] { ".cshtml" });
        }


        protected override IView CreatePartialView(ControllerContext controllerContext, string viewPath)
        {
            string layoutPath = "/";
            return new RazorView(controllerContext, CombinePath(viewPath), CombinePath(layoutPath), true, new[] { ".cshtml" });
        }


        public static string GetSubdomain(string host)
        {
            return "www";
            if (host.IndexOf(":") >= 0)
                host = host.Substring(0, host.IndexOf(":"));

            Regex tldRegex = new Regex(@"\.[a-z]{2,3}\.[a-z]{2}$");
            host = tldRegex.Replace(host, "");
            tldRegex = new Regex(@"\.[a-z]{2,4}$");
            host = tldRegex.Replace(host, "");

            if (host.Split('.').Length > 1)
                return host.Substring(0, host.IndexOf("."));
            else
                return string.Empty;
        }

        private Regex CreateRegex(string source)
        {
            source = source.Replace("/", @"\/?");
            source = source.Replace(".", @"\.?");
            source = source.Replace("-", @"\-?");
            source = source.Replace("{", @"(?<");
            source = source.Replace("}", @">([a-zA-Z0-9_-]*))");
            return new Regex("^" + source + "$");
        }


    }
}