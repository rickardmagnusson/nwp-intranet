﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using NWP.Registration.Models;
using System.IO;
using System.Data.SqlServerCe;
using System.Data;
using System.Net.Mail;

namespace NWP.Registration.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult LogOn()
        {
            return View();
        }

        [Authorize]
        public ActionResult AdmAccounts()
        {
            //New Wave;Tomas Jansson;0524-28300;Åkarevägen 18;Dingle;455 83;tomas.jansson@newwave.se;www.newwave.se;-;www
            string sql = "<br/>INSERT INTO Users(Company, Name, LastName, Phone, Address, City, Zip, Email, Homepage, Password, IsRegistered, ProfileImage, Description, Position, Birth, UserType, NewPassword, EmailUserList, Domain) VALUES(";
            //sql += "'{0}',"; //ID
            sql += "'{0}',"; //Company
            sql += "'{1}',"; //Name
            sql += "'{2}',"; //LastName
            sql += "'{3}',"; //Phone
            sql += "'{4}',"; //Address
            sql += "'{5}',"; //City
            sql += "'{6}',"; //Zip
            sql += "'{7}',"; //Email
            sql += "'{8}',"; //HomePage
            sql += "'{9}',"; //Password
            sql +=  "{10},"; //IsRegistered
            sql += "'{11}',"; //ProfileImage
            sql += "'{12}',"; //Description
            sql += "'{13}',"; //Position
            sql += "'{14}',"; //Birth
            sql += " {15},"; //UserType
            sql += "'{16}',"; //NewPassword 
            sql += "'{17}',"; //EmailList
            sql += "'{18}'"; //Domain
            sql += ");";

            

            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();

            int c = 0;
            string o = string.Empty;
            
                
            string f = HttpContext.Server.MapPath("/App_Data/import2.txt");
	        List<string> data = new List<string>();
	           
	        using (StreamReader r = new StreamReader(f, System.Text.Encoding.Default))
	        {
	            string line;
	            while ((line = r.ReadLine()) != null)
	            {
		            data.Add(line);
                   
	            }
	        }
            string template = "";
       

            string id = "";
            try
            {
                foreach (var line in data)
                {
                    c += 1;
                    var lines = line.Split(new char[] { ';' }, StringSplitOptions.None);

                    if (line.StartsWith("#")) continue; //Skip comments

                    var Company = lines[0];
                    var FullName = lines[1];
                    var l = FullName.Split(' ');
                    var Name = l[0];
                    var LastName = l[1];
                    var Phone = lines[2];
                    var Address = lines[3];
                    var City = lines[4];
                    var Zip = lines[5];
                    var Email = lines[6];
                    var Homepage = lines[7];
                    var Password = lines[8]; //New wave mode handle before insert
                    var Domain = lines[9];

                    var IsRegistered = "0";
                    var ProfileImage = "";

                    //var user = new NUser { ID = id, Company = Company, Name = Name, LastName = LastName, Phone = Phone, Address = Address, City = City, Zip = Zip, Email = Email, Homepage = Homepage, Password = Password, IsRegistered= "false", ProfileImage= string.Empty };
                    //users.Add(user);

                    template = string.Format(sql,
                          Company,
                          Name,
                          LastName,
                          Phone,
                          Address,
                          City,
                          Zip,
                          Email,
                          Homepage,
                          Password,
                          IsRegistered,
                          ProfileImage,
                          "", //Desc
                          "", // Position
                          "", //Birth
                          1, //UserType
                          "", //NewPassword 
                          "", //EmailUserList
                          Domain
                      );

                    if (!Exist(Email))
                    {
                        //SqlCeCommand sc = new SqlCeCommand(template,conn);

                       // sc.ExecuteNonQuery();
                    }


                    string body = string.Format(@"<h1>Registreringsuppgifter New Wave Profile intranet</h1>
                            <p>Nu lanserar vi vårt nya intranet för New Wave Profile. Här kommer vi samla all gemensam information för de bolag som ingår i konceptet. Initialt kommer du finna allt som rör nya varumärket och sedan fylls det på kring årsskiftet med mer info från övriga bolag.<br/><br/>
                            För att kunna logga in i behöver vi lite uppgifter ifrån dig, så klicka på länken: <a href='http://registrering.newwaveprofile.com'>http://registrering.newwaveprofile.com</a> och logga in med följande uppgifter.<br/>
                            Epost: {0}<br/>
                            Lösenord: {1}<br/><br/>
                            Camilla Fjordland <br/>New Wave Profile</p><br/><br/><br/>", Email, "12345");


                    o += body;
                    
                    
                    //string d = id + ", " + Company + "," + Name + "," + LastName + "," + Phone + "," + Address + "," + City + "," + Zip + "," + Email + "," + Homepage + "," + Password + "," + IsRegistered + "," + ProfileImage;
                    //o += d;
                }
            }
            catch (Exception ex) {
                throw new Exception("At line: " +  c + " :: " + template + " ("+ex.Message+") ");
            }         
            ViewBag.Users = o;

            return View();
        }

        public ActionResult ParseAccounts()
        {
            string lista = string.Empty;
            try
                {
            string sql = @"SELECT EmailUserList AS Emails FROM Users WHERE (NOT (EmailUserList LIKE ''))";
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();
            SqlCeCommand sc = new SqlCeCommand(sql, conn);

            IDataReader dr = sc.ExecuteReader();

            List<string> emails = new List<string>();


                while (dr.Read())
                {
                    var line = dr["Emails"].ToString();

                    string[] lines = line.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                    foreach (var email in lines)
                    {
                        if (!string.IsNullOrEmpty(email))
                        {
                            if (IsValid(email))
                            {

string body = string.Format(@"Registreringsuppgifter New Wave Profile intranet%0D
Nu lanserar vi vårt nya intranet för New Wave Profile. Här kommer vi samla all gemensam information för de bolag som ingår i konceptet. Initialt kommer du finna allt som rör nya varumärket och sedan fylls det på kring årsskiftet med mer info från övriga bolag.%0D
För att kunna logga in i behöver vi lite uppgifter ifrån dig, så klicka på länken: http://registrering.newwaveprofile.com och logga in med följande uppgifter.%0D
Epost: {0}%0D
Lösenord: {1}%0D
%0D
Mvh%0D
Camilla Fjordland%0D
New Wave Profile", email, "123456");

                                string lnk = string.Format("<br/><a href='mailto:{0}?subject=Registreringsuppgifter New Wave Profile intranet&body={1}'>{0}</a><br/>",email,body);
                                lista += lnk;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lista += "<br/><br/>" + ex.Message + "<br><br/>";
            }
            ViewBag.Lista = lista;

            return View();
        }

        public bool IsValid(string emailaddress)
        {
            try
            {
                var m = new MailAddress(emailaddress);
                if (Exist(emailaddress))
                {
                    //string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
                    //SqlCeConnection conn = new SqlCeConnection(connstr);
                    //conn.Open();
                    //string sql = string.Format("UPDATE Users set Password='123456' WHERE Email='{0}'", emailaddress);
                    //SqlCeCommand sc = new SqlCeCommand(sql, conn);
                    //sc.ExecuteNonQuery();
                    //ViewBag.Lista += sql + "<br/>";
                    //conn.Close();
                }

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public bool Exist(string email) 
        {
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();
            SqlCeCommand sc = new SqlCeCommand("SELECT * FROM Users WHERE Email='"+email+"'",conn);

            IDataReader dr = sc.ExecuteReader();
            bool val = false;
            if (dr.Read())
                val = true;

            conn.Close();
            return val;
        }

        

        [HttpPost]
        public ActionResult LogOn(LogOnModel model)
        {
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();

            string sql = string.Format("SELECT * FROM Users Where Email='{0}' AND Password = '{1}'", model.Email, model.Password);
            SqlCeCommand command = new SqlCeCommand(sql, conn);
            SqlCeDataReader dr = command.ExecuteReader();


            if (ModelState.IsValid)
            {
                if(dr.Read()){
                    FormsAuthentication.SetAuthCookie(model.Email, false);
                    return View("Register");
                }
                else
                {
                    ModelState.AddModelError("", "Lösenordet är inte korrekt, försök igen.");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/LogOff

        [Authorize]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }


        //
        // GET: /Account/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }
    }
}
