﻿using NWP.Registration.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace NWP.Registration.Controllers
{
    public class ProfileController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {            
            ProfileViewModel pmodel = new ProfileViewModel();            
            return View(pmodel);
        }

        [Authorize]
        public ActionResult Upload(ProfileViewModel model)
        {
            var image = WebImage.GetImageFromRequest();      
        
            if (image != null)       
            {           
                if (image.Width > 500)
                {             
                    image.Resize(500, ((500 * image.Height) / image.Width));          
                }         
                var filename = Path.GetFileName(image.FileName);         
                image.Save(Path.Combine("~/Images", filename));         
                filename = Path.Combine("~/Images", filename);           
                model.ImageUrl = Url.Content(filename);            
                var editModel = new EditorInputModel()
                {                   
                    Profile = model,                   
                    Width = image.Width,                  
                    Height = image.Height,                   
                    Top = image.Height * 0.1,                  
                    Left = image.Width * 0.9,                   
                    Right = image.Width * 0.9,                   
                    Bottom = image.Height * 0.9              
                };     
                return View("Editor", editModel);    
            }     
            return View("Index", model); 
        }

        [Authorize]
        public ActionResult Edit(EditorInputModel editor) 
        {          
            var image = new WebImage("~" + editor.Profile.ImageUrl);           
            var height = image.Height;          
            var width = image.Width;            
            image.Crop((int)editor.Top, (int)editor.Left, (int)(height - editor.Bottom), (int)(width - editor.Right));           
            var originalFile = editor.Profile.ImageUrl;           
            editor.Profile.ImageUrl = Url.Content("~/Uploads/" + Path.GetFileName(image.FileName));           
            image.Resize(100, 100, true, false);           
            image.Save(@"~" + editor.Profile.ImageUrl);           
            //System.IO.File.Delete(Server.MapPath(originalFile));      
     
            return View("Index", editor.Profile);        
        }   
    }
}
