﻿using MySql.Data.MySqlClient;
using NWP.Registration.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;

namespace NWP.Registration.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "New Wave Profile Registrering";
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("UserDetails");
            return View();
        }

        public ActionResult Logout()
        {
            if (User.Identity.IsAuthenticated)
                FormsAuthentication.SignOut();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Index(LogOnModel model)
        {
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();

            string sql = string.Format("SELECT * FROM Users Where Email='{0}' AND Password = '{1}'", model.Email, model.Password);
            SqlCeCommand command = new SqlCeCommand(sql, conn);
            SqlCeDataReader dr = command.ExecuteReader();


            try
            {
                if (Convert.ToBoolean(dr["IsRegistered"]))
                {
                    Response.Redirect("http://intranet.newwaveprofile.com");
                }

            }
            catch { }

            if (ModelState.IsValid)
            {
                if (dr.Read())
                {
                    FormsAuthentication.SetAuthCookie(model.Email, false);
                    return View("Register");
                }
                else
                {
                    ModelState.AddModelError("", "Lösenordet är inte korrekt, försök igen.");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private int GetUserByEmail(string email)
        {
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();

            string sql = string.Format("SELECT * FROM Users Where Email='{0}'", email);
            SqlCeCommand command = new SqlCeCommand(sql, conn);
            SqlCeDataReader dr = command.ExecuteReader();
            if (dr.Read()) {
                return Convert.ToInt32(dr["Id"]);
            }

            return 0;

        }

        [Authorize]
        public ActionResult UserDetails()
        {
            int userId = GetUserByEmail(User.Identity.Name);
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();

            UserViewModel model = new UserViewModel();

            string sql = string.Format("SELECT * FROM Users WHERE Id={0}", userId);
            SqlCeCommand command = new SqlCeCommand(sql, conn);
            SqlCeDataReader dr = command.ExecuteReader();

            if (dr.Read())
            {
                model.Company = Convert.ToString(dr["Company"]);
                model.Name = Convert.ToString(dr["Name"]);
                model.LastName = Convert.ToString(dr["LastName"]);
                model.Phone = Convert.ToString(dr["Phone"]);
                model.Address = Convert.ToString(dr["Address"]);
                model.City = Convert.ToString(dr["City"]);
                model.Zip = Convert.ToString(dr["Zip"]);
                model.Email = Convert.ToString(dr["Email"]);
                model.Homepage = Convert.ToString(dr["Homepage"]);
                model.Password = Convert.ToString(dr["Password"]);
                model.Description = Convert.ToString(dr["Description"]);
                model.Birth = Convert.ToString(dr["Birth"]);
                model.Position = Convert.ToString(dr["Position"]);
                model.NewPassword = Convert.ToString(dr["NewPassword"]);
                model.Domain = Convert.ToString(dr["Domain"]);
                model.EmailUserList = Convert.ToString(dr["EmailUserList"]);
                try
                {
                    model.UserType = Convert.ToInt32(dr["UserType"]);
                }
                catch {
                    model.UserType = 1;
                }
            }
            return View(model);
        }

        public bool IsValid(string emailaddress)
        {
            try
            {
                var m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private string fname = string.Empty;
        private string lname = string.Empty;

        public void ParseName(string email)
        {   
            fname = string.Empty;
            lname = string.Empty;

            string[] fullname = email.Split('@');

            var cname = fullname[0];
            if (cname.IndexOf(".") > 0)
            {
                fname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cname.Split('.')[0]);
                lname = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cname.Split('.')[1]);
            }
            else {
                fname =cname;
            }
        }

        private string CreateToken(byte Length)
        {
            char[] Chars = new char[] {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        };
            string String = string.Empty;
            Random Random = new Random();

            for (byte a = 0; a < Length; a++)
            {
                String += Chars[Random.Next(0, 61)];
            };

            return (String);
        }

        [Authorize]
        [HttpPost]
        public ActionResult UserDetails(UserViewModel model)
        {
           
            string sqldata = ""; ;
            if (ModelState.IsValid)
            {
                    int userId = GetUserByEmail(model.Email);

                    string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
                    SqlCeConnection conn = new SqlCeConnection(connstr);
                    conn.Open();

                    string sql = "Company='{0}', "; //Company
                    sql += "Name='{1}', "; //Name
                    sql += "LastName='{2}', "; //LastName
                    sql += "Phone='{3}', "; //Phone
                    sql += "Address='{4}', "; //Address
                    sql += "City='{5}', "; //City
                    sql += "Zip='{6}', "; //Zip
                    sql += "Email='{7}', "; //Email
                    sql += "Homepage='{8}', "; //HomePage
                    sql += "Description='{9}', "; //Desc
                    sql += "Position='{10}', "; //Position
                    sql += "Birth='{11}', "; //Birth
                    sql += "UserType={12}, "; //UserType
                    sql += "NewPassword='{13}', "; //NewPassword
                    sql += "EmailUserList='{14}',";
                    sql += "Domain='{15}'";

                    string data = string.Format(sql, model.Company, model.Name, model.LastName, model.Phone, model.Address, model.City, model.Zip, model.Email, model.Homepage, model.Description, model.Position, model.Birth, model.UserType, model.NewPassword, model.EmailUserList, model.Domain);
                    sqldata = string.Format("UPDATE Users SET " + data + " Where Id={0}", userId);

                    SqlCeCommand command = new SqlCeCommand(sqldata, conn);
                    command.ExecuteNonQuery();
                    conn.Close();

                    List<string> emails = new List<string>();

                    if (!string.IsNullOrEmpty(model.EmailUserList)){
                        using (StringReader reader = new StringReader(model.EmailUserList)){
                            string email;
                            while ((email = reader.ReadLine()) != null){
                                if (IsValid(email) && !EmailExists(email))
                                    emails.Add(email);
                            }
                        }
                    }

                    if(emails.Any())
                    {
                        foreach (var email in emails)
                        {
                            ParseName(email); //Each email is unique, dont attach it.
                            string last = (string.IsNullOrEmpty(lname)) ? fname : lname;

                            sql = "'{0}', "; //Company
                            sql += "'{1}', "; //Name
                            sql += "'{2}', "; //LastName
                            sql += "'{3}', "; //Phone
                            sql += "'{4}', "; //Address
                            sql += "'{5}', "; //City
                            sql += "'{6}', "; //Zip
                            sql += "'{7}', "; //Email
                            sql += "'{8}', "; //Homepage
                            sql += "'{9}', "; //Password
                            sql += " {10}, "; //IsRegistered
                            sql += "'{11}', "; //ProfileImage
                            sql += "'{12}', "; //Desc
                            sql += "'{13}', "; //Position
                            sql += "'{14}', "; //Birth
                            sql +=  "{15}, "; //UserType
                            sql += "'{16}', "; //NewPassword
                            sql += "'{17}',"; //EmailUserList
                            sql += "'{18}'"; //Domain

                            conn.Open();
                            string password = CreateToken(6);

                            string sdata = string.Format(sql, model.Company, fname, last, "", model.Address, model.City, model.Zip, email, "",  password, 0 , "", "","","", 1, "","", model.Domain);
                            string ext = string.Format("INSERT INTO Users (Company, Name, LastName, Phone, Address, City, Zip, Email, Homepage, Password, IsRegistered, ProfileImage, Description, Position, Birth, UserType, NewPassword, EmailUserList, Domain) VALUES({0})", sdata);
                            command = new SqlCeCommand(ext, conn);
                            command.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    return RedirectToAction("ProfileImageUpload");
            }
            return View(model);
        }


        public bool EmailExists(string email) 
        {
            try
            {
                string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
                SqlCeConnection conn = new SqlCeConnection(connstr);
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                SqlCeCommand command = new SqlCeCommand(string.Format("SELECT Email FROM Users WHERE Email='{0}'", email), conn);
                
                var dr = command.ExecuteReader();
                if (dr.Read())
                    return true;

             
            }
            catch (SqlCeException ex) {
                throw ex;
            }

            return false;
        }

        public ActionResult ProfileImageUpload() 
        {
            ProfileViewModel pmodel = new ProfileViewModel();
            int userId = GetUserByEmail(User.Identity.Name);
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();

            string sql = string.Format("SELECT * FROM Users WHERE Id={0}", userId);
            SqlCeCommand command = new SqlCeCommand(sql, conn);
            SqlCeDataReader dr = command.ExecuteReader();
            pmodel.ImageUrl = string.Empty;
            if (dr.Read()) {
                pmodel.ImageUrl = Convert.ToString(dr["ProfileImage"]);
            }
            return View(pmodel);
        }


        [Authorize]
        public ActionResult Upload(ProfileViewModel model)
        {
            var image = WebImage.GetImageFromRequest();

            if (image != null)
            {
                if (image.Width > 500)
                {
                    image.Resize(500, ((500 * image.Height) / image.Width));
                }
                var filename = Path.GetFileName(image.FileName);
                image.Save(Path.Combine("~/Images", filename));
                filename = Path.Combine("~/Images", filename);
                model.ImageUrl = Url.Content(filename);
                var editModel = new EditorInputModel()
                {
                    Profile = model,
                    Width = image.Width,
                    Height = image.Height,
                    Top = image.Height * 0.1,
                    Left = image.Width * 0.9,
                    Right = image.Width * 0.9,
                    Bottom = image.Height * 0.9
                };
                return View("Editor", editModel);
            }
            return View("Index", model);
        }

        [Authorize]
        public ActionResult Edit(EditorInputModel editor)
        {
            var image = new WebImage("~" + editor.Profile.ImageUrl);
            var height = image.Height;
            var width = image.Width;
            image.Crop((int)editor.Top, (int)editor.Left, (int)(height - editor.Bottom), (int)(width - editor.Right));
            var originalFile = editor.Profile.ImageUrl;
            editor.Profile.ImageUrl = Url.Content("~/Uploads/" + Path.GetFileName(image.FileName));
            image.Resize(100, 100, true, false);
            image.Save(@"~" + editor.Profile.ImageUrl);

            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();
            string sql = "UPDATE Users Set ProfileImage='" + editor.Profile.ImageUrl + "' WHERE Id=" + GetUserByEmail(User.Identity.Name);
            SqlCeCommand command = new SqlCeCommand(sql, conn);
            command.ExecuteNonQuery();

            //System.IO.File.Delete(Server.MapPath(originalFile));      

            return RedirectToAction("PasswordChange");
        }


        public ActionResult PasswordChange()
        {
            int userId = GetUserByEmail(User.Identity.Name);
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();

            UserViewModel model = new UserViewModel();

            string sql = string.Format("SELECT * FROM Users WHERE Id={0}", userId);
            SqlCeCommand command = new SqlCeCommand(sql, conn);
            SqlCeDataReader dr = command.ExecuteReader();

            //model.Name = "wallex";

            if (dr.Read())
            {
                if(string.IsNullOrEmpty(model.NewPassword))
                    model.NewPassword = Convert.ToString(dr["Password"]);
                else
                    model.NewPassword = Convert.ToString(dr["NewPassword"]);
            }

            return View(model);
        }

         [Authorize]
         [HttpPost]
         public ActionResult SetPassword(UserViewModel model)
         {
            int userId = GetUserByEmail(User.Identity.Name);
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();

            string sql = string.Format("UPDATE Users SET NewPassword='{1}' WHERE Id={0}", userId, model.NewPassword);
            SqlCeCommand command = new SqlCeCommand(sql, conn);
            command.ExecuteNonQuery();
            return RedirectToAction("Finish");
        }


        [Authorize]
        public ActionResult Finish()
        {
            UserViewModel model = new UserViewModel();
            int userId = GetUserByEmail(User.Identity.Name);
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();
            string sql = string.Format("SELECT * FROM Users WHERE Id={0}", userId);
            SqlCeCommand command = new SqlCeCommand(sql, conn);
            SqlCeDataReader dr = command.ExecuteReader();
            
            if (dr.Read())
            {
                model.Company = Convert.ToString(dr["Company"]);
                model.Name = Convert.ToString(dr["Name"]);
                model.LastName = Convert.ToString(dr["LastName"]);
                model.Phone = Convert.ToString(dr["Phone"]);
                model.Address = Convert.ToString(dr["Address"]);
                model.City = Convert.ToString(dr["City"]);
                model.Zip = Convert.ToString(dr["Zip"]);
                model.Email = Convert.ToString(dr["Email"]);
                model.Homepage = Convert.ToString(dr["Homepage"]);
                model.NewPassword = Convert.ToString(dr["NewPassword"]);
                model.ProfileImage = Convert.ToString(dr["ProfileImage"]);
                model.Description = Convert.ToString(dr["Description"]);
                model.Birth = Convert.ToString(dr["Birth"]);
                model.Position = Convert.ToString(dr["Position"]);
                model.UserType = Convert.ToInt32(dr["UserType"]);
                model.EmailUserList = "<br/>" + Convert.ToString(dr["EmailUserList"]).Replace(Environment.NewLine, "<br/>");
                model.Domain = Convert.ToString(dr["Domain"]);
                model.IsRegistered = Convert.ToInt32(dr["IsRegistered"]);
            }
            return View(model);            
        }

        public ActionResult TransportUser()
        {
            UserViewModel model = new UserViewModel();
            int userId = GetUserByEmail(User.Identity.Name);
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();
            string sql = string.Format("SELECT * FROM Users WHERE Id={0}", userId);
            SqlCeCommand command = new SqlCeCommand(sql, conn);
            SqlCeDataReader dr = command.ExecuteReader();

            if (dr.Read())
            {
                model.Company = Convert.ToString(dr["Company"]);
                model.Name = Convert.ToString(dr["Name"]);
                model.LastName = Convert.ToString(dr["LastName"]);
                model.Phone = Convert.ToString(dr["Phone"]);
                model.Address = Convert.ToString(dr["Address"]);
                model.City = Convert.ToString(dr["City"]);
                model.Zip = Convert.ToString(dr["Zip"]);
                model.Email = Convert.ToString(dr["Email"]);
                model.Homepage = Convert.ToString(dr["Homepage"]);
                model.Password = Convert.ToString(dr["Password"]);
                model.NewPassword = Convert.ToString(dr["NewPassword"]);
                model.ProfileImage = Convert.ToString(dr["ProfileImage"]);
                model.Description = Convert.ToString(dr["Description"]);
                model.Birth = Convert.ToString(dr["Birth"]);
                model.Position = Convert.ToString(dr["Position"]);
                model.UserType = Convert.ToInt32(dr["UserType"]);
                model.EmailUserList = "<br/>" + Convert.ToString(dr["EmailUserList"]).Replace(Environment.NewLine, "<br/>");
                model.Domain = Convert.ToString(dr["Domain"]);
                model.IsRegistered = Convert.ToInt32(dr["IsRegistered"]);

                if(model.IsRegistered==0)
                    CreateUserAccount(model);
            }

            return View();
        }

        public bool CheckIfExist(string email)
        {
            MySqlConnection ssoconn = new MySqlConnection("Server=127.0.0.1;Database=SSO;Uid=root;Pwd=;");
            ssoconn.Open();
            MySqlCommand ssocomm = ssoconn.CreateCommand();
            string s = string.Format("SELECT UserName From Users WHERE UserName='{0}'", email);
            ssocomm.CommandText = s;
            var dr = ssocomm.ExecuteReader();
            if (dr.Read())
                return true;

            return false;
        }

        public bool CreateUserAccount(UserViewModel user) 
        { 

            MySqlConnection conn = new MySqlConnection("Server=127.0.0.1;Database=nwp;Uid=root;Pwd=;");
            conn.Open();

            var displayName = user.Name + " " + user.LastName;


            string Sql = @"INSERT INTO user (`Active`, `Birth`, `Company`, `CompanyName`, `DisplayName`, `Email`, `Homepage`,`PersonalDetails`, `Phone`, `Position`, `TrashBin`, `UserIcon`, `UserName`) VALUES({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},'{11}','{12}');";
            string values = string.Format(Sql, 1, user.Birth, user.Domain, user.Company, displayName, user.Email, user.Homepage, user.Description, user.Phone, user.Position, 0, "", user.Email);
                
            //Put userdata in Intranet database
            MySqlCommand comm = conn.CreateCommand();
            comm.CommandText = values;
            comm.CommandText += "SELECT LAST_INSERT_ID()";
            var id = Convert.ToInt32(comm.ExecuteScalar());

            //Transfer user image
            string oldfile = HttpContext.Server.MapPath(user.ProfileImage);
            string newfile = string.Format(@"C:\inetpub\Sites\intranet.newwaveprofile.com\NewWaveMode\NWP\NWP\NWP.Intranet\Themes\www\Styles\Images\users\{0}{1}", id, Path.GetExtension(oldfile));
            System.IO.File.Copy(oldfile, newfile, true);

            Sql = string.Format("UPDATE User SET UserIcon='{0}' WHERE Id={1}", Path.GetFileName(newfile), id);
            comm = conn.CreateCommand();
            comm.CommandText = Sql;
            comm.ExecuteNonQuery();
            conn.Close();

            //Alter SSO, add user.
            var pass = (string.IsNullOrEmpty(user.NewPassword)) ? user.Password : user.NewPassword;
            var username = user.Email;
            MySqlConnection ssoconn = new MySqlConnection("Server=127.0.0.1;Database=SSO;Uid=root;Pwd=;");
            ssoconn.Open();
            MySqlCommand ssocomm = ssoconn.CreateCommand();
            string s = string.Format("INSERT INTO users (`UserName`, `Password`) VALUES ('{0}','{1}');", username, pass);
            ssocomm.CommandText = s;
            ssocomm.ExecuteNonQuery();
            ssoconn.Close();


            //Add current user to Roles.
            MySqlConnection r = new MySqlConnection("Server=127.0.0.1;Database=nwp;Uid=root;Pwd=;");
            r.Open();
            MySqlCommand rm = r.CreateCommand();
            string rq = string.Format("INSERT INTO userinrole (RoleId, UserId, TrashBin) VALUES (2,{0},false);", id);
           // throw new Exception(rq);
            rm.CommandText = rq;
            rm.ExecuteNonQuery();
            r.Close();

            int userId = GetUserByEmail(User.Identity.Name);
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection con = new SqlCeConnection(connstr);
            con.Open();
            string sql = string.Format("UPDATE Users SET IsRegistered=1 WHERE Id={0}", userId);
            var com = new SqlCeCommand(sql, con);
            com.ExecuteNonQuery();
            con.Close();


            return true;
        }
    }
}
