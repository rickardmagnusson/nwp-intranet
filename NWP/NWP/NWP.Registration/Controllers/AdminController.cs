﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace NWP.Registration.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        private EmailHelper Current { get; set; }

        public ActionResult SendMail()
        {
            List<EmailHelper> emails = new List<EmailHelper>();
            try
            {
                string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
                SqlCeConnection conn = new SqlCeConnection(connstr);
                conn.Open();

                string sql = "SELECT * FROM Users";
                SqlCeCommand command = new SqlCeCommand(sql, conn);
                SqlCeDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    emails.Add(new EmailHelper { Email = dr["Email"].ToString(), Password = dr["Password"].ToString(), Sent = false });
                }

                MailAddress from = new MailAddress("info@newwaveprofile.com", "New Wave Profile");
                MailMessage message = new MailMessage();
                message.Subject = "Registrering New Wave Profile Intranet.";

                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Host = "mail1.comhem.se";
                client.Port = 25;
                
                foreach (var user in emails)
                {
                    string body = string.Format(@"<h1>Registreringsuppgifter New Wave Profile intranet</h1>
                            <p>Nu lanserar vi vårt nya intranet för New Wave Profile. Här kommer vi samla all gemensam information för de bolag som ingår i konceptet. Initialt kommer du finna allt som rör nya varumärket och sedan fylls det på kring årsskiftet med mer info från övriga bolag.<br/><br/>
                            För att kunna logga in i behöver vi lite uppgifter ifrån dig, så klicka på länken: <a href='http://registrering.newwaveprofile.com'>http://registrering.newwaveprofile.com</a> och logga in med följande uppgifter.<br/>
                            Epost: {0}<br/>
                            Lösenord: {1}<br/><br/>
                            Camilla Fjordland <br/>New Wave Profile</p>", user.Email, user.Password);

                    string plain = string.Format(@"<h1>Registreringsuppgifter New Wave Profile intranet</h1>
                            Nu lanserar vi vårt nya intranet för New Wave Profile. Här kommer vi samla all gemensam information för de bolag som ingår i konceptet. Initialt kommer du finna allt som rör nya varumärket och sedan fylls det på kring årsskiftet med mer info från övriga bolag.
                            
                            För att kunna logga in i behöver vi lite uppgifter ifrån dig, så klicka på länken:http://registrering.newwaveprofile.com och logga in med följande uppgifter.
                            Epost: {0}
                            Lösenord: {1}
                            Camilla Fjordland 
                            New Wave Profile", user.Email, user.Password);

                    message.IsBodyHtml = true;
                    message.From = from;
                    message.To.Add(new MailAddress(user.Email));
                    message.Body = body;
                    AlternateView bd = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
                    AlternateView pl = AlternateView.CreateAlternateViewFromString(plain, null, MediaTypeNames.Text.Plain);
                    message.AlternateViews.Add(bd);
                    message.AlternateViews.Add(pl);

                    client.Send(message);
                    Current = user;
                    client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);
                    user.Sent = Current.Sent;
                }


            }
            catch (Exception ex) {
               
            }

            return View("SendEmail", emails);
        }
        void client_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e) {
            Current.Sent = true;
        }

        public ActionResult ImportUsers()
        { 
            return  View();
        }

        public ActionResult ImportStart()
        {
            string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
            SqlCeConnection conn = new SqlCeConnection(connstr);
            conn.Open();

            string sql = string.Format("SELECT * FROM Users Where IsRegistered = {0}" , "1");
            SqlCeCommand command = new SqlCeCommand(sql, conn);
            SqlCeDataReader dr = command.ExecuteReader();

            List<UserViewModel> list = new List<UserViewModel>();

            while (dr.Read()) {
                list.Add(new UserViewModel 
                { 
                    ID = dr["ID"].ToString(),
                    Company = dr["Company"].ToString(),
                    Name = dr["Name"].ToString(),
                    LastName = dr["LastName"].ToString(),
                    Phone = dr["Phone"].ToString(),
                    Address = dr["Addess"].ToString(),
                    City = dr["City"].ToString(),
                    Zip = dr["Zip"].ToString(),
                    Email = dr["Email"].ToString(),
                    Homepage = dr["Homepage"].ToString(),
                    Password = dr["Password"].ToString(),
                    IsRegistered = Convert.ToInt32(dr["IsRegistered"].ToString()),
                    ProfileImage = dr["ProfileImage"].ToString(),
                    Description = dr["Description"].ToString(),
                    Position = dr["Position"].ToString(),
                    Birth = dr["Birth"].ToString(),
                    UserType = Convert.ToInt32(dr["UserType"].ToString()),
                    NewPassword = dr["NewPassword"].ToString(),
                    EmailUserList = dr["EmailUserList"].ToString()
                });
            }
            return View();
        }

        public ActionResult UserImport() 
        {
            return View();
        }


        public ActionResult AddUser()
        {
            var Companies = new List<MediaToken>
                {
                  //new MediaToken{ Domain="addprofile", Logo="new_wave_logotyp_add_profile_cmyk.jpg"},
                    new MediaToken{ Domain="arenareklam", Logo="new_wave_logotyp_arena_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="barekosport", Logo="new_wave_logotyp_bareko_sport_cmyk.jpg"},
                    new MediaToken{ Domain="bravoprofil", Logo="new_wave_logotyp_bravo_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="broderiet", Logo="new_wave_logotyp_broderiet_cmyk.jpg"},
                    new MediaToken{ Domain="chardenreklam", Logo="new_wave_logotyp_charden_reklam_cmyk.jpg"},
                    //new MediaToken{ Domain="clijofashion", Logo="new_wave_logotyp_clijo_fashion_cmyk.jpg"},
                    new MediaToken{ Domain="companyline", Logo="new_wave_logotyp_companyline_cmyk.jpg"},
                    new MediaToken{ Domain="daylight", Logo="new_wave_logotyp_daylight_cmyk.jpg"},
                    new MediaToken{ Domain="defactoyrkesklader", Logo="new_wave_logotyp_de_facto_yrkesklader_cmyk.jpg"},
                    new MediaToken{ Domain="felestad", Logo="new_wave_logotyp_felestad_cmyk.jpg"},
                    new MediaToken{ Domain="flinkreklam", Logo="new_wave_logotyp_flink_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="foretagsprofilen", Logo="new_wave_logotyp_foretagsprofilen_cmyk.jpg"},
                    new MediaToken{ Domain="gewepromotion", Logo="new_wave_logotyp_gewe_cmyk.jpg"},
                    new MediaToken{ Domain="industriprofil", Logo="new_wave_logotyp_industriprofil_cmyk.jpg"},
                    new MediaToken{ Domain="intersport", Logo="new_wave_logotyp_intersport_cmyk.jpg"},
                    new MediaToken{ Domain="jobb-fritid", Logo="new_wave_logotyp_jobb_&_fritid_cmyk.jpg"},
                    //new MediaToken{ Domain="json", Logo="new_wave_logotyp_json_cmyk.jpg"},
                    //new MediaToken{ Domain="justintime", Logo="new_wave_logotyp_just_in_time_cmyk.jpg"},
                    new MediaToken{ Domain="kents", Logo="new_wave_logotyp_kents_cmyk.jpg"},
                    new MediaToken{ Domain="kereklamtryck", Logo="new_wave_logotyp_ke_reklamtryck_cmyk.jpg"},
                    new MediaToken{ Domain="lhprofildesign", Logo="new_wave_logotyp_lh_profildesign_cmyk.jpg"},
                    new MediaToken{ Domain="lptreklam", Logo="new_wave_logotyp_lpt_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="meraprofil-reklam", Logo="new_wave_logotyp_mera_profil_&_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="meri", Logo="new_wave_logotyp_meri_cmyk.jpg"},
                    new MediaToken{ Domain="myshkin", Logo="new_wave_logotyp_myshkin_cmyk.jpg"},
                    new MediaToken{ Domain="newpromotion", Logo="new_wave_logotyp_new_promotion_cmyk.jpg"},
                    new MediaToken{ Domain="nobleart", Logo="new_wave_logotyp_noble_art_cmyk.jpg"},
                    new MediaToken{ Domain="nojab", Logo="new_wave_logotyp_nojab_cmyk.jpg"},
                    new MediaToken{ Domain="nordtrend", Logo="new_wave_logotyp_nordtrend_cmyk.jpg"},
                    new MediaToken{ Domain="olympus", Logo="new_wave_logotyp_olympus_cmyk.jpg"},
                    new MediaToken{ Domain="orebroscreen", Logo="new_wave_logotyp_orebro_screen_cmyk.jpg"},
                    new MediaToken{ Domain="pahlsonsreklam", Logo="new_wave_logotyp_pahlsons_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="produktpartners", Logo="new_wave_logotyp_produkt_partners_cmyk.jpg"},
                    new MediaToken{ Domain="profilesupport", Logo="new_wave_logotyp_profile_support_cmyk.jpg"},
                    new MediaToken{ Domain="protex", Logo="new_wave_logotyp_protex_cmyk.jpg"},
                    new MediaToken{ Domain="practive", Logo="new_wave_logotyp_pr_active_cmyk.jpg"},
                    new MediaToken{ Domain="reklamproffsen", Logo="new_wave_logotyp_reklamproffsen_cmyk.jpg"},
                    new MediaToken{ Domain="roupez", Logo="new_wave_logotyp_roupez_cmyk.jpg"},
                    new MediaToken{ Domain="screenbolaget", Logo="new_wave_logotyp_screenbolaget_cmyk.jpg"},
                    new MediaToken{ Domain="sormlindsreklam", Logo="new_wave_logotyp_sormlinds_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="sunscreen", Logo="new_wave_logotyp_sun_screen_cmyk.jpg"},
                    new MediaToken{ Domain="teamgamewi", Logo="new_wave_logotyp_team_gamewi_cmyk.jpg"},
                    new MediaToken{ Domain="tm", Logo="new_wave_logotyp_tm_cmyk.jpg"},
                    new MediaToken{ Domain="toolsskeppa", Logo="new_wave_logotyp_tools_skeppa_cmyk.jpg"},
                    new MediaToken{ Domain="topline", Logo="new_wave_logotyp_topline_cmyk.jpg"},
                    new MediaToken{ Domain="triffiq", Logo="new_wave_logotyp_triffiq_cmyk.jpg"},
                    new MediaToken{ Domain="trimdon", Logo="new_wave_logotyp_trimdon_cmyk.jpg"},
                    new MediaToken{ Domain="trycket", Logo="new_wave_logotyp_trycket_cmyk.jpg"},
                    new MediaToken{ Domain="wallex", Logo="new_wave_logotyp_wallex_cmyk.jpg"},
                    new MediaToken{ Domain="waypoint", Logo="new_wave_logotyp_waypoint_cmyk.jpg"},
                    new MediaToken{ Domain="yrkesoprofilklader", Logo="new_wave_logotyp_yrkes_o_profil_klader_cmyk.jpg"},
                    new MediaToken{ Domain="www", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="sagaform", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="dochj", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="toppoint", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="projob", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="newwavesports", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="jobman", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="orreforskostaboda", Logo="new_wave_logotyp_www_cmyk.jpg"}
                };
            ViewBag.Companies = Companies.OrderBy(d => d.Domain);


            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddUser(string users)
        {

            List<string> data = new List<string>();

            using (StringReader r = new StringReader(users))
            {
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    data.Add(line);
                }
            }

            List<EmailH> Text = new List<EmailH>();
            string inserted = "Lade till användare:<br/>";
            string body = string.Empty;
            foreach (var userline in data)
            {

                var lines = userline.Split(new char[] { ';' }, StringSplitOptions.None);
              
                    string email = lines[0];
                    string domain = lines[1];



                    body = string.Format(@"Registreringsuppgifter New Wave Profile intranet%0D %0D
Nu lanserar vi vårt nya intranet för New Wave Profile. Här kommer vi samla all gemensam information för de bolag som ingår i konceptet. Initialt kommer du finna allt som rör nya varumärket och sedan fylls det på kring årsskiftet med mer info från övriga bolag.%0D
För att kunna logga in i behöver vi lite uppgifter ifrån dig, så klicka på länken: http://registrering.newwaveprofile.com och logga in med följande uppgifter.%0D
%0D
Efter registreringen loggar du alltid in på http://intranet.newwaveprofile.com %0D %0D
Epost: {0}%0D
Lösenord: {1}%0D
%0D
Mvh%0D
Camilla Fjordland%0D
New Wave Profile", email, "123456");
                Text.Add(new EmailH { Email=email, body=body });


                string connstr = "Data Source=" + Server.MapPath("/App_Data/TempCustData.sdf");
                SqlCeConnection conn = new SqlCeConnection(connstr);
                conn.Open();
                SqlCeCommand cmd = new SqlCeCommand(string.Format("INSERT INTO Users (Name, Email, Password, Domain, UserType, Company) VALUES('{0}','{1}','{2}','{3}', {4}, '{5}')", email, email, "123456", domain, 2, domain), conn);
                cmd.ExecuteNonQuery();
                


                inserted += email + "<br/>";

                conn.Close();

            }

            ViewBag.Inserted = inserted;

            var Companies = new List<MediaToken>
                {
                  //new MediaToken{ Domain="addprofile", Logo="new_wave_logotyp_add_profile_cmyk.jpg"},
                    new MediaToken{ Domain="arenareklam", Logo="new_wave_logotyp_arena_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="barekosport", Logo="new_wave_logotyp_bareko_sport_cmyk.jpg"},
                    new MediaToken{ Domain="bravoprofil", Logo="new_wave_logotyp_bravo_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="broderiet", Logo="new_wave_logotyp_broderiet_cmyk.jpg"},
                    new MediaToken{ Domain="chardenreklam", Logo="new_wave_logotyp_charden_reklam_cmyk.jpg"},
                    //new MediaToken{ Domain="clijofashion", Logo="new_wave_logotyp_clijo_fashion_cmyk.jpg"},
                    new MediaToken{ Domain="companyline", Logo="new_wave_logotyp_companyline_cmyk.jpg"},
                    new MediaToken{ Domain="daylight", Logo="new_wave_logotyp_daylight_cmyk.jpg"},
                    new MediaToken{ Domain="defactoyrkesklader", Logo="new_wave_logotyp_de_facto_yrkesklader_cmyk.jpg"},
                    new MediaToken{ Domain="felestad", Logo="new_wave_logotyp_felestad_cmyk.jpg"},
                    new MediaToken{ Domain="flinkreklam", Logo="new_wave_logotyp_flink_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="foretagsprofilen", Logo="new_wave_logotyp_foretagsprofilen_cmyk.jpg"},
                    new MediaToken{ Domain="gewepromotion", Logo="new_wave_logotyp_gewe_cmyk.jpg"},
                    new MediaToken{ Domain="industriprofil", Logo="new_wave_logotyp_industriprofil_cmyk.jpg"},
                    new MediaToken{ Domain="intersport", Logo="new_wave_logotyp_intersport_cmyk.jpg"},
                    new MediaToken{ Domain="jobb-fritid", Logo="new_wave_logotyp_jobb_&_fritid_cmyk.jpg"},
                    //new MediaToken{ Domain="json", Logo="new_wave_logotyp_json_cmyk.jpg"},
                    //new MediaToken{ Domain="justintime", Logo="new_wave_logotyp_just_in_time_cmyk.jpg"},
                    new MediaToken{ Domain="kents", Logo="new_wave_logotyp_kents_cmyk.jpg"},
                    new MediaToken{ Domain="kereklamtryck", Logo="new_wave_logotyp_ke_reklamtryck_cmyk.jpg"},
                    new MediaToken{ Domain="lhprofildesign", Logo="new_wave_logotyp_lh_profildesign_cmyk.jpg"},
                    new MediaToken{ Domain="lptreklam", Logo="new_wave_logotyp_lpt_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="meraprofil-reklam", Logo="new_wave_logotyp_mera_profil_&_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="meri", Logo="new_wave_logotyp_meri_cmyk.jpg"},
                    new MediaToken{ Domain="myshkin", Logo="new_wave_logotyp_myshkin_cmyk.jpg"},
                    new MediaToken{ Domain="newpromotion", Logo="new_wave_logotyp_new_promotion_cmyk.jpg"},
                    new MediaToken{ Domain="nobleart", Logo="new_wave_logotyp_noble_art_cmyk.jpg"},
                    new MediaToken{ Domain="nojab", Logo="new_wave_logotyp_nojab_cmyk.jpg"},
                    new MediaToken{ Domain="nordtrend", Logo="new_wave_logotyp_nordtrend_cmyk.jpg"},
                    new MediaToken{ Domain="olympus", Logo="new_wave_logotyp_olympus_cmyk.jpg"},
                    new MediaToken{ Domain="orebroscreen", Logo="new_wave_logotyp_orebro_screen_cmyk.jpg"},
                    new MediaToken{ Domain="pahlsonsreklam", Logo="new_wave_logotyp_pahlsons_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="produktpartners", Logo="new_wave_logotyp_produkt_partners_cmyk.jpg"},
                    new MediaToken{ Domain="profilesupport", Logo="new_wave_logotyp_profile_support_cmyk.jpg"},
                    new MediaToken{ Domain="protex", Logo="new_wave_logotyp_protex_cmyk.jpg"},
                    new MediaToken{ Domain="practive", Logo="new_wave_logotyp_pr_active_cmyk.jpg"},
                    new MediaToken{ Domain="reklamproffsen", Logo="new_wave_logotyp_reklamproffsen_cmyk.jpg"},
                    new MediaToken{ Domain="roupez", Logo="new_wave_logotyp_roupez_cmyk.jpg"},
                    new MediaToken{ Domain="screenbolaget", Logo="new_wave_logotyp_screenbolaget_cmyk.jpg"},
                    new MediaToken{ Domain="sormlindsreklam", Logo="new_wave_logotyp_sormlinds_reklam_cmyk.jpg"},
                    new MediaToken{ Domain="sunscreen", Logo="new_wave_logotyp_sun_screen_cmyk.jpg"},
                    new MediaToken{ Domain="teamgamewi", Logo="new_wave_logotyp_team_gamewi_cmyk.jpg"},
                    new MediaToken{ Domain="tm", Logo="new_wave_logotyp_tm_cmyk.jpg"},
                    new MediaToken{ Domain="toolsskeppa", Logo="new_wave_logotyp_tools_skeppa_cmyk.jpg"},
                    new MediaToken{ Domain="topline", Logo="new_wave_logotyp_topline_cmyk.jpg"},
                    new MediaToken{ Domain="triffiq", Logo="new_wave_logotyp_triffiq_cmyk.jpg"},
                    new MediaToken{ Domain="trimdon", Logo="new_wave_logotyp_trimdon_cmyk.jpg"},
                    new MediaToken{ Domain="trycket", Logo="new_wave_logotyp_trycket_cmyk.jpg"},
                    new MediaToken{ Domain="wallex", Logo="new_wave_logotyp_wallex_cmyk.jpg"},
                    new MediaToken{ Domain="waypoint", Logo="new_wave_logotyp_waypoint_cmyk.jpg"},
                    new MediaToken{ Domain="yrkesoprofilklader", Logo="new_wave_logotyp_yrkes_o_profil_klader_cmyk.jpg"},
                    new MediaToken{ Domain="www", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="sagaform", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="dochj", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="toppoint", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="projob", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="newwavesports", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="jobman", Logo="new_wave_logotyp_www_cmyk.jpg"},
                    new MediaToken{ Domain="orreforskostaboda", Logo="new_wave_logotyp_www_cmyk.jpg"}
                };
            ViewBag.Companies = Companies.OrderBy(d=> d.Domain);
           
            return View(Text);
        }


    }


    public class MediaToken
    {
        public string Domain { get; set; }
        public string Logo { get; set; }
    }

    public class EmailH
    {
        public string Email { get; set; }
        public string body { get; set; }
    }


    public class EmailHelper {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Sent { get; set; }
    }
}
