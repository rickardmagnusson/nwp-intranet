﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NWP.Registration.Controllers
{
    public class UserViewModel
    {
        public string ID { get; set; }

        [Required]
        [Display(Name = "Företagsnamn")]
        public string Company { get; set; }

        [Required]
        [Display(Name = "Namn")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Efternamn")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Telefon")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "Adress")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Stad")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Postnr")]
        public string Zip { get; set; }

        [Required]
        //[DataType(DataType.EmailAddress)]
        [Display(Name = "E-post")]
        public string Email { get; set; }

        [Display(Name = "Hemsida")]
        public string Homepage { get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "{0} måste vara minst 1 tecken långt.")]
        [Display(Name = "Lösenord")]
        public string Password { get; set; }

        [Display(Name = "Registrerad")]
        public int IsRegistered { get; set; }

        [Display(Name = "Profilbild")]
        public string ProfileImage { get; set; }

        [Display(Name = "Beskrivning av dig")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Yrke/roll i företaget")]
        public string Position { get; set; }

        [Required]
        [Display(Name = "Personnummer(YYYYMMDD,4 sista krävs inte.)")]
        public string Birth { get; set; }

        [Required]
        [Display(Name = "Jag/mitt företag är: ")]
        public Int32 UserType { get; set; }

        [Display(Name = "Lösenord")]
        public string NewPassword { get; set; }

        [Display(Name = "Ange om det är fler som skall kunna logga in. En epostadress på varje ny rad.")]
        public string EmailUserList { get; set; }

        [Display(Name = "Domain")]
        public string Domain { get; set; }

    }
}