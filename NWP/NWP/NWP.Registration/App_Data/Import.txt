﻿New Wave;Tomas Jansson;0524-28300;Åkarevägen 18;Dingle;455 83;tomas.jansson@newwave.se;www.newwave.se;-;www
New Wave;Roger Andersson;0524-28300;Åkarevägen 18;Dingle;455 83;roger.andersson@newwave.se;www.newwave.se;-;www
New Wave;Klas Blomqvist;0524-28300;Åkarevägen 18;Dingle;455 83;klas.blomqvist@newwav.se;www.newwave.se;-;www
New Wave;Roder Sandberg;0524-28300;Åkarevägen 18;Dingle;455 83;roger.sandberg@newwave.se;www.newwave.se;-;www
New Wave;Mattias Svenningsson;0524-28300;Åkarevägen 18;Dingle;455 83;mattias.svenningsson@newwave.se;www.newwave.se;-;www
New Wave;Kristina Benjaminsson;0524-28300;Åkarevägen 18;Dingle;455 83;kristina.benjaminsson@newwave.se;www.newwave.se;-;www
New Wave;Monica Gustavsson;0524-28300;Åkarevägen 18;Dingle;455 83;monica.gustavsson@newwave.se;www.newwave.se;-;www
New Wave;Marie Mossberg;0524-28300;Åkarevägen 18;Dingle;455 83;marie.mossberg@newwave.se;www.newwave.se;-;www
New Wave;Linda Pettersson;0524-28300;Åkarevägen 18;Dingle;455 83;linda.pettersson@newwave.se;www.newwave.se;-;www
New Wave;Anders Dahlgren;0524-28300;Åkarevägen 18;Dingle;455 83;Anders.Dahlgren@toppoint.se;www.newwave.se;-;www
New Wave;Ann Järves;0524-28300;Åkarevägen 18;Dingle;455 83;ann.jarves@orrefors.se;www.newwave.se;-;www
New Wave;Anna-Karin Hansson;0524-28300;Åkarevägen 18;Dingle;455 83;anna-karin.hansson@toppoint.se;www.newwave.se;-;www
New Wave;Thomas Carleklo;0524-28300;Åkarevägen 18;Dingle;455 83;thomas.carleklo@orrefors.se;www.newwave.se;-;www
New Wave;Henrik Johansson;0524-28300;Åkarevägen 18;Dingle;455 83;Henrik.Johansson@sagaform.com;www.newwave.se;-;www
New Wave;Marcus Persson;0524-28300;Åkarevägen 18;Dingle;455 83;marcus.persson@sagaform.se;www.newwave.se;-;www
New Wave;Johanna Joelsson;0524-28300;Åkarevägen 18;Dingle;455 83;johanna.joelsson@sagaform.se;www.newwave.se;-;www
New Wave;Anna Sand;0524-28300;Åkarevägen 18;Dingle;455 83;anna.sand@sagaform.se;www.newwave.se;-;www
New Wave;Joakim Gunberg;0524-28300;Åkarevägen 18;Dingle;455 83;joakim.gunberg@dochj.se;www.newwave.se;-;www
New Wave;Johan Svanhed;0524-28300;Åkarevägen 18;Dingle;455 83;Johan.Svanehed@newwavesports.se;www.newwave.se;-;www
New Wave;Magnus Axelsson;0524-28300;Åkarevägen 18;Dingle;455 83;magnus.axelsson@orrefors.se;www.newwave.se;-;www
New Wave;Maria Green;0524-28300;Åkarevägen 18;Dingle;455 83;Maria.Gren@toppoint.se;www.newwave.se;-;www
New Wave;Michael Forslund;0524-28300;Åkarevägen 18;Dingle;455 83;Michael.Forslund@newwavesports.se;www.newwave.se;-;www
New Wave;Partik Andersson;0524-28300;Åkarevägen 18;Dingle;455 83;Patrik.Andersson@projob.se;www.newwave.se;-;www
New Wave;Henrik Olausson;0524-28300;Åkarevägen 18;Dingle;455 83;henrik.olausson@projob.se;www.newwave.se;-;www
New Wave;Stefan Wendelin;0524-28300;Åkarevägen 18;Dingle;455 83;stefan.wendelin@djfrantextil.se;www.newwave.se;-;www
New Wave;Urban Flodin;0524-28300;Åkarevägen 18;Dingle;455 83;urban.flodin@jobman.se;www.newwave.se;-;www
New Wave;Åsa Lindberg;0524-28300;Åkarevägen 18;Dingle;455 83;Asa.Liberg@djfrantextil.se;www.newwave.se;-;www
New Wave;Linus Tagesson;0524-28300;Åkarevägen 18;Dingle;455 83;linus.tagesson@djfrantextil.se;www.newwave.se;-;www
New Wave;Carl Höjden;0524-28300;Åkarevägen 18;Dingle;455 83;carl.hojden@toppoint.se;www.newwave.se;-;www
#
# Import NWP users
Broderiet;Thomas Gunnarsson;0300-73330;Kungsparksvägen 31;Kungsbacka;434 39;thomas.gunnarsson@nwpp.se;broderiet.se;VTMODv;broderiet
Wallex AB;Ulf Jacobsson;031-228080;Aröds Industriväg 34;Göteborg;422 43;ulf@wallex.se;www.wallex.se;t!rHbg;wallex
Olympus AB;Bengt Svanberg;0522-81380;Björbäcksvägen 6;Uddevalla;451 55;bengt@olympus.nu;www.olympus.nu;Aq5jqf;olympus
Bareko Sport & Profil AB;Ulf Höst;042-295080;Södra Brunnsvägen 69;Helsingborg;253 60;ulf.host@bareko.se;www.bareko.se;-#MuF7;bravoprofil
Felestad Trading;Ingela Engman;046-712640;Profilvägen 4;Löddeköpinge;246 43;ingela.engman@felestad.se;www.felestad.se;ORnzgN;felestad
Kents Textiltryckeri AB;Kent Poulsen;035-210075;Stationsgatan 37;Halmstad;302 45;kent@kents.se;www.kents.se;YQRd*Q;kents
Nordtrend;Jonas Blomberg;040-933005;Stadiongatan 40A;Malmö;217 62;jonas.blomberg@nwpp.se;nordtrend.se;rU4Asq;nordtrend
Nöjab Present & Reklam AB;PG Gustafsson;0480-26512;Kalmar Tech Park;Kalmar;392 39;pg@nojab.se;www.nojab.se;f5*hba;nojab
Team Gamewi AB;Pär-Håkan Andersson;040-160085;Gustavsgatan 34;Malmö;216 11;ph@teamgamewi.se;www.teamgamewi.se;GJ2EMb;teamgamewi
Waypoint/Trycket i Kristianstad;Per Tyrén;044-128600;Vattentornsvägen;Kristianstad;291 32;per@waypoint.nu;www.waypoint.nu;QHf2td;waypoint
Arena Reklam;Jenny Thermell;0586-30730;Skogsbovägen 5;Karlskoga;691 47;jenny.thermell@arenareklam.com;www.arenareklam.com;Y3XB50;arenareklam
FöretagsPRofilen;Dennie Breander;08-6111101;Engelbrektsgatan 7;Stockholm;114 32;db@foretagsprofilen.se;www.foretagsprofilen.se;fg2hkl;foretagsprofilen
Gewe Promotion;Jeanette Liedgren;08-6899025;Dalhemsvägen 44;Stockholm;141 46;jeanette.liedgren@gewe.se;www.gewe.se;4ebbzL;gewepromotion
Intersport Väst AB;Lena Larsson;031-3010320;Parkgatan 53A;Göteborg;411 38;lena.larsson@intersport.se;www.intersport.se;aaac8p;intersport
Profile Support i Vara;Leif Magnusson;0512-16200;Gradgatan 2;Vara;534 50;leif.magnusson@profilesupport.se;profilesupport.se;akEAzR;profilesupport
LPT Reklam;Krister Landström;0582-10007;Östra Storgatan 3;Hallsberg;694 31;krister@lptreklam.nu;www.lptreklam.nu;GovgVz;lptreklam
Mera Produktion AB;Erland Hallberg;031-3409999;Vagnmakaregatan 1B;Göteborg;415 07;infor@mera.se;www.mera.se;p05apy;meraprofiloreklam
Myshkin AB;Christer Dalgård;054-155200;Magasin 2 Löfbergskajen;Karlstad;652 24;christer.dalgard@myshkin.se;www.myshkin.se;LKo5*K;myshkin
New Promition Sverige AB;Peter Börje;0510-219 50;Fabriksgatan 2;Lidköping;531 30;peter.borje@newpromotion.se;www.newpromotion.se;GovgVz;newpromotion
Nordtrend;Jonas Thunberg;031-151054;Venusgatan 2-6;Göteborg;416 64;jonas.thunberg@nwpp.se;nordtrend.se;sfxxRJ;nordtrend
Profile Support;Patrik Andersson;0500-499590;Aspelundsvägen 1;Skövde;541 34;patrik.andersson@profilesupport.se;www.profilesupport.se;p05apy;profilesupport
Påhlsons Reklam AB;Staffan Påhlsson;0155-37590;G:a Oxelösundsvägen 1;Nyköping;611 38;staffan.pahlson@nwpp.se;www.pahlssons.se;EMx7S7;pahlssonsreklam
Sun Screen;Curt Rudin;0565-711611;Lövnäsvägen 8;Sunne;686 35;curt@sunscreen.se;www.sunscreen.se;Fi-#dk;sunscreen
TM Kontor & Reklam AB;Thomas Lindén;011-100556;Koppargatan 11;Norrköping;602 23;thomas.linden@nwpp.se;www.tmkontor.se;RSagS-;tm
Top Line Arrangemang & Profil;Lars Johansson;033-205900;Badvägen 1;Borås;518 31;lars@topline.se;www.topline.se;-aMuF7;topline
Trimdon AB;Daniel Rudin;0303-195 40;Svarvaregatan 5;Kungälv;442 34;daniel@trimdon.se;www.trimdon.se;eOJasj;trimdon
Örebro Screen;Stefan Strandell;019-130200;Stångjärnsgatan 5;Örebro;703 63;stefan.strandell@orebroscreen.se;www.orebroscreen.se;q*m9h@;orebroscreen
Company Line;Jonas Andersson - Junkka;0920-226699;Midgårdsvägen 9;Luleå;973 34;robert.lindstrom@nwpp.se;www.companyline.nu;eOJasj;companyline
KE Reklam AB;Krister Eriksson;0970-55824;Stallarevägen 4;Gällivare;982 38;kerstin@ke-tryck.se;www.ke-tryck.se;t!rHbg;kereklam
NWPP Protex;Håkan Bodare;023-15000;Tullkammargatan 2;Falun;791 31;hakan.bodare@nwpp.se;www.protex.se;6bgqsx;protex
Trycket i Mora AB;Per Hofvander;0250-13525;Badstugatan 9;Mora;792 32;mathias.hofvander@trycketmora.se;www.trycketmora.se;OxgKqk;trycket
Company Line;Jonas Andersson - Junkka;0980-66102;Österleden 12;Kiruna;981 38;info@companyline.nu;www.companyline.se;*oA36x;companyline
#
#Partners
LH Profildesign AB;Malin Carlsson;0322-634755;Metallgatan 2-4;Alingsås;441 32;malin.carlsson@lhprofil.se;www.lhprofil.se;$$$c8p;profildesignalingsas
Noble Art AB;Pelle Lundqvist;021-120896;Generatorgatan 7;Västerås;722 24;pelle@nobleart.se;www.nobleart.se;p05apy;nobleart
Nordic Produkt Partners AB;Annelie Gemark;036-129092;Glansgatan 1;Jönköping;554 54;annelie@produktpartners.se;www.produktpartners.se;p05apy;produktpartners
Tools Sydost / Norrköpings Skeppsfournering;Lasse Wessman;011-211 400;Lindövägen 49;Norrköping;602 28;lars.wessman@skeppa.se;www.skeppa.se;HzpM@R;skeppa
Reklamproffsen AB;Andreas Blomkvist;019-562000;Marsvägen 2B;Kumla;692 34;andreas@reklamproffsen.se;www.reklamproffsen.se;OxgKqk;reklamproffsen
Triffiq Företagsprofilering AB;Rolf Sjölander;08-6413735;Maria Skolgatan 81;Stockholm;118 53;rolf@triffiq.se;www.triffiq.se;b4K1qr;triffiq
Y&P Kläder/Responsor;Pelle Lundberg;018-127777;Kungsgatan 78;Uppsala;753 18;brev@yp.nu;www.yp.nu;NFtC8v;yp