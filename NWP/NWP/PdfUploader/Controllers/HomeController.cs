﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PdfUploader.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        [HttpPost] 
        public ActionResult Upload()
        {
            Request.Files.CreatePDFImage();
            return View("Index");
        }
    }

    public static class PDF
    {

        /// <summary>
        /// Generates a preview image from a PDF file.
        /// </summary>
        /// <param name="files"></param>
        public static void CreatePDFImage(this HttpFileCollectionBase files)
        {
            PdfUploader.Models.PdfTools.GeneratePreview(files[0]);
        }
    }
}
