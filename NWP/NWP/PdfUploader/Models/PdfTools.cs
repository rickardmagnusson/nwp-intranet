﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using BitMiracle.Docotic.Pdf;
using System.Diagnostics;

namespace PdfUploader.Models
{
    public static class PdfTools
    {
        static string path = HttpContext.Current.Server.MapPath("/Media/Pdf");

        public static void GeneratePreview(HttpPostedFileBase file)
        {
           
            string filename = Path.Combine(path, Path.GetFileName(file.FileName));
            file.SaveAs(filename);
            CreateImage(file.FileName);
        }

        public static void CreateImage(string file)
        {
            string filename = Path.Combine(path, Path.GetFileName(file));
            //PdfDocument pdf = new PdfDocument(filename);
            //pdf.Images[1].Save(filename);


            using (PdfDocument pdf = new PdfDocument(file))
            {
 
                PdfDrawOptions options = PdfDrawOptions.CreateFitSize(new PdfSize(200, 200), false);
                options.BackgroundColor = new PdfGrayColor(100);
                options.Format = PdfDrawFormat.Png;


                pdf.Pages[0].Save( filename + ".png", options);
            }



        }


        static void ExtractAllImages()
        {
            string path = "";
            using (PdfDocument pdf = new PdfDocument(path))
            {
                for (int i = 0; i < pdf.Images.Count; i++)
                {
                    string imageName = string.Format("image{0}", i);
                    string imagePath = pdf.Images[i].Save(imageName);
                }
            }
        } 



      
      
    }
}