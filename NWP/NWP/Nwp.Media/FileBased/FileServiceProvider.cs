﻿
using Nwp.Media.Services;

namespace Nwp.Media.FileBased
{
	public class FileServiceProvider : IFileServiceProvider
	{
		private readonly string rootPath;

		private readonly char directorySeparator;

		private readonly bool encodeUsername;

		public FileServiceProvider(string rootPath)
			: this(rootPath, false, '|')
		{
		}

		public FileServiceProvider(string rootPath, bool encodeUsername)
			: this(rootPath, encodeUsername, '|')
		{
		}

		public FileServiceProvider(string rootPath, bool encodeUsername, char directorySeparator)
		{
			Helper.ValidateNotNullOrEmpty(rootPath, "rootPath");

			this.rootPath = rootPath;
			this.directorySeparator = directorySeparator;
			this.encodeUsername = encodeUsername;
		}

		public IFileService GetFileService(string username)
		{
			return new FileService(rootPath, username, encodeUsername, directorySeparator);
		}

		public IFolderService GetFolderService(string username)
		{
			return new FolderService(rootPath, username, encodeUsername, directorySeparator);
		}
	}
}
