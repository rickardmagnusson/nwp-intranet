﻿using System.Web.Mvc;

namespace Nwp.Media
{
	public static class AjaxExtensions
	{
		public static string Json(this AjaxHelper ajax, object jsonObj)
		{
			return Helper.ToJsonString(jsonObj);
		}
	}
}
