using System.Configuration;

namespace Nwp.Media.Configuration
{
    /// <summary>
    /// A collection of ExtensionConfig instances.
    /// </summary>
    [ConfigurationCollection(typeof(ExtensionConfig), CollectionType=ConfigurationElementCollectionType.AddRemoveClearMap)]
    public class ExtensionsConfig : ConfigurationElementCollection
    {
        /// <summary>
        /// The XML name of the individual <see cref="T:Nwp.Media.Configuration.ExtensionConfig" /> instances in this collection.
        /// </summary>
        internal const string ExtensionConfigPropertyName = "extension";

        /// <summary>
        /// Adds the specified <see cref="T:Nwp.Media.Configuration.ExtensionConfig" />.
        /// </summary>
        /// <param name="extension">The <see cref="T:Nwp.Media.Configuration.ExtensionConfig" /> to add.</param>
        public void Add(ExtensionConfig extension)
        {
            base.BaseAdd(extension);
        }

        /// <summary>
        /// When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement" />.
        /// </summary>
        /// <returns>
        /// A new <see cref="T:System.Configuration.ConfigurationElement" />.
        /// </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new ExtensionConfig();
        }

        /// <summary>
        /// Gets the element key for a specified configuration element when overridden in a derived class.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement" /> to return the key for.</param>
        /// <returns>
        /// An <see cref="T:System.Object" /> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement" />.
        /// </returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ExtensionConfig) element).Extension;
        }

        /// <summary>
        /// Indicates whether the specified <see cref="T:System.Configuration.ConfigurationElement" /> exists in the <see cref="T:System.Configuration.ConfigurationElementCollection" />.
        /// </summary>
        /// <param name="elementName">The name of the element to verify.</param>
        /// <returns>
        /// <see langword="true" /> if the element exists in the collection; otherwise, <see langword="false" />. The default is <see langword="false" />.
        /// </returns>
        protected override bool IsElementName(string elementName)
        {
            return (elementName == "extension");
        }

        /// <summary>
        /// Removes the specified <see cref="T:Nwp.Media.Configuration.ExtensionConfig" />.
        /// </summary>
        /// <param name="extension">The <see cref="T:Nwp.Media.Configuration.ExtensionConfig" /> to remove.</param>
        public void Remove(ExtensionConfig extension)
        {
            base.BaseRemove(extension);
        }

        /// <summary>
        /// Gets the type of the <see cref="T:System.Configuration.ConfigurationElementCollection" />.
        /// </summary>
        /// <returns>The <see cref="T:System.Configuration.ConfigurationElementCollectionType" /> of this collection.</returns>
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        /// <summary>
        /// Gets the <see cref="T:Nwp.Media.Configuration.ExtensionConfig" /> at the specified index.
        /// </summary>
        /// <param name="index">The index of the <see cref="T:Nwp.Media.Configuration.ExtensionConfig" /> to retrieve</param>
        public ExtensionConfig this[int index]
        {
            get
            {
                return (ExtensionConfig) base.BaseGet(index);
            }
        }
    }
}

