﻿

using System.Collections.Generic;

namespace Nwp.Media.Services
{
    public interface IFolderService
    {
		/// <summary>
		/// Creates a new folder.
		/// </summary>
		/// <param name="path">
		/// The related path of the new folder.
		/// </param>
		/// <returns>
		/// One of following results:
		/// <list type="bullet">
		/// <item><see cref="OpResult.Succeed"/> - folder created;</item>
		/// <item><see cref="OpResult.AlreadyExist"/> - The folder with specified path already exists.</item>
		/// </list>
		/// </returns>
		OpResult Create(string path);

		/// <summary>
		/// Moves a folder to new location.
		/// </summary>
		/// <param name="srcPath">
		/// The related path of the source folder.
		/// </param>
		/// <param name="destPath">
		/// The related path of the destination folder.
		/// </param>
		/// <returns>
		/// One of following results:
		/// <list type="bullet">
		/// <item><see cref="OpResult.Succeed"/> - folder moved;</item>
		/// <item><see cref="OpResult.NotFound"/> - the source folder not found;</item>
		/// <item><see cref="OpResult.FolderNotFound"/> - parent of the destination folder not found;</item>
		/// <item><see cref="OpResult.AlreadyExist"/> - a namesake folder alreay exists.</item>
		/// </list>
		/// </returns>
		OpResult Move(string srcPath, string destPath);

		/// <summary>
		/// Deletes a folder.
		/// </summary>
		/// <param name="path">
		/// The related path of the folder.
		/// </param>
		/// <returns>
		/// One of following results:
		/// <list type="bullet">
		/// <item><see cref="OpResult.Succeed"/> - folder deleted;</item>
		/// <item><see cref="OpResult.NotFound"/> - the folder not found.</item>
		/// </list>
		/// </returns>
        OpResult Delete(string path);

		IEnumerable<UserFolder> GetAllFolders();
		int Count();
    }
}
